"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _runtime = _interopRequireDefault(require("regenerator-runtime/runtime"));

var _lodash = require("lodash");

var _log = _interopRequireDefault(require("./log"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * Utility class designed for merging dependencies list with simple validation and duplicate
 * detection.
 *
 * @class
 */
var DependenciesManager =
/*#__PURE__*/
function () {
  /**
   * @param {MeteorDesktop} $                   - context
   * @param {Object}        defaultDependencies - core dependencies list
   * @constructor
   */
  function DependenciesManager($, defaultDependencies) {
    _classCallCheck(this, DependenciesManager);

    this.log = new _log.default('dependenciesManager');
    this.$ = $;
    this.dependencies = defaultDependencies; // Regexes for matching certain types of dependencies version.
    // https://docs.npmjs.com/files/package.json#dependencies

    this.regexes = {
      local: /^(\.\.\/|~\/|\.\/|\/)/,
      git: /^git(\+(ssh|http)s?)?/,
      github: /^\w+-?\w+(?!-)\//,
      http: /^https?.+tar\.gz/,
      file: /^file:/
    }; // Check for commit hashes.

    var gitCheck = {
      type: 'regex',
      regex: /#[a-f0-9]{7,40}/,
      test: 'match',
      message: 'git or github link must have a commit hash'
    }; // Check for displaying warnings when npm package from local path is used.

    var localCheck = {
      onceName: 'localCheck',
      type: 'warning',
      message: 'using dependencies from local paths is permitted' + ' but dangerous - read more in README.md'
    };
    this.checks = {
      local: localCheck,
      file: localCheck,
      git: gitCheck,
      github: gitCheck,
      version: {
        type: 'regex',
        // Matches all the semver ranges operators, empty strings and `*`.
        regex: /[|><= ~-]|\.x|$^|^\*$/,
        test: 'do not match',
        message: 'semver ranges are forbidden, please specify exact version'
      }
    };
  }
  /**
   * Just a public getter.
   * @returns {Object}
   */


  _createClass(DependenciesManager, [{
    key: "getDependencies",
    value: function getDependencies() {
      return this.dependencies;
    }
    /**
     * Returns local dependencies.
     * @returns {Object}
     */

  }, {
    key: "getLocalDependencies",
    value: function getLocalDependencies() {
      var _this = this;

      return Object.keys(this.dependencies).filter(function (dependency) {
        return _this.regexes.local.test(_this.dependencies[dependency]) || _this.regexes.file.test(_this.dependencies[dependency]);
      }).reduce(function (localDependencies, currentDependency) {
        return Object.assign(localDependencies, {
          [currentDependency]: _this.dependencies[currentDependency]
        });
      }, {});
    }
    /**
     * Returns remote dependencies.
     * @returns {Object}
     */

  }, {
    key: "getRemoteDependencies",
    value: function getRemoteDependencies() {
      var _this2 = this;

      return Object.keys(this.dependencies).filter(function (dependency) {
        return !_this2.regexes.local.test(_this2.dependencies[dependency]) && !_this2.regexes.file.test(_this2.dependencies[dependency]);
      }).reduce(function (localDependencies, currentDependency) {
        return Object.assign(localDependencies, {
          [currentDependency]: _this2.dependencies[currentDependency]
        });
      }, {});
    }
    /**
     * Merges dependencies into one list.
     *
     * @param {string} from         - describes where the dependencies were set
     * @param {Object} dependencies - dependencies list
     */

  }, {
    key: "mergeDependencies",
    value: function mergeDependencies(from, dependencies) {
      if (this.validateDependenciesVersions(from, dependencies)) {
        this.detectDuplicatedDependencies(from, dependencies);
        (0, _lodash.assignIn)(this.dependencies, dependencies);
      }
    }
    /**
     * Detects dependency version type.
     * @param {string} version - version string of the dependency
     * @return {string}
     */

  }, {
    key: "detectDependencyVersionType",
    value: function detectDependencyVersionType(version) {
      var _this3 = this;

      var type = Object.keys(this.regexes).find(function (dependencyType) {
        return _this3.regexes[dependencyType].test(version);
      });
      return type || 'version';
    }
    /**
     * Validates semver and detect ranges.
     *
     * @param {string} from         - describes where the dependencies were set
     * @param {Object} dependencies - dependencies list
     */

  }, {
    key: "validateDependenciesVersions",
    value: function validateDependenciesVersions(from, dependencies) {
      var _this4 = this;

      var warningsShown = {};
      (0, _lodash.forEach)(dependencies, function (version, name) {
        var type = _this4.detectDependencyVersionType(version);

        if (_this4.checks[type]) {
          var check = _this4.checks[type];

          if (check.type === 'regex') {
            var checkResult = check.test === 'match' ? _this4.checks[type].regex.test(version) : !_this4.checks[type].regex.test(version);

            if (!checkResult) {
              throw new Error(`dependency ${name}:${version} from ${from} failed version ` + `check with message: ${_this4.checks[type].message}`);
            }
          }

          if (check.type === 'warning' && !warningsShown[check.onceName]) {
            warningsShown[check.onceName] = true;

            _this4.log.warn(`dependency ${name}:${version} from ${from} caused a` + ` warning: ${check.message}`);
          }
        }
      });
      return true;
    }
    /**
     * Detects duplicates.
     *
     * @param {string} from         - describes where the dependencies were set
     * @param {Object} dependencies - dependencies list
     */

  }, {
    key: "detectDuplicatedDependencies",
    value: function detectDuplicatedDependencies(from, dependencies) {
      var _this5 = this;

      var duplicates = (0, _lodash.intersection)(Object.keys(dependencies), Object.keys(this.dependencies));

      if (duplicates.length > 0) {
        duplicates.forEach(function (name) {
          if (dependencies[name] !== _this5.dependencies[name]) {
            throw new Error(`While processing dependencies from ${from}, a dependency ` + `${name}: ${dependencies[name]} was found to be conflicting with a ` + `dependency (${_this5.dependencies[name]}) that was already declared in ` + 'other module or it is used in core of the electron app.');
          }
        });
      }
    }
  }]);

  return DependenciesManager;
}();

exports.default = DependenciesManager;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL2xpYi9kZXBlbmRlbmNpZXNNYW5hZ2VyLmpzIl0sIm5hbWVzIjpbIkRlcGVuZGVuY2llc01hbmFnZXIiLCIkIiwiZGVmYXVsdERlcGVuZGVuY2llcyIsImxvZyIsImRlcGVuZGVuY2llcyIsInJlZ2V4ZXMiLCJsb2NhbCIsImdpdCIsImdpdGh1YiIsImh0dHAiLCJmaWxlIiwiZ2l0Q2hlY2siLCJ0eXBlIiwicmVnZXgiLCJ0ZXN0IiwibWVzc2FnZSIsImxvY2FsQ2hlY2siLCJvbmNlTmFtZSIsImNoZWNrcyIsInZlcnNpb24iLCJPYmplY3QiLCJrZXlzIiwiZmlsdGVyIiwiZGVwZW5kZW5jeSIsInJlZHVjZSIsImxvY2FsRGVwZW5kZW5jaWVzIiwiY3VycmVudERlcGVuZGVuY3kiLCJhc3NpZ24iLCJmcm9tIiwidmFsaWRhdGVEZXBlbmRlbmNpZXNWZXJzaW9ucyIsImRldGVjdER1cGxpY2F0ZWREZXBlbmRlbmNpZXMiLCJmaW5kIiwiZGVwZW5kZW5jeVR5cGUiLCJ3YXJuaW5nc1Nob3duIiwibmFtZSIsImRldGVjdERlcGVuZGVuY3lWZXJzaW9uVHlwZSIsImNoZWNrIiwiY2hlY2tSZXN1bHQiLCJFcnJvciIsIndhcm4iLCJkdXBsaWNhdGVzIiwibGVuZ3RoIiwiZm9yRWFjaCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUVBOzs7Ozs7Ozs7O0FBRUE7Ozs7OztJQU1xQkEsbUI7OztBQUNqQjs7Ozs7QUFLQSwrQkFBWUMsQ0FBWixFQUFlQyxtQkFBZixFQUFvQztBQUFBOztBQUNoQyxTQUFLQyxHQUFMLEdBQVcsaUJBQVEscUJBQVIsQ0FBWDtBQUNBLFNBQUtGLENBQUwsR0FBU0EsQ0FBVDtBQUNBLFNBQUtHLFlBQUwsR0FBb0JGLG1CQUFwQixDQUhnQyxDQUtoQztBQUNBOztBQUNBLFNBQUtHLE9BQUwsR0FBZTtBQUNYQyxhQUFPLHVCQURJO0FBRVhDLFdBQUssdUJBRk07QUFHWEMsY0FBUSxrQkFIRztBQUlYQyxZQUFNLGtCQUpLO0FBS1hDLFlBQU07QUFMSyxLQUFmLENBUGdDLENBZWhDOztBQUNBLFFBQU1DLFdBQVc7QUFDYkMsWUFBTSxPQURPO0FBRWJDLGFBQU8saUJBRk07QUFHYkMsWUFBTSxPQUhPO0FBSWJDLGVBQVM7QUFKSSxLQUFqQixDQWhCZ0MsQ0F1QmhDOztBQUNBLFFBQU1DLGFBQWE7QUFDZkMsZ0JBQVUsWUFESztBQUVmTCxZQUFNLFNBRlM7QUFHZkcsZUFBUyxxREFDVDtBQUplLEtBQW5CO0FBT0EsU0FBS0csTUFBTCxHQUFjO0FBQ1ZaLGFBQU9VLFVBREc7QUFFVk4sWUFBTU0sVUFGSTtBQUdWVCxXQUFLSSxRQUhLO0FBSVZILGNBQVFHLFFBSkU7QUFLVlEsZUFBUztBQUNMUCxjQUFNLE9BREQ7QUFFTDtBQUNBQyxlQUFPLHVCQUhGO0FBSUxDLGNBQU0sY0FKRDtBQUtMQyxpQkFBUztBQUxKO0FBTEMsS0FBZDtBQWFIO0FBRUQ7Ozs7Ozs7O3NDQUlrQjtBQUNkLGFBQU8sS0FBS1gsWUFBWjtBQUNIO0FBRUQ7Ozs7Ozs7MkNBSXVCO0FBQUE7O0FBQ25CLGFBQU9nQixPQUNGQyxJQURFLENBQ0csS0FBS2pCLFlBRFIsRUFFRmtCLE1BRkUsQ0FHQztBQUFBLGVBQ0ksTUFBS2pCLE9BQUwsQ0FBYUMsS0FBYixDQUFtQlEsSUFBbkIsQ0FBd0IsTUFBS1YsWUFBTCxDQUFrQm1CLFVBQWxCLENBQXhCLEtBQ0EsTUFBS2xCLE9BQUwsQ0FBYUssSUFBYixDQUFrQkksSUFBbEIsQ0FBdUIsTUFBS1YsWUFBTCxDQUFrQm1CLFVBQWxCLENBQXZCLENBRko7QUFBQSxPQUhELEVBT0ZDLE1BUEUsQ0FRQyxVQUFDQyxpQkFBRCxFQUFvQkMsaUJBQXBCO0FBQUEsZUFDSU4sT0FBT08sTUFBUCxDQUNJRixpQkFESixFQUVJO0FBQUUsV0FBQ0MsaUJBQUQsR0FBcUIsTUFBS3RCLFlBQUwsQ0FBa0JzQixpQkFBbEI7QUFBdkIsU0FGSixDQURKO0FBQUEsT0FSRCxFQWFDLEVBYkQsQ0FBUDtBQWVIO0FBRUQ7Ozs7Ozs7NENBSXdCO0FBQUE7O0FBQ3BCLGFBQU9OLE9BQ0ZDLElBREUsQ0FDRyxLQUFLakIsWUFEUixFQUVGa0IsTUFGRSxDQUdDO0FBQUEsZUFDSSxDQUFDLE9BQUtqQixPQUFMLENBQWFDLEtBQWIsQ0FBbUJRLElBQW5CLENBQXdCLE9BQUtWLFlBQUwsQ0FBa0JtQixVQUFsQixDQUF4QixDQUFELElBQ0EsQ0FBQyxPQUFLbEIsT0FBTCxDQUFhSyxJQUFiLENBQWtCSSxJQUFsQixDQUF1QixPQUFLVixZQUFMLENBQWtCbUIsVUFBbEIsQ0FBdkIsQ0FGTDtBQUFBLE9BSEQsRUFPRkMsTUFQRSxDQVFDLFVBQUNDLGlCQUFELEVBQW9CQyxpQkFBcEI7QUFBQSxlQUNJTixPQUFPTyxNQUFQLENBQ0lGLGlCQURKLEVBRUk7QUFBRSxXQUFDQyxpQkFBRCxHQUFxQixPQUFLdEIsWUFBTCxDQUFrQnNCLGlCQUFsQjtBQUF2QixTQUZKLENBREo7QUFBQSxPQVJELEVBYUMsRUFiRCxDQUFQO0FBZUg7QUFFRDs7Ozs7Ozs7O3NDQU1rQkUsSSxFQUFNeEIsWSxFQUFjO0FBQ2xDLFVBQUksS0FBS3lCLDRCQUFMLENBQWtDRCxJQUFsQyxFQUF3Q3hCLFlBQXhDLENBQUosRUFBMkQ7QUFDdkQsYUFBSzBCLDRCQUFMLENBQWtDRixJQUFsQyxFQUF3Q3hCLFlBQXhDO0FBQ0EsOEJBQVMsS0FBS0EsWUFBZCxFQUE0QkEsWUFBNUI7QUFDSDtBQUNKO0FBRUQ7Ozs7Ozs7O2dEQUs0QmUsTyxFQUFTO0FBQUE7O0FBQ2pDLFVBQU1QLE9BQU9RLE9BQU9DLElBQVAsQ0FBWSxLQUFLaEIsT0FBakIsRUFDUjBCLElBRFEsQ0FDSDtBQUFBLGVBQWtCLE9BQUsxQixPQUFMLENBQWEyQixjQUFiLEVBQTZCbEIsSUFBN0IsQ0FBa0NLLE9BQWxDLENBQWxCO0FBQUEsT0FERyxDQUFiO0FBRUEsYUFBT1AsUUFBUSxTQUFmO0FBQ0g7QUFFRDs7Ozs7Ozs7O2lEQU02QmdCLEksRUFBTXhCLFksRUFBYztBQUFBOztBQUM3QyxVQUFNNkIsZ0JBQWdCLEVBQXRCO0FBQ0EsMkJBQVE3QixZQUFSLEVBQXNCLFVBQUNlLE9BQUQsRUFBVWUsSUFBVixFQUFtQjtBQUNyQyxZQUFNdEIsT0FBTyxPQUFLdUIsMkJBQUwsQ0FBaUNoQixPQUFqQyxDQUFiOztBQUNBLFlBQUksT0FBS0QsTUFBTCxDQUFZTixJQUFaLENBQUosRUFBdUI7QUFDbkIsY0FBTXdCLFFBQVEsT0FBS2xCLE1BQUwsQ0FBWU4sSUFBWixDQUFkOztBQUNBLGNBQUl3QixNQUFNeEIsSUFBTixLQUFlLE9BQW5CLEVBQTRCO0FBQ3hCLGdCQUFNeUIsY0FBY0QsTUFBTXRCLElBQU4sS0FBZSxPQUFmLEdBQ2hCLE9BQUtJLE1BQUwsQ0FBWU4sSUFBWixFQUFrQkMsS0FBbEIsQ0FBd0JDLElBQXhCLENBQTZCSyxPQUE3QixDQURnQixHQUVoQixDQUFDLE9BQUtELE1BQUwsQ0FBWU4sSUFBWixFQUFrQkMsS0FBbEIsQ0FBd0JDLElBQXhCLENBQTZCSyxPQUE3QixDQUZMOztBQUdBLGdCQUFJLENBQUNrQixXQUFMLEVBQWtCO0FBQ2Qsb0JBQU0sSUFBSUMsS0FBSixDQUFXLGNBQWFKLElBQUssSUFBR2YsT0FBUSxTQUFRUyxJQUFLLGtCQUEzQyxHQUNYLHVCQUFzQixPQUFLVixNQUFMLENBQVlOLElBQVosRUFBa0JHLE9BQVEsRUFEL0MsQ0FBTjtBQUVIO0FBQ0o7O0FBQ0QsY0FBSXFCLE1BQU14QixJQUFOLEtBQWUsU0FBZixJQUE0QixDQUFDcUIsY0FBY0csTUFBTW5CLFFBQXBCLENBQWpDLEVBQWdFO0FBQzVEZ0IsMEJBQWNHLE1BQU1uQixRQUFwQixJQUFnQyxJQUFoQzs7QUFDQSxtQkFBS2QsR0FBTCxDQUFTb0MsSUFBVCxDQUFlLGNBQWFMLElBQUssSUFBR2YsT0FBUSxTQUFRUyxJQUFLLFdBQTNDLEdBQ1QsYUFBWVEsTUFBTXJCLE9BQVEsRUFEL0I7QUFFSDtBQUNKO0FBQ0osT0FuQkQ7QUFvQkEsYUFBTyxJQUFQO0FBQ0g7QUFFRDs7Ozs7Ozs7O2lEQU02QmEsSSxFQUFNeEIsWSxFQUFjO0FBQUE7O0FBQzdDLFVBQU1vQyxhQUFhLDBCQUFhcEIsT0FBT0MsSUFBUCxDQUFZakIsWUFBWixDQUFiLEVBQXdDZ0IsT0FBT0MsSUFBUCxDQUFZLEtBQUtqQixZQUFqQixDQUF4QyxDQUFuQjs7QUFDQSxVQUFJb0MsV0FBV0MsTUFBWCxHQUFvQixDQUF4QixFQUEyQjtBQUN2QkQsbUJBQVdFLE9BQVgsQ0FBbUIsVUFBQ1IsSUFBRCxFQUFVO0FBQ3pCLGNBQUk5QixhQUFhOEIsSUFBYixNQUF1QixPQUFLOUIsWUFBTCxDQUFrQjhCLElBQWxCLENBQTNCLEVBQW9EO0FBQ2hELGtCQUFNLElBQUlJLEtBQUosQ0FBVyxzQ0FBcUNWLElBQUssaUJBQTNDLEdBQ1gsR0FBRU0sSUFBSyxLQUFJOUIsYUFBYThCLElBQWIsQ0FBbUIsc0NBRG5CLEdBRVgsZUFBYyxPQUFLOUIsWUFBTCxDQUFrQjhCLElBQWxCLENBQXdCLGlDQUYzQixHQUdaLHlEQUhFLENBQU47QUFJSDtBQUNKLFNBUEQ7QUFRSDtBQUNKIiwiZmlsZSI6ImRlcGVuZGVuY2llc01hbmFnZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgcmVnZW5lcmF0b3JSdW50aW1lIGZyb20gJ3JlZ2VuZXJhdG9yLXJ1bnRpbWUvcnVudGltZSc7XG5pbXBvcnQgeyBmb3JFYWNoLCBhc3NpZ25JbiwgaW50ZXJzZWN0aW9uIH0gZnJvbSAnbG9kYXNoJztcblxuaW1wb3J0IExvZyBmcm9tICcuL2xvZyc7XG5cbi8qKlxuICogVXRpbGl0eSBjbGFzcyBkZXNpZ25lZCBmb3IgbWVyZ2luZyBkZXBlbmRlbmNpZXMgbGlzdCB3aXRoIHNpbXBsZSB2YWxpZGF0aW9uIGFuZCBkdXBsaWNhdGVcbiAqIGRldGVjdGlvbi5cbiAqXG4gKiBAY2xhc3NcbiAqL1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRGVwZW5kZW5jaWVzTWFuYWdlciB7XG4gICAgLyoqXG4gICAgICogQHBhcmFtIHtNZXRlb3JEZXNrdG9wfSAkICAgICAgICAgICAgICAgICAgIC0gY29udGV4dFxuICAgICAqIEBwYXJhbSB7T2JqZWN0fSAgICAgICAgZGVmYXVsdERlcGVuZGVuY2llcyAtIGNvcmUgZGVwZW5kZW5jaWVzIGxpc3RcbiAgICAgKiBAY29uc3RydWN0b3JcbiAgICAgKi9cbiAgICBjb25zdHJ1Y3RvcigkLCBkZWZhdWx0RGVwZW5kZW5jaWVzKSB7XG4gICAgICAgIHRoaXMubG9nID0gbmV3IExvZygnZGVwZW5kZW5jaWVzTWFuYWdlcicpO1xuICAgICAgICB0aGlzLiQgPSAkO1xuICAgICAgICB0aGlzLmRlcGVuZGVuY2llcyA9IGRlZmF1bHREZXBlbmRlbmNpZXM7XG5cbiAgICAgICAgLy8gUmVnZXhlcyBmb3IgbWF0Y2hpbmcgY2VydGFpbiB0eXBlcyBvZiBkZXBlbmRlbmNpZXMgdmVyc2lvbi5cbiAgICAgICAgLy8gaHR0cHM6Ly9kb2NzLm5wbWpzLmNvbS9maWxlcy9wYWNrYWdlLmpzb24jZGVwZW5kZW5jaWVzXG4gICAgICAgIHRoaXMucmVnZXhlcyA9IHtcbiAgICAgICAgICAgIGxvY2FsOiAvXihcXC5cXC5cXC98flxcL3xcXC5cXC98XFwvKS8sXG4gICAgICAgICAgICBnaXQ6IC9eZ2l0KFxcKyhzc2h8aHR0cClzPyk/LyxcbiAgICAgICAgICAgIGdpdGh1YjogL15cXHcrLT9cXHcrKD8hLSlcXC8vLFxuICAgICAgICAgICAgaHR0cDogL15odHRwcz8uK3RhclxcLmd6LyxcbiAgICAgICAgICAgIGZpbGU6IC9eZmlsZTovXG4gICAgICAgIH07XG5cbiAgICAgICAgLy8gQ2hlY2sgZm9yIGNvbW1pdCBoYXNoZXMuXG4gICAgICAgIGNvbnN0IGdpdENoZWNrID0ge1xuICAgICAgICAgICAgdHlwZTogJ3JlZ2V4JyxcbiAgICAgICAgICAgIHJlZ2V4OiAvI1thLWYwLTldezcsNDB9LyxcbiAgICAgICAgICAgIHRlc3Q6ICdtYXRjaCcsXG4gICAgICAgICAgICBtZXNzYWdlOiAnZ2l0IG9yIGdpdGh1YiBsaW5rIG11c3QgaGF2ZSBhIGNvbW1pdCBoYXNoJ1xuICAgICAgICB9O1xuXG4gICAgICAgIC8vIENoZWNrIGZvciBkaXNwbGF5aW5nIHdhcm5pbmdzIHdoZW4gbnBtIHBhY2thZ2UgZnJvbSBsb2NhbCBwYXRoIGlzIHVzZWQuXG4gICAgICAgIGNvbnN0IGxvY2FsQ2hlY2sgPSB7XG4gICAgICAgICAgICBvbmNlTmFtZTogJ2xvY2FsQ2hlY2snLFxuICAgICAgICAgICAgdHlwZTogJ3dhcm5pbmcnLFxuICAgICAgICAgICAgbWVzc2FnZTogJ3VzaW5nIGRlcGVuZGVuY2llcyBmcm9tIGxvY2FsIHBhdGhzIGlzIHBlcm1pdHRlZCcgK1xuICAgICAgICAgICAgJyBidXQgZGFuZ2Vyb3VzIC0gcmVhZCBtb3JlIGluIFJFQURNRS5tZCdcbiAgICAgICAgfTtcblxuICAgICAgICB0aGlzLmNoZWNrcyA9IHtcbiAgICAgICAgICAgIGxvY2FsOiBsb2NhbENoZWNrLFxuICAgICAgICAgICAgZmlsZTogbG9jYWxDaGVjayxcbiAgICAgICAgICAgIGdpdDogZ2l0Q2hlY2ssXG4gICAgICAgICAgICBnaXRodWI6IGdpdENoZWNrLFxuICAgICAgICAgICAgdmVyc2lvbjoge1xuICAgICAgICAgICAgICAgIHR5cGU6ICdyZWdleCcsXG4gICAgICAgICAgICAgICAgLy8gTWF0Y2hlcyBhbGwgdGhlIHNlbXZlciByYW5nZXMgb3BlcmF0b3JzLCBlbXB0eSBzdHJpbmdzIGFuZCBgKmAuXG4gICAgICAgICAgICAgICAgcmVnZXg6IC9bfD48PSB+LV18XFwueHwkXnxeXFwqJC8sXG4gICAgICAgICAgICAgICAgdGVzdDogJ2RvIG5vdCBtYXRjaCcsXG4gICAgICAgICAgICAgICAgbWVzc2FnZTogJ3NlbXZlciByYW5nZXMgYXJlIGZvcmJpZGRlbiwgcGxlYXNlIHNwZWNpZnkgZXhhY3QgdmVyc2lvbidcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBKdXN0IGEgcHVibGljIGdldHRlci5cbiAgICAgKiBAcmV0dXJucyB7T2JqZWN0fVxuICAgICAqL1xuICAgIGdldERlcGVuZGVuY2llcygpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGVwZW5kZW5jaWVzO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFJldHVybnMgbG9jYWwgZGVwZW5kZW5jaWVzLlxuICAgICAqIEByZXR1cm5zIHtPYmplY3R9XG4gICAgICovXG4gICAgZ2V0TG9jYWxEZXBlbmRlbmNpZXMoKSB7XG4gICAgICAgIHJldHVybiBPYmplY3RcbiAgICAgICAgICAgIC5rZXlzKHRoaXMuZGVwZW5kZW5jaWVzKVxuICAgICAgICAgICAgLmZpbHRlcihcbiAgICAgICAgICAgICAgICBkZXBlbmRlbmN5ID0+XG4gICAgICAgICAgICAgICAgICAgIHRoaXMucmVnZXhlcy5sb2NhbC50ZXN0KHRoaXMuZGVwZW5kZW5jaWVzW2RlcGVuZGVuY3ldKSB8fFxuICAgICAgICAgICAgICAgICAgICB0aGlzLnJlZ2V4ZXMuZmlsZS50ZXN0KHRoaXMuZGVwZW5kZW5jaWVzW2RlcGVuZGVuY3ldKVxuICAgICAgICAgICAgKVxuICAgICAgICAgICAgLnJlZHVjZShcbiAgICAgICAgICAgICAgICAobG9jYWxEZXBlbmRlbmNpZXMsIGN1cnJlbnREZXBlbmRlbmN5KSA9PlxuICAgICAgICAgICAgICAgICAgICBPYmplY3QuYXNzaWduKFxuICAgICAgICAgICAgICAgICAgICAgICAgbG9jYWxEZXBlbmRlbmNpZXMsXG4gICAgICAgICAgICAgICAgICAgICAgICB7IFtjdXJyZW50RGVwZW5kZW5jeV06IHRoaXMuZGVwZW5kZW5jaWVzW2N1cnJlbnREZXBlbmRlbmN5XSB9XG4gICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAge31cbiAgICAgICAgICAgICk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyByZW1vdGUgZGVwZW5kZW5jaWVzLlxuICAgICAqIEByZXR1cm5zIHtPYmplY3R9XG4gICAgICovXG4gICAgZ2V0UmVtb3RlRGVwZW5kZW5jaWVzKCkge1xuICAgICAgICByZXR1cm4gT2JqZWN0XG4gICAgICAgICAgICAua2V5cyh0aGlzLmRlcGVuZGVuY2llcylcbiAgICAgICAgICAgIC5maWx0ZXIoXG4gICAgICAgICAgICAgICAgZGVwZW5kZW5jeSA9PlxuICAgICAgICAgICAgICAgICAgICAhdGhpcy5yZWdleGVzLmxvY2FsLnRlc3QodGhpcy5kZXBlbmRlbmNpZXNbZGVwZW5kZW5jeV0pICYmXG4gICAgICAgICAgICAgICAgICAgICF0aGlzLnJlZ2V4ZXMuZmlsZS50ZXN0KHRoaXMuZGVwZW5kZW5jaWVzW2RlcGVuZGVuY3ldKVxuICAgICAgICAgICAgKVxuICAgICAgICAgICAgLnJlZHVjZShcbiAgICAgICAgICAgICAgICAobG9jYWxEZXBlbmRlbmNpZXMsIGN1cnJlbnREZXBlbmRlbmN5KSA9PlxuICAgICAgICAgICAgICAgICAgICBPYmplY3QuYXNzaWduKFxuICAgICAgICAgICAgICAgICAgICAgICAgbG9jYWxEZXBlbmRlbmNpZXMsXG4gICAgICAgICAgICAgICAgICAgICAgICB7IFtjdXJyZW50RGVwZW5kZW5jeV06IHRoaXMuZGVwZW5kZW5jaWVzW2N1cnJlbnREZXBlbmRlbmN5XSB9XG4gICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAge31cbiAgICAgICAgICAgICk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogTWVyZ2VzIGRlcGVuZGVuY2llcyBpbnRvIG9uZSBsaXN0LlxuICAgICAqXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGZyb20gICAgICAgICAtIGRlc2NyaWJlcyB3aGVyZSB0aGUgZGVwZW5kZW5jaWVzIHdlcmUgc2V0XG4gICAgICogQHBhcmFtIHtPYmplY3R9IGRlcGVuZGVuY2llcyAtIGRlcGVuZGVuY2llcyBsaXN0XG4gICAgICovXG4gICAgbWVyZ2VEZXBlbmRlbmNpZXMoZnJvbSwgZGVwZW5kZW5jaWVzKSB7XG4gICAgICAgIGlmICh0aGlzLnZhbGlkYXRlRGVwZW5kZW5jaWVzVmVyc2lvbnMoZnJvbSwgZGVwZW5kZW5jaWVzKSkge1xuICAgICAgICAgICAgdGhpcy5kZXRlY3REdXBsaWNhdGVkRGVwZW5kZW5jaWVzKGZyb20sIGRlcGVuZGVuY2llcyk7XG4gICAgICAgICAgICBhc3NpZ25Jbih0aGlzLmRlcGVuZGVuY2llcywgZGVwZW5kZW5jaWVzKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIERldGVjdHMgZGVwZW5kZW5jeSB2ZXJzaW9uIHR5cGUuXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHZlcnNpb24gLSB2ZXJzaW9uIHN0cmluZyBvZiB0aGUgZGVwZW5kZW5jeVxuICAgICAqIEByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cbiAgICBkZXRlY3REZXBlbmRlbmN5VmVyc2lvblR5cGUodmVyc2lvbikge1xuICAgICAgICBjb25zdCB0eXBlID0gT2JqZWN0LmtleXModGhpcy5yZWdleGVzKVxuICAgICAgICAgICAgLmZpbmQoZGVwZW5kZW5jeVR5cGUgPT4gdGhpcy5yZWdleGVzW2RlcGVuZGVuY3lUeXBlXS50ZXN0KHZlcnNpb24pKTtcbiAgICAgICAgcmV0dXJuIHR5cGUgfHwgJ3ZlcnNpb24nO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFZhbGlkYXRlcyBzZW12ZXIgYW5kIGRldGVjdCByYW5nZXMuXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gZnJvbSAgICAgICAgIC0gZGVzY3JpYmVzIHdoZXJlIHRoZSBkZXBlbmRlbmNpZXMgd2VyZSBzZXRcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gZGVwZW5kZW5jaWVzIC0gZGVwZW5kZW5jaWVzIGxpc3RcbiAgICAgKi9cbiAgICB2YWxpZGF0ZURlcGVuZGVuY2llc1ZlcnNpb25zKGZyb20sIGRlcGVuZGVuY2llcykge1xuICAgICAgICBjb25zdCB3YXJuaW5nc1Nob3duID0ge307XG4gICAgICAgIGZvckVhY2goZGVwZW5kZW5jaWVzLCAodmVyc2lvbiwgbmFtZSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgdHlwZSA9IHRoaXMuZGV0ZWN0RGVwZW5kZW5jeVZlcnNpb25UeXBlKHZlcnNpb24pO1xuICAgICAgICAgICAgaWYgKHRoaXMuY2hlY2tzW3R5cGVdKSB7XG4gICAgICAgICAgICAgICAgY29uc3QgY2hlY2sgPSB0aGlzLmNoZWNrc1t0eXBlXTtcbiAgICAgICAgICAgICAgICBpZiAoY2hlY2sudHlwZSA9PT0gJ3JlZ2V4Jykge1xuICAgICAgICAgICAgICAgICAgICBjb25zdCBjaGVja1Jlc3VsdCA9IGNoZWNrLnRlc3QgPT09ICdtYXRjaCcgP1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jaGVja3NbdHlwZV0ucmVnZXgudGVzdCh2ZXJzaW9uKSA6XG4gICAgICAgICAgICAgICAgICAgICAgICAhdGhpcy5jaGVja3NbdHlwZV0ucmVnZXgudGVzdCh2ZXJzaW9uKTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFjaGVja1Jlc3VsdCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKGBkZXBlbmRlbmN5ICR7bmFtZX06JHt2ZXJzaW9ufSBmcm9tICR7ZnJvbX0gZmFpbGVkIHZlcnNpb24gYCArXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYGNoZWNrIHdpdGggbWVzc2FnZTogJHt0aGlzLmNoZWNrc1t0eXBlXS5tZXNzYWdlfWApO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmIChjaGVjay50eXBlID09PSAnd2FybmluZycgJiYgIXdhcm5pbmdzU2hvd25bY2hlY2sub25jZU5hbWVdKSB7XG4gICAgICAgICAgICAgICAgICAgIHdhcm5pbmdzU2hvd25bY2hlY2sub25jZU5hbWVdID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2cud2FybihgZGVwZW5kZW5jeSAke25hbWV9OiR7dmVyc2lvbn0gZnJvbSAke2Zyb219IGNhdXNlZCBhYCArXG4gICAgICAgICAgICAgICAgICAgICAgICBgIHdhcm5pbmc6ICR7Y2hlY2subWVzc2FnZX1gKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBEZXRlY3RzIGR1cGxpY2F0ZXMuXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gZnJvbSAgICAgICAgIC0gZGVzY3JpYmVzIHdoZXJlIHRoZSBkZXBlbmRlbmNpZXMgd2VyZSBzZXRcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gZGVwZW5kZW5jaWVzIC0gZGVwZW5kZW5jaWVzIGxpc3RcbiAgICAgKi9cbiAgICBkZXRlY3REdXBsaWNhdGVkRGVwZW5kZW5jaWVzKGZyb20sIGRlcGVuZGVuY2llcykge1xuICAgICAgICBjb25zdCBkdXBsaWNhdGVzID0gaW50ZXJzZWN0aW9uKE9iamVjdC5rZXlzKGRlcGVuZGVuY2llcyksIE9iamVjdC5rZXlzKHRoaXMuZGVwZW5kZW5jaWVzKSk7XG4gICAgICAgIGlmIChkdXBsaWNhdGVzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgIGR1cGxpY2F0ZXMuZm9yRWFjaCgobmFtZSkgPT4ge1xuICAgICAgICAgICAgICAgIGlmIChkZXBlbmRlbmNpZXNbbmFtZV0gIT09IHRoaXMuZGVwZW5kZW5jaWVzW25hbWVdKSB7XG4gICAgICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihgV2hpbGUgcHJvY2Vzc2luZyBkZXBlbmRlbmNpZXMgZnJvbSAke2Zyb219LCBhIGRlcGVuZGVuY3kgYCArXG4gICAgICAgICAgICAgICAgICAgICAgICBgJHtuYW1lfTogJHtkZXBlbmRlbmNpZXNbbmFtZV19IHdhcyBmb3VuZCB0byBiZSBjb25mbGljdGluZyB3aXRoIGEgYCArXG4gICAgICAgICAgICAgICAgICAgICAgICBgZGVwZW5kZW5jeSAoJHt0aGlzLmRlcGVuZGVuY2llc1tuYW1lXX0pIHRoYXQgd2FzIGFscmVhZHkgZGVjbGFyZWQgaW4gYCArXG4gICAgICAgICAgICAgICAgICAgICAgICAnb3RoZXIgbW9kdWxlIG9yIGl0IGlzIHVzZWQgaW4gY29yZSBvZiB0aGUgZWxlY3Ryb24gYXBwLicpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgfVxufVxuIl19