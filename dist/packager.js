"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _assignIn = _interopRequireDefault(require("lodash/assignIn"));

var _path = _interopRequireDefault(require("path"));

var _fs = _interopRequireDefault(require("fs"));

var _shelljs = _interopRequireDefault(require("shelljs"));

var _electronPackager = _interopRequireDefault(require("electron-packager"));

var _log = _interopRequireDefault(require("./log"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var join = _path.default.join;
/**
 * Wrapper around electron-packager.
 * @class
 */

var ElectronPackager =
/*#__PURE__*/
function () {
  function ElectronPackager($) {
    _classCallCheck(this, ElectronPackager);

    this.log = new _log.default('electron-packager');
    this.$ = $;
  }
  /**
   * Runs the packager with provided arguments.
   *
   * @param {Object} args
   * @returns {Promise}
   */


  _createClass(ElectronPackager, [{
    key: "runPackager",
    value: function runPackager(args) {
      var _this = this;

      return new Promise(function (resolve, reject) {
        (0, _electronPackager.default)(args, function (err) {
          if (err) {
            reject(err);
          } else {
            _this.log.info(`wrote packaged app to ${_this.$.env.paths.packageDir}`);

            resolve();
          }
        });
      });
    }
  }, {
    key: "packageApp",
    value: function () {
      var _packageApp = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee() {
        var _this2 = this;

        var _JSON$parse, version, settings, name, arch, args, packagerOptions, extracted;

        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _JSON$parse = JSON.parse(_fs.default.readFileSync(join(this.$.env.paths.meteorApp.root, 'node_modules', 'electron', 'package.json'), 'UTF-8')), version = _JSON$parse.version;
                settings = this.$.desktop.getSettings();
                name = settings.name;

                if (!name) {
                  this.log.error('`name` field in settings.json not set');
                  process.exit(1);
                }

                arch = this.$.env.options.ia32 ? 'ia32' : 'x64';
                this.log.info(`packaging '${name}' for platform '${this.$.env.sys.platform}-${arch}'` + ` using electron v${version}`);
                _context.prev = 6;
                _context.next = 9;
                return this.$.utils.rmWithRetries('-rf', _path.default.join(this.$.env.options.output, this.$.env.paths.packageDir));

              case 9:
                _context.next = 14;
                break;

              case 11:
                _context.prev = 11;
                _context.t0 = _context["catch"](6);
                throw new Error(_context.t0);

              case 14:
                args = {
                  name,
                  arch,
                  prune: false,
                  electronVersion: version,
                  platform: this.$.env.sys.platform,
                  dir: this.$.env.paths.electronApp.root,
                  out: _path.default.join(this.$.env.options.output, this.$.env.paths.packageDir)
                };

                if ('packagerOptions' in settings) {
                  packagerOptions = settings.packagerOptions;
                  ['windows', 'linux', 'osx'].forEach(function (system) {
                    if (_this2.$.env.os[`is${system[0].toUpperCase()}${system.substring(1)}`] && `_${system}` in packagerOptions) {
                      (0, _assignIn.default)(packagerOptions, packagerOptions[`_${system}`]);
                    }
                  });
                  Object.keys(packagerOptions).forEach(function (field) {
                    if (packagerOptions[field] === '@version') {
                      packagerOptions[field] = settings.version;
                    }
                  });
                  (0, _assignIn.default)(args, packagerOptions);
                } // Move node_modules away. We do not want to delete it, just temporarily remove it from
                // our way.


                _fs.default.renameSync(this.$.env.paths.electronApp.nodeModules, this.$.env.paths.electronApp.tmpNodeModules);

                extracted = false;

                if (this.$.utils.exists(this.$.env.paths.electronApp.extractedNodeModules)) {
                  _fs.default.renameSync(this.$.env.paths.electronApp.extractedNodeModules, this.$.env.paths.electronApp.nodeModules);

                  extracted = true;
                }

                _context.prev = 19;
                _context.next = 22;
                return this.runPackager(args);

              case 22:
                _context.prev = 22;

                if (extracted) {
                  _shelljs.default.rm('-rf', this.$.env.paths.electronApp.extractedNodeModules);

                  _shelljs.default.rm('-rf', this.$.env.paths.electronApp.nodeModules);
                } // Move node_modules back.


                _fs.default.renameSync(this.$.env.paths.electronApp.tmpNodeModules, this.$.env.paths.electronApp.nodeModules);

                return _context.finish(22);

              case 26:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[6, 11], [19,, 22, 26]]);
      }));

      return function packageApp() {
        return _packageApp.apply(this, arguments);
      };
    }()
  }]);

  return ElectronPackager;
}();

exports.default = ElectronPackager;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL2xpYi9wYWNrYWdlci5qcyJdLCJuYW1lcyI6WyJqb2luIiwiRWxlY3Ryb25QYWNrYWdlciIsIiQiLCJsb2ciLCJhcmdzIiwiUHJvbWlzZSIsInJlc29sdmUiLCJyZWplY3QiLCJlcnIiLCJpbmZvIiwiZW52IiwicGF0aHMiLCJwYWNrYWdlRGlyIiwiSlNPTiIsInBhcnNlIiwicmVhZEZpbGVTeW5jIiwibWV0ZW9yQXBwIiwicm9vdCIsInZlcnNpb24iLCJzZXR0aW5ncyIsImRlc2t0b3AiLCJnZXRTZXR0aW5ncyIsIm5hbWUiLCJlcnJvciIsInByb2Nlc3MiLCJleGl0IiwiYXJjaCIsIm9wdGlvbnMiLCJpYTMyIiwic3lzIiwicGxhdGZvcm0iLCJ1dGlscyIsInJtV2l0aFJldHJpZXMiLCJvdXRwdXQiLCJFcnJvciIsInBydW5lIiwiZWxlY3Ryb25WZXJzaW9uIiwiZGlyIiwiZWxlY3Ryb25BcHAiLCJvdXQiLCJwYWNrYWdlck9wdGlvbnMiLCJmb3JFYWNoIiwic3lzdGVtIiwib3MiLCJ0b1VwcGVyQ2FzZSIsInN1YnN0cmluZyIsIk9iamVjdCIsImtleXMiLCJmaWVsZCIsInJlbmFtZVN5bmMiLCJub2RlTW9kdWxlcyIsInRtcE5vZGVNb2R1bGVzIiwiZXh0cmFjdGVkIiwiZXhpc3RzIiwiZXh0cmFjdGVkTm9kZU1vZHVsZXMiLCJydW5QYWNrYWdlciIsInJtIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7OztJQUVRQSxJLGlCQUFBQSxJO0FBRVI7Ozs7O0lBSXFCQyxnQjs7O0FBQ2pCLDRCQUFZQyxDQUFaLEVBQWU7QUFBQTs7QUFDWCxTQUFLQyxHQUFMLEdBQVcsaUJBQVEsbUJBQVIsQ0FBWDtBQUNBLFNBQUtELENBQUwsR0FBU0EsQ0FBVDtBQUNIO0FBRUQ7Ozs7Ozs7Ozs7Z0NBTVlFLEksRUFBTTtBQUFBOztBQUNkLGFBQU8sSUFBSUMsT0FBSixDQUFZLFVBQUNDLE9BQUQsRUFBVUMsTUFBVixFQUFxQjtBQUNwQyx1Q0FBU0gsSUFBVCxFQUFlLFVBQUNJLEdBQUQsRUFBUztBQUNwQixjQUFJQSxHQUFKLEVBQVM7QUFDTEQsbUJBQU9DLEdBQVA7QUFDSCxXQUZELE1BRU87QUFDSCxrQkFBS0wsR0FBTCxDQUFTTSxJQUFULENBQWUseUJBQXdCLE1BQUtQLENBQUwsQ0FBT1EsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxVQUFXLEVBQW5FOztBQUNBTjtBQUNIO0FBQ0osU0FQRDtBQVFILE9BVE0sQ0FBUDtBQVVIOzs7Ozs7Ozs7Ozs7Ozs7OEJBR3VCTyxLQUFLQyxLQUFMLENBQVcsWUFBR0MsWUFBSCxDQUMzQmYsS0FDSSxLQUFLRSxDQUFMLENBQU9RLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkssU0FBakIsQ0FBMkJDLElBRC9CLEVBRUksY0FGSixFQUdJLFVBSEosRUFJSSxjQUpKLENBRDJCLEVBTXhCLE9BTndCLENBQVgsQyxFQUFaQyxPLGVBQUFBLE87QUFTRkMsd0IsR0FBVyxLQUFLakIsQ0FBTCxDQUFPa0IsT0FBUCxDQUFlQyxXQUFmLEU7QUFDVEMsb0IsR0FBU0gsUSxDQUFURyxJOztBQUNSLG9CQUFJLENBQUNBLElBQUwsRUFBVztBQUNQLHVCQUFLbkIsR0FBTCxDQUFTb0IsS0FBVCxDQUFlLHVDQUFmO0FBQ0FDLDBCQUFRQyxJQUFSLENBQWEsQ0FBYjtBQUNIOztBQUVLQyxvQixHQUFPLEtBQUt4QixDQUFMLENBQU9RLEdBQVAsQ0FBV2lCLE9BQVgsQ0FBbUJDLElBQW5CLEdBQTBCLE1BQTFCLEdBQW1DLEs7QUFFaEQscUJBQUt6QixHQUFMLENBQVNNLElBQVQsQ0FDSyxjQUFhYSxJQUFLLG1CQUFrQixLQUFLcEIsQ0FBTCxDQUFPUSxHQUFQLENBQVdtQixHQUFYLENBQWVDLFFBQVMsSUFBR0osSUFBSyxHQUFyRSxHQUNDLG9CQUFtQlIsT0FBUSxFQUZoQzs7O3VCQU1VLEtBQUtoQixDQUFMLENBQU82QixLQUFQLENBQWFDLGFBQWIsQ0FDRixLQURFLEVBQ0ssY0FBS2hDLElBQUwsQ0FBVSxLQUFLRSxDQUFMLENBQU9RLEdBQVAsQ0FBV2lCLE9BQVgsQ0FBbUJNLE1BQTdCLEVBQXFDLEtBQUsvQixDQUFMLENBQU9RLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsVUFBdEQsQ0FETCxDOzs7Ozs7Ozs7c0JBSUEsSUFBSXNCLEtBQUosYTs7O0FBR0o5QixvQixHQUFPO0FBQ1RrQixzQkFEUztBQUVUSSxzQkFGUztBQUdUUyx5QkFBTyxLQUhFO0FBSVRDLG1DQUFpQmxCLE9BSlI7QUFLVFksNEJBQVUsS0FBSzVCLENBQUwsQ0FBT1EsR0FBUCxDQUFXbUIsR0FBWCxDQUFlQyxRQUxoQjtBQU1UTyx1QkFBSyxLQUFLbkMsQ0FBTCxDQUFPUSxHQUFQLENBQVdDLEtBQVgsQ0FBaUIyQixXQUFqQixDQUE2QnJCLElBTnpCO0FBT1RzQix1QkFBSyxjQUFLdkMsSUFBTCxDQUFVLEtBQUtFLENBQUwsQ0FBT1EsR0FBUCxDQUFXaUIsT0FBWCxDQUFtQk0sTUFBN0IsRUFBcUMsS0FBSy9CLENBQUwsQ0FBT1EsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxVQUF0RDtBQVBJLGlCOztBQVViLG9CQUFJLHFCQUFxQk8sUUFBekIsRUFBbUM7QUFDdkJxQixpQ0FEdUIsR0FDSHJCLFFBREcsQ0FDdkJxQixlQUR1QjtBQUcvQixtQkFBQyxTQUFELEVBQVksT0FBWixFQUFxQixLQUFyQixFQUE0QkMsT0FBNUIsQ0FBb0MsVUFBQ0MsTUFBRCxFQUFZO0FBQzVDLHdCQUNJLE9BQUt4QyxDQUFMLENBQU9RLEdBQVAsQ0FBV2lDLEVBQVgsQ0FBZSxLQUFJRCxPQUFPLENBQVAsRUFBVUUsV0FBVixFQUF3QixHQUFFRixPQUFPRyxTQUFQLENBQWlCLENBQWpCLENBQW9CLEVBQWpFLEtBQ0UsSUFBR0gsTUFBTyxFQUFaLElBQWtCRixlQUZ0QixFQUdFO0FBQ0UsNkNBQVNBLGVBQVQsRUFBMEJBLGdCQUFpQixJQUFHRSxNQUFPLEVBQTNCLENBQTFCO0FBQ0g7QUFDSixtQkFQRDtBQVNBSSx5QkFBT0MsSUFBUCxDQUFZUCxlQUFaLEVBQTZCQyxPQUE3QixDQUFxQyxVQUFDTyxLQUFELEVBQVc7QUFDNUMsd0JBQUlSLGdCQUFnQlEsS0FBaEIsTUFBMkIsVUFBL0IsRUFBMkM7QUFDdkNSLHNDQUFnQlEsS0FBaEIsSUFBeUI3QixTQUFTRCxPQUFsQztBQUNIO0FBQ0osbUJBSkQ7QUFNQSx5Q0FBU2QsSUFBVCxFQUFlb0MsZUFBZjtBQUNILGlCLENBRUQ7QUFDQTs7O0FBQ0EsNEJBQUdTLFVBQUgsQ0FDSSxLQUFLL0MsQ0FBTCxDQUFPUSxHQUFQLENBQVdDLEtBQVgsQ0FBaUIyQixXQUFqQixDQUE2QlksV0FEakMsRUFFSSxLQUFLaEQsQ0FBTCxDQUFPUSxHQUFQLENBQVdDLEtBQVgsQ0FBaUIyQixXQUFqQixDQUE2QmEsY0FGakM7O0FBS0lDLHlCLEdBQVksSzs7QUFFaEIsb0JBQUksS0FBS2xELENBQUwsQ0FBTzZCLEtBQVAsQ0FBYXNCLE1BQWIsQ0FBb0IsS0FBS25ELENBQUwsQ0FBT1EsR0FBUCxDQUFXQyxLQUFYLENBQWlCMkIsV0FBakIsQ0FBNkJnQixvQkFBakQsQ0FBSixFQUE0RTtBQUN4RSw4QkFBR0wsVUFBSCxDQUNJLEtBQUsvQyxDQUFMLENBQU9RLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQjJCLFdBQWpCLENBQTZCZ0Isb0JBRGpDLEVBRUksS0FBS3BELENBQUwsQ0FBT1EsR0FBUCxDQUFXQyxLQUFYLENBQWlCMkIsV0FBakIsQ0FBNkJZLFdBRmpDOztBQUlBRSw4QkFBWSxJQUFaO0FBRUg7Ozs7dUJBSVMsS0FBS0csV0FBTCxDQUFpQm5ELElBQWpCLEM7Ozs7O0FBR04sb0JBQUlnRCxTQUFKLEVBQWU7QUFDWCxtQ0FBTUksRUFBTixDQUFTLEtBQVQsRUFBZ0IsS0FBS3RELENBQUwsQ0FBT1EsR0FBUCxDQUFXQyxLQUFYLENBQWlCMkIsV0FBakIsQ0FBNkJnQixvQkFBN0M7O0FBQ0EsbUNBQU1FLEVBQU4sQ0FBUyxLQUFULEVBQWdCLEtBQUt0RCxDQUFMLENBQU9RLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQjJCLFdBQWpCLENBQTZCWSxXQUE3QztBQUNILGlCLENBQ0Q7OztBQUNBLDRCQUFHRCxVQUFILENBQ0ksS0FBSy9DLENBQUwsQ0FBT1EsR0FBUCxDQUFXQyxLQUFYLENBQWlCMkIsV0FBakIsQ0FBNkJhLGNBRGpDLEVBRUksS0FBS2pELENBQUwsQ0FBT1EsR0FBUCxDQUFXQyxLQUFYLENBQWlCMkIsV0FBakIsQ0FBNkJZLFdBRmpDIiwiZmlsZSI6InBhY2thZ2VyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGFzc2lnbkluIGZyb20gJ2xvZGFzaC9hc3NpZ25Jbic7XG5pbXBvcnQgcGF0aCBmcm9tICdwYXRoJztcbmltcG9ydCBmcyBmcm9tICdmcyc7XG5pbXBvcnQgc2hlbGwgZnJvbSAnc2hlbGxqcyc7XG5pbXBvcnQgcGFja2FnZXIgZnJvbSAnZWxlY3Ryb24tcGFja2FnZXInO1xuXG5pbXBvcnQgTG9nIGZyb20gJy4vbG9nJztcblxuY29uc3QgeyBqb2luIH0gPSBwYXRoO1xuXG4vKipcbiAqIFdyYXBwZXIgYXJvdW5kIGVsZWN0cm9uLXBhY2thZ2VyLlxuICogQGNsYXNzXG4gKi9cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEVsZWN0cm9uUGFja2FnZXIge1xuICAgIGNvbnN0cnVjdG9yKCQpIHtcbiAgICAgICAgdGhpcy5sb2cgPSBuZXcgTG9nKCdlbGVjdHJvbi1wYWNrYWdlcicpO1xuICAgICAgICB0aGlzLiQgPSAkO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFJ1bnMgdGhlIHBhY2thZ2VyIHdpdGggcHJvdmlkZWQgYXJndW1lbnRzLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtPYmplY3R9IGFyZ3NcbiAgICAgKiBAcmV0dXJucyB7UHJvbWlzZX1cbiAgICAgKi9cbiAgICBydW5QYWNrYWdlcihhcmdzKSB7XG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICAgICAgICBwYWNrYWdlcihhcmdzLCAoZXJyKSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKGVycikge1xuICAgICAgICAgICAgICAgICAgICByZWplY3QoZXJyKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvZy5pbmZvKGB3cm90ZSBwYWNrYWdlZCBhcHAgdG8gJHt0aGlzLiQuZW52LnBhdGhzLnBhY2thZ2VEaXJ9YCk7XG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgYXN5bmMgcGFja2FnZUFwcCgpIHtcbiAgICAgICAgY29uc3QgeyB2ZXJzaW9uIH0gPSBKU09OLnBhcnNlKGZzLnJlYWRGaWxlU3luYyhcbiAgICAgICAgICAgIGpvaW4oXG4gICAgICAgICAgICAgICAgdGhpcy4kLmVudi5wYXRocy5tZXRlb3JBcHAucm9vdCxcbiAgICAgICAgICAgICAgICAnbm9kZV9tb2R1bGVzJyxcbiAgICAgICAgICAgICAgICAnZWxlY3Ryb24nLFxuICAgICAgICAgICAgICAgICdwYWNrYWdlLmpzb24nXG4gICAgICAgICAgICApLCAnVVRGLTgnXG4gICAgICAgICkpO1xuXG4gICAgICAgIGNvbnN0IHNldHRpbmdzID0gdGhpcy4kLmRlc2t0b3AuZ2V0U2V0dGluZ3MoKTtcbiAgICAgICAgY29uc3QgeyBuYW1lIH0gPSBzZXR0aW5ncztcbiAgICAgICAgaWYgKCFuYW1lKSB7XG4gICAgICAgICAgICB0aGlzLmxvZy5lcnJvcignYG5hbWVgIGZpZWxkIGluIHNldHRpbmdzLmpzb24gbm90IHNldCcpO1xuICAgICAgICAgICAgcHJvY2Vzcy5leGl0KDEpO1xuICAgICAgICB9XG5cbiAgICAgICAgY29uc3QgYXJjaCA9IHRoaXMuJC5lbnYub3B0aW9ucy5pYTMyID8gJ2lhMzInIDogJ3g2NCc7XG5cbiAgICAgICAgdGhpcy5sb2cuaW5mbyhcbiAgICAgICAgICAgIGBwYWNrYWdpbmcgJyR7bmFtZX0nIGZvciBwbGF0Zm9ybSAnJHt0aGlzLiQuZW52LnN5cy5wbGF0Zm9ybX0tJHthcmNofSdgICtcbiAgICAgICAgICAgIGAgdXNpbmcgZWxlY3Ryb24gdiR7dmVyc2lvbn1gXG4gICAgICAgICk7XG5cbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGF3YWl0IHRoaXMuJC51dGlscy5ybVdpdGhSZXRyaWVzKFxuICAgICAgICAgICAgICAgICctcmYnLCBwYXRoLmpvaW4odGhpcy4kLmVudi5vcHRpb25zLm91dHB1dCwgdGhpcy4kLmVudi5wYXRocy5wYWNrYWdlRGlyKVxuICAgICAgICAgICAgKTtcbiAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKGUpO1xuICAgICAgICB9XG5cbiAgICAgICAgY29uc3QgYXJncyA9IHtcbiAgICAgICAgICAgIG5hbWUsXG4gICAgICAgICAgICBhcmNoLFxuICAgICAgICAgICAgcHJ1bmU6IGZhbHNlLFxuICAgICAgICAgICAgZWxlY3Ryb25WZXJzaW9uOiB2ZXJzaW9uLFxuICAgICAgICAgICAgcGxhdGZvcm06IHRoaXMuJC5lbnYuc3lzLnBsYXRmb3JtLFxuICAgICAgICAgICAgZGlyOiB0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLnJvb3QsXG4gICAgICAgICAgICBvdXQ6IHBhdGguam9pbih0aGlzLiQuZW52Lm9wdGlvbnMub3V0cHV0LCB0aGlzLiQuZW52LnBhdGhzLnBhY2thZ2VEaXIpXG4gICAgICAgIH07XG5cbiAgICAgICAgaWYgKCdwYWNrYWdlck9wdGlvbnMnIGluIHNldHRpbmdzKSB7XG4gICAgICAgICAgICBjb25zdCB7IHBhY2thZ2VyT3B0aW9ucyB9ID0gc2V0dGluZ3M7XG5cbiAgICAgICAgICAgIFsnd2luZG93cycsICdsaW51eCcsICdvc3gnXS5mb3JFYWNoKChzeXN0ZW0pID0+IHtcbiAgICAgICAgICAgICAgICBpZiAoXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJC5lbnYub3NbYGlzJHtzeXN0ZW1bMF0udG9VcHBlckNhc2UoKX0ke3N5c3RlbS5zdWJzdHJpbmcoMSl9YF0gJiZcbiAgICAgICAgICAgICAgICAgICAgKGBfJHtzeXN0ZW19YCkgaW4gcGFja2FnZXJPcHRpb25zXG4gICAgICAgICAgICAgICAgKSB7XG4gICAgICAgICAgICAgICAgICAgIGFzc2lnbkluKHBhY2thZ2VyT3B0aW9ucywgcGFja2FnZXJPcHRpb25zW2BfJHtzeXN0ZW19YF0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICBPYmplY3Qua2V5cyhwYWNrYWdlck9wdGlvbnMpLmZvckVhY2goKGZpZWxkKSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKHBhY2thZ2VyT3B0aW9uc1tmaWVsZF0gPT09ICdAdmVyc2lvbicpIHtcbiAgICAgICAgICAgICAgICAgICAgcGFja2FnZXJPcHRpb25zW2ZpZWxkXSA9IHNldHRpbmdzLnZlcnNpb247XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIGFzc2lnbkluKGFyZ3MsIHBhY2thZ2VyT3B0aW9ucyk7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBNb3ZlIG5vZGVfbW9kdWxlcyBhd2F5LiBXZSBkbyBub3Qgd2FudCB0byBkZWxldGUgaXQsIGp1c3QgdGVtcG9yYXJpbHkgcmVtb3ZlIGl0IGZyb21cbiAgICAgICAgLy8gb3VyIHdheS5cbiAgICAgICAgZnMucmVuYW1lU3luYyhcbiAgICAgICAgICAgIHRoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAubm9kZU1vZHVsZXMsXG4gICAgICAgICAgICB0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLnRtcE5vZGVNb2R1bGVzXG4gICAgICAgICk7XG5cbiAgICAgICAgbGV0IGV4dHJhY3RlZCA9IGZhbHNlO1xuXG4gICAgICAgIGlmICh0aGlzLiQudXRpbHMuZXhpc3RzKHRoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAuZXh0cmFjdGVkTm9kZU1vZHVsZXMpKSB7XG4gICAgICAgICAgICBmcy5yZW5hbWVTeW5jKFxuICAgICAgICAgICAgICAgIHRoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAuZXh0cmFjdGVkTm9kZU1vZHVsZXMsXG4gICAgICAgICAgICAgICAgdGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5ub2RlTW9kdWxlc1xuICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIGV4dHJhY3RlZCA9IHRydWU7XG5cbiAgICAgICAgfVxuXG5cbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGF3YWl0IHRoaXMucnVuUGFja2FnZXIoYXJncyk7XG4gICAgICAgIH0gZmluYWxseSB7XG5cbiAgICAgICAgICAgIGlmIChleHRyYWN0ZWQpIHtcbiAgICAgICAgICAgICAgICBzaGVsbC5ybSgnLXJmJywgdGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5leHRyYWN0ZWROb2RlTW9kdWxlcyk7XG4gICAgICAgICAgICAgICAgc2hlbGwucm0oJy1yZicsIHRoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAubm9kZU1vZHVsZXMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy8gTW92ZSBub2RlX21vZHVsZXMgYmFjay5cbiAgICAgICAgICAgIGZzLnJlbmFtZVN5bmMoXG4gICAgICAgICAgICAgICAgdGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC50bXBOb2RlTW9kdWxlcyxcbiAgICAgICAgICAgICAgICB0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLm5vZGVNb2R1bGVzXG4gICAgICAgICAgICApO1xuXG4gICAgICAgIH1cbiAgICB9XG59XG4iXX0=