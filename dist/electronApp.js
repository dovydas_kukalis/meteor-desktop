"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _asar = _interopRequireDefault(require("asar"));

var _assignIn = _interopRequireDefault(require("lodash/assignIn"));

var _lodash = _interopRequireDefault(require("lodash"));

var _installLocal = require("install-local");

var _core = require("@babel/core");

var _crypto = _interopRequireDefault(require("crypto"));

var _del = _interopRequireDefault(require("del"));

var _presetEnv = _interopRequireDefault(require("@babel/preset-env"));

var _fs = _interopRequireDefault(require("fs"));

var _glob = _interopRequireDefault(require("glob"));

var _path = _interopRequireDefault(require("path"));

var _shelljs = _interopRequireDefault(require("shelljs"));

var _crossSpawn = _interopRequireDefault(require("cross-spawn"));

var _semver = _interopRequireDefault(require("semver"));

var _uglifyEs = _interopRequireDefault(require("uglify-es"));

var _yarn = require("electron-builder-lib/out/util/yarn");

var _log = _interopRequireDefault(require("./log"));

var _electronAppScaffold = _interopRequireDefault(require("./electronAppScaffold"));

var _dependenciesManager = _interopRequireDefault(require("./dependenciesManager"));

var _binaryModulesDetector = _interopRequireDefault(require("./binaryModulesDetector"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

_shelljs.default.config.fatal = true;
/**
 * Represents the .desktop dir scaffold.
 * @class
 */

var ElectronApp =
/*#__PURE__*/
function () {
  /**
   * @param {MeteorDesktop} $ - context
   * @constructor
   */
  function ElectronApp($) {
    _classCallCheck(this, ElectronApp);

    this.log = new _log.default('electronApp');
    this.scaffold = new _electronAppScaffold.default($);
    this.depsManager = new _dependenciesManager.default($, this.scaffold.getDefaultPackageJson().dependencies);
    this.$ = $;
    this.meteorApp = this.$.meteorApp;
    this.packageJson = null;
    this.version = null;
    this.compatibilityVersion = null;
  }
  /**
   * Makes an app.asar from the skeleton app.
   * @property {Array} excludeFromDel - list of paths to exclude from deleting
   * @returns {Promise}
   */


  _createClass(ElectronApp, [{
    key: "packSkeletonToAsar",
    value: function packSkeletonToAsar() {
      var _this = this;

      var excludeFromDel = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
      this.log.info('packing skeleton app and node_modules to asar archive');
      return new Promise(function (resolve) {
        var extract = _this.getModulesToExtract(); // We want to pack skeleton app and node_modules together, so we need to temporarily
        // move node_modules to app dir.


        _this.log.debug('moving node_modules to app dir');

        _fs.default.renameSync(_this.$.env.paths.electronApp.nodeModules, _path.default.join(_this.$.env.paths.electronApp.appRoot, 'node_modules'));

        var extracted = false;
        extracted = _this.extractModules(extract);

        _this.log.debug('packing');

        _asar.default.createPackage(_this.$.env.paths.electronApp.appRoot, _this.$.env.paths.electronApp.appAsar, function () {
          // Lets move the node_modules back.
          _this.log.debug('moving node_modules back from app dir');

          _shelljs.default.mv(_path.default.join(_this.$.env.paths.electronApp.appRoot, 'node_modules'), _this.$.env.paths.electronApp.nodeModules);

          if (extracted) {
            // We need to create a full node modules back. In other words we want
            // the extracted modules back.
            extract.forEach(function (module) {
              return _shelljs.default.cp('-rf', _path.default.join(_this.$.env.paths.electronApp.extractedNodeModules, module), _path.default.join(_this.$.env.paths.electronApp.nodeModules, module));
            }); // Get the .bin back.

            if (_this.$.utils.exists(_this.$.env.paths.electronApp.extractedNodeModulesBin)) {
              _shelljs.default.cp(_path.default.join(_this.$.env.paths.electronApp.extractedNodeModulesBin, '*'), _path.default.join(_this.$.env.paths.electronApp.nodeModules, '.bin'));
            }
          }

          _this.log.debug('deleting source files');

          var exclude = [_this.$.env.paths.electronApp.nodeModules].concat([_this.$.env.paths.electronApp.appAsar, _this.$.env.paths.electronApp.packageJson], excludeFromDel);

          _del.default.sync([`${_this.$.env.paths.electronApp.root}${_path.default.sep}*`].concat(exclude.map(function (pathToExclude) {
            return `!${pathToExclude}`;
          })));

          resolve();
        });
      });
    }
    /**
     * Moves specified node modules to a separate directory.
     * @param {Array} extract
     * @returns {boolean}
     */

  }, {
    key: "extractModules",
    value: function extractModules(extract) {
      var _this2 = this;

      var ext = ['.js', '.bat', '.sh', '.cmd', ''];

      if (extract.length > 0) {
        if (this.$.utils.exists(this.$.env.paths.electronApp.extractedNodeModules)) {
          _shelljs.default.rm('-rf', this.$.env.paths.electronApp.extractedNodeModules);
        }

        _fs.default.mkdirSync(this.$.env.paths.electronApp.extractedNodeModules);

        _fs.default.mkdirSync(this.$.env.paths.electronApp.extractedNodeModulesBin);

        extract.forEach(function (module) {
          _fs.default.renameSync(_path.default.join(_this2.$.env.paths.electronApp.appRoot, 'node_modules', module), _path.default.join(_this2.$.env.paths.electronApp.extractedNodeModules, module)); // Move bins.


          _this2.extractBin(module, ext);
        });
        return true;
      }

      return false;
    }
    /**
     * Extracts the bin files associated with a certain node modules.
     *
     * @param module
     * @param ext
     */

  }, {
    key: "extractBin",
    value: function extractBin(module, ext) {
      var _this3 = this;

      var packageJson;

      try {
        packageJson = JSON.parse(_fs.default.readFileSync(_path.default.join(this.$.env.paths.electronApp.extractedNodeModules, module, 'package.json'), 'utf8'));
      } catch (e) {
        packageJson = {};
      }

      var bins = 'bin' in packageJson && typeof packageJson.bin === 'object' ? Object.keys(packageJson.bin) : [];

      if (bins.length > 0) {
        bins.forEach(function (bin) {
          ext.forEach(function (extension) {
            var binFilePath = _path.default.join(_this3.$.env.paths.electronApp.appRoot, 'node_modules', '.bin', `${bin}${extension}`);

            if (_this3.$.utils.exists(binFilePath) || _this3.$.utils.symlinkExists(binFilePath)) {
              _fs.default.renameSync(binFilePath, _path.default.join(_this3.$.env.paths.electronApp.extractedNodeModulesBin, `${bin}${extension}`));
            }
          });
        });
      }
    }
    /**
     * Merges the `extract` field with automatically detected modules.
     */

  }, {
    key: "getModulesToExtract",
    value: function getModulesToExtract() {
      var binaryModulesDetector = new _binaryModulesDetector.default(this.$.env.paths.electronApp.nodeModules);
      var toBeExtracted = binaryModulesDetector.detect();

      var _$$desktop$getSetting = this.$.desktop.getSettings(),
          extract = _$$desktop$getSetting.extract;

      if (!Array.isArray(extract)) {
        extract = [];
      }

      var merge = {};
      toBeExtracted.concat(extract).forEach(function (module) {
        merge[module] = true;
      });
      extract = Object.keys(merge);

      if (extract.length > 0) {
        this.log.verbose(`resultant modules to extract list is: ${extract.join(', ')}`);
      }

      return extract;
    }
    /**
     * Calculates a md5 from all dependencies.
     */

  }, {
    key: "calculateCompatibilityVersion",
    value: function calculateCompatibilityVersion() {
      var _this4 = this;

      this.log.verbose('calculating compatibility version');
      var settings = this.$.desktop.getSettings();

      if ('desktopHCPCompatibilityVersion' in settings) {
        this.compatibilityVersion = `${settings.desktopHCPCompatibilityVersion}`;
        this.log.warn(`compatibility version overridden to ${this.compatibilityVersion}`);
        return;
      }

      var md5 = _crypto.default.createHash('md5');

      var dependencies = Object.keys(this.depsManager.getDependencies()).sort();
      dependencies = dependencies.map(function (dependency) {
        return `${dependency}:${_this4.packageJson.dependencies[dependency]}`;
      });
      var mainCompatibilityVersion = this.$.getVersion().split('.');
      this.log.debug('meteor-desktop compatibility version is ', `${mainCompatibilityVersion[0]}.${mainCompatibilityVersion[1]}`);
      dependencies.push(`meteor-desktop:${mainCompatibilityVersion[0]}.${mainCompatibilityVersion[1]}`);
      var desktopCompatibilityVersion = settings.version.split('.')[0];
      this.log.debug('.desktop compatibility version is ', desktopCompatibilityVersion);
      dependencies.push(`desktop-app:${desktopCompatibilityVersion}`);

      if (process.env.METEOR_DESKTOP_DEBUG_DESKTOP_COMPATIBILITY_VERSION || process.env.METEOR_DESKTOP_DEBUG) {
        this.log.debug(`compatibility version calculated from ${JSON.stringify(dependencies)}`);
      }

      md5.update(JSON.stringify(dependencies));
      this.compatibilityVersion = md5.digest('hex');
    }
    /**
     * Runs all necessary tasks to build the desktopified app.
     */

  }, {
    key: "build",
    value: function () {
      var _build = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee() {
        var run,
            nodeModulesRemoved,
            _args = arguments;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                run = _args.length > 0 && _args[0] !== undefined ? _args[0] : false;
                // TODO: refactor to a task runner
                this.log.info('scaffolding');

                if (!this.$.desktop.check()) {
                  if (!this.$.env.options.scaffold) {
                    this.log.error('seems that you do not have a .desktop dir in your project or it is' + ' corrupted. Run \'npm run desktop -- init\' to get a new one.'); // Do not fail, so that npm will not print his error stuff to console.

                    process.exit(0);
                  } else {
                    this.$.desktop.scaffold();
                    this.$.meteorApp.updateGitIgnore();
                  }
                }

                try {
                  this.$.meteorApp.updateGitIgnore();
                } catch (e) {
                  this.log.warn(`error occurred while adding ${this.$.env.paths.electronApp.rootName}` + 'to .gitignore: ', e);
                }

                _context.prev = 4;
                _context.next = 7;
                return this.$.meteorApp.ensureDesktopHCPPackages();

              case 7:
                _context.next = 13;
                break;

              case 9:
                _context.prev = 9;
                _context.t0 = _context["catch"](4);
                this.log.error('error while checking for required packages: ', _context.t0);
                process.exit(1);

              case 13:
                _context.prev = 13;
                _context.next = 16;
                return this.scaffold.make();

              case 16:
                _context.next = 22;
                break;

              case 18:
                _context.prev = 18;
                _context.t1 = _context["catch"](13);
                this.log.error('error while scaffolding: ', _context.t1);
                process.exit(1);

              case 22:
                try {
                  this.updatePackageJsonFields();
                } catch (e) {
                  this.log.error('error while updating package.json: ', e);
                }

                try {
                  this.updateDependenciesList();
                } catch (e) {
                  this.log.error('error while merging dependencies list: ', e);
                }

                try {
                  this.calculateCompatibilityVersion();
                } catch (e) {
                  this.log.error('error while calculating compatibility version: ', e);
                  process.exit(1);
                }

                _context.prev = 25;
                _context.next = 28;
                return this.handleTemporaryNodeModules();

              case 28:
                _context.next = 34;
                break;

              case 30:
                _context.prev = 30;
                _context.t2 = _context["catch"](25);
                this.log.error('error occurred while handling temporary node_modules: ', _context.t2);
                process.exit(1);

              case 34:
                _context.prev = 34;
                _context.next = 37;
                return this.handleStateOfNodeModules();

              case 37:
                nodeModulesRemoved = _context.sent;
                _context.next = 44;
                break;

              case 40:
                _context.prev = 40;
                _context.t3 = _context["catch"](34);
                this.log.error('error occurred while clearing node_modules: ', _context.t3);
                process.exit(1);

              case 44:
                _context.prev = 44;
                _context.next = 47;
                return this.rebuildDeps(true);

              case 47:
                _context.next = 53;
                break;

              case 49:
                _context.prev = 49;
                _context.t4 = _context["catch"](44);
                this.log.error('error occurred while installing node_modules: ', _context.t4);
                process.exit(1);

              case 53:
                if (nodeModulesRemoved) {
                  _context.next = 63;
                  break;
                }

                _context.prev = 54;
                _context.next = 57;
                return this.rebuildDeps();

              case 57:
                _context.next = 63;
                break;

              case 59:
                _context.prev = 59;
                _context.t5 = _context["catch"](54);
                this.log.error('error occurred while rebuilding native node modules: ', _context.t5);
                process.exit(1);

              case 63:
                _context.prev = 63;
                _context.next = 66;
                return this.installLocalNodeModules();

              case 66:
                _context.next = 72;
                break;

              case 68:
                _context.prev = 68;
                _context.t6 = _context["catch"](63);
                this.log.error('error occurred while installing local node modules: ', _context.t6);
                process.exit(1);

              case 72:
                _context.prev = 72;
                _context.next = 75;
                return this.ensureMeteorDependencies();

              case 75:
                _context.next = 81;
                break;

              case 77:
                _context.prev = 77;
                _context.t7 = _context["catch"](72);
                this.log.error('error occurred while ensuring meteor dependencies are installed: ', _context.t7);
                process.exit(1);

              case 81:
                if (!this.$.env.isProductionBuild()) {
                  _context.next = 91;
                  break;
                }

                _context.prev = 82;
                _context.next = 85;
                return this.packSkeletonToAsar();

              case 85:
                _context.next = 91;
                break;

              case 87:
                _context.prev = 87;
                _context.t8 = _context["catch"](82);
                this.log.error('error while packing skeleton to asar: ', _context.t8);
                process.exit(1);

              case 91:
                // TODO: find a way to avoid copying .desktop to a temp location
                try {
                  this.copyDesktopToDesktopTemp();
                } catch (e) {
                  this.log.error('error while copying .desktop to a temporary location: ', e);
                  process.exit(1);
                }

                try {
                  this.updateSettingsJsonFields();
                } catch (e) {
                  this.log.error('error while updating settings.json: ', e);
                  process.exit(1);
                }

                _context.prev = 93;
                _context.next = 96;
                return this.excludeFilesFromArchive();

              case 96:
                _context.next = 102;
                break;

              case 98:
                _context.prev = 98;
                _context.t9 = _context["catch"](93);
                this.log.error('error while excluding files from packing to asar: ', _context.t9);
                process.exit(1);

              case 102:
                try {
                  this.transpileAndMinify();
                } catch (e) {
                  this.log.error('error while transpiling or minifying: ', e);
                }

                _context.prev = 103;
                _context.next = 106;
                return this.packDesktopToAsar();

              case 106:
                _context.next = 112;
                break;

              case 108:
                _context.prev = 108;
                _context.t10 = _context["catch"](103);
                this.log.error('error occurred while packing .desktop to asar: ', _context.t10);
                process.exit(1);

              case 112:
                _context.prev = 112;
                _context.next = 115;
                return this.getMeteorClientBuild();

              case 115:
                _context.next = 120;
                break;

              case 117:
                _context.prev = 117;
                _context.t11 = _context["catch"](112);
                this.log.error('error occurred during getting meteor mobile build: ', _context.t11);

              case 120:
                if (run) {
                  this.log.info('running');
                  this.$.electron.run();
                } else {
                  this.log.info('built');
                }

              case 121:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[4, 9], [13, 18], [25, 30], [34, 40], [44, 49], [54, 59], [63, 68], [72, 77], [82, 87], [93, 98], [103, 108], [112, 117]]);
      }));

      return function build() {
        return _build.apply(this, arguments);
      };
    }()
    /**
     * Ensures all required dependencies are added to the Meteor project.
     * @returns {Promise.<void>}
     */

  }, {
    key: "ensureMeteorDependencies",
    value: function () {
      var _ensureMeteorDependencies = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2() {
        var _this5 = this;

        var packages, packagesWithVersion, plugins;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                packages = [];
                packagesWithVersion = [];
                plugins = 'plugins [';
                Object.keys(this.$.desktop.getDependencies().plugins).forEach(function (plugin) {
                  // Read package.json of the plugin.
                  var packageJson = JSON.parse(_fs.default.readFileSync(_path.default.join(_this5.$.env.paths.electronApp.nodeModules, plugin, 'package.json'), 'utf8'));

                  if ('meteorDependencies' in packageJson && typeof packageJson.meteorDependencies === 'object') {
                    plugins += `${plugin}, `;
                    packages.unshift.apply(packages, _toConsumableArray(Object.keys(packageJson.meteorDependencies)));
                    packagesWithVersion.unshift.apply(packagesWithVersion, _toConsumableArray(packages.map(function (packageName) {
                      if (packageJson.meteorDependencies[packageName] === '@version') {
                        return `${packageName}@${packageJson.version}`;
                      }

                      return `${packageName}@${packageJson.meteorDependencies[packageName]}`;
                    })));
                  }
                });

                if (!(packages.length > 0)) {
                  _context2.next = 14;
                  break;
                }

                plugins = `${plugins.substr(0, plugins.length - 2)}]`;
                _context2.prev = 6;
                _context2.next = 9;
                return this.$.meteorApp.meteorManager.ensurePackages(packages, packagesWithVersion, plugins);

              case 9:
                _context2.next = 14;
                break;

              case 11:
                _context2.prev = 11;
                _context2.t0 = _context2["catch"](6);
                throw new Error(_context2.t0);

              case 14:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[6, 11]]);
      }));

      return function ensureMeteorDependencies() {
        return _ensureMeteorDependencies.apply(this, arguments);
      };
    }()
    /**
     * Builds meteor app.
     */

  }, {
    key: "getMeteorClientBuild",
    value: function () {
      var _getMeteorClientBuild = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee3() {
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return this.$.meteorApp.build();

              case 2:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      return function getMeteorClientBuild() {
        return _getMeteorClientBuild.apply(this, arguments);
      };
    }()
    /**
     * Removes node_modules if needed.
     * @returns {Promise<void>}
     */

  }, {
    key: "handleStateOfNodeModules",
    value: function () {
      var _handleStateOfNodeModules = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee4() {
        return regeneratorRuntime.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                if (!(this.$.env.isProductionBuild() || this.$.env.options.ia32)) {
                  _context4.next = 11;
                  break;
                }

                if (!this.$.env.isProductionBuild()) {
                  this.log.info('clearing node_modules because we need to have it clear for ia32 rebuild');
                } else {
                  this.log.info('clearing node_modules because this is a production build');
                }

                _context4.prev = 2;
                _context4.next = 5;
                return this.$.utils.rmWithRetries('-rf', this.$.env.paths.electronApp.nodeModules);

              case 5:
                _context4.next = 10;
                break;

              case 7:
                _context4.prev = 7;
                _context4.t0 = _context4["catch"](2);
                throw new Error(_context4.t0);

              case 10:
                return _context4.abrupt("return", true);

              case 11:
                return _context4.abrupt("return", false);

              case 12:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this, [[2, 7]]);
      }));

      return function handleStateOfNodeModules() {
        return _handleStateOfNodeModules.apply(this, arguments);
      };
    }()
    /**
     * If there is a temporary node_modules folder and no node_modules folder, we will
     * restore it, as it might be a leftover from an interrupted flow.
     * @returns {Promise<void>}
     */

  }, {
    key: "handleTemporaryNodeModules",
    value: function () {
      var _handleTemporaryNodeModules = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee5() {
        return regeneratorRuntime.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                if (!this.$.utils.exists(this.$.env.paths.electronApp.tmpNodeModules)) {
                  _context5.next = 15;
                  break;
                }

                if (this.$.utils.exists(this.$.env.paths.electronApp.nodeModules)) {
                  _context5.next = 6;
                  break;
                }

                this.log.debug('moving temp node_modules back');

                _shelljs.default.mv(this.$.env.paths.electronApp.tmpNodeModules, this.$.env.paths.electronApp.nodeModules);

                _context5.next = 15;
                break;

              case 6:
                // If there is a node_modules folder, we should clear the temporary one.
                this.log.debug('clearing temp node_modules because new one is already created');
                _context5.prev = 7;
                _context5.next = 10;
                return this.$.utils.rmWithRetries('-rf', this.$.env.paths.electronApp.tmpNodeModules);

              case 10:
                _context5.next = 15;
                break;

              case 12:
                _context5.prev = 12;
                _context5.t0 = _context5["catch"](7);
                throw new Error(_context5.t0);

              case 15:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this, [[7, 12]]);
      }));

      return function handleTemporaryNodeModules() {
        return _handleTemporaryNodeModules.apply(this, arguments);
      };
    }()
    /**
     * NOT IN USE RIGHT NOW // DEPRECATED
     *
     * Wrapper for spawning npm.
     * @param {Array}  commands - commands for spawn
     * @param {string} stdio
     * @return {Promise}
     */

  }, {
    key: "runNpm",
    value: function runNpm(commands) {
      var _this6 = this;

      var stdio = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'ignore';
      return new Promise(function (resolve, reject) {
        // TODO: find a way to run npm without depending on it cause it's a huge dependency.
        var npm = _path.default.join(_this6.$.env.paths.meteorApp.root, 'node_modules', '.bin', 'npm');

        if (!_this6.$.utils.exists(npm)) {
          npm = _path.default.join(_this6.$.env.paths.meteorApp.root, 'node_modules', 'meteor-desktop', 'node_modules', '.bin', 'npm');
        }

        _this6.log.verbose(`executing npm ${commands.join(' ')}`);

        (0, _crossSpawn.default)(npm, commands, {
          cwd: _this6.$.env.paths.electronApp.root,
          stdio
        }).on('exit', function (code) {
          return code === 0 ? resolve() : reject(new Error(`npm exit code was ${code}`));
        });
      });
    }
    /**
     * Runs npm link for every package specified in settings.json->linkPackages.
     */

  }, {
    key: "linkNpmPackages",
    value: function () {
      var _linkNpmPackages = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee6() {
        var _this7 = this;

        var settings, promises;
        return regeneratorRuntime.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                settings = this.$.desktop.getSettings();
                promises = [];

                if ('linkPackages' in this.$.desktop.getSettings()) {
                  if (Array.isArray(settings.linkPackages)) {
                    settings.linkPackages.forEach(function (packageName) {
                      return promises.push(_this7.runNpm(['link', packageName]));
                    });
                  }
                }

                _context6.next = 5;
                return Promise.all(promises);

              case 5:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this);
      }));

      return function linkNpmPackages() {
        return _linkNpmPackages.apply(this, arguments);
      };
    }()
    /**
     * Runs npm in the electron app to get the dependencies installed.
     * @returns {Promise}
     */

  }, {
    key: "ensureDeps",
    value: function () {
      var _ensureDeps = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee7() {
        return regeneratorRuntime.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _context7.next = 2;
                return this.linkNpmPackages();

              case 2:
                this.log.info('installing dependencies');

                if (!this.$.utils.exists(this.$.env.paths.electronApp.nodeModules)) {
                  _context7.next = 13;
                  break;
                }

                this.log.debug('running npm prune to wipe unneeded dependencies');
                _context7.prev = 5;
                _context7.next = 8;
                return this.runNpm(['prune']);

              case 8:
                _context7.next = 13;
                break;

              case 10:
                _context7.prev = 10;
                _context7.t0 = _context7["catch"](5);
                throw new Error(_context7.t0);

              case 13:
                _context7.prev = 13;
                _context7.next = 16;
                return this.runNpm(['install'], this.$.env.stdio);

              case 16:
                _context7.next = 21;
                break;

              case 18:
                _context7.prev = 18;
                _context7.t1 = _context7["catch"](13);
                throw new Error(_context7.t1);

              case 21:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, this, [[5, 10], [13, 18]]);
      }));

      return function ensureDeps() {
        return _ensureDeps.apply(this, arguments);
      };
    }()
    /**
     * Warns if plugins version are outdated in compare to the newest scaffold.
     * @param {Object} pluginsVersions - current plugins versions from settings.json
     */

  }, {
    key: "checkPluginsVersion",
    value: function checkPluginsVersion(pluginsVersions) {
      var _this8 = this;

      var settingsJson = JSON.parse(_fs.default.readFileSync(_path.default.join(this.$.env.paths.scaffold, 'settings.json')));
      var scaffoldPluginsVersion = this.$.desktop.getDependencies(settingsJson, false).plugins;
      Object.keys(pluginsVersions).forEach(function (pluginName) {
        if (pluginName in scaffoldPluginsVersion && scaffoldPluginsVersion[pluginName] !== pluginsVersions[pluginName] && _semver.default.lt(pluginsVersions[pluginName], scaffoldPluginsVersion[pluginName])) {
          _this8.log.warn(`you are using outdated version ${pluginsVersions[pluginName]} of ` + `${pluginName}, the suggested version to use is ` + `${scaffoldPluginsVersion[pluginName]}`);
        }
      });
    }
    /**
     * Merges core dependency list with the dependencies from .desktop.
     */

  }, {
    key: "updateDependenciesList",
    value: function updateDependenciesList() {
      var _this9 = this;

      this.log.info('updating list of package.json\'s dependencies');
      var desktopDependencies = this.$.desktop.getDependencies();
      this.checkPluginsVersion(desktopDependencies.plugins);
      this.log.debug('merging settings.json[dependencies]');
      this.depsManager.mergeDependencies('settings.json[dependencies]', desktopDependencies.fromSettings);
      this.log.debug('merging settings.json[plugins]');
      this.depsManager.mergeDependencies('settings.json[plugins]', desktopDependencies.plugins);
      this.log.debug('merging dependencies from modules');
      Object.keys(desktopDependencies.modules).forEach(function (module) {
        return _this9.depsManager.mergeDependencies(`module[${module}]`, desktopDependencies.modules[module]);
      });
      this.packageJson.dependencies = this.depsManager.getRemoteDependencies();
      this.packageJson.localDependencies = this.depsManager.getLocalDependencies();
      this.log.debug('writing updated package.json');

      _fs.default.writeFileSync(this.$.env.paths.electronApp.packageJson, JSON.stringify(this.packageJson, null, 2));
    }
    /**
     * Install node modules from local paths using local-install.
     *
     * @param {string} arch
     * @returns {Promise}
     */

  }, {
    key: "installLocalNodeModules",
    value: function installLocalNodeModules() {
      var arch = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.$.env.options.ia32 || process.arch === 'ia32' ? 'ia32' : 'x64';

      var localDependencies = _lodash.default.values(this.packageJson.localDependencies);

      if (localDependencies.length === 0) {
        return Promise.resolve();
      }

      this.log.info('installing local node modules');
      var lastRebuild = this.$.electronBuilder.prepareLastRebuildObject(arch);
      var env = (0, _yarn.getGypEnv)(lastRebuild.frameworkInfo, lastRebuild.platform, lastRebuild.arch);
      var installer = new _installLocal.LocalInstaller({
        [this.$.env.paths.electronApp.root]: localDependencies
      }, {
        npmEnv: env
      });
      (0, _installLocal.progress)(installer);
      return installer.install();
    }
    /**
     * Rebuild binary dependencies against Electron's node headers.
     * @returns {Promise}
     */

  }, {
    key: "rebuildDeps",
    value: function rebuildDeps() {
      var install = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

      if (install) {
        this.log.info('issuing node_modules install from electron-builder');
      } else {
        this.log.info('issuing native modules rebuild from electron-builder');
      }

      var arch = this.$.env.options.ia32 || process.arch === 'ia32' ? 'ia32' : 'x64';

      if (this.$.env.options.ia32) {
        this.log.verbose('forcing rebuild for 32bit');
      } else {
        this.log.verbose(`rebuilding for ${arch}`);
      }

      return this.$.electronBuilder.installOrRebuild(arch, undefined, install);
    }
    /**
     * Update package.json fields accordingly to what is set in settings.json.
     *
     * packageJson.name = settings.projectName
     * packageJson.version = settings.version
     * packageJson.* = settings.packageJsonFields
     */

  }, {
    key: "updatePackageJsonFields",
    value: function updatePackageJsonFields() {
      this.log.verbose('updating package.json fields');
      var settings = this.$.desktop.getSettings();
      /** @type {desktopSettings} */

      var packageJson = this.scaffold.getDefaultPackageJson();
      packageJson.version = settings.version;

      if ('packageJsonFields' in settings) {
        (0, _assignIn.default)(packageJson, settings.packageJsonFields);
      }

      (0, _assignIn.default)(packageJson, {
        name: settings.projectName
      });
      this.log.debug('writing updated package.json');

      _fs.default.writeFileSync(this.$.env.paths.electronApp.packageJson, JSON.stringify(packageJson, null, 4));

      this.packageJson = packageJson;
    }
    /**
     * Updates settings.json with env (prod/dev) information and versions.
     */

  }, {
    key: "updateSettingsJsonFields",
    value: function updateSettingsJsonFields() {
      this.log.debug('updating settings.json fields');
      var settings = this.$.desktop.getSettings(); // Save versions.

      settings.compatibilityVersion = this.compatibilityVersion; // Pass information about build type to the settings.json.

      settings.env = this.$.env.isProductionBuild() ? 'prod' : 'dev';
      settings.desktopVersion = `${this.$.desktop.getHashVersion()}_${settings.env}`;
      settings.meteorDesktopVersion = this.$.getVersion();

      _fs.default.writeFileSync(this.$.env.paths.desktopTmp.settings, JSON.stringify(settings, null, 4));
    }
    /**
     * Copies files from prepared .desktop to desktop.asar in electron app.
     */

  }, {
    key: "packDesktopToAsar",
    value: function packDesktopToAsar() {
      var _this10 = this;

      this.log.info('packing .desktop to asar');
      return new Promise(function (resolve, reject) {
        _asar.default.createPackage(_this10.$.env.paths.desktopTmp.root, _this10.$.env.paths.electronApp.desktopAsar, function () {
          _this10.log.verbose('clearing temporary .desktop');

          _this10.$.utils.rmWithRetries('-rf', _this10.$.env.paths.desktopTmp.root).then(function () {
            resolve();
          }).catch(function (e) {
            reject(e);
          });

          resolve();
        });
      });
    }
    /**
     * Makes a temporary copy of .desktop.
     */

  }, {
    key: "copyDesktopToDesktopTemp",
    value: function copyDesktopToDesktopTemp() {
      this.log.verbose('copying .desktop to temporary location');

      _shelljs.default.cp('-rf', this.$.env.paths.desktop.root, this.$.env.paths.desktopTmp.root); // Remove test files.


      _del.default.sync([_path.default.join(this.$.env.paths.desktopTmp.root, '**', '*.test.js')]);
    }
    /**
     * Runs babel and uglify over .desktop if requested.
     */

  }, {
    key: "transpileAndMinify",
    value: function transpileAndMinify() {
      this.log.info('transpiling and uglifying');
      var settings = this.$.desktop.getSettings();
      var options = 'uglifyOptions' in settings ? settings.uglifyOptions : {};
      var uglifyingEnabled = 'uglify' in settings && !!settings.uglify;
      var preset = (0, _presetEnv.default)(undefined, {
        targets: {
          node: '7'
        }
      });

      _glob.default.sync(`${this.$.env.paths.desktopTmp.root}/**/*.js`).forEach(function (file) {
        var _transformFileSync = (0, _core.transformFileSync)(file, {
          presets: [preset]
        }),
            code = _transformFileSync.code;

        var error;

        if (settings.env === 'prod' && uglifyingEnabled) {
          var _uglify$minify = _uglifyEs.default.minify(code, options);

          code = _uglify$minify.code;
          error = _uglify$minify.error;
        }

        if (error) {
          throw new Error(error);
        }

        _fs.default.writeFileSync(file, code);
      });
    }
    /**
     * Moves all the files that should not be packed into asar into a safe location which is the
     * 'extracted' dir in the electron app.
     */

  }, {
    key: "excludeFilesFromArchive",
    value: function () {
      var _excludeFilesFromArchive = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee8() {
        var _this11 = this;

        var configs;
        return regeneratorRuntime.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                this.log.info('excluding files from packing'); // Ensure empty `extracted` dir

                _context8.prev = 1;
                _context8.next = 4;
                return this.$.utils.rmWithRetries('-rf', this.$.env.paths.electronApp.extracted);

              case 4:
                _context8.next = 9;
                break;

              case 6:
                _context8.prev = 6;
                _context8.t0 = _context8["catch"](1);
                throw new Error(_context8.t0);

              case 9:
                _shelljs.default.mkdir(this.$.env.paths.electronApp.extracted);

                configs = this.$.desktop.gatherModuleConfigs(); // Move files that should not be asar'ed.

                configs.forEach(function (config) {
                  var moduleConfig = config;

                  if ('extract' in moduleConfig) {
                    if (!Array.isArray(moduleConfig.extract)) {
                      moduleConfig.extract = [moduleConfig.extract];
                    }

                    moduleConfig.extract.forEach(function (file) {
                      _this11.log.debug(`excluding ${file} from ${config.name}`);

                      var filePath = _path.default.join(_this11.$.env.paths.desktopTmp.modules, moduleConfig.dirName, file);

                      var destinationPath = _path.default.join(_this11.$.env.paths.electronApp.extracted, moduleConfig.dirName);

                      if (!_this11.$.utils.exists(destinationPath)) {
                        _shelljs.default.mkdir(destinationPath);
                      }

                      _shelljs.default.mv(filePath, destinationPath);
                    });
                  }
                });

              case 12:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8, this, [[1, 6]]);
      }));

      return function excludeFilesFromArchive() {
        return _excludeFilesFromArchive.apply(this, arguments);
      };
    }()
  }]);

  return ElectronApp;
}();

exports.default = ElectronApp;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL2xpYi9lbGVjdHJvbkFwcC5qcyJdLCJuYW1lcyI6WyJjb25maWciLCJmYXRhbCIsIkVsZWN0cm9uQXBwIiwiJCIsImxvZyIsInNjYWZmb2xkIiwiZGVwc01hbmFnZXIiLCJnZXREZWZhdWx0UGFja2FnZUpzb24iLCJkZXBlbmRlbmNpZXMiLCJtZXRlb3JBcHAiLCJwYWNrYWdlSnNvbiIsInZlcnNpb24iLCJjb21wYXRpYmlsaXR5VmVyc2lvbiIsImV4Y2x1ZGVGcm9tRGVsIiwiaW5mbyIsIlByb21pc2UiLCJyZXNvbHZlIiwiZXh0cmFjdCIsImdldE1vZHVsZXNUb0V4dHJhY3QiLCJkZWJ1ZyIsInJlbmFtZVN5bmMiLCJlbnYiLCJwYXRocyIsImVsZWN0cm9uQXBwIiwibm9kZU1vZHVsZXMiLCJqb2luIiwiYXBwUm9vdCIsImV4dHJhY3RlZCIsImV4dHJhY3RNb2R1bGVzIiwiY3JlYXRlUGFja2FnZSIsImFwcEFzYXIiLCJtdiIsImZvckVhY2giLCJjcCIsImV4dHJhY3RlZE5vZGVNb2R1bGVzIiwibW9kdWxlIiwidXRpbHMiLCJleGlzdHMiLCJleHRyYWN0ZWROb2RlTW9kdWxlc0JpbiIsImV4Y2x1ZGUiLCJjb25jYXQiLCJzeW5jIiwicm9vdCIsInNlcCIsIm1hcCIsInBhdGhUb0V4Y2x1ZGUiLCJleHQiLCJsZW5ndGgiLCJybSIsIm1rZGlyU3luYyIsImV4dHJhY3RCaW4iLCJKU09OIiwicGFyc2UiLCJyZWFkRmlsZVN5bmMiLCJlIiwiYmlucyIsImJpbiIsIk9iamVjdCIsImtleXMiLCJleHRlbnNpb24iLCJiaW5GaWxlUGF0aCIsInN5bWxpbmtFeGlzdHMiLCJiaW5hcnlNb2R1bGVzRGV0ZWN0b3IiLCJ0b0JlRXh0cmFjdGVkIiwiZGV0ZWN0IiwiZGVza3RvcCIsImdldFNldHRpbmdzIiwiQXJyYXkiLCJpc0FycmF5IiwibWVyZ2UiLCJ2ZXJib3NlIiwic2V0dGluZ3MiLCJkZXNrdG9wSENQQ29tcGF0aWJpbGl0eVZlcnNpb24iLCJ3YXJuIiwibWQ1IiwiY3JlYXRlSGFzaCIsImdldERlcGVuZGVuY2llcyIsInNvcnQiLCJkZXBlbmRlbmN5IiwibWFpbkNvbXBhdGliaWxpdHlWZXJzaW9uIiwiZ2V0VmVyc2lvbiIsInNwbGl0IiwicHVzaCIsImRlc2t0b3BDb21wYXRpYmlsaXR5VmVyc2lvbiIsInByb2Nlc3MiLCJNRVRFT1JfREVTS1RPUF9ERUJVR19ERVNLVE9QX0NPTVBBVElCSUxJVFlfVkVSU0lPTiIsIk1FVEVPUl9ERVNLVE9QX0RFQlVHIiwic3RyaW5naWZ5IiwidXBkYXRlIiwiZGlnZXN0IiwicnVuIiwiY2hlY2siLCJvcHRpb25zIiwiZXJyb3IiLCJleGl0IiwidXBkYXRlR2l0SWdub3JlIiwicm9vdE5hbWUiLCJlbnN1cmVEZXNrdG9wSENQUGFja2FnZXMiLCJtYWtlIiwidXBkYXRlUGFja2FnZUpzb25GaWVsZHMiLCJ1cGRhdGVEZXBlbmRlbmNpZXNMaXN0IiwiY2FsY3VsYXRlQ29tcGF0aWJpbGl0eVZlcnNpb24iLCJoYW5kbGVUZW1wb3JhcnlOb2RlTW9kdWxlcyIsImhhbmRsZVN0YXRlT2ZOb2RlTW9kdWxlcyIsIm5vZGVNb2R1bGVzUmVtb3ZlZCIsInJlYnVpbGREZXBzIiwiaW5zdGFsbExvY2FsTm9kZU1vZHVsZXMiLCJlbnN1cmVNZXRlb3JEZXBlbmRlbmNpZXMiLCJpc1Byb2R1Y3Rpb25CdWlsZCIsInBhY2tTa2VsZXRvblRvQXNhciIsImNvcHlEZXNrdG9wVG9EZXNrdG9wVGVtcCIsInVwZGF0ZVNldHRpbmdzSnNvbkZpZWxkcyIsImV4Y2x1ZGVGaWxlc0Zyb21BcmNoaXZlIiwidHJhbnNwaWxlQW5kTWluaWZ5IiwicGFja0Rlc2t0b3BUb0FzYXIiLCJnZXRNZXRlb3JDbGllbnRCdWlsZCIsImVsZWN0cm9uIiwicGFja2FnZXMiLCJwYWNrYWdlc1dpdGhWZXJzaW9uIiwicGx1Z2lucyIsInBsdWdpbiIsIm1ldGVvckRlcGVuZGVuY2llcyIsInVuc2hpZnQiLCJwYWNrYWdlTmFtZSIsInN1YnN0ciIsIm1ldGVvck1hbmFnZXIiLCJlbnN1cmVQYWNrYWdlcyIsIkVycm9yIiwiYnVpbGQiLCJpYTMyIiwicm1XaXRoUmV0cmllcyIsInRtcE5vZGVNb2R1bGVzIiwiY29tbWFuZHMiLCJzdGRpbyIsInJlamVjdCIsIm5wbSIsImN3ZCIsIm9uIiwiY29kZSIsInByb21pc2VzIiwibGlua1BhY2thZ2VzIiwicnVuTnBtIiwiYWxsIiwibGlua05wbVBhY2thZ2VzIiwicGx1Z2luc1ZlcnNpb25zIiwic2V0dGluZ3NKc29uIiwic2NhZmZvbGRQbHVnaW5zVmVyc2lvbiIsInBsdWdpbk5hbWUiLCJsdCIsImRlc2t0b3BEZXBlbmRlbmNpZXMiLCJjaGVja1BsdWdpbnNWZXJzaW9uIiwibWVyZ2VEZXBlbmRlbmNpZXMiLCJmcm9tU2V0dGluZ3MiLCJtb2R1bGVzIiwiZ2V0UmVtb3RlRGVwZW5kZW5jaWVzIiwibG9jYWxEZXBlbmRlbmNpZXMiLCJnZXRMb2NhbERlcGVuZGVuY2llcyIsIndyaXRlRmlsZVN5bmMiLCJhcmNoIiwidmFsdWVzIiwibGFzdFJlYnVpbGQiLCJlbGVjdHJvbkJ1aWxkZXIiLCJwcmVwYXJlTGFzdFJlYnVpbGRPYmplY3QiLCJmcmFtZXdvcmtJbmZvIiwicGxhdGZvcm0iLCJpbnN0YWxsZXIiLCJucG1FbnYiLCJpbnN0YWxsIiwiaW5zdGFsbE9yUmVidWlsZCIsInVuZGVmaW5lZCIsInBhY2thZ2VKc29uRmllbGRzIiwibmFtZSIsInByb2plY3ROYW1lIiwiZGVza3RvcFZlcnNpb24iLCJnZXRIYXNoVmVyc2lvbiIsIm1ldGVvckRlc2t0b3BWZXJzaW9uIiwiZGVza3RvcFRtcCIsImRlc2t0b3BBc2FyIiwidGhlbiIsImNhdGNoIiwidWdsaWZ5T3B0aW9ucyIsInVnbGlmeWluZ0VuYWJsZWQiLCJ1Z2xpZnkiLCJwcmVzZXQiLCJ0YXJnZXRzIiwibm9kZSIsImZpbGUiLCJwcmVzZXRzIiwibWluaWZ5IiwibWtkaXIiLCJjb25maWdzIiwiZ2F0aGVyTW9kdWxlQ29uZmlncyIsIm1vZHVsZUNvbmZpZyIsImZpbGVQYXRoIiwiZGlyTmFtZSIsImRlc3RpbmF0aW9uUGF0aCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOztBQUVBOztBQUNBOztBQUNBOztBQUNBOzs7Ozs7Ozs7Ozs7OztBQUVBLGlCQUFNQSxNQUFOLENBQWFDLEtBQWIsR0FBcUIsSUFBckI7QUFFQTs7Ozs7SUFJcUJDLFc7OztBQUNqQjs7OztBQUlBLHVCQUFZQyxDQUFaLEVBQWU7QUFBQTs7QUFDWCxTQUFLQyxHQUFMLEdBQVcsaUJBQVEsYUFBUixDQUFYO0FBQ0EsU0FBS0MsUUFBTCxHQUFnQixpQ0FBd0JGLENBQXhCLENBQWhCO0FBQ0EsU0FBS0csV0FBTCxHQUFtQixpQ0FDZkgsQ0FEZSxFQUVmLEtBQUtFLFFBQUwsQ0FBY0UscUJBQWQsR0FBc0NDLFlBRnZCLENBQW5CO0FBSUEsU0FBS0wsQ0FBTCxHQUFTQSxDQUFUO0FBQ0EsU0FBS00sU0FBTCxHQUFpQixLQUFLTixDQUFMLENBQU9NLFNBQXhCO0FBQ0EsU0FBS0MsV0FBTCxHQUFtQixJQUFuQjtBQUNBLFNBQUtDLE9BQUwsR0FBZSxJQUFmO0FBQ0EsU0FBS0Msb0JBQUwsR0FBNEIsSUFBNUI7QUFDSDtBQUVEOzs7Ozs7Ozs7eUNBS3dDO0FBQUE7O0FBQUEsVUFBckJDLGNBQXFCLHVFQUFKLEVBQUk7QUFDcEMsV0FBS1QsR0FBTCxDQUFTVSxJQUFULENBQWMsdURBQWQ7QUFDQSxhQUFPLElBQUlDLE9BQUosQ0FBWSxVQUFDQyxPQUFELEVBQWE7QUFDNUIsWUFBTUMsVUFBVSxNQUFLQyxtQkFBTCxFQUFoQixDQUQ0QixDQUc1QjtBQUNBOzs7QUFDQSxjQUFLZCxHQUFMLENBQVNlLEtBQVQsQ0FBZSxnQ0FBZjs7QUFFQSxvQkFBR0MsVUFBSCxDQUNJLE1BQUtqQixDQUFMLENBQU9rQixHQUFQLENBQVdDLEtBQVgsQ0FBaUJDLFdBQWpCLENBQTZCQyxXQURqQyxFQUVJLGNBQUtDLElBQUwsQ0FBVSxNQUFLdEIsQ0FBTCxDQUFPa0IsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxXQUFqQixDQUE2QkcsT0FBdkMsRUFBZ0QsY0FBaEQsQ0FGSjs7QUFLQSxZQUFJQyxZQUFZLEtBQWhCO0FBQ0FBLG9CQUFZLE1BQUtDLGNBQUwsQ0FBb0JYLE9BQXBCLENBQVo7O0FBRUEsY0FBS2IsR0FBTCxDQUFTZSxLQUFULENBQWUsU0FBZjs7QUFDQSxzQkFBS1UsYUFBTCxDQUNJLE1BQUsxQixDQUFMLENBQU9rQixHQUFQLENBQVdDLEtBQVgsQ0FBaUJDLFdBQWpCLENBQTZCRyxPQURqQyxFQUVJLE1BQUt2QixDQUFMLENBQU9rQixHQUFQLENBQVdDLEtBQVgsQ0FBaUJDLFdBQWpCLENBQTZCTyxPQUZqQyxFQUdJLFlBQU07QUFDRjtBQUNBLGdCQUFLMUIsR0FBTCxDQUFTZSxLQUFULENBQWUsdUNBQWY7O0FBRUEsMkJBQU1ZLEVBQU4sQ0FDSSxjQUFLTixJQUFMLENBQVUsTUFBS3RCLENBQUwsQ0FBT2tCLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkJHLE9BQXZDLEVBQWdELGNBQWhELENBREosRUFFSSxNQUFLdkIsQ0FBTCxDQUFPa0IsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxXQUFqQixDQUE2QkMsV0FGakM7O0FBS0EsY0FBSUcsU0FBSixFQUFlO0FBQ1g7QUFDQTtBQUNBVixvQkFBUWUsT0FBUixDQUFnQjtBQUFBLHFCQUFVLGlCQUFNQyxFQUFOLENBQ3RCLEtBRHNCLEVBRXRCLGNBQUtSLElBQUwsQ0FBVSxNQUFLdEIsQ0FBTCxDQUFPa0IsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxXQUFqQixDQUE2Qlcsb0JBQXZDLEVBQTZEQyxNQUE3RCxDQUZzQixFQUd0QixjQUFLVixJQUFMLENBQVUsTUFBS3RCLENBQUwsQ0FBT2tCLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkJDLFdBQXZDLEVBQW9EVyxNQUFwRCxDQUhzQixDQUFWO0FBQUEsYUFBaEIsRUFIVyxDQVNYOztBQUNBLGdCQUFJLE1BQUtoQyxDQUFMLENBQU9pQyxLQUFQLENBQWFDLE1BQWIsQ0FDQSxNQUFLbEMsQ0FBTCxDQUFPa0IsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxXQUFqQixDQUE2QmUsdUJBRDdCLENBQUosRUFFRztBQUNDLCtCQUFNTCxFQUFOLENBQ0ksY0FBS1IsSUFBTCxDQUFVLE1BQUt0QixDQUFMLENBQU9rQixHQUFQLENBQVdDLEtBQVgsQ0FBaUJDLFdBQWpCLENBQTZCZSx1QkFBdkMsRUFBZ0UsR0FBaEUsQ0FESixFQUVJLGNBQUtiLElBQUwsQ0FBVSxNQUFLdEIsQ0FBTCxDQUFPa0IsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxXQUFqQixDQUE2QkMsV0FBdkMsRUFBb0QsTUFBcEQsQ0FGSjtBQUlIO0FBQ0o7O0FBRUQsZ0JBQUtwQixHQUFMLENBQVNlLEtBQVQsQ0FBZSx1QkFBZjs7QUFDQSxjQUFNb0IsVUFBVSxDQUFDLE1BQUtwQyxDQUFMLENBQU9rQixHQUFQLENBQVdDLEtBQVgsQ0FBaUJDLFdBQWpCLENBQTZCQyxXQUE5QixFQUEyQ2dCLE1BQTNDLENBQ1osQ0FDSSxNQUFLckMsQ0FBTCxDQUFPa0IsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxXQUFqQixDQUE2Qk8sT0FEakMsRUFFSSxNQUFLM0IsQ0FBTCxDQUFPa0IsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxXQUFqQixDQUE2QmIsV0FGakMsQ0FEWSxFQUtaRyxjQUxZLENBQWhCOztBQVFBLHVCQUFJNEIsSUFBSixDQUNJLENBQUUsR0FBRSxNQUFLdEMsQ0FBTCxDQUFPa0IsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxXQUFqQixDQUE2Qm1CLElBQUssR0FBRSxjQUFLQyxHQUFJLEdBQWpELEVBQXFESCxNQUFyRCxDQUNJRCxRQUFRSyxHQUFSLENBQVk7QUFBQSxtQkFBa0IsSUFBR0MsYUFBYyxFQUFuQztBQUFBLFdBQVosQ0FESixDQURKOztBQUtBN0I7QUFDSCxTQS9DTDtBQWlESCxPQWpFTSxDQUFQO0FBa0VIO0FBRUQ7Ozs7Ozs7O21DQUtlQyxPLEVBQVM7QUFBQTs7QUFDcEIsVUFBTTZCLE1BQU0sQ0FBQyxLQUFELEVBQVEsTUFBUixFQUFnQixLQUFoQixFQUF1QixNQUF2QixFQUErQixFQUEvQixDQUFaOztBQUVBLFVBQUk3QixRQUFROEIsTUFBUixHQUFpQixDQUFyQixFQUF3QjtBQUNwQixZQUFJLEtBQUs1QyxDQUFMLENBQU9pQyxLQUFQLENBQWFDLE1BQWIsQ0FBb0IsS0FBS2xDLENBQUwsQ0FBT2tCLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkJXLG9CQUFqRCxDQUFKLEVBQTRFO0FBQ3hFLDJCQUFNYyxFQUFOLENBQVMsS0FBVCxFQUFnQixLQUFLN0MsQ0FBTCxDQUFPa0IsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxXQUFqQixDQUE2Qlcsb0JBQTdDO0FBQ0g7O0FBQ0Qsb0JBQUdlLFNBQUgsQ0FBYSxLQUFLOUMsQ0FBTCxDQUFPa0IsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxXQUFqQixDQUE2Qlcsb0JBQTFDOztBQUNBLG9CQUFHZSxTQUFILENBQWEsS0FBSzlDLENBQUwsQ0FBT2tCLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkJlLHVCQUExQzs7QUFFQXJCLGdCQUFRZSxPQUFSLENBQWdCLFVBQUNHLE1BQUQsRUFBWTtBQUN4QixzQkFBR2YsVUFBSCxDQUNJLGNBQUtLLElBQUwsQ0FBVSxPQUFLdEIsQ0FBTCxDQUFPa0IsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxXQUFqQixDQUE2QkcsT0FBdkMsRUFBZ0QsY0FBaEQsRUFBZ0VTLE1BQWhFLENBREosRUFFSSxjQUFLVixJQUFMLENBQVUsT0FBS3RCLENBQUwsQ0FBT2tCLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkJXLG9CQUF2QyxFQUE2REMsTUFBN0QsQ0FGSixFQUR3QixDQUt4Qjs7O0FBQ0EsaUJBQUtlLFVBQUwsQ0FBZ0JmLE1BQWhCLEVBQXdCVyxHQUF4QjtBQUNILFNBUEQ7QUFTQSxlQUFPLElBQVA7QUFDSDs7QUFDRCxhQUFPLEtBQVA7QUFDSDtBQUVEOzs7Ozs7Ozs7K0JBTVdYLE0sRUFBUVcsRyxFQUFLO0FBQUE7O0FBQ3BCLFVBQUlwQyxXQUFKOztBQUNBLFVBQUk7QUFDQUEsc0JBQWN5QyxLQUFLQyxLQUFMLENBQ1YsWUFBR0MsWUFBSCxDQUNJLGNBQUs1QixJQUFMLENBQ0ksS0FBS3RCLENBQUwsQ0FBT2tCLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkJXLG9CQURqQyxFQUN1REMsTUFEdkQsRUFDK0QsY0FEL0QsQ0FESixFQUlJLE1BSkosQ0FEVSxDQUFkO0FBUUgsT0FURCxDQVNFLE9BQU9tQixDQUFQLEVBQVU7QUFDUjVDLHNCQUFjLEVBQWQ7QUFDSDs7QUFHRCxVQUFNNkMsT0FBUSxTQUFTN0MsV0FBVCxJQUF3QixPQUFPQSxZQUFZOEMsR0FBbkIsS0FBMkIsUUFBcEQsR0FBZ0VDLE9BQU9DLElBQVAsQ0FBWWhELFlBQVk4QyxHQUF4QixDQUFoRSxHQUErRixFQUE1Rzs7QUFFQSxVQUFJRCxLQUFLUixNQUFMLEdBQWMsQ0FBbEIsRUFBcUI7QUFDakJRLGFBQUt2QixPQUFMLENBQWEsVUFBQ3dCLEdBQUQsRUFBUztBQUNsQlYsY0FBSWQsT0FBSixDQUFZLFVBQUMyQixTQUFELEVBQWU7QUFDdkIsZ0JBQU1DLGNBQWMsY0FBS25DLElBQUwsQ0FDaEIsT0FBS3RCLENBQUwsQ0FBT2tCLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkJHLE9BRGIsRUFFaEIsY0FGZ0IsRUFHaEIsTUFIZ0IsRUFJZixHQUFFOEIsR0FBSSxHQUFFRyxTQUFVLEVBSkgsQ0FBcEI7O0FBTUEsZ0JBQUksT0FBS3hELENBQUwsQ0FBT2lDLEtBQVAsQ0FBYUMsTUFBYixDQUFvQnVCLFdBQXBCLEtBQ0EsT0FBS3pELENBQUwsQ0FBT2lDLEtBQVAsQ0FBYXlCLGFBQWIsQ0FBMkJELFdBQTNCLENBREosRUFFRTtBQUNFLDBCQUFHeEMsVUFBSCxDQUNJd0MsV0FESixFQUVJLGNBQUtuQyxJQUFMLENBQ0ksT0FBS3RCLENBQUwsQ0FBT2tCLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkJlLHVCQURqQyxFQUVLLEdBQUVrQixHQUFJLEdBQUVHLFNBQVUsRUFGdkIsQ0FGSjtBQVFIO0FBQ0osV0FuQkQ7QUFvQkgsU0FyQkQ7QUFzQkg7QUFDSjtBQUVEOzs7Ozs7MENBR3NCO0FBQ2xCLFVBQU1HLHdCQUNGLG1DQUF5QixLQUFLM0QsQ0FBTCxDQUFPa0IsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxXQUFqQixDQUE2QkMsV0FBdEQsQ0FESjtBQUVBLFVBQU11QyxnQkFBZ0JELHNCQUFzQkUsTUFBdEIsRUFBdEI7O0FBSGtCLGtDQUtBLEtBQUs3RCxDQUFMLENBQU84RCxPQUFQLENBQWVDLFdBQWYsRUFMQTtBQUFBLFVBS1pqRCxPQUxZLHlCQUtaQSxPQUxZOztBQU9sQixVQUFJLENBQUNrRCxNQUFNQyxPQUFOLENBQWNuRCxPQUFkLENBQUwsRUFBNkI7QUFDekJBLGtCQUFVLEVBQVY7QUFDSDs7QUFFRCxVQUFNb0QsUUFBUSxFQUFkO0FBQ0FOLG9CQUFjdkIsTUFBZCxDQUFxQnZCLE9BQXJCLEVBQThCZSxPQUE5QixDQUFzQyxrQkFBVTtBQUM1Q3FDLGNBQU1sQyxNQUFOLElBQWdCLElBQWhCO0FBQ0gsT0FGRDtBQUdBbEIsZ0JBQVV3QyxPQUFPQyxJQUFQLENBQVlXLEtBQVosQ0FBVjs7QUFDQSxVQUFJcEQsUUFBUThCLE1BQVIsR0FBaUIsQ0FBckIsRUFBd0I7QUFDcEIsYUFBSzNDLEdBQUwsQ0FBU2tFLE9BQVQsQ0FBa0IseUNBQXdDckQsUUFBUVEsSUFBUixDQUFhLElBQWIsQ0FBbUIsRUFBN0U7QUFDSDs7QUFDRCxhQUFPUixPQUFQO0FBQ0g7QUFFRDs7Ozs7O29EQUdnQztBQUFBOztBQUM1QixXQUFLYixHQUFMLENBQVNrRSxPQUFULENBQWlCLG1DQUFqQjtBQUNBLFVBQU1DLFdBQVcsS0FBS3BFLENBQUwsQ0FBTzhELE9BQVAsQ0FBZUMsV0FBZixFQUFqQjs7QUFFQSxVQUFLLG9DQUFvQ0ssUUFBekMsRUFBb0Q7QUFDaEQsYUFBSzNELG9CQUFMLEdBQTZCLEdBQUUyRCxTQUFTQyw4QkFBK0IsRUFBdkU7QUFDQSxhQUFLcEUsR0FBTCxDQUFTcUUsSUFBVCxDQUFlLHVDQUFzQyxLQUFLN0Qsb0JBQXFCLEVBQS9FO0FBQ0E7QUFDSDs7QUFFRCxVQUFNOEQsTUFBTSxnQkFBT0MsVUFBUCxDQUFrQixLQUFsQixDQUFaOztBQUNBLFVBQUluRSxlQUFlaUQsT0FBT0MsSUFBUCxDQUFZLEtBQUtwRCxXQUFMLENBQWlCc0UsZUFBakIsRUFBWixFQUFnREMsSUFBaEQsRUFBbkI7QUFDQXJFLHFCQUFlQSxhQUFhb0MsR0FBYixDQUFpQjtBQUFBLGVBQzNCLEdBQUVrQyxVQUFXLElBQUcsT0FBS3BFLFdBQUwsQ0FBaUJGLFlBQWpCLENBQThCc0UsVUFBOUIsQ0FBMEMsRUFEL0I7QUFBQSxPQUFqQixDQUFmO0FBRUEsVUFBTUMsMkJBQTJCLEtBQUs1RSxDQUFMLENBQU82RSxVQUFQLEdBQW9CQyxLQUFwQixDQUEwQixHQUExQixDQUFqQztBQUNBLFdBQUs3RSxHQUFMLENBQVNlLEtBQVQsQ0FBZSwwQ0FBZixFQUNLLEdBQUU0RCx5QkFBeUIsQ0FBekIsQ0FBNEIsSUFBR0EseUJBQXlCLENBQXpCLENBQTRCLEVBRGxFO0FBRUF2RSxtQkFBYTBFLElBQWIsQ0FDSyxrQkFBaUJILHlCQUF5QixDQUF6QixDQUE0QixJQUFHQSx5QkFBeUIsQ0FBekIsQ0FBNEIsRUFEakY7QUFJQSxVQUFNSSw4QkFBOEJaLFNBQVM1RCxPQUFULENBQWlCc0UsS0FBakIsQ0FBdUIsR0FBdkIsRUFBNEIsQ0FBNUIsQ0FBcEM7QUFDQSxXQUFLN0UsR0FBTCxDQUFTZSxLQUFULENBQWUsb0NBQWYsRUFBcURnRSwyQkFBckQ7QUFDQTNFLG1CQUFhMEUsSUFBYixDQUNLLGVBQWNDLDJCQUE0QixFQUQvQzs7QUFJQSxVQUFJQyxRQUFRL0QsR0FBUixDQUFZZ0Usa0RBQVosSUFDQUQsUUFBUS9ELEdBQVIsQ0FBWWlFLG9CQURoQixFQUVFO0FBQ0UsYUFBS2xGLEdBQUwsQ0FBU2UsS0FBVCxDQUFnQix5Q0FBd0NnQyxLQUFLb0MsU0FBTCxDQUFlL0UsWUFBZixDQUE2QixFQUFyRjtBQUNIOztBQUVEa0UsVUFBSWMsTUFBSixDQUFXckMsS0FBS29DLFNBQUwsQ0FBZS9FLFlBQWYsQ0FBWDtBQUVBLFdBQUtJLG9CQUFMLEdBQTRCOEQsSUFBSWUsTUFBSixDQUFXLEtBQVgsQ0FBNUI7QUFDSDtBQUVEOzs7Ozs7Ozs7Ozs7Ozs7OztBQUdZQyxtQiwyREFBTSxLO0FBQ2Q7QUFDQSxxQkFBS3RGLEdBQUwsQ0FBU1UsSUFBVCxDQUFjLGFBQWQ7O0FBRUEsb0JBQUksQ0FBQyxLQUFLWCxDQUFMLENBQU84RCxPQUFQLENBQWUwQixLQUFmLEVBQUwsRUFBNkI7QUFDekIsc0JBQUksQ0FBQyxLQUFLeEYsQ0FBTCxDQUFPa0IsR0FBUCxDQUFXdUUsT0FBWCxDQUFtQnZGLFFBQXhCLEVBQWtDO0FBQzlCLHlCQUFLRCxHQUFMLENBQVN5RixLQUFULENBQWUsdUVBQ1gsK0RBREosRUFEOEIsQ0FHOUI7O0FBQ0FULDRCQUFRVSxJQUFSLENBQWEsQ0FBYjtBQUNILG1CQUxELE1BS087QUFDSCx5QkFBSzNGLENBQUwsQ0FBTzhELE9BQVAsQ0FBZTVELFFBQWY7QUFDQSx5QkFBS0YsQ0FBTCxDQUFPTSxTQUFQLENBQWlCc0YsZUFBakI7QUFDSDtBQUNKOztBQUVELG9CQUFJO0FBQ0EsdUJBQUs1RixDQUFMLENBQU9NLFNBQVAsQ0FBaUJzRixlQUFqQjtBQUNILGlCQUZELENBRUUsT0FBT3pDLENBQVAsRUFBVTtBQUNSLHVCQUFLbEQsR0FBTCxDQUFTcUUsSUFBVCxDQUFlLCtCQUE4QixLQUFLdEUsQ0FBTCxDQUFPa0IsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxXQUFqQixDQUE2QnlFLFFBQVMsRUFBckUsR0FDVixpQkFESixFQUN1QjFDLENBRHZCO0FBRUg7Ozs7dUJBR1MsS0FBS25ELENBQUwsQ0FBT00sU0FBUCxDQUFpQndGLHdCQUFqQixFOzs7Ozs7Ozs7QUFFTixxQkFBSzdGLEdBQUwsQ0FBU3lGLEtBQVQsQ0FBZSw4Q0FBZjtBQUNBVCx3QkFBUVUsSUFBUixDQUFhLENBQWI7Ozs7O3VCQUlNLEtBQUt6RixRQUFMLENBQWM2RixJQUFkLEU7Ozs7Ozs7OztBQUVOLHFCQUFLOUYsR0FBTCxDQUFTeUYsS0FBVCxDQUFlLDJCQUFmO0FBQ0FULHdCQUFRVSxJQUFSLENBQWEsQ0FBYjs7O0FBR0osb0JBQUk7QUFDQSx1QkFBS0ssdUJBQUw7QUFDSCxpQkFGRCxDQUVFLE9BQU83QyxDQUFQLEVBQVU7QUFDUix1QkFBS2xELEdBQUwsQ0FBU3lGLEtBQVQsQ0FBZSxxQ0FBZixFQUFzRHZDLENBQXREO0FBQ0g7O0FBRUQsb0JBQUk7QUFDQSx1QkFBSzhDLHNCQUFMO0FBQ0gsaUJBRkQsQ0FFRSxPQUFPOUMsQ0FBUCxFQUFVO0FBQ1IsdUJBQUtsRCxHQUFMLENBQVN5RixLQUFULENBQWUseUNBQWYsRUFBMER2QyxDQUExRDtBQUNIOztBQUVELG9CQUFJO0FBQ0EsdUJBQUsrQyw2QkFBTDtBQUNILGlCQUZELENBRUUsT0FBTy9DLENBQVAsRUFBVTtBQUNSLHVCQUFLbEQsR0FBTCxDQUFTeUYsS0FBVCxDQUFlLGlEQUFmLEVBQWtFdkMsQ0FBbEU7QUFDQThCLDBCQUFRVSxJQUFSLENBQWEsQ0FBYjtBQUNIOzs7O3VCQUdTLEtBQUtRLDBCQUFMLEU7Ozs7Ozs7OztBQUVOLHFCQUFLbEcsR0FBTCxDQUFTeUYsS0FBVCxDQUFlLHdEQUFmO0FBQ0FULHdCQUFRVSxJQUFSLENBQWEsQ0FBYjs7Ozs7dUJBSzJCLEtBQUtTLHdCQUFMLEU7OztBQUEzQkMsa0M7Ozs7Ozs7QUFFQSxxQkFBS3BHLEdBQUwsQ0FBU3lGLEtBQVQsQ0FBZSw4Q0FBZjtBQUNBVCx3QkFBUVUsSUFBUixDQUFhLENBQWI7Ozs7O3VCQUlNLEtBQUtXLFdBQUwsQ0FBaUIsSUFBakIsQzs7Ozs7Ozs7O0FBRU4scUJBQUtyRyxHQUFMLENBQVN5RixLQUFULENBQWUsZ0RBQWY7QUFDQVQsd0JBQVFVLElBQVIsQ0FBYSxDQUFiOzs7b0JBR0NVLGtCOzs7Ozs7O3VCQUVTLEtBQUtDLFdBQUwsRTs7Ozs7Ozs7O0FBRU4scUJBQUtyRyxHQUFMLENBQVN5RixLQUFULENBQWUsdURBQWY7QUFDQVQsd0JBQVFVLElBQVIsQ0FBYSxDQUFiOzs7Ozt1QkFLRSxLQUFLWSx1QkFBTCxFOzs7Ozs7Ozs7QUFFTixxQkFBS3RHLEdBQUwsQ0FBU3lGLEtBQVQsQ0FBZSxzREFBZjtBQUNBVCx3QkFBUVUsSUFBUixDQUFhLENBQWI7Ozs7O3VCQUtNLEtBQUthLHdCQUFMLEU7Ozs7Ozs7OztBQUVOLHFCQUFLdkcsR0FBTCxDQUFTeUYsS0FBVCxDQUFlLG1FQUFmO0FBQ0FULHdCQUFRVSxJQUFSLENBQWEsQ0FBYjs7O3FCQUlBLEtBQUszRixDQUFMLENBQU9rQixHQUFQLENBQVd1RixpQkFBWCxFOzs7Ozs7O3VCQUVVLEtBQUtDLGtCQUFMLEU7Ozs7Ozs7OztBQUVOLHFCQUFLekcsR0FBTCxDQUFTeUYsS0FBVCxDQUFlLHdDQUFmO0FBQ0FULHdCQUFRVSxJQUFSLENBQWEsQ0FBYjs7O0FBSVI7QUFDQSxvQkFBSTtBQUNBLHVCQUFLZ0Isd0JBQUw7QUFDSCxpQkFGRCxDQUVFLE9BQU94RCxDQUFQLEVBQVU7QUFDUix1QkFBS2xELEdBQUwsQ0FBU3lGLEtBQVQsQ0FBZSx3REFBZixFQUF5RXZDLENBQXpFO0FBQ0E4QiwwQkFBUVUsSUFBUixDQUFhLENBQWI7QUFDSDs7QUFFRCxvQkFBSTtBQUNBLHVCQUFLaUIsd0JBQUw7QUFDSCxpQkFGRCxDQUVFLE9BQU96RCxDQUFQLEVBQVU7QUFDUix1QkFBS2xELEdBQUwsQ0FBU3lGLEtBQVQsQ0FBZSxzQ0FBZixFQUF1RHZDLENBQXZEO0FBQ0E4QiwwQkFBUVUsSUFBUixDQUFhLENBQWI7QUFDSDs7Ozt1QkFHUyxLQUFLa0IsdUJBQUwsRTs7Ozs7Ozs7O0FBRU4scUJBQUs1RyxHQUFMLENBQVN5RixLQUFULENBQWUsb0RBQWY7QUFDQVQsd0JBQVFVLElBQVIsQ0FBYSxDQUFiOzs7QUFHSixvQkFBSTtBQUNBLHVCQUFLbUIsa0JBQUw7QUFDSCxpQkFGRCxDQUVFLE9BQU8zRCxDQUFQLEVBQVU7QUFDUix1QkFBS2xELEdBQUwsQ0FBU3lGLEtBQVQsQ0FBZSx3Q0FBZixFQUF5RHZDLENBQXpEO0FBQ0g7Ozs7dUJBR1MsS0FBSzRELGlCQUFMLEU7Ozs7Ozs7OztBQUVOLHFCQUFLOUcsR0FBTCxDQUFTeUYsS0FBVCxDQUFlLGlEQUFmO0FBQ0FULHdCQUFRVSxJQUFSLENBQWEsQ0FBYjs7Ozs7dUJBSU0sS0FBS3FCLG9CQUFMLEU7Ozs7Ozs7OztBQUVOLHFCQUFLL0csR0FBTCxDQUFTeUYsS0FBVCxDQUFlLHFEQUFmOzs7QUFHSixvQkFBSUgsR0FBSixFQUFTO0FBQ0wsdUJBQUt0RixHQUFMLENBQVNVLElBQVQsQ0FBYyxTQUFkO0FBQ0EsdUJBQUtYLENBQUwsQ0FBT2lILFFBQVAsQ0FBZ0IxQixHQUFoQjtBQUNILGlCQUhELE1BR087QUFDSCx1QkFBS3RGLEdBQUwsQ0FBU1UsSUFBVCxDQUFjLE9BQWQ7QUFDSDs7Ozs7Ozs7Ozs7Ozs7QUFHTDs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBS1V1Ryx3QixHQUFXLEU7QUFDWEMsbUMsR0FBc0IsRTtBQUN4QkMsdUIsR0FBVSxXO0FBRWQ5RCx1QkFBT0MsSUFBUCxDQUFZLEtBQUt2RCxDQUFMLENBQU84RCxPQUFQLENBQWVXLGVBQWYsR0FBaUMyQyxPQUE3QyxFQUFzRHZGLE9BQXRELENBQThELFVBQUN3RixNQUFELEVBQVk7QUFDdEU7QUFDQSxzQkFBTTlHLGNBQ0Z5QyxLQUFLQyxLQUFMLENBQ0ksWUFBR0MsWUFBSCxDQUNJLGNBQUs1QixJQUFMLENBQ0ksT0FBS3RCLENBQUwsQ0FBT2tCLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkJDLFdBRGpDLEVBQzhDZ0csTUFEOUMsRUFDc0QsY0FEdEQsQ0FESixFQUlJLE1BSkosQ0FESixDQURKOztBQVVBLHNCQUFJLHdCQUF3QjlHLFdBQXhCLElBQXVDLE9BQU9BLFlBQVkrRyxrQkFBbkIsS0FBMEMsUUFBckYsRUFBK0Y7QUFDM0ZGLCtCQUFZLEdBQUVDLE1BQU8sSUFBckI7QUFDQUgsNkJBQVNLLE9BQVQsb0NBQW9CakUsT0FBT0MsSUFBUCxDQUFZaEQsWUFBWStHLGtCQUF4QixDQUFwQjtBQUNBSCx3Q0FBb0JJLE9BQXBCLCtDQUErQkwsU0FBU3pFLEdBQVQsQ0FBYSxVQUFDK0UsV0FBRCxFQUFpQjtBQUN6RCwwQkFBSWpILFlBQVkrRyxrQkFBWixDQUErQkUsV0FBL0IsTUFBZ0QsVUFBcEQsRUFBZ0U7QUFDNUQsK0JBQVEsR0FBRUEsV0FBWSxJQUFHakgsWUFBWUMsT0FBUSxFQUE3QztBQUNIOztBQUNELDZCQUFRLEdBQUVnSCxXQUFZLElBQUdqSCxZQUFZK0csa0JBQVosQ0FBK0JFLFdBQS9CLENBQTRDLEVBQXJFO0FBQ0gscUJBTDhCLENBQS9CO0FBTUg7QUFDSixpQkF0QkQ7O3NCQXdCSU4sU0FBU3RFLE1BQVQsR0FBa0IsQzs7Ozs7QUFDbEJ3RSwwQkFBVyxHQUFFQSxRQUFRSyxNQUFSLENBQWUsQ0FBZixFQUFrQkwsUUFBUXhFLE1BQVIsR0FBaUIsQ0FBbkMsQ0FBc0MsR0FBbkQ7Ozt1QkFFVSxLQUFLNUMsQ0FBTCxDQUFPTSxTQUFQLENBQWlCb0gsYUFBakIsQ0FBK0JDLGNBQS9CLENBQ0ZULFFBREUsRUFDUUMsbUJBRFIsRUFDNkJDLE9BRDdCLEM7Ozs7Ozs7OztzQkFJQSxJQUFJUSxLQUFKLGM7Ozs7Ozs7Ozs7Ozs7O0FBS2xCOzs7Ozs7Ozs7Ozs7Ozs7dUJBSVUsS0FBSzVILENBQUwsQ0FBT00sU0FBUCxDQUFpQnVILEtBQWpCLEU7Ozs7Ozs7Ozs7Ozs7O0FBR1Y7Ozs7Ozs7Ozs7Ozs7OztzQkFLUSxLQUFLN0gsQ0FBTCxDQUFPa0IsR0FBUCxDQUFXdUYsaUJBQVgsTUFBa0MsS0FBS3pHLENBQUwsQ0FBT2tCLEdBQVAsQ0FBV3VFLE9BQVgsQ0FBbUJxQyxJOzs7OztBQUNyRCxvQkFBSSxDQUFDLEtBQUs5SCxDQUFMLENBQU9rQixHQUFQLENBQVd1RixpQkFBWCxFQUFMLEVBQXFDO0FBQ2pDLHVCQUFLeEcsR0FBTCxDQUFTVSxJQUFULENBQWMseUVBQWQ7QUFDSCxpQkFGRCxNQUVPO0FBQ0gsdUJBQUtWLEdBQUwsQ0FBU1UsSUFBVCxDQUFjLDBEQUFkO0FBQ0g7Ozs7dUJBRVMsS0FBS1gsQ0FBTCxDQUFPaUMsS0FBUCxDQUFhOEYsYUFBYixDQUNGLEtBREUsRUFDSyxLQUFLL0gsQ0FBTCxDQUFPa0IsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxXQUFqQixDQUE2QkMsV0FEbEMsQzs7Ozs7Ozs7O3NCQUlBLElBQUl1RyxLQUFKLGM7OztrREFFSCxJOzs7a0RBRUosSzs7Ozs7Ozs7Ozs7Ozs7QUFHWDs7Ozs7Ozs7Ozs7Ozs7OztxQkFNUSxLQUFLNUgsQ0FBTCxDQUFPaUMsS0FBUCxDQUFhQyxNQUFiLENBQW9CLEtBQUtsQyxDQUFMLENBQU9rQixHQUFQLENBQVdDLEtBQVgsQ0FBaUJDLFdBQWpCLENBQTZCNEcsY0FBakQsQzs7Ozs7b0JBQ0ssS0FBS2hJLENBQUwsQ0FBT2lDLEtBQVAsQ0FBYUMsTUFBYixDQUFvQixLQUFLbEMsQ0FBTCxDQUFPa0IsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxXQUFqQixDQUE2QkMsV0FBakQsQzs7Ozs7QUFDRCxxQkFBS3BCLEdBQUwsQ0FBU2UsS0FBVCxDQUFlLCtCQUFmOztBQUNBLGlDQUFNWSxFQUFOLENBQ0ksS0FBSzVCLENBQUwsQ0FBT2tCLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkI0RyxjQURqQyxFQUVJLEtBQUtoSSxDQUFMLENBQU9rQixHQUFQLENBQVdDLEtBQVgsQ0FBaUJDLFdBQWpCLENBQTZCQyxXQUZqQzs7Ozs7O0FBS0E7QUFDQSxxQkFBS3BCLEdBQUwsQ0FBU2UsS0FBVCxDQUFlLCtEQUFmOzs7dUJBRVUsS0FBS2hCLENBQUwsQ0FBT2lDLEtBQVAsQ0FBYThGLGFBQWIsQ0FDRixLQURFLEVBQ0ssS0FBSy9ILENBQUwsQ0FBT2tCLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkI0RyxjQURsQyxDOzs7Ozs7Ozs7c0JBSUEsSUFBSUosS0FBSixjOzs7Ozs7Ozs7Ozs7OztBQU10Qjs7Ozs7Ozs7Ozs7MkJBUU9LLFEsRUFBNEI7QUFBQTs7QUFBQSxVQUFsQkMsS0FBa0IsdUVBQVYsUUFBVTtBQUMvQixhQUFPLElBQUl0SCxPQUFKLENBQVksVUFBQ0MsT0FBRCxFQUFVc0gsTUFBVixFQUFxQjtBQUNwQztBQUNBLFlBQUlDLE1BQU0sY0FBSzlHLElBQUwsQ0FDTixPQUFLdEIsQ0FBTCxDQUFPa0IsR0FBUCxDQUFXQyxLQUFYLENBQWlCYixTQUFqQixDQUEyQmlDLElBRHJCLEVBQzJCLGNBRDNCLEVBQzJDLE1BRDNDLEVBQ21ELEtBRG5ELENBQVY7O0FBSUEsWUFBSSxDQUFDLE9BQUt2QyxDQUFMLENBQU9pQyxLQUFQLENBQWFDLE1BQWIsQ0FBb0JrRyxHQUFwQixDQUFMLEVBQStCO0FBQzNCQSxnQkFBTSxjQUFLOUcsSUFBTCxDQUNGLE9BQUt0QixDQUFMLENBQU9rQixHQUFQLENBQVdDLEtBQVgsQ0FBaUJiLFNBQWpCLENBQTJCaUMsSUFEekIsRUFDK0IsY0FEL0IsRUFDK0MsZ0JBRC9DLEVBQ2lFLGNBRGpFLEVBQ2lGLE1BRGpGLEVBQ3lGLEtBRHpGLENBQU47QUFHSDs7QUFFRCxlQUFLdEMsR0FBTCxDQUFTa0UsT0FBVCxDQUFrQixpQkFBZ0I4RCxTQUFTM0csSUFBVCxDQUFjLEdBQWQsQ0FBbUIsRUFBckQ7O0FBRUEsaUNBQU04RyxHQUFOLEVBQVdILFFBQVgsRUFBcUI7QUFDakJJLGVBQUssT0FBS3JJLENBQUwsQ0FBT2tCLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkJtQixJQURqQjtBQUVqQjJGO0FBRmlCLFNBQXJCLEVBR0dJLEVBSEgsQ0FHTSxNQUhOLEVBR2M7QUFBQSxpQkFDVEMsU0FBUyxDQUFWLEdBQWUxSCxTQUFmLEdBQTJCc0gsT0FBTyxJQUFJUCxLQUFKLENBQVcscUJBQW9CVyxJQUFLLEVBQXBDLENBQVAsQ0FEakI7QUFBQSxTQUhkO0FBTUgsT0FwQk0sQ0FBUDtBQXFCSDtBQUdEOzs7Ozs7Ozs7Ozs7Ozs7OztBQUlVbkUsd0IsR0FBVyxLQUFLcEUsQ0FBTCxDQUFPOEQsT0FBUCxDQUFlQyxXQUFmLEU7QUFDWHlFLHdCLEdBQVcsRTs7QUFDakIsb0JBQUksa0JBQWtCLEtBQUt4SSxDQUFMLENBQU84RCxPQUFQLENBQWVDLFdBQWYsRUFBdEIsRUFBb0Q7QUFDaEQsc0JBQUlDLE1BQU1DLE9BQU4sQ0FBY0csU0FBU3FFLFlBQXZCLENBQUosRUFBMEM7QUFDdENyRSw2QkFBU3FFLFlBQVQsQ0FBc0I1RyxPQUF0QixDQUE4QjtBQUFBLDZCQUMxQjJHLFNBQVN6RCxJQUFULENBQWMsT0FBSzJELE1BQUwsQ0FBWSxDQUFDLE1BQUQsRUFBU2xCLFdBQVQsQ0FBWixDQUFkLENBRDBCO0FBQUEscUJBQTlCO0FBRUg7QUFDSjs7O3VCQUNLNUcsUUFBUStILEdBQVIsQ0FBWUgsUUFBWixDOzs7Ozs7Ozs7Ozs7OztBQUdWOzs7Ozs7Ozs7Ozs7Ozs7O3VCQUtVLEtBQUtJLGVBQUwsRTs7O0FBRU4scUJBQUszSSxHQUFMLENBQVNVLElBQVQsQ0FBYyx5QkFBZDs7cUJBQ0ksS0FBS1gsQ0FBTCxDQUFPaUMsS0FBUCxDQUFhQyxNQUFiLENBQW9CLEtBQUtsQyxDQUFMLENBQU9rQixHQUFQLENBQVdDLEtBQVgsQ0FBaUJDLFdBQWpCLENBQTZCQyxXQUFqRCxDOzs7OztBQUNBLHFCQUFLcEIsR0FBTCxDQUFTZSxLQUFULENBQWUsaURBQWY7Ozt1QkFFVSxLQUFLMEgsTUFBTCxDQUFZLENBQUMsT0FBRCxDQUFaLEM7Ozs7Ozs7OztzQkFFQSxJQUFJZCxLQUFKLGM7Ozs7O3VCQUlKLEtBQUtjLE1BQUwsQ0FBWSxDQUFDLFNBQUQsQ0FBWixFQUF5QixLQUFLMUksQ0FBTCxDQUFPa0IsR0FBUCxDQUFXZ0gsS0FBcEMsQzs7Ozs7Ozs7O3NCQUVBLElBQUlOLEtBQUosYzs7Ozs7Ozs7Ozs7Ozs7QUFJZDs7Ozs7Ozt3Q0FJb0JpQixlLEVBQWlCO0FBQUE7O0FBQ2pDLFVBQU1DLGVBQWU5RixLQUFLQyxLQUFMLENBQ2pCLFlBQUdDLFlBQUgsQ0FBZ0IsY0FBSzVCLElBQUwsQ0FBVSxLQUFLdEIsQ0FBTCxDQUFPa0IsR0FBUCxDQUFXQyxLQUFYLENBQWlCakIsUUFBM0IsRUFBcUMsZUFBckMsQ0FBaEIsQ0FEaUIsQ0FBckI7QUFHQSxVQUFNNkkseUJBQXlCLEtBQUsvSSxDQUFMLENBQU84RCxPQUFQLENBQWVXLGVBQWYsQ0FBK0JxRSxZQUEvQixFQUE2QyxLQUE3QyxFQUFvRDFCLE9BQW5GO0FBQ0E5RCxhQUFPQyxJQUFQLENBQVlzRixlQUFaLEVBQTZCaEgsT0FBN0IsQ0FBcUMsVUFBQ21ILFVBQUQsRUFBZ0I7QUFDakQsWUFBSUEsY0FBY0Qsc0JBQWQsSUFDQUEsdUJBQXVCQyxVQUF2QixNQUF1Q0gsZ0JBQWdCRyxVQUFoQixDQUR2QyxJQUVBLGdCQUFPQyxFQUFQLENBQVVKLGdCQUFnQkcsVUFBaEIsQ0FBVixFQUF1Q0QsdUJBQXVCQyxVQUF2QixDQUF2QyxDQUZKLEVBR0U7QUFDRSxpQkFBSy9JLEdBQUwsQ0FBU3FFLElBQVQsQ0FBZSxrQ0FBaUN1RSxnQkFBZ0JHLFVBQWhCLENBQTRCLE1BQTlELEdBQ1QsR0FBRUEsVUFBVyxvQ0FESixHQUVULEdBQUVELHVCQUF1QkMsVUFBdkIsQ0FBbUMsRUFGMUM7QUFHSDtBQUNKLE9BVEQ7QUFVSDtBQUVEOzs7Ozs7NkNBR3lCO0FBQUE7O0FBQ3JCLFdBQUsvSSxHQUFMLENBQVNVLElBQVQsQ0FBYywrQ0FBZDtBQUNBLFVBQU11SSxzQkFBc0IsS0FBS2xKLENBQUwsQ0FBTzhELE9BQVAsQ0FBZVcsZUFBZixFQUE1QjtBQUVBLFdBQUswRSxtQkFBTCxDQUF5QkQsb0JBQW9COUIsT0FBN0M7QUFFQSxXQUFLbkgsR0FBTCxDQUFTZSxLQUFULENBQWUscUNBQWY7QUFDQSxXQUFLYixXQUFMLENBQWlCaUosaUJBQWpCLENBQ0ksNkJBREosRUFFSUYsb0JBQW9CRyxZQUZ4QjtBQUlBLFdBQUtwSixHQUFMLENBQVNlLEtBQVQsQ0FBZSxnQ0FBZjtBQUNBLFdBQUtiLFdBQUwsQ0FBaUJpSixpQkFBakIsQ0FDSSx3QkFESixFQUVJRixvQkFBb0I5QixPQUZ4QjtBQUtBLFdBQUtuSCxHQUFMLENBQVNlLEtBQVQsQ0FBZSxtQ0FBZjtBQUNBc0MsYUFBT0MsSUFBUCxDQUFZMkYsb0JBQW9CSSxPQUFoQyxFQUF5Q3pILE9BQXpDLENBQWlEO0FBQUEsZUFDN0MsT0FBSzFCLFdBQUwsQ0FBaUJpSixpQkFBakIsQ0FDSyxVQUFTcEgsTUFBTyxHQURyQixFQUVJa0gsb0JBQW9CSSxPQUFwQixDQUE0QnRILE1BQTVCLENBRkosQ0FENkM7QUFBQSxPQUFqRDtBQU1BLFdBQUt6QixXQUFMLENBQWlCRixZQUFqQixHQUFnQyxLQUFLRixXQUFMLENBQWlCb0oscUJBQWpCLEVBQWhDO0FBQ0EsV0FBS2hKLFdBQUwsQ0FBaUJpSixpQkFBakIsR0FBcUMsS0FBS3JKLFdBQUwsQ0FBaUJzSixvQkFBakIsRUFBckM7QUFFQSxXQUFLeEosR0FBTCxDQUFTZSxLQUFULENBQWUsOEJBQWY7O0FBQ0Esa0JBQUcwSSxhQUFILENBQ0ksS0FBSzFKLENBQUwsQ0FBT2tCLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkJiLFdBRGpDLEVBQzhDeUMsS0FBS29DLFNBQUwsQ0FBZSxLQUFLN0UsV0FBcEIsRUFBaUMsSUFBakMsRUFBdUMsQ0FBdkMsQ0FEOUM7QUFHSDtBQUVEOzs7Ozs7Ozs7OENBTW9HO0FBQUEsVUFBNUVvSixJQUE0RSx1RUFBckUsS0FBSzNKLENBQUwsQ0FBT2tCLEdBQVAsQ0FBV3VFLE9BQVgsQ0FBbUJxQyxJQUFuQixJQUEyQjdDLFFBQVEwRSxJQUFSLEtBQWlCLE1BQTVDLEdBQXFELE1BQXJELEdBQThELEtBQU87O0FBQ2hHLFVBQU1ILG9CQUFvQixnQkFBRUksTUFBRixDQUFTLEtBQUtySixXQUFMLENBQWlCaUosaUJBQTFCLENBQTFCOztBQUNBLFVBQUlBLGtCQUFrQjVHLE1BQWxCLEtBQTZCLENBQWpDLEVBQW9DO0FBQ2hDLGVBQU9oQyxRQUFRQyxPQUFSLEVBQVA7QUFDSDs7QUFDRCxXQUFLWixHQUFMLENBQVNVLElBQVQsQ0FBYywrQkFBZDtBQUNBLFVBQU1rSixjQUFjLEtBQUs3SixDQUFMLENBQU84SixlQUFQLENBQXVCQyx3QkFBdkIsQ0FBZ0RKLElBQWhELENBQXBCO0FBQ0EsVUFBTXpJLE1BQU0scUJBQVUySSxZQUFZRyxhQUF0QixFQUFxQ0gsWUFBWUksUUFBakQsRUFBMkRKLFlBQVlGLElBQXZFLENBQVo7QUFDQSxVQUFNTyxZQUFZLGlDQUNkO0FBQUUsU0FBQyxLQUFLbEssQ0FBTCxDQUFPa0IsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxXQUFqQixDQUE2Qm1CLElBQTlCLEdBQXFDaUg7QUFBdkMsT0FEYyxFQUVkO0FBQUVXLGdCQUFRako7QUFBVixPQUZjLENBQWxCO0FBSUEsa0NBQVNnSixTQUFUO0FBQ0EsYUFBT0EsVUFBVUUsT0FBVixFQUFQO0FBQ0g7QUFFRDs7Ozs7OztrQ0FJNkI7QUFBQSxVQUFqQkEsT0FBaUIsdUVBQVAsS0FBTzs7QUFDekIsVUFBSUEsT0FBSixFQUFhO0FBQ1QsYUFBS25LLEdBQUwsQ0FBU1UsSUFBVCxDQUFjLG9EQUFkO0FBQ0gsT0FGRCxNQUVPO0FBQ0gsYUFBS1YsR0FBTCxDQUFTVSxJQUFULENBQWMsc0RBQWQ7QUFDSDs7QUFFRCxVQUFNZ0osT0FBTyxLQUFLM0osQ0FBTCxDQUFPa0IsR0FBUCxDQUFXdUUsT0FBWCxDQUFtQnFDLElBQW5CLElBQTJCN0MsUUFBUTBFLElBQVIsS0FBaUIsTUFBNUMsR0FBcUQsTUFBckQsR0FBOEQsS0FBM0U7O0FBRUEsVUFBSSxLQUFLM0osQ0FBTCxDQUFPa0IsR0FBUCxDQUFXdUUsT0FBWCxDQUFtQnFDLElBQXZCLEVBQTZCO0FBQ3pCLGFBQUs3SCxHQUFMLENBQVNrRSxPQUFULENBQWlCLDJCQUFqQjtBQUNILE9BRkQsTUFFTztBQUNILGFBQUtsRSxHQUFMLENBQVNrRSxPQUFULENBQWtCLGtCQUFpQndGLElBQUssRUFBeEM7QUFDSDs7QUFFRCxhQUFPLEtBQUszSixDQUFMLENBQU84SixlQUFQLENBQXVCTyxnQkFBdkIsQ0FBd0NWLElBQXhDLEVBQThDVyxTQUE5QyxFQUF5REYsT0FBekQsQ0FBUDtBQUNIO0FBRUQ7Ozs7Ozs7Ozs7OENBTzBCO0FBQ3RCLFdBQUtuSyxHQUFMLENBQVNrRSxPQUFULENBQWlCLDhCQUFqQjtBQUNBLFVBQU1DLFdBQVcsS0FBS3BFLENBQUwsQ0FBTzhELE9BQVAsQ0FBZUMsV0FBZixFQUFqQjtBQUNBOztBQUNBLFVBQU14RCxjQUFjLEtBQUtMLFFBQUwsQ0FBY0UscUJBQWQsRUFBcEI7QUFFQUcsa0JBQVlDLE9BQVosR0FBc0I0RCxTQUFTNUQsT0FBL0I7O0FBQ0EsVUFBSSx1QkFBdUI0RCxRQUEzQixFQUFxQztBQUNqQywrQkFBUzdELFdBQVQsRUFBc0I2RCxTQUFTbUcsaUJBQS9CO0FBQ0g7O0FBQ0QsNkJBQVNoSyxXQUFULEVBQXNCO0FBQUVpSyxjQUFNcEcsU0FBU3FHO0FBQWpCLE9BQXRCO0FBRUEsV0FBS3hLLEdBQUwsQ0FBU2UsS0FBVCxDQUFlLDhCQUFmOztBQUNBLGtCQUFHMEksYUFBSCxDQUNJLEtBQUsxSixDQUFMLENBQU9rQixHQUFQLENBQVdDLEtBQVgsQ0FBaUJDLFdBQWpCLENBQTZCYixXQURqQyxFQUM4Q3lDLEtBQUtvQyxTQUFMLENBQWU3RSxXQUFmLEVBQTRCLElBQTVCLEVBQWtDLENBQWxDLENBRDlDOztBQUdBLFdBQUtBLFdBQUwsR0FBbUJBLFdBQW5CO0FBQ0g7QUFFRDs7Ozs7OytDQUcyQjtBQUN2QixXQUFLTixHQUFMLENBQVNlLEtBQVQsQ0FBZSwrQkFBZjtBQUNBLFVBQU1vRCxXQUFXLEtBQUtwRSxDQUFMLENBQU84RCxPQUFQLENBQWVDLFdBQWYsRUFBakIsQ0FGdUIsQ0FJdkI7O0FBQ0FLLGVBQVMzRCxvQkFBVCxHQUFnQyxLQUFLQSxvQkFBckMsQ0FMdUIsQ0FPdkI7O0FBQ0EyRCxlQUFTbEQsR0FBVCxHQUFnQixLQUFLbEIsQ0FBTCxDQUFPa0IsR0FBUCxDQUFXdUYsaUJBQVgsRUFBRCxHQUNYLE1BRFcsR0FDRixLQURiO0FBR0FyQyxlQUFTc0csY0FBVCxHQUEyQixHQUFFLEtBQUsxSyxDQUFMLENBQU84RCxPQUFQLENBQWU2RyxjQUFmLEVBQWdDLElBQUd2RyxTQUFTbEQsR0FBSSxFQUE3RTtBQUVBa0QsZUFBU3dHLG9CQUFULEdBQWdDLEtBQUs1SyxDQUFMLENBQU82RSxVQUFQLEVBQWhDOztBQUVBLGtCQUFHNkUsYUFBSCxDQUNJLEtBQUsxSixDQUFMLENBQU9rQixHQUFQLENBQVdDLEtBQVgsQ0FBaUIwSixVQUFqQixDQUE0QnpHLFFBRGhDLEVBQzBDcEIsS0FBS29DLFNBQUwsQ0FBZWhCLFFBQWYsRUFBeUIsSUFBekIsRUFBK0IsQ0FBL0IsQ0FEMUM7QUFHSDtBQUVEOzs7Ozs7d0NBR29CO0FBQUE7O0FBQ2hCLFdBQUtuRSxHQUFMLENBQVNVLElBQVQsQ0FBYywwQkFBZDtBQUNBLGFBQU8sSUFBSUMsT0FBSixDQUFZLFVBQUNDLE9BQUQsRUFBVXNILE1BQVYsRUFBcUI7QUFDcEMsc0JBQUt6RyxhQUFMLENBQ0ksUUFBSzFCLENBQUwsQ0FBT2tCLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQjBKLFVBQWpCLENBQTRCdEksSUFEaEMsRUFFSSxRQUFLdkMsQ0FBTCxDQUFPa0IsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxXQUFqQixDQUE2QjBKLFdBRmpDLEVBR0ksWUFBTTtBQUNGLGtCQUFLN0ssR0FBTCxDQUFTa0UsT0FBVCxDQUFpQiw2QkFBakI7O0FBQ0Esa0JBQUtuRSxDQUFMLENBQU9pQyxLQUFQLENBQ0s4RixhQURMLENBQ21CLEtBRG5CLEVBQzBCLFFBQUsvSCxDQUFMLENBQU9rQixHQUFQLENBQVdDLEtBQVgsQ0FBaUIwSixVQUFqQixDQUE0QnRJLElBRHRELEVBRUt3SSxJQUZMLENBRVUsWUFBTTtBQUNSbEs7QUFDSCxXQUpMLEVBS0ttSyxLQUxMLENBS1csVUFBQzdILENBQUQsRUFBTztBQUNWZ0YsbUJBQU9oRixDQUFQO0FBQ0gsV0FQTDs7QUFRQXRDO0FBQ0gsU0FkTDtBQWdCSCxPQWpCTSxDQUFQO0FBa0JIO0FBRUQ7Ozs7OzsrQ0FHMkI7QUFDdkIsV0FBS1osR0FBTCxDQUFTa0UsT0FBVCxDQUFpQix3Q0FBakI7O0FBQ0EsdUJBQU1yQyxFQUFOLENBQVMsS0FBVCxFQUFnQixLQUFLOUIsQ0FBTCxDQUFPa0IsR0FBUCxDQUFXQyxLQUFYLENBQWlCMkMsT0FBakIsQ0FBeUJ2QixJQUF6QyxFQUErQyxLQUFLdkMsQ0FBTCxDQUFPa0IsR0FBUCxDQUFXQyxLQUFYLENBQWlCMEosVUFBakIsQ0FBNEJ0SSxJQUEzRSxFQUZ1QixDQUd2Qjs7O0FBQ0EsbUJBQUlELElBQUosQ0FBUyxDQUNMLGNBQUtoQixJQUFMLENBQVUsS0FBS3RCLENBQUwsQ0FBT2tCLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQjBKLFVBQWpCLENBQTRCdEksSUFBdEMsRUFBNEMsSUFBNUMsRUFBa0QsV0FBbEQsQ0FESyxDQUFUO0FBR0g7QUFHRDs7Ozs7O3lDQUdxQjtBQUNqQixXQUFLdEMsR0FBTCxDQUFTVSxJQUFULENBQWMsMkJBQWQ7QUFFQSxVQUFNeUQsV0FBVyxLQUFLcEUsQ0FBTCxDQUFPOEQsT0FBUCxDQUFlQyxXQUFmLEVBQWpCO0FBQ0EsVUFBTTBCLFVBQVUsbUJBQW1CckIsUUFBbkIsR0FBOEJBLFNBQVM2RyxhQUF2QyxHQUF1RCxFQUF2RTtBQUVBLFVBQU1DLG1CQUFtQixZQUFZOUcsUUFBWixJQUF3QixDQUFDLENBQUNBLFNBQVMrRyxNQUE1RDtBQUVBLFVBQU1DLFNBQVMsd0JBQVVkLFNBQVYsRUFBcUI7QUFBRWUsaUJBQVM7QUFBRUMsZ0JBQU07QUFBUjtBQUFYLE9BQXJCLENBQWY7O0FBRUEsb0JBQUtoSixJQUFMLENBQVcsR0FBRSxLQUFLdEMsQ0FBTCxDQUFPa0IsR0FBUCxDQUFXQyxLQUFYLENBQWlCMEosVUFBakIsQ0FBNEJ0SSxJQUFLLFVBQTlDLEVBQXlEVixPQUF6RCxDQUFpRSxVQUFDMEosSUFBRCxFQUFVO0FBQUEsaUNBQ3hELDZCQUFrQkEsSUFBbEIsRUFBd0I7QUFDbkNDLG1CQUFTLENBQUNKLE1BQUQ7QUFEMEIsU0FBeEIsQ0FEd0Q7QUFBQSxZQUNqRTdDLElBRGlFLHNCQUNqRUEsSUFEaUU7O0FBSXZFLFlBQUk3QyxLQUFKOztBQUNBLFlBQUl0QixTQUFTbEQsR0FBVCxLQUFpQixNQUFqQixJQUEyQmdLLGdCQUEvQixFQUFpRDtBQUFBLCtCQUMxQixrQkFBT08sTUFBUCxDQUFjbEQsSUFBZCxFQUFvQjlDLE9BQXBCLENBRDBCOztBQUMxQzhDLGNBRDBDLGtCQUMxQ0EsSUFEMEM7QUFDcEM3QyxlQURvQyxrQkFDcENBLEtBRG9DO0FBRWhEOztBQUNELFlBQUlBLEtBQUosRUFBVztBQUNQLGdCQUFNLElBQUlrQyxLQUFKLENBQVVsQyxLQUFWLENBQU47QUFDSDs7QUFDRCxvQkFBR2dFLGFBQUgsQ0FBaUI2QixJQUFqQixFQUF1QmhELElBQXZCO0FBQ0gsT0FaRDtBQWFIO0FBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUtJLHFCQUFLdEksR0FBTCxDQUFTVSxJQUFULENBQWMsOEJBQWQsRSxDQUVBOzs7O3VCQUdVLEtBQUtYLENBQUwsQ0FBT2lDLEtBQVAsQ0FBYThGLGFBQWIsQ0FBMkIsS0FBM0IsRUFBa0MsS0FBSy9ILENBQUwsQ0FBT2tCLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkJJLFNBQS9ELEM7Ozs7Ozs7OztzQkFFQSxJQUFJb0csS0FBSixjOzs7QUFHVixpQ0FBTThELEtBQU4sQ0FBWSxLQUFLMUwsQ0FBTCxDQUFPa0IsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxXQUFqQixDQUE2QkksU0FBekM7O0FBRU1tSyx1QixHQUFVLEtBQUszTCxDQUFMLENBQU84RCxPQUFQLENBQWU4SCxtQkFBZixFLEVBRWhCOztBQUNBRCx3QkFBUTlKLE9BQVIsQ0FBZ0IsVUFBQ2hDLE1BQUQsRUFBWTtBQUN4QixzQkFBTWdNLGVBQWVoTSxNQUFyQjs7QUFDQSxzQkFBSSxhQUFhZ00sWUFBakIsRUFBK0I7QUFDM0Isd0JBQUksQ0FBQzdILE1BQU1DLE9BQU4sQ0FBYzRILGFBQWEvSyxPQUEzQixDQUFMLEVBQTBDO0FBQ3RDK0ssbUNBQWEvSyxPQUFiLEdBQXVCLENBQUMrSyxhQUFhL0ssT0FBZCxDQUF2QjtBQUNIOztBQUNEK0ssaUNBQWEvSyxPQUFiLENBQXFCZSxPQUFyQixDQUE2QixVQUFDMEosSUFBRCxFQUFVO0FBQ25DLDhCQUFLdEwsR0FBTCxDQUFTZSxLQUFULENBQWdCLGFBQVl1SyxJQUFLLFNBQVExTCxPQUFPMkssSUFBSyxFQUFyRDs7QUFDQSwwQkFBTXNCLFdBQVcsY0FBS3hLLElBQUwsQ0FDYixRQUFLdEIsQ0FBTCxDQUFPa0IsR0FBUCxDQUFXQyxLQUFYLENBQWlCMEosVUFBakIsQ0FBNEJ2QixPQURmLEVBQ3dCdUMsYUFBYUUsT0FEckMsRUFDOENSLElBRDlDLENBQWpCOztBQUdBLDBCQUFNUyxrQkFBa0IsY0FBSzFLLElBQUwsQ0FDcEIsUUFBS3RCLENBQUwsQ0FBT2tCLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkJJLFNBRFQsRUFDb0JxSyxhQUFhRSxPQURqQyxDQUF4Qjs7QUFJQSwwQkFBSSxDQUFDLFFBQUsvTCxDQUFMLENBQU9pQyxLQUFQLENBQWFDLE1BQWIsQ0FBb0I4SixlQUFwQixDQUFMLEVBQTJDO0FBQ3ZDLHlDQUFNTixLQUFOLENBQVlNLGVBQVo7QUFDSDs7QUFDRCx1Q0FBTXBLLEVBQU4sQ0FBU2tLLFFBQVQsRUFBbUJFLGVBQW5CO0FBQ0gscUJBYkQ7QUFjSDtBQUNKLGlCQXJCRCIsImZpbGUiOiJlbGVjdHJvbkFwcC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBhc2FyIGZyb20gJ2FzYXInO1xuaW1wb3J0IGFzc2lnbkluIGZyb20gJ2xvZGFzaC9hc3NpZ25Jbic7XG5pbXBvcnQgXyBmcm9tICdsb2Rhc2gnO1xuaW1wb3J0IHsgTG9jYWxJbnN0YWxsZXIsIHByb2dyZXNzIH0gZnJvbSAnaW5zdGFsbC1sb2NhbCc7XG5pbXBvcnQgeyB0cmFuc2Zvcm1GaWxlU3luYyB9IGZyb20gJ0BiYWJlbC9jb3JlJztcbmltcG9ydCBjcnlwdG8gZnJvbSAnY3J5cHRvJztcbmltcG9ydCBkZWwgZnJvbSAnZGVsJztcbmltcG9ydCBwcmVzZXRFbnYgZnJvbSAnQGJhYmVsL3ByZXNldC1lbnYnO1xuaW1wb3J0IGZzIGZyb20gJ2ZzJztcbmltcG9ydCBnbG9iIGZyb20gJ2dsb2InO1xuaW1wb3J0IHBhdGggZnJvbSAncGF0aCc7XG5pbXBvcnQgc2hlbGwgZnJvbSAnc2hlbGxqcyc7XG5pbXBvcnQgc3Bhd24gZnJvbSAnY3Jvc3Mtc3Bhd24nO1xuaW1wb3J0IHNlbXZlciBmcm9tICdzZW12ZXInO1xuaW1wb3J0IHVnbGlmeSBmcm9tICd1Z2xpZnktZXMnO1xuXG5pbXBvcnQgeyBnZXRHeXBFbnYgfSBmcm9tICdlbGVjdHJvbi1idWlsZGVyLWxpYi9vdXQvdXRpbC95YXJuJztcblxuaW1wb3J0IExvZyBmcm9tICcuL2xvZyc7XG5pbXBvcnQgRWxlY3Ryb25BcHBTY2FmZm9sZCBmcm9tICcuL2VsZWN0cm9uQXBwU2NhZmZvbGQnO1xuaW1wb3J0IERlcGVuZGVuY2llc01hbmFnZXIgZnJvbSAnLi9kZXBlbmRlbmNpZXNNYW5hZ2VyJztcbmltcG9ydCBCaW5hcnlNb2R1bGVEZXRlY3RvciBmcm9tICcuL2JpbmFyeU1vZHVsZXNEZXRlY3Rvcic7XG5cbnNoZWxsLmNvbmZpZy5mYXRhbCA9IHRydWU7XG5cbi8qKlxuICogUmVwcmVzZW50cyB0aGUgLmRlc2t0b3AgZGlyIHNjYWZmb2xkLlxuICogQGNsYXNzXG4gKi9cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEVsZWN0cm9uQXBwIHtcbiAgICAvKipcbiAgICAgKiBAcGFyYW0ge01ldGVvckRlc2t0b3B9ICQgLSBjb250ZXh0XG4gICAgICogQGNvbnN0cnVjdG9yXG4gICAgICovXG4gICAgY29uc3RydWN0b3IoJCkge1xuICAgICAgICB0aGlzLmxvZyA9IG5ldyBMb2coJ2VsZWN0cm9uQXBwJyk7XG4gICAgICAgIHRoaXMuc2NhZmZvbGQgPSBuZXcgRWxlY3Ryb25BcHBTY2FmZm9sZCgkKTtcbiAgICAgICAgdGhpcy5kZXBzTWFuYWdlciA9IG5ldyBEZXBlbmRlbmNpZXNNYW5hZ2VyKFxuICAgICAgICAgICAgJCxcbiAgICAgICAgICAgIHRoaXMuc2NhZmZvbGQuZ2V0RGVmYXVsdFBhY2thZ2VKc29uKCkuZGVwZW5kZW5jaWVzXG4gICAgICAgICk7XG4gICAgICAgIHRoaXMuJCA9ICQ7XG4gICAgICAgIHRoaXMubWV0ZW9yQXBwID0gdGhpcy4kLm1ldGVvckFwcDtcbiAgICAgICAgdGhpcy5wYWNrYWdlSnNvbiA9IG51bGw7XG4gICAgICAgIHRoaXMudmVyc2lvbiA9IG51bGw7XG4gICAgICAgIHRoaXMuY29tcGF0aWJpbGl0eVZlcnNpb24gPSBudWxsO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIE1ha2VzIGFuIGFwcC5hc2FyIGZyb20gdGhlIHNrZWxldG9uIGFwcC5cbiAgICAgKiBAcHJvcGVydHkge0FycmF5fSBleGNsdWRlRnJvbURlbCAtIGxpc3Qgb2YgcGF0aHMgdG8gZXhjbHVkZSBmcm9tIGRlbGV0aW5nXG4gICAgICogQHJldHVybnMge1Byb21pc2V9XG4gICAgICovXG4gICAgcGFja1NrZWxldG9uVG9Bc2FyKGV4Y2x1ZGVGcm9tRGVsID0gW10pIHtcbiAgICAgICAgdGhpcy5sb2cuaW5mbygncGFja2luZyBza2VsZXRvbiBhcHAgYW5kIG5vZGVfbW9kdWxlcyB0byBhc2FyIGFyY2hpdmUnKTtcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlKSA9PiB7XG4gICAgICAgICAgICBjb25zdCBleHRyYWN0ID0gdGhpcy5nZXRNb2R1bGVzVG9FeHRyYWN0KCk7XG5cbiAgICAgICAgICAgIC8vIFdlIHdhbnQgdG8gcGFjayBza2VsZXRvbiBhcHAgYW5kIG5vZGVfbW9kdWxlcyB0b2dldGhlciwgc28gd2UgbmVlZCB0byB0ZW1wb3JhcmlseVxuICAgICAgICAgICAgLy8gbW92ZSBub2RlX21vZHVsZXMgdG8gYXBwIGRpci5cbiAgICAgICAgICAgIHRoaXMubG9nLmRlYnVnKCdtb3Zpbmcgbm9kZV9tb2R1bGVzIHRvIGFwcCBkaXInKTtcblxuICAgICAgICAgICAgZnMucmVuYW1lU3luYyhcbiAgICAgICAgICAgICAgICB0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLm5vZGVNb2R1bGVzLFxuICAgICAgICAgICAgICAgIHBhdGguam9pbih0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLmFwcFJvb3QsICdub2RlX21vZHVsZXMnKVxuICAgICAgICAgICAgKTtcblxuICAgICAgICAgICAgbGV0IGV4dHJhY3RlZCA9IGZhbHNlO1xuICAgICAgICAgICAgZXh0cmFjdGVkID0gdGhpcy5leHRyYWN0TW9kdWxlcyhleHRyYWN0KTtcblxuICAgICAgICAgICAgdGhpcy5sb2cuZGVidWcoJ3BhY2tpbmcnKTtcbiAgICAgICAgICAgIGFzYXIuY3JlYXRlUGFja2FnZShcbiAgICAgICAgICAgICAgICB0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLmFwcFJvb3QsXG4gICAgICAgICAgICAgICAgdGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5hcHBBc2FyLFxuICAgICAgICAgICAgICAgICgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgLy8gTGV0cyBtb3ZlIHRoZSBub2RlX21vZHVsZXMgYmFjay5cbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2cuZGVidWcoJ21vdmluZyBub2RlX21vZHVsZXMgYmFjayBmcm9tIGFwcCBkaXInKTtcblxuICAgICAgICAgICAgICAgICAgICBzaGVsbC5tdihcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhdGguam9pbih0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLmFwcFJvb3QsICdub2RlX21vZHVsZXMnKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAubm9kZU1vZHVsZXNcbiAgICAgICAgICAgICAgICAgICAgKTtcblxuICAgICAgICAgICAgICAgICAgICBpZiAoZXh0cmFjdGVkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBXZSBuZWVkIHRvIGNyZWF0ZSBhIGZ1bGwgbm9kZSBtb2R1bGVzIGJhY2suIEluIG90aGVyIHdvcmRzIHdlIHdhbnRcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHRoZSBleHRyYWN0ZWQgbW9kdWxlcyBiYWNrLlxuICAgICAgICAgICAgICAgICAgICAgICAgZXh0cmFjdC5mb3JFYWNoKG1vZHVsZSA9PiBzaGVsbC5jcChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnLXJmJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYXRoLmpvaW4odGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5leHRyYWN0ZWROb2RlTW9kdWxlcywgbW9kdWxlKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYXRoLmpvaW4odGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5ub2RlTW9kdWxlcywgbW9kdWxlKVxuICAgICAgICAgICAgICAgICAgICAgICAgKSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIEdldCB0aGUgLmJpbiBiYWNrLlxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuJC51dGlscy5leGlzdHMoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5leHRyYWN0ZWROb2RlTW9kdWxlc0JpblxuICAgICAgICAgICAgICAgICAgICAgICAgKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNoZWxsLmNwKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYXRoLmpvaW4odGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5leHRyYWN0ZWROb2RlTW9kdWxlc0JpbiwgJyonKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGF0aC5qb2luKHRoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAubm9kZU1vZHVsZXMsICcuYmluJylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2cuZGVidWcoJ2RlbGV0aW5nIHNvdXJjZSBmaWxlcycpO1xuICAgICAgICAgICAgICAgICAgICBjb25zdCBleGNsdWRlID0gW3RoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAubm9kZU1vZHVsZXNdLmNvbmNhdChcbiAgICAgICAgICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLmFwcEFzYXIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5wYWNrYWdlSnNvblxuICAgICAgICAgICAgICAgICAgICAgICAgXSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGV4Y2x1ZGVGcm9tRGVsXG4gICAgICAgICAgICAgICAgICAgICk7XG5cbiAgICAgICAgICAgICAgICAgICAgZGVsLnN5bmMoXG4gICAgICAgICAgICAgICAgICAgICAgICBbYCR7dGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5yb290fSR7cGF0aC5zZXB9KmBdLmNvbmNhdChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBleGNsdWRlLm1hcChwYXRoVG9FeGNsdWRlID0+IGAhJHtwYXRoVG9FeGNsdWRlfWApXG4gICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICApO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBNb3ZlcyBzcGVjaWZpZWQgbm9kZSBtb2R1bGVzIHRvIGEgc2VwYXJhdGUgZGlyZWN0b3J5LlxuICAgICAqIEBwYXJhbSB7QXJyYXl9IGV4dHJhY3RcbiAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICAgKi9cbiAgICBleHRyYWN0TW9kdWxlcyhleHRyYWN0KSB7XG4gICAgICAgIGNvbnN0IGV4dCA9IFsnLmpzJywgJy5iYXQnLCAnLnNoJywgJy5jbWQnLCAnJ107XG5cbiAgICAgICAgaWYgKGV4dHJhY3QubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgaWYgKHRoaXMuJC51dGlscy5leGlzdHModGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5leHRyYWN0ZWROb2RlTW9kdWxlcykpIHtcbiAgICAgICAgICAgICAgICBzaGVsbC5ybSgnLXJmJywgdGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5leHRyYWN0ZWROb2RlTW9kdWxlcyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBmcy5ta2RpclN5bmModGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5leHRyYWN0ZWROb2RlTW9kdWxlcyk7XG4gICAgICAgICAgICBmcy5ta2RpclN5bmModGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5leHRyYWN0ZWROb2RlTW9kdWxlc0Jpbik7XG5cbiAgICAgICAgICAgIGV4dHJhY3QuZm9yRWFjaCgobW9kdWxlKSA9PiB7XG4gICAgICAgICAgICAgICAgZnMucmVuYW1lU3luYyhcbiAgICAgICAgICAgICAgICAgICAgcGF0aC5qb2luKHRoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAuYXBwUm9vdCwgJ25vZGVfbW9kdWxlcycsIG1vZHVsZSksXG4gICAgICAgICAgICAgICAgICAgIHBhdGguam9pbih0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLmV4dHJhY3RlZE5vZGVNb2R1bGVzLCBtb2R1bGUpLFxuICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgLy8gTW92ZSBiaW5zLlxuICAgICAgICAgICAgICAgIHRoaXMuZXh0cmFjdEJpbihtb2R1bGUsIGV4dCk7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEV4dHJhY3RzIHRoZSBiaW4gZmlsZXMgYXNzb2NpYXRlZCB3aXRoIGEgY2VydGFpbiBub2RlIG1vZHVsZXMuXG4gICAgICpcbiAgICAgKiBAcGFyYW0gbW9kdWxlXG4gICAgICogQHBhcmFtIGV4dFxuICAgICAqL1xuICAgIGV4dHJhY3RCaW4obW9kdWxlLCBleHQpIHtcbiAgICAgICAgbGV0IHBhY2thZ2VKc29uO1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgcGFja2FnZUpzb24gPSBKU09OLnBhcnNlKFxuICAgICAgICAgICAgICAgIGZzLnJlYWRGaWxlU3luYyhcbiAgICAgICAgICAgICAgICAgICAgcGF0aC5qb2luKFxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5leHRyYWN0ZWROb2RlTW9kdWxlcywgbW9kdWxlLCAncGFja2FnZS5qc29uJ1xuICAgICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgICAndXRmOCdcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApO1xuICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgICBwYWNrYWdlSnNvbiA9IHt9O1xuICAgICAgICB9XG5cblxuICAgICAgICBjb25zdCBiaW5zID0gKCdiaW4nIGluIHBhY2thZ2VKc29uICYmIHR5cGVvZiBwYWNrYWdlSnNvbi5iaW4gPT09ICdvYmplY3QnKSA/IE9iamVjdC5rZXlzKHBhY2thZ2VKc29uLmJpbikgOiBbXTtcblxuICAgICAgICBpZiAoYmlucy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICBiaW5zLmZvckVhY2goKGJpbikgPT4ge1xuICAgICAgICAgICAgICAgIGV4dC5mb3JFYWNoKChleHRlbnNpb24pID0+IHtcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgYmluRmlsZVBhdGggPSBwYXRoLmpvaW4oXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLmFwcFJvb3QsXG4gICAgICAgICAgICAgICAgICAgICAgICAnbm9kZV9tb2R1bGVzJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICcuYmluJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIGAke2Jpbn0ke2V4dGVuc2lvbn1gXG4gICAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLiQudXRpbHMuZXhpc3RzKGJpbkZpbGVQYXRoKSB8fFxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kLnV0aWxzLnN5bWxpbmtFeGlzdHMoYmluRmlsZVBhdGgpXG4gICAgICAgICAgICAgICAgICAgICkge1xuICAgICAgICAgICAgICAgICAgICAgICAgZnMucmVuYW1lU3luYyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBiaW5GaWxlUGF0aCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYXRoLmpvaW4oXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAuZXh0cmFjdGVkTm9kZU1vZHVsZXNCaW4sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGAke2Jpbn0ke2V4dGVuc2lvbn1gXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgKTtcblxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIE1lcmdlcyB0aGUgYGV4dHJhY3RgIGZpZWxkIHdpdGggYXV0b21hdGljYWxseSBkZXRlY3RlZCBtb2R1bGVzLlxuICAgICAqL1xuICAgIGdldE1vZHVsZXNUb0V4dHJhY3QoKSB7XG4gICAgICAgIGNvbnN0IGJpbmFyeU1vZHVsZXNEZXRlY3RvciA9XG4gICAgICAgICAgICBuZXcgQmluYXJ5TW9kdWxlRGV0ZWN0b3IodGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5ub2RlTW9kdWxlcyk7XG4gICAgICAgIGNvbnN0IHRvQmVFeHRyYWN0ZWQgPSBiaW5hcnlNb2R1bGVzRGV0ZWN0b3IuZGV0ZWN0KCk7XG5cbiAgICAgICAgbGV0IHsgZXh0cmFjdCB9ID0gdGhpcy4kLmRlc2t0b3AuZ2V0U2V0dGluZ3MoKTtcblxuICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkoZXh0cmFjdCkpIHtcbiAgICAgICAgICAgIGV4dHJhY3QgPSBbXTtcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IG1lcmdlID0ge307XG4gICAgICAgIHRvQmVFeHRyYWN0ZWQuY29uY2F0KGV4dHJhY3QpLmZvckVhY2gobW9kdWxlID0+IHtcbiAgICAgICAgICAgIG1lcmdlW21vZHVsZV0gPSB0cnVlO1xuICAgICAgICB9KTtcbiAgICAgICAgZXh0cmFjdCA9IE9iamVjdC5rZXlzKG1lcmdlKTtcbiAgICAgICAgaWYgKGV4dHJhY3QubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgdGhpcy5sb2cudmVyYm9zZShgcmVzdWx0YW50IG1vZHVsZXMgdG8gZXh0cmFjdCBsaXN0IGlzOiAke2V4dHJhY3Quam9pbignLCAnKX1gKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZXh0cmFjdDtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBDYWxjdWxhdGVzIGEgbWQ1IGZyb20gYWxsIGRlcGVuZGVuY2llcy5cbiAgICAgKi9cbiAgICBjYWxjdWxhdGVDb21wYXRpYmlsaXR5VmVyc2lvbigpIHtcbiAgICAgICAgdGhpcy5sb2cudmVyYm9zZSgnY2FsY3VsYXRpbmcgY29tcGF0aWJpbGl0eSB2ZXJzaW9uJyk7XG4gICAgICAgIGNvbnN0IHNldHRpbmdzID0gdGhpcy4kLmRlc2t0b3AuZ2V0U2V0dGluZ3MoKTtcblxuICAgICAgICBpZiAoKCdkZXNrdG9wSENQQ29tcGF0aWJpbGl0eVZlcnNpb24nIGluIHNldHRpbmdzKSkge1xuICAgICAgICAgICAgdGhpcy5jb21wYXRpYmlsaXR5VmVyc2lvbiA9IGAke3NldHRpbmdzLmRlc2t0b3BIQ1BDb21wYXRpYmlsaXR5VmVyc2lvbn1gO1xuICAgICAgICAgICAgdGhpcy5sb2cud2FybihgY29tcGF0aWJpbGl0eSB2ZXJzaW9uIG92ZXJyaWRkZW4gdG8gJHt0aGlzLmNvbXBhdGliaWxpdHlWZXJzaW9ufWApO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgY29uc3QgbWQ1ID0gY3J5cHRvLmNyZWF0ZUhhc2goJ21kNScpO1xuICAgICAgICBsZXQgZGVwZW5kZW5jaWVzID0gT2JqZWN0LmtleXModGhpcy5kZXBzTWFuYWdlci5nZXREZXBlbmRlbmNpZXMoKSkuc29ydCgpO1xuICAgICAgICBkZXBlbmRlbmNpZXMgPSBkZXBlbmRlbmNpZXMubWFwKGRlcGVuZGVuY3kgPT5cbiAgICAgICAgICAgIGAke2RlcGVuZGVuY3l9OiR7dGhpcy5wYWNrYWdlSnNvbi5kZXBlbmRlbmNpZXNbZGVwZW5kZW5jeV19YCk7XG4gICAgICAgIGNvbnN0IG1haW5Db21wYXRpYmlsaXR5VmVyc2lvbiA9IHRoaXMuJC5nZXRWZXJzaW9uKCkuc3BsaXQoJy4nKTtcbiAgICAgICAgdGhpcy5sb2cuZGVidWcoJ21ldGVvci1kZXNrdG9wIGNvbXBhdGliaWxpdHkgdmVyc2lvbiBpcyAnLFxuICAgICAgICAgICAgYCR7bWFpbkNvbXBhdGliaWxpdHlWZXJzaW9uWzBdfS4ke21haW5Db21wYXRpYmlsaXR5VmVyc2lvblsxXX1gKTtcbiAgICAgICAgZGVwZW5kZW5jaWVzLnB1c2goXG4gICAgICAgICAgICBgbWV0ZW9yLWRlc2t0b3A6JHttYWluQ29tcGF0aWJpbGl0eVZlcnNpb25bMF19LiR7bWFpbkNvbXBhdGliaWxpdHlWZXJzaW9uWzFdfWBcbiAgICAgICAgKTtcblxuICAgICAgICBjb25zdCBkZXNrdG9wQ29tcGF0aWJpbGl0eVZlcnNpb24gPSBzZXR0aW5ncy52ZXJzaW9uLnNwbGl0KCcuJylbMF07XG4gICAgICAgIHRoaXMubG9nLmRlYnVnKCcuZGVza3RvcCBjb21wYXRpYmlsaXR5IHZlcnNpb24gaXMgJywgZGVza3RvcENvbXBhdGliaWxpdHlWZXJzaW9uKTtcbiAgICAgICAgZGVwZW5kZW5jaWVzLnB1c2goXG4gICAgICAgICAgICBgZGVza3RvcC1hcHA6JHtkZXNrdG9wQ29tcGF0aWJpbGl0eVZlcnNpb259YFxuICAgICAgICApO1xuXG4gICAgICAgIGlmIChwcm9jZXNzLmVudi5NRVRFT1JfREVTS1RPUF9ERUJVR19ERVNLVE9QX0NPTVBBVElCSUxJVFlfVkVSU0lPTiB8fFxuICAgICAgICAgICAgcHJvY2Vzcy5lbnYuTUVURU9SX0RFU0tUT1BfREVCVUdcbiAgICAgICAgKSB7XG4gICAgICAgICAgICB0aGlzLmxvZy5kZWJ1ZyhgY29tcGF0aWJpbGl0eSB2ZXJzaW9uIGNhbGN1bGF0ZWQgZnJvbSAke0pTT04uc3RyaW5naWZ5KGRlcGVuZGVuY2llcyl9YCk7XG4gICAgICAgIH1cblxuICAgICAgICBtZDUudXBkYXRlKEpTT04uc3RyaW5naWZ5KGRlcGVuZGVuY2llcykpO1xuXG4gICAgICAgIHRoaXMuY29tcGF0aWJpbGl0eVZlcnNpb24gPSBtZDUuZGlnZXN0KCdoZXgnKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBSdW5zIGFsbCBuZWNlc3NhcnkgdGFza3MgdG8gYnVpbGQgdGhlIGRlc2t0b3BpZmllZCBhcHAuXG4gICAgICovXG4gICAgYXN5bmMgYnVpbGQocnVuID0gZmFsc2UpIHtcbiAgICAgICAgLy8gVE9ETzogcmVmYWN0b3IgdG8gYSB0YXNrIHJ1bm5lclxuICAgICAgICB0aGlzLmxvZy5pbmZvKCdzY2FmZm9sZGluZycpO1xuXG4gICAgICAgIGlmICghdGhpcy4kLmRlc2t0b3AuY2hlY2soKSkge1xuICAgICAgICAgICAgaWYgKCF0aGlzLiQuZW52Lm9wdGlvbnMuc2NhZmZvbGQpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmxvZy5lcnJvcignc2VlbXMgdGhhdCB5b3UgZG8gbm90IGhhdmUgYSAuZGVza3RvcCBkaXIgaW4geW91ciBwcm9qZWN0IG9yIGl0IGlzJyArXG4gICAgICAgICAgICAgICAgICAgICcgY29ycnVwdGVkLiBSdW4gXFwnbnBtIHJ1biBkZXNrdG9wIC0tIGluaXRcXCcgdG8gZ2V0IGEgbmV3IG9uZS4nKTtcbiAgICAgICAgICAgICAgICAvLyBEbyBub3QgZmFpbCwgc28gdGhhdCBucG0gd2lsbCBub3QgcHJpbnQgaGlzIGVycm9yIHN0dWZmIHRvIGNvbnNvbGUuXG4gICAgICAgICAgICAgICAgcHJvY2Vzcy5leGl0KDApO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB0aGlzLiQuZGVza3RvcC5zY2FmZm9sZCgpO1xuICAgICAgICAgICAgICAgIHRoaXMuJC5tZXRlb3JBcHAudXBkYXRlR2l0SWdub3JlKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgdGhpcy4kLm1ldGVvckFwcC51cGRhdGVHaXRJZ25vcmUoKTtcbiAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgdGhpcy5sb2cud2FybihgZXJyb3Igb2NjdXJyZWQgd2hpbGUgYWRkaW5nICR7dGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5yb290TmFtZX1gICtcbiAgICAgICAgICAgICAgICAndG8gLmdpdGlnbm9yZTogJywgZSk7XG4gICAgICAgIH1cblxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgYXdhaXQgdGhpcy4kLm1ldGVvckFwcC5lbnN1cmVEZXNrdG9wSENQUGFja2FnZXMoKTtcbiAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgdGhpcy5sb2cuZXJyb3IoJ2Vycm9yIHdoaWxlIGNoZWNraW5nIGZvciByZXF1aXJlZCBwYWNrYWdlczogJywgZSk7XG4gICAgICAgICAgICBwcm9jZXNzLmV4aXQoMSk7XG4gICAgICAgIH1cblxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgYXdhaXQgdGhpcy5zY2FmZm9sZC5tYWtlKCk7XG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgIHRoaXMubG9nLmVycm9yKCdlcnJvciB3aGlsZSBzY2FmZm9sZGluZzogJywgZSk7XG4gICAgICAgICAgICBwcm9jZXNzLmV4aXQoMSk7XG4gICAgICAgIH1cblxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgdGhpcy51cGRhdGVQYWNrYWdlSnNvbkZpZWxkcygpO1xuICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgICB0aGlzLmxvZy5lcnJvcignZXJyb3Igd2hpbGUgdXBkYXRpbmcgcGFja2FnZS5qc29uOiAnLCBlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICB0aGlzLnVwZGF0ZURlcGVuZGVuY2llc0xpc3QoKTtcbiAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgdGhpcy5sb2cuZXJyb3IoJ2Vycm9yIHdoaWxlIG1lcmdpbmcgZGVwZW5kZW5jaWVzIGxpc3Q6ICcsIGUpO1xuICAgICAgICB9XG5cbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIHRoaXMuY2FsY3VsYXRlQ29tcGF0aWJpbGl0eVZlcnNpb24oKTtcbiAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgdGhpcy5sb2cuZXJyb3IoJ2Vycm9yIHdoaWxlIGNhbGN1bGF0aW5nIGNvbXBhdGliaWxpdHkgdmVyc2lvbjogJywgZSk7XG4gICAgICAgICAgICBwcm9jZXNzLmV4aXQoMSk7XG4gICAgICAgIH1cblxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgYXdhaXQgdGhpcy5oYW5kbGVUZW1wb3JhcnlOb2RlTW9kdWxlcygpO1xuICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgICB0aGlzLmxvZy5lcnJvcignZXJyb3Igb2NjdXJyZWQgd2hpbGUgaGFuZGxpbmcgdGVtcG9yYXJ5IG5vZGVfbW9kdWxlczogJywgZSk7XG4gICAgICAgICAgICBwcm9jZXNzLmV4aXQoMSk7XG4gICAgICAgIH1cblxuICAgICAgICBsZXQgbm9kZU1vZHVsZXNSZW1vdmVkO1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgbm9kZU1vZHVsZXNSZW1vdmVkID0gYXdhaXQgdGhpcy5oYW5kbGVTdGF0ZU9mTm9kZU1vZHVsZXMoKTtcbiAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgdGhpcy5sb2cuZXJyb3IoJ2Vycm9yIG9jY3VycmVkIHdoaWxlIGNsZWFyaW5nIG5vZGVfbW9kdWxlczogJywgZSk7XG4gICAgICAgICAgICBwcm9jZXNzLmV4aXQoMSk7XG4gICAgICAgIH1cblxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgYXdhaXQgdGhpcy5yZWJ1aWxkRGVwcyh0cnVlKTtcbiAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgdGhpcy5sb2cuZXJyb3IoJ2Vycm9yIG9jY3VycmVkIHdoaWxlIGluc3RhbGxpbmcgbm9kZV9tb2R1bGVzOiAnLCBlKTtcbiAgICAgICAgICAgIHByb2Nlc3MuZXhpdCgxKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICghbm9kZU1vZHVsZXNSZW1vdmVkKSB7XG4gICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMucmVidWlsZERlcHMoKTtcbiAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmxvZy5lcnJvcignZXJyb3Igb2NjdXJyZWQgd2hpbGUgcmVidWlsZGluZyBuYXRpdmUgbm9kZSBtb2R1bGVzOiAnLCBlKTtcbiAgICAgICAgICAgICAgICBwcm9jZXNzLmV4aXQoMSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgYXdhaXQgdGhpcy5pbnN0YWxsTG9jYWxOb2RlTW9kdWxlcygpO1xuICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgICB0aGlzLmxvZy5lcnJvcignZXJyb3Igb2NjdXJyZWQgd2hpbGUgaW5zdGFsbGluZyBsb2NhbCBub2RlIG1vZHVsZXM6ICcsIGUpO1xuICAgICAgICAgICAgcHJvY2Vzcy5leGl0KDEpO1xuICAgICAgICB9XG5cblxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgYXdhaXQgdGhpcy5lbnN1cmVNZXRlb3JEZXBlbmRlbmNpZXMoKTtcbiAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgdGhpcy5sb2cuZXJyb3IoJ2Vycm9yIG9jY3VycmVkIHdoaWxlIGVuc3VyaW5nIG1ldGVvciBkZXBlbmRlbmNpZXMgYXJlIGluc3RhbGxlZDogJywgZSk7XG4gICAgICAgICAgICBwcm9jZXNzLmV4aXQoMSk7XG4gICAgICAgIH1cblxuXG4gICAgICAgIGlmICh0aGlzLiQuZW52LmlzUHJvZHVjdGlvbkJ1aWxkKCkpIHtcbiAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgYXdhaXQgdGhpcy5wYWNrU2tlbGV0b25Ub0FzYXIoKTtcbiAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmxvZy5lcnJvcignZXJyb3Igd2hpbGUgcGFja2luZyBza2VsZXRvbiB0byBhc2FyOiAnLCBlKTtcbiAgICAgICAgICAgICAgICBwcm9jZXNzLmV4aXQoMSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAvLyBUT0RPOiBmaW5kIGEgd2F5IHRvIGF2b2lkIGNvcHlpbmcgLmRlc2t0b3AgdG8gYSB0ZW1wIGxvY2F0aW9uXG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICB0aGlzLmNvcHlEZXNrdG9wVG9EZXNrdG9wVGVtcCgpO1xuICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgICB0aGlzLmxvZy5lcnJvcignZXJyb3Igd2hpbGUgY29weWluZyAuZGVza3RvcCB0byBhIHRlbXBvcmFyeSBsb2NhdGlvbjogJywgZSk7XG4gICAgICAgICAgICBwcm9jZXNzLmV4aXQoMSk7XG4gICAgICAgIH1cblxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgdGhpcy51cGRhdGVTZXR0aW5nc0pzb25GaWVsZHMoKTtcbiAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgdGhpcy5sb2cuZXJyb3IoJ2Vycm9yIHdoaWxlIHVwZGF0aW5nIHNldHRpbmdzLmpzb246ICcsIGUpO1xuICAgICAgICAgICAgcHJvY2Vzcy5leGl0KDEpO1xuICAgICAgICB9XG5cbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGF3YWl0IHRoaXMuZXhjbHVkZUZpbGVzRnJvbUFyY2hpdmUoKTtcbiAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgdGhpcy5sb2cuZXJyb3IoJ2Vycm9yIHdoaWxlIGV4Y2x1ZGluZyBmaWxlcyBmcm9tIHBhY2tpbmcgdG8gYXNhcjogJywgZSk7XG4gICAgICAgICAgICBwcm9jZXNzLmV4aXQoMSk7XG4gICAgICAgIH1cblxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgdGhpcy50cmFuc3BpbGVBbmRNaW5pZnkoKTtcbiAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgdGhpcy5sb2cuZXJyb3IoJ2Vycm9yIHdoaWxlIHRyYW5zcGlsaW5nIG9yIG1pbmlmeWluZzogJywgZSk7XG4gICAgICAgIH1cblxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgYXdhaXQgdGhpcy5wYWNrRGVza3RvcFRvQXNhcigpO1xuICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgICB0aGlzLmxvZy5lcnJvcignZXJyb3Igb2NjdXJyZWQgd2hpbGUgcGFja2luZyAuZGVza3RvcCB0byBhc2FyOiAnLCBlKTtcbiAgICAgICAgICAgIHByb2Nlc3MuZXhpdCgxKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBhd2FpdCB0aGlzLmdldE1ldGVvckNsaWVudEJ1aWxkKCk7XG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgIHRoaXMubG9nLmVycm9yKCdlcnJvciBvY2N1cnJlZCBkdXJpbmcgZ2V0dGluZyBtZXRlb3IgbW9iaWxlIGJ1aWxkOiAnLCBlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChydW4pIHtcbiAgICAgICAgICAgIHRoaXMubG9nLmluZm8oJ3J1bm5pbmcnKTtcbiAgICAgICAgICAgIHRoaXMuJC5lbGVjdHJvbi5ydW4oKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMubG9nLmluZm8oJ2J1aWx0Jyk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBFbnN1cmVzIGFsbCByZXF1aXJlZCBkZXBlbmRlbmNpZXMgYXJlIGFkZGVkIHRvIHRoZSBNZXRlb3IgcHJvamVjdC5cbiAgICAgKiBAcmV0dXJucyB7UHJvbWlzZS48dm9pZD59XG4gICAgICovXG4gICAgYXN5bmMgZW5zdXJlTWV0ZW9yRGVwZW5kZW5jaWVzKCkge1xuICAgICAgICBjb25zdCBwYWNrYWdlcyA9IFtdO1xuICAgICAgICBjb25zdCBwYWNrYWdlc1dpdGhWZXJzaW9uID0gW107XG4gICAgICAgIGxldCBwbHVnaW5zID0gJ3BsdWdpbnMgWyc7XG5cbiAgICAgICAgT2JqZWN0LmtleXModGhpcy4kLmRlc2t0b3AuZ2V0RGVwZW5kZW5jaWVzKCkucGx1Z2lucykuZm9yRWFjaCgocGx1Z2luKSA9PiB7XG4gICAgICAgICAgICAvLyBSZWFkIHBhY2thZ2UuanNvbiBvZiB0aGUgcGx1Z2luLlxuICAgICAgICAgICAgY29uc3QgcGFja2FnZUpzb24gPVxuICAgICAgICAgICAgICAgIEpTT04ucGFyc2UoXG4gICAgICAgICAgICAgICAgICAgIGZzLnJlYWRGaWxlU3luYyhcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhdGguam9pbihcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLm5vZGVNb2R1bGVzLCBwbHVnaW4sICdwYWNrYWdlLmpzb24nXG4gICAgICAgICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ3V0ZjgnXG4gICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICApO1xuXG4gICAgICAgICAgICBpZiAoJ21ldGVvckRlcGVuZGVuY2llcycgaW4gcGFja2FnZUpzb24gJiYgdHlwZW9mIHBhY2thZ2VKc29uLm1ldGVvckRlcGVuZGVuY2llcyA9PT0gJ29iamVjdCcpIHtcbiAgICAgICAgICAgICAgICBwbHVnaW5zICs9IGAke3BsdWdpbn0sIGA7XG4gICAgICAgICAgICAgICAgcGFja2FnZXMudW5zaGlmdCguLi5PYmplY3Qua2V5cyhwYWNrYWdlSnNvbi5tZXRlb3JEZXBlbmRlbmNpZXMpKTtcbiAgICAgICAgICAgICAgICBwYWNrYWdlc1dpdGhWZXJzaW9uLnVuc2hpZnQoLi4ucGFja2FnZXMubWFwKChwYWNrYWdlTmFtZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBpZiAocGFja2FnZUpzb24ubWV0ZW9yRGVwZW5kZW5jaWVzW3BhY2thZ2VOYW1lXSA9PT0gJ0B2ZXJzaW9uJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGAke3BhY2thZ2VOYW1lfUAke3BhY2thZ2VKc29uLnZlcnNpb259YDtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gYCR7cGFja2FnZU5hbWV9QCR7cGFja2FnZUpzb24ubWV0ZW9yRGVwZW5kZW5jaWVzW3BhY2thZ2VOYW1lXX1gO1xuICAgICAgICAgICAgICAgIH0pKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgaWYgKHBhY2thZ2VzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgIHBsdWdpbnMgPSBgJHtwbHVnaW5zLnN1YnN0cigwLCBwbHVnaW5zLmxlbmd0aCAtIDIpfV1gO1xuICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLiQubWV0ZW9yQXBwLm1ldGVvck1hbmFnZXIuZW5zdXJlUGFja2FnZXMoXG4gICAgICAgICAgICAgICAgICAgIHBhY2thZ2VzLCBwYWNrYWdlc1dpdGhWZXJzaW9uLCBwbHVnaW5zXG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBCdWlsZHMgbWV0ZW9yIGFwcC5cbiAgICAgKi9cbiAgICBhc3luYyBnZXRNZXRlb3JDbGllbnRCdWlsZCgpIHtcbiAgICAgICAgYXdhaXQgdGhpcy4kLm1ldGVvckFwcC5idWlsZCgpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFJlbW92ZXMgbm9kZV9tb2R1bGVzIGlmIG5lZWRlZC5cbiAgICAgKiBAcmV0dXJucyB7UHJvbWlzZTx2b2lkPn1cbiAgICAgKi9cbiAgICBhc3luYyBoYW5kbGVTdGF0ZU9mTm9kZU1vZHVsZXMoKSB7XG4gICAgICAgIGlmICh0aGlzLiQuZW52LmlzUHJvZHVjdGlvbkJ1aWxkKCkgfHwgdGhpcy4kLmVudi5vcHRpb25zLmlhMzIpIHtcbiAgICAgICAgICAgIGlmICghdGhpcy4kLmVudi5pc1Byb2R1Y3Rpb25CdWlsZCgpKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5sb2cuaW5mbygnY2xlYXJpbmcgbm9kZV9tb2R1bGVzIGJlY2F1c2Ugd2UgbmVlZCB0byBoYXZlIGl0IGNsZWFyIGZvciBpYTMyIHJlYnVpbGQnKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy5sb2cuaW5mbygnY2xlYXJpbmcgbm9kZV9tb2R1bGVzIGJlY2F1c2UgdGhpcyBpcyBhIHByb2R1Y3Rpb24gYnVpbGQnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgYXdhaXQgdGhpcy4kLnV0aWxzLnJtV2l0aFJldHJpZXMoXG4gICAgICAgICAgICAgICAgICAgICctcmYnLCB0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLm5vZGVNb2R1bGVzXG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogSWYgdGhlcmUgaXMgYSB0ZW1wb3Jhcnkgbm9kZV9tb2R1bGVzIGZvbGRlciBhbmQgbm8gbm9kZV9tb2R1bGVzIGZvbGRlciwgd2Ugd2lsbFxuICAgICAqIHJlc3RvcmUgaXQsIGFzIGl0IG1pZ2h0IGJlIGEgbGVmdG92ZXIgZnJvbSBhbiBpbnRlcnJ1cHRlZCBmbG93LlxuICAgICAqIEByZXR1cm5zIHtQcm9taXNlPHZvaWQ+fVxuICAgICAqL1xuICAgIGFzeW5jIGhhbmRsZVRlbXBvcmFyeU5vZGVNb2R1bGVzKCkge1xuICAgICAgICBpZiAodGhpcy4kLnV0aWxzLmV4aXN0cyh0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLnRtcE5vZGVNb2R1bGVzKSkge1xuICAgICAgICAgICAgaWYgKCF0aGlzLiQudXRpbHMuZXhpc3RzKHRoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAubm9kZU1vZHVsZXMpKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5sb2cuZGVidWcoJ21vdmluZyB0ZW1wIG5vZGVfbW9kdWxlcyBiYWNrJyk7XG4gICAgICAgICAgICAgICAgc2hlbGwubXYoXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAudG1wTm9kZU1vZHVsZXMsXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAubm9kZU1vZHVsZXNcbiAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAvLyBJZiB0aGVyZSBpcyBhIG5vZGVfbW9kdWxlcyBmb2xkZXIsIHdlIHNob3VsZCBjbGVhciB0aGUgdGVtcG9yYXJ5IG9uZS5cbiAgICAgICAgICAgICAgICB0aGlzLmxvZy5kZWJ1ZygnY2xlYXJpbmcgdGVtcCBub2RlX21vZHVsZXMgYmVjYXVzZSBuZXcgb25lIGlzIGFscmVhZHkgY3JlYXRlZCcpO1xuICAgICAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMuJC51dGlscy5ybVdpdGhSZXRyaWVzKFxuICAgICAgICAgICAgICAgICAgICAgICAgJy1yZicsIHRoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAudG1wTm9kZU1vZHVsZXNcbiAgICAgICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihlKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBOT1QgSU4gVVNFIFJJR0hUIE5PVyAvLyBERVBSRUNBVEVEXG4gICAgICpcbiAgICAgKiBXcmFwcGVyIGZvciBzcGF3bmluZyBucG0uXG4gICAgICogQHBhcmFtIHtBcnJheX0gIGNvbW1hbmRzIC0gY29tbWFuZHMgZm9yIHNwYXduXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHN0ZGlvXG4gICAgICogQHJldHVybiB7UHJvbWlzZX1cbiAgICAgKi9cbiAgICBydW5OcG0oY29tbWFuZHMsIHN0ZGlvID0gJ2lnbm9yZScpIHtcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgICAgICAgIC8vIFRPRE86IGZpbmQgYSB3YXkgdG8gcnVuIG5wbSB3aXRob3V0IGRlcGVuZGluZyBvbiBpdCBjYXVzZSBpdCdzIGEgaHVnZSBkZXBlbmRlbmN5LlxuICAgICAgICAgICAgbGV0IG5wbSA9IHBhdGguam9pbihcbiAgICAgICAgICAgICAgICB0aGlzLiQuZW52LnBhdGhzLm1ldGVvckFwcC5yb290LCAnbm9kZV9tb2R1bGVzJywgJy5iaW4nLCAnbnBtJ1xuICAgICAgICAgICAgKTtcblxuICAgICAgICAgICAgaWYgKCF0aGlzLiQudXRpbHMuZXhpc3RzKG5wbSkpIHtcbiAgICAgICAgICAgICAgICBucG0gPSBwYXRoLmpvaW4oXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJC5lbnYucGF0aHMubWV0ZW9yQXBwLnJvb3QsICdub2RlX21vZHVsZXMnLCAnbWV0ZW9yLWRlc2t0b3AnLCAnbm9kZV9tb2R1bGVzJywgJy5iaW4nLCAnbnBtJ1xuICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHRoaXMubG9nLnZlcmJvc2UoYGV4ZWN1dGluZyBucG0gJHtjb21tYW5kcy5qb2luKCcgJyl9YCk7XG5cbiAgICAgICAgICAgIHNwYXduKG5wbSwgY29tbWFuZHMsIHtcbiAgICAgICAgICAgICAgICBjd2Q6IHRoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAucm9vdCxcbiAgICAgICAgICAgICAgICBzdGRpb1xuICAgICAgICAgICAgfSkub24oJ2V4aXQnLCBjb2RlID0+IChcbiAgICAgICAgICAgICAgICAoY29kZSA9PT0gMCkgPyByZXNvbHZlKCkgOiByZWplY3QobmV3IEVycm9yKGBucG0gZXhpdCBjb2RlIHdhcyAke2NvZGV9YCkpXG4gICAgICAgICAgICApKTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG5cbiAgICAvKipcbiAgICAgKiBSdW5zIG5wbSBsaW5rIGZvciBldmVyeSBwYWNrYWdlIHNwZWNpZmllZCBpbiBzZXR0aW5ncy5qc29uLT5saW5rUGFja2FnZXMuXG4gICAgICovXG4gICAgYXN5bmMgbGlua05wbVBhY2thZ2VzKCkge1xuICAgICAgICBjb25zdCBzZXR0aW5ncyA9IHRoaXMuJC5kZXNrdG9wLmdldFNldHRpbmdzKCk7XG4gICAgICAgIGNvbnN0IHByb21pc2VzID0gW107XG4gICAgICAgIGlmICgnbGlua1BhY2thZ2VzJyBpbiB0aGlzLiQuZGVza3RvcC5nZXRTZXR0aW5ncygpKSB7XG4gICAgICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShzZXR0aW5ncy5saW5rUGFja2FnZXMpKSB7XG4gICAgICAgICAgICAgICAgc2V0dGluZ3MubGlua1BhY2thZ2VzLmZvckVhY2gocGFja2FnZU5hbWUgPT5cbiAgICAgICAgICAgICAgICAgICAgcHJvbWlzZXMucHVzaCh0aGlzLnJ1bk5wbShbJ2xpbmsnLCBwYWNrYWdlTmFtZV0pKSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgYXdhaXQgUHJvbWlzZS5hbGwocHJvbWlzZXMpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFJ1bnMgbnBtIGluIHRoZSBlbGVjdHJvbiBhcHAgdG8gZ2V0IHRoZSBkZXBlbmRlbmNpZXMgaW5zdGFsbGVkLlxuICAgICAqIEByZXR1cm5zIHtQcm9taXNlfVxuICAgICAqL1xuICAgIGFzeW5jIGVuc3VyZURlcHMoKSB7XG4gICAgICAgIGF3YWl0IHRoaXMubGlua05wbVBhY2thZ2VzKCk7XG5cbiAgICAgICAgdGhpcy5sb2cuaW5mbygnaW5zdGFsbGluZyBkZXBlbmRlbmNpZXMnKTtcbiAgICAgICAgaWYgKHRoaXMuJC51dGlscy5leGlzdHModGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5ub2RlTW9kdWxlcykpIHtcbiAgICAgICAgICAgIHRoaXMubG9nLmRlYnVnKCdydW5uaW5nIG5wbSBwcnVuZSB0byB3aXBlIHVubmVlZGVkIGRlcGVuZGVuY2llcycpO1xuICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLnJ1bk5wbShbJ3BydW5lJ10pO1xuICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgYXdhaXQgdGhpcy5ydW5OcG0oWydpbnN0YWxsJ10sIHRoaXMuJC5lbnYuc3RkaW8pO1xuICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoZSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBXYXJucyBpZiBwbHVnaW5zIHZlcnNpb24gYXJlIG91dGRhdGVkIGluIGNvbXBhcmUgdG8gdGhlIG5ld2VzdCBzY2FmZm9sZC5cbiAgICAgKiBAcGFyYW0ge09iamVjdH0gcGx1Z2luc1ZlcnNpb25zIC0gY3VycmVudCBwbHVnaW5zIHZlcnNpb25zIGZyb20gc2V0dGluZ3MuanNvblxuICAgICAqL1xuICAgIGNoZWNrUGx1Z2luc1ZlcnNpb24ocGx1Z2luc1ZlcnNpb25zKSB7XG4gICAgICAgIGNvbnN0IHNldHRpbmdzSnNvbiA9IEpTT04ucGFyc2UoXG4gICAgICAgICAgICBmcy5yZWFkRmlsZVN5bmMocGF0aC5qb2luKHRoaXMuJC5lbnYucGF0aHMuc2NhZmZvbGQsICdzZXR0aW5ncy5qc29uJykpXG4gICAgICAgICk7XG4gICAgICAgIGNvbnN0IHNjYWZmb2xkUGx1Z2luc1ZlcnNpb24gPSB0aGlzLiQuZGVza3RvcC5nZXREZXBlbmRlbmNpZXMoc2V0dGluZ3NKc29uLCBmYWxzZSkucGx1Z2lucztcbiAgICAgICAgT2JqZWN0LmtleXMocGx1Z2luc1ZlcnNpb25zKS5mb3JFYWNoKChwbHVnaW5OYW1lKSA9PiB7XG4gICAgICAgICAgICBpZiAocGx1Z2luTmFtZSBpbiBzY2FmZm9sZFBsdWdpbnNWZXJzaW9uICYmXG4gICAgICAgICAgICAgICAgc2NhZmZvbGRQbHVnaW5zVmVyc2lvbltwbHVnaW5OYW1lXSAhPT0gcGx1Z2luc1ZlcnNpb25zW3BsdWdpbk5hbWVdICYmXG4gICAgICAgICAgICAgICAgc2VtdmVyLmx0KHBsdWdpbnNWZXJzaW9uc1twbHVnaW5OYW1lXSwgc2NhZmZvbGRQbHVnaW5zVmVyc2lvbltwbHVnaW5OYW1lXSlcbiAgICAgICAgICAgICkge1xuICAgICAgICAgICAgICAgIHRoaXMubG9nLndhcm4oYHlvdSBhcmUgdXNpbmcgb3V0ZGF0ZWQgdmVyc2lvbiAke3BsdWdpbnNWZXJzaW9uc1twbHVnaW5OYW1lXX0gb2YgYCArXG4gICAgICAgICAgICAgICAgICAgIGAke3BsdWdpbk5hbWV9LCB0aGUgc3VnZ2VzdGVkIHZlcnNpb24gdG8gdXNlIGlzIGAgK1xuICAgICAgICAgICAgICAgICAgICBgJHtzY2FmZm9sZFBsdWdpbnNWZXJzaW9uW3BsdWdpbk5hbWVdfWApO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBNZXJnZXMgY29yZSBkZXBlbmRlbmN5IGxpc3Qgd2l0aCB0aGUgZGVwZW5kZW5jaWVzIGZyb20gLmRlc2t0b3AuXG4gICAgICovXG4gICAgdXBkYXRlRGVwZW5kZW5jaWVzTGlzdCgpIHtcbiAgICAgICAgdGhpcy5sb2cuaW5mbygndXBkYXRpbmcgbGlzdCBvZiBwYWNrYWdlLmpzb25cXCdzIGRlcGVuZGVuY2llcycpO1xuICAgICAgICBjb25zdCBkZXNrdG9wRGVwZW5kZW5jaWVzID0gdGhpcy4kLmRlc2t0b3AuZ2V0RGVwZW5kZW5jaWVzKCk7XG5cbiAgICAgICAgdGhpcy5jaGVja1BsdWdpbnNWZXJzaW9uKGRlc2t0b3BEZXBlbmRlbmNpZXMucGx1Z2lucyk7XG5cbiAgICAgICAgdGhpcy5sb2cuZGVidWcoJ21lcmdpbmcgc2V0dGluZ3MuanNvbltkZXBlbmRlbmNpZXNdJyk7XG4gICAgICAgIHRoaXMuZGVwc01hbmFnZXIubWVyZ2VEZXBlbmRlbmNpZXMoXG4gICAgICAgICAgICAnc2V0dGluZ3MuanNvbltkZXBlbmRlbmNpZXNdJyxcbiAgICAgICAgICAgIGRlc2t0b3BEZXBlbmRlbmNpZXMuZnJvbVNldHRpbmdzXG4gICAgICAgICk7XG4gICAgICAgIHRoaXMubG9nLmRlYnVnKCdtZXJnaW5nIHNldHRpbmdzLmpzb25bcGx1Z2luc10nKTtcbiAgICAgICAgdGhpcy5kZXBzTWFuYWdlci5tZXJnZURlcGVuZGVuY2llcyhcbiAgICAgICAgICAgICdzZXR0aW5ncy5qc29uW3BsdWdpbnNdJyxcbiAgICAgICAgICAgIGRlc2t0b3BEZXBlbmRlbmNpZXMucGx1Z2luc1xuICAgICAgICApO1xuXG4gICAgICAgIHRoaXMubG9nLmRlYnVnKCdtZXJnaW5nIGRlcGVuZGVuY2llcyBmcm9tIG1vZHVsZXMnKTtcbiAgICAgICAgT2JqZWN0LmtleXMoZGVza3RvcERlcGVuZGVuY2llcy5tb2R1bGVzKS5mb3JFYWNoKG1vZHVsZSA9PlxuICAgICAgICAgICAgdGhpcy5kZXBzTWFuYWdlci5tZXJnZURlcGVuZGVuY2llcyhcbiAgICAgICAgICAgICAgICBgbW9kdWxlWyR7bW9kdWxlfV1gLFxuICAgICAgICAgICAgICAgIGRlc2t0b3BEZXBlbmRlbmNpZXMubW9kdWxlc1ttb2R1bGVdXG4gICAgICAgICAgICApKTtcblxuICAgICAgICB0aGlzLnBhY2thZ2VKc29uLmRlcGVuZGVuY2llcyA9IHRoaXMuZGVwc01hbmFnZXIuZ2V0UmVtb3RlRGVwZW5kZW5jaWVzKCk7XG4gICAgICAgIHRoaXMucGFja2FnZUpzb24ubG9jYWxEZXBlbmRlbmNpZXMgPSB0aGlzLmRlcHNNYW5hZ2VyLmdldExvY2FsRGVwZW5kZW5jaWVzKCk7XG5cbiAgICAgICAgdGhpcy5sb2cuZGVidWcoJ3dyaXRpbmcgdXBkYXRlZCBwYWNrYWdlLmpzb24nKTtcbiAgICAgICAgZnMud3JpdGVGaWxlU3luYyhcbiAgICAgICAgICAgIHRoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAucGFja2FnZUpzb24sIEpTT04uc3RyaW5naWZ5KHRoaXMucGFja2FnZUpzb24sIG51bGwsIDIpXG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogSW5zdGFsbCBub2RlIG1vZHVsZXMgZnJvbSBsb2NhbCBwYXRocyB1c2luZyBsb2NhbC1pbnN0YWxsLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGFyY2hcbiAgICAgKiBAcmV0dXJucyB7UHJvbWlzZX1cbiAgICAgKi9cbiAgICBpbnN0YWxsTG9jYWxOb2RlTW9kdWxlcyhhcmNoID0gdGhpcy4kLmVudi5vcHRpb25zLmlhMzIgfHwgcHJvY2Vzcy5hcmNoID09PSAnaWEzMicgPyAnaWEzMicgOiAneDY0Jykge1xuICAgICAgICBjb25zdCBsb2NhbERlcGVuZGVuY2llcyA9IF8udmFsdWVzKHRoaXMucGFja2FnZUpzb24ubG9jYWxEZXBlbmRlbmNpZXMpO1xuICAgICAgICBpZiAobG9jYWxEZXBlbmRlbmNpZXMubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKCk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5sb2cuaW5mbygnaW5zdGFsbGluZyBsb2NhbCBub2RlIG1vZHVsZXMnKTtcbiAgICAgICAgY29uc3QgbGFzdFJlYnVpbGQgPSB0aGlzLiQuZWxlY3Ryb25CdWlsZGVyLnByZXBhcmVMYXN0UmVidWlsZE9iamVjdChhcmNoKTtcbiAgICAgICAgY29uc3QgZW52ID0gZ2V0R3lwRW52KGxhc3RSZWJ1aWxkLmZyYW1ld29ya0luZm8sIGxhc3RSZWJ1aWxkLnBsYXRmb3JtLCBsYXN0UmVidWlsZC5hcmNoKTtcbiAgICAgICAgY29uc3QgaW5zdGFsbGVyID0gbmV3IExvY2FsSW5zdGFsbGVyKFxuICAgICAgICAgICAgeyBbdGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5yb290XTogbG9jYWxEZXBlbmRlbmNpZXMgfSxcbiAgICAgICAgICAgIHsgbnBtRW52OiBlbnYgfVxuICAgICAgICApO1xuICAgICAgICBwcm9ncmVzcyhpbnN0YWxsZXIpO1xuICAgICAgICByZXR1cm4gaW5zdGFsbGVyLmluc3RhbGwoKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBSZWJ1aWxkIGJpbmFyeSBkZXBlbmRlbmNpZXMgYWdhaW5zdCBFbGVjdHJvbidzIG5vZGUgaGVhZGVycy5cbiAgICAgKiBAcmV0dXJucyB7UHJvbWlzZX1cbiAgICAgKi9cbiAgICByZWJ1aWxkRGVwcyhpbnN0YWxsID0gZmFsc2UpIHtcbiAgICAgICAgaWYgKGluc3RhbGwpIHtcbiAgICAgICAgICAgIHRoaXMubG9nLmluZm8oJ2lzc3Vpbmcgbm9kZV9tb2R1bGVzIGluc3RhbGwgZnJvbSBlbGVjdHJvbi1idWlsZGVyJyk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLmxvZy5pbmZvKCdpc3N1aW5nIG5hdGl2ZSBtb2R1bGVzIHJlYnVpbGQgZnJvbSBlbGVjdHJvbi1idWlsZGVyJyk7XG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCBhcmNoID0gdGhpcy4kLmVudi5vcHRpb25zLmlhMzIgfHwgcHJvY2Vzcy5hcmNoID09PSAnaWEzMicgPyAnaWEzMicgOiAneDY0JztcblxuICAgICAgICBpZiAodGhpcy4kLmVudi5vcHRpb25zLmlhMzIpIHtcbiAgICAgICAgICAgIHRoaXMubG9nLnZlcmJvc2UoJ2ZvcmNpbmcgcmVidWlsZCBmb3IgMzJiaXQnKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMubG9nLnZlcmJvc2UoYHJlYnVpbGRpbmcgZm9yICR7YXJjaH1gKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiB0aGlzLiQuZWxlY3Ryb25CdWlsZGVyLmluc3RhbGxPclJlYnVpbGQoYXJjaCwgdW5kZWZpbmVkLCBpbnN0YWxsKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBVcGRhdGUgcGFja2FnZS5qc29uIGZpZWxkcyBhY2NvcmRpbmdseSB0byB3aGF0IGlzIHNldCBpbiBzZXR0aW5ncy5qc29uLlxuICAgICAqXG4gICAgICogcGFja2FnZUpzb24ubmFtZSA9IHNldHRpbmdzLnByb2plY3ROYW1lXG4gICAgICogcGFja2FnZUpzb24udmVyc2lvbiA9IHNldHRpbmdzLnZlcnNpb25cbiAgICAgKiBwYWNrYWdlSnNvbi4qID0gc2V0dGluZ3MucGFja2FnZUpzb25GaWVsZHNcbiAgICAgKi9cbiAgICB1cGRhdGVQYWNrYWdlSnNvbkZpZWxkcygpIHtcbiAgICAgICAgdGhpcy5sb2cudmVyYm9zZSgndXBkYXRpbmcgcGFja2FnZS5qc29uIGZpZWxkcycpO1xuICAgICAgICBjb25zdCBzZXR0aW5ncyA9IHRoaXMuJC5kZXNrdG9wLmdldFNldHRpbmdzKCk7XG4gICAgICAgIC8qKiBAdHlwZSB7ZGVza3RvcFNldHRpbmdzfSAqL1xuICAgICAgICBjb25zdCBwYWNrYWdlSnNvbiA9IHRoaXMuc2NhZmZvbGQuZ2V0RGVmYXVsdFBhY2thZ2VKc29uKCk7XG5cbiAgICAgICAgcGFja2FnZUpzb24udmVyc2lvbiA9IHNldHRpbmdzLnZlcnNpb247XG4gICAgICAgIGlmICgncGFja2FnZUpzb25GaWVsZHMnIGluIHNldHRpbmdzKSB7XG4gICAgICAgICAgICBhc3NpZ25JbihwYWNrYWdlSnNvbiwgc2V0dGluZ3MucGFja2FnZUpzb25GaWVsZHMpO1xuICAgICAgICB9XG4gICAgICAgIGFzc2lnbkluKHBhY2thZ2VKc29uLCB7IG5hbWU6IHNldHRpbmdzLnByb2plY3ROYW1lIH0pO1xuXG4gICAgICAgIHRoaXMubG9nLmRlYnVnKCd3cml0aW5nIHVwZGF0ZWQgcGFja2FnZS5qc29uJyk7XG4gICAgICAgIGZzLndyaXRlRmlsZVN5bmMoXG4gICAgICAgICAgICB0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLnBhY2thZ2VKc29uLCBKU09OLnN0cmluZ2lmeShwYWNrYWdlSnNvbiwgbnVsbCwgNClcbiAgICAgICAgKTtcbiAgICAgICAgdGhpcy5wYWNrYWdlSnNvbiA9IHBhY2thZ2VKc29uO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFVwZGF0ZXMgc2V0dGluZ3MuanNvbiB3aXRoIGVudiAocHJvZC9kZXYpIGluZm9ybWF0aW9uIGFuZCB2ZXJzaW9ucy5cbiAgICAgKi9cbiAgICB1cGRhdGVTZXR0aW5nc0pzb25GaWVsZHMoKSB7XG4gICAgICAgIHRoaXMubG9nLmRlYnVnKCd1cGRhdGluZyBzZXR0aW5ncy5qc29uIGZpZWxkcycpO1xuICAgICAgICBjb25zdCBzZXR0aW5ncyA9IHRoaXMuJC5kZXNrdG9wLmdldFNldHRpbmdzKCk7XG5cbiAgICAgICAgLy8gU2F2ZSB2ZXJzaW9ucy5cbiAgICAgICAgc2V0dGluZ3MuY29tcGF0aWJpbGl0eVZlcnNpb24gPSB0aGlzLmNvbXBhdGliaWxpdHlWZXJzaW9uO1xuXG4gICAgICAgIC8vIFBhc3MgaW5mb3JtYXRpb24gYWJvdXQgYnVpbGQgdHlwZSB0byB0aGUgc2V0dGluZ3MuanNvbi5cbiAgICAgICAgc2V0dGluZ3MuZW52ID0gKHRoaXMuJC5lbnYuaXNQcm9kdWN0aW9uQnVpbGQoKSkgP1xuICAgICAgICAgICAgJ3Byb2QnIDogJ2Rldic7XG5cbiAgICAgICAgc2V0dGluZ3MuZGVza3RvcFZlcnNpb24gPSBgJHt0aGlzLiQuZGVza3RvcC5nZXRIYXNoVmVyc2lvbigpfV8ke3NldHRpbmdzLmVudn1gO1xuXG4gICAgICAgIHNldHRpbmdzLm1ldGVvckRlc2t0b3BWZXJzaW9uID0gdGhpcy4kLmdldFZlcnNpb24oKTtcblxuICAgICAgICBmcy53cml0ZUZpbGVTeW5jKFxuICAgICAgICAgICAgdGhpcy4kLmVudi5wYXRocy5kZXNrdG9wVG1wLnNldHRpbmdzLCBKU09OLnN0cmluZ2lmeShzZXR0aW5ncywgbnVsbCwgNClcbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBDb3BpZXMgZmlsZXMgZnJvbSBwcmVwYXJlZCAuZGVza3RvcCB0byBkZXNrdG9wLmFzYXIgaW4gZWxlY3Ryb24gYXBwLlxuICAgICAqL1xuICAgIHBhY2tEZXNrdG9wVG9Bc2FyKCkge1xuICAgICAgICB0aGlzLmxvZy5pbmZvKCdwYWNraW5nIC5kZXNrdG9wIHRvIGFzYXInKTtcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgICAgICAgIGFzYXIuY3JlYXRlUGFja2FnZShcbiAgICAgICAgICAgICAgICB0aGlzLiQuZW52LnBhdGhzLmRlc2t0b3BUbXAucm9vdCxcbiAgICAgICAgICAgICAgICB0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLmRlc2t0b3BBc2FyLFxuICAgICAgICAgICAgICAgICgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2cudmVyYm9zZSgnY2xlYXJpbmcgdGVtcG9yYXJ5IC5kZXNrdG9wJyk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJC51dGlsc1xuICAgICAgICAgICAgICAgICAgICAgICAgLnJtV2l0aFJldHJpZXMoJy1yZicsIHRoaXMuJC5lbnYucGF0aHMuZGVza3RvcFRtcC5yb290KVxuICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc29sdmUoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAuY2F0Y2goKGUpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZWplY3QoZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIE1ha2VzIGEgdGVtcG9yYXJ5IGNvcHkgb2YgLmRlc2t0b3AuXG4gICAgICovXG4gICAgY29weURlc2t0b3BUb0Rlc2t0b3BUZW1wKCkge1xuICAgICAgICB0aGlzLmxvZy52ZXJib3NlKCdjb3B5aW5nIC5kZXNrdG9wIHRvIHRlbXBvcmFyeSBsb2NhdGlvbicpO1xuICAgICAgICBzaGVsbC5jcCgnLXJmJywgdGhpcy4kLmVudi5wYXRocy5kZXNrdG9wLnJvb3QsIHRoaXMuJC5lbnYucGF0aHMuZGVza3RvcFRtcC5yb290KTtcbiAgICAgICAgLy8gUmVtb3ZlIHRlc3QgZmlsZXMuXG4gICAgICAgIGRlbC5zeW5jKFtcbiAgICAgICAgICAgIHBhdGguam9pbih0aGlzLiQuZW52LnBhdGhzLmRlc2t0b3BUbXAucm9vdCwgJyoqJywgJyoudGVzdC5qcycpXG4gICAgICAgIF0pO1xuICAgIH1cblxuXG4gICAgLyoqXG4gICAgICogUnVucyBiYWJlbCBhbmQgdWdsaWZ5IG92ZXIgLmRlc2t0b3AgaWYgcmVxdWVzdGVkLlxuICAgICAqL1xuICAgIHRyYW5zcGlsZUFuZE1pbmlmeSgpIHtcbiAgICAgICAgdGhpcy5sb2cuaW5mbygndHJhbnNwaWxpbmcgYW5kIHVnbGlmeWluZycpO1xuXG4gICAgICAgIGNvbnN0IHNldHRpbmdzID0gdGhpcy4kLmRlc2t0b3AuZ2V0U2V0dGluZ3MoKTtcbiAgICAgICAgY29uc3Qgb3B0aW9ucyA9ICd1Z2xpZnlPcHRpb25zJyBpbiBzZXR0aW5ncyA/IHNldHRpbmdzLnVnbGlmeU9wdGlvbnMgOiB7fTtcblxuICAgICAgICBjb25zdCB1Z2xpZnlpbmdFbmFibGVkID0gJ3VnbGlmeScgaW4gc2V0dGluZ3MgJiYgISFzZXR0aW5ncy51Z2xpZnk7XG5cbiAgICAgICAgY29uc3QgcHJlc2V0ID0gcHJlc2V0RW52KHVuZGVmaW5lZCwgeyB0YXJnZXRzOiB7IG5vZGU6ICc3JyB9IH0pO1xuXG4gICAgICAgIGdsb2Iuc3luYyhgJHt0aGlzLiQuZW52LnBhdGhzLmRlc2t0b3BUbXAucm9vdH0vKiovKi5qc2ApLmZvckVhY2goKGZpbGUpID0+IHtcbiAgICAgICAgICAgIGxldCB7IGNvZGUgfSA9IHRyYW5zZm9ybUZpbGVTeW5jKGZpbGUsIHtcbiAgICAgICAgICAgICAgICBwcmVzZXRzOiBbcHJlc2V0XVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBsZXQgZXJyb3I7XG4gICAgICAgICAgICBpZiAoc2V0dGluZ3MuZW52ID09PSAncHJvZCcgJiYgdWdsaWZ5aW5nRW5hYmxlZCkge1xuICAgICAgICAgICAgICAgICh7IGNvZGUsIGVycm9yIH0gPSB1Z2xpZnkubWluaWZ5KGNvZGUsIG9wdGlvbnMpKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChlcnJvcikge1xuICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihlcnJvcik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBmcy53cml0ZUZpbGVTeW5jKGZpbGUsIGNvZGUpO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBNb3ZlcyBhbGwgdGhlIGZpbGVzIHRoYXQgc2hvdWxkIG5vdCBiZSBwYWNrZWQgaW50byBhc2FyIGludG8gYSBzYWZlIGxvY2F0aW9uIHdoaWNoIGlzIHRoZVxuICAgICAqICdleHRyYWN0ZWQnIGRpciBpbiB0aGUgZWxlY3Ryb24gYXBwLlxuICAgICAqL1xuICAgIGFzeW5jIGV4Y2x1ZGVGaWxlc0Zyb21BcmNoaXZlKCkge1xuICAgICAgICB0aGlzLmxvZy5pbmZvKCdleGNsdWRpbmcgZmlsZXMgZnJvbSBwYWNraW5nJyk7XG5cbiAgICAgICAgLy8gRW5zdXJlIGVtcHR5IGBleHRyYWN0ZWRgIGRpclxuXG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBhd2FpdCB0aGlzLiQudXRpbHMucm1XaXRoUmV0cmllcygnLXJmJywgdGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5leHRyYWN0ZWQpO1xuICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoZSk7XG4gICAgICAgIH1cblxuICAgICAgICBzaGVsbC5ta2Rpcih0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLmV4dHJhY3RlZCk7XG5cbiAgICAgICAgY29uc3QgY29uZmlncyA9IHRoaXMuJC5kZXNrdG9wLmdhdGhlck1vZHVsZUNvbmZpZ3MoKTtcblxuICAgICAgICAvLyBNb3ZlIGZpbGVzIHRoYXQgc2hvdWxkIG5vdCBiZSBhc2FyJ2VkLlxuICAgICAgICBjb25maWdzLmZvckVhY2goKGNvbmZpZykgPT4ge1xuICAgICAgICAgICAgY29uc3QgbW9kdWxlQ29uZmlnID0gY29uZmlnO1xuICAgICAgICAgICAgaWYgKCdleHRyYWN0JyBpbiBtb2R1bGVDb25maWcpIHtcbiAgICAgICAgICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkobW9kdWxlQ29uZmlnLmV4dHJhY3QpKSB7XG4gICAgICAgICAgICAgICAgICAgIG1vZHVsZUNvbmZpZy5leHRyYWN0ID0gW21vZHVsZUNvbmZpZy5leHRyYWN0XTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgbW9kdWxlQ29uZmlnLmV4dHJhY3QuZm9yRWFjaCgoZmlsZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvZy5kZWJ1ZyhgZXhjbHVkaW5nICR7ZmlsZX0gZnJvbSAke2NvbmZpZy5uYW1lfWApO1xuICAgICAgICAgICAgICAgICAgICBjb25zdCBmaWxlUGF0aCA9IHBhdGguam9pbihcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJC5lbnYucGF0aHMuZGVza3RvcFRtcC5tb2R1bGVzLCBtb2R1bGVDb25maWcuZGlyTmFtZSwgZmlsZVxuICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgICAgICBjb25zdCBkZXN0aW5hdGlvblBhdGggPSBwYXRoLmpvaW4oXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLmV4dHJhY3RlZCwgbW9kdWxlQ29uZmlnLmRpck5hbWVcbiAgICAgICAgICAgICAgICAgICAgKTtcblxuICAgICAgICAgICAgICAgICAgICBpZiAoIXRoaXMuJC51dGlscy5leGlzdHMoZGVzdGluYXRpb25QYXRoKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgc2hlbGwubWtkaXIoZGVzdGluYXRpb25QYXRoKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBzaGVsbC5tdihmaWxlUGF0aCwgZGVzdGluYXRpb25QYXRoKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxufVxuIl19