"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _path = _interopRequireDefault(require("path"));

var _os = _interopRequireDefault(require("os"));

var _assignIn = _interopRequireDefault(require("lodash/assignIn"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var join = _path.default.join;
/**
 * @class
 * @property {packagePaths} paths
 */

var Env =
/*#__PURE__*/
function () {
  function Env(input, output, options) {
    _classCallCheck(this, Env);

    this.options = options;

    if (this.isProductionBuild()) {
      process.env.NODE_ENV = 'production';
    }

    this.sys = {
      platform: process.platform,
      arch: process.arch
    }; // Operational System.

    this.os = {
      isWindows: process.platform === 'win32',
      isLinux: process.platform === 'linux',
      isOsx: process.platform === 'darwin'
    };
    this.stdio = 'inherit';
    this.os.name = this.sys.platform === 'darwin' ? 'osx' : this.sys.platform;
    this.os.home = process.env[this.os.isWindows ? 'USERPROFILE' : 'HOME'];
    this.os.tmp = _os.default.tmpdir();
    /** @type {packagePaths} */

    this.paths = {};
    /** @type {meteorDesktopPaths} */

    this.paths.meteorDesktop = {
      root: _path.default.resolve(__dirname, '..')
    };
    this.paths.meteorDesktop.skeleton = join(this.paths.meteorDesktop.root, 'skeleton');
    /** @type {meteorAppPaths} */

    this.paths.meteorApp = {
      root: input
    };
    /** @type {desktopPaths} */

    this.paths.desktop = {
      rootName: '.desktop',
      root: join(this.paths.meteorApp.root, '.desktop')
    };
    (0, _assignIn.default)(this.paths.desktop, {
      modules: join(this.paths.desktop.root, 'modules'),
      import: join(this.paths.desktop.root, 'import'),
      assets: join(this.paths.desktop.root, 'assets'),
      settings: join(this.paths.desktop.root, 'settings.json'),
      desktop: join(this.paths.desktop.root, 'desktop.js')
    });
    this.paths.desktop.splashScreen = join(this.paths.desktop.assets, 'splashScreen.png');
    this.paths.desktop.loadingGif = join(this.paths.desktop.assets, 'loading.gif');
    this.paths.desktop.meteorIco = join(this.paths.desktop.assets, 'meteor.ico');
    /** @type {electronAppPaths} */

    this.paths.electronApp = {
      rootName: 'desktop-build'
    };
    this.paths.electronApp.root = join(this.paths.meteorApp.root, '.meteor', this.paths.electronApp.rootName);
    this.paths.electronApp.tmpNodeModules = join(this.paths.meteorApp.root, '.meteor', '.desktop_node_modules');
    this.paths.electronApp.extractedNodeModules = join(this.paths.meteorApp.root, '.meteor', '.desktop_extracted_node_modules');
    this.paths.electronApp.extractedNodeModulesBin = join(this.paths.electronApp.extractedNodeModules, '.bin');
    this.paths.electronApp.appRoot = join(this.paths.electronApp.root, 'app');
    (0, _assignIn.default)(this.paths.electronApp, {
      app: join(this.paths.electronApp.appRoot, 'app.js'),
      cordova: join(this.paths.electronApp.appRoot, 'cordova.js'),
      index: join(this.paths.electronApp.appRoot, 'index.js'),
      preload: join(this.paths.electronApp.appRoot, 'preload.js'),
      modules: join(this.paths.electronApp.appRoot, 'modules'),
      desktopAsar: join(this.paths.electronApp.root, 'desktop.asar'),
      extracted: join(this.paths.electronApp.root, 'extracted'),
      appAsar: join(this.paths.electronApp.root, 'app.asar'),
      import: join(this.paths.electronApp.root, 'import'),
      assets: join(this.paths.electronApp.root, 'assets'),
      packageJson: join(this.paths.electronApp.root, 'package.json'),
      settings: join(this.paths.electronApp.root, 'settings.json'),
      desktop: join(this.paths.electronApp.root, 'desktop.js'),
      desktopTmp: join(this.paths.electronApp.root, '__desktop'),
      nodeModules: join(this.paths.electronApp.root, 'node_modules'),
      meteorAsar: join(this.paths.electronApp.root, 'meteor.asar'),
      meteorApp: join(this.paths.electronApp.root, 'meteor'),
      meteorAppIndex: join(this.paths.electronApp.root, 'meteor', 'index.html'),
      meteorAppProgramJson: join(this.paths.electronApp.root, 'meteor', 'program.json'),
      skeleton: join(this.paths.electronApp.root, 'skeleton')
    });
    (0, _assignIn.default)(this.paths.meteorApp, {
      platforms: join(this.paths.meteorApp.root, '.meteor', 'platforms'),
      packages: join(this.paths.meteorApp.root, '.meteor', 'packages'),
      versions: join(this.paths.meteorApp.root, '.meteor', 'versions'),
      release: join(this.paths.meteorApp.root, '.meteor', 'release'),
      packageJson: join(this.paths.meteorApp.root, 'package.json'),
      gitIgnore: join(this.paths.meteorApp.root, '.meteor', '.gitignore'),
      cordovaBuild: join(this.paths.meteorApp.root, '.meteor', 'local', 'cordova-build', 'www', 'application'),
      webCordova: join(this.paths.meteorApp.root, '.meteor', 'local', 'build', 'programs', 'web.cordova')
    });
    (0, _assignIn.default)(this.paths.meteorApp, {
      cordovaBuildIndex: join(this.paths.meteorApp.cordovaBuild, 'index.html'),
      cordovaBuildProgramJson: join(this.paths.meteorApp.cordovaBuild, 'program.json')
    });
    (0, _assignIn.default)(this.paths.meteorApp, {
      webCordovaProgramJson: join(this.paths.meteorApp.webCordova, 'program.json')
    });
    /** @type {desktopTmpPaths} */

    this.paths.desktopTmp = {
      root: join(this.paths.electronApp.root, '__desktop')
    };
    (0, _assignIn.default)(this.paths.desktopTmp, {
      modules: join(this.paths.desktopTmp.root, 'modules'),
      settings: join(this.paths.desktopTmp.root, 'settings.json')
    });
    this.paths.packageDir = '.desktop-package';
    this.paths.installerDir = '.desktop-installer'; // Scaffold

    this.paths.scaffold = join(__dirname, '..', 'scaffold');
  }
  /**
   * @returns {boolean|*}
   * @public
   */


  _createClass(Env, [{
    key: "isProductionBuild",
    value: function isProductionBuild() {
      return !!('production' in this.options && this.options.production);
    }
  }]);

  return Env;
}();

exports.default = Env;
module.exports = Env;
/**
 * @typedef {Object} desktopPaths
 * @property {string} rootName
 * @property {string} root
 * @property {string} modules
 * @property {string} import
 * @property {string} assets
 * @property {string} settings
 * @property {string} desktop
 * @property {string} splashScreen
 * @property {string} loadingGif
 * @property {string} meteorIco
 */

/**
 * @typedef {Object} meteorAppPaths
 * @property {string} root
 * @property {string} platforms
 * @property {string} release
 * @property {string} packages
 * @property {string} versions
 * @property {string} gitIgnore
 * @property {string} packageJson
 * @property {string} cordovaBuild
 * @property {string} cordovaBuildIndex
 * @property {string} cordovaBuildProgramJson
 * @property {string} webCordova
 * @property {string} webCordovaIndex
 * @property {string} webCordovaProgramJson
 */

/**
 * @typedef {Object} electronAppPaths
 * @property {string} rootName
 * @property {string} root
 * @property {Object} appRoot
 * @property {string} appRoot.cordova
 * @property {string} appRoot.index
 * @property {string} appRoot.app
 * @property {string} appRoot.modules
 * @property {string} desktopAsar
 * @property {string} extracted
 * @property {string} appAsar
 * @property {string} preload
 * @property {string} import
 * @property {string} assets
 * @property {string} gitIgnore
 * @property {string} packageJson
 * @property {string} settings
 * @property {string} desktop
 * @property {string} desktopTmp
 * @property {string} nodeModules
 * @property {string} meteorAsar
 * @property {string} meteorApp
 * @property {string} meteorAppIndex
 * @property {string} meteorAppProgramJson
 * @property {string} skeleton
 * @property {string} tmpNodeModules
 * @property {string} extractedNodeModules
 * @property {string} extractedNodeModulesBin
 */

/**
 * @typedef {Object} desktopTmpPaths
 * @property {string} root
 * @property {string} modules
 * @property {string} settings
 */

/**
 * @typedef {Object} meteorDesktopPaths
 * @property {string} root
 * @property {string} skeleton
 */

/**
 * @typedef {Object} packagePaths
 * @property {meteorAppPaths} meteorApp
 * @property {desktopPaths} desktop
 * @property {electronAppPaths} electronApp
 * @property {desktopTmpPaths} desktopTmp
 * @property {meteorDesktopPaths} meteorDesktop
 * @property {string} packageDir
 * @property {string} scaffold
 */
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL2xpYi9lbnYuanMiXSwibmFtZXMiOlsiam9pbiIsIkVudiIsImlucHV0Iiwib3V0cHV0Iiwib3B0aW9ucyIsImlzUHJvZHVjdGlvbkJ1aWxkIiwicHJvY2VzcyIsImVudiIsIk5PREVfRU5WIiwic3lzIiwicGxhdGZvcm0iLCJhcmNoIiwib3MiLCJpc1dpbmRvd3MiLCJpc0xpbnV4IiwiaXNPc3giLCJzdGRpbyIsIm5hbWUiLCJob21lIiwidG1wIiwidG1wZGlyIiwicGF0aHMiLCJtZXRlb3JEZXNrdG9wIiwicm9vdCIsInJlc29sdmUiLCJfX2Rpcm5hbWUiLCJza2VsZXRvbiIsIm1ldGVvckFwcCIsImRlc2t0b3AiLCJyb290TmFtZSIsIm1vZHVsZXMiLCJpbXBvcnQiLCJhc3NldHMiLCJzZXR0aW5ncyIsInNwbGFzaFNjcmVlbiIsImxvYWRpbmdHaWYiLCJtZXRlb3JJY28iLCJlbGVjdHJvbkFwcCIsInRtcE5vZGVNb2R1bGVzIiwiZXh0cmFjdGVkTm9kZU1vZHVsZXMiLCJleHRyYWN0ZWROb2RlTW9kdWxlc0JpbiIsImFwcFJvb3QiLCJhcHAiLCJjb3Jkb3ZhIiwiaW5kZXgiLCJwcmVsb2FkIiwiZGVza3RvcEFzYXIiLCJleHRyYWN0ZWQiLCJhcHBBc2FyIiwicGFja2FnZUpzb24iLCJkZXNrdG9wVG1wIiwibm9kZU1vZHVsZXMiLCJtZXRlb3JBc2FyIiwibWV0ZW9yQXBwSW5kZXgiLCJtZXRlb3JBcHBQcm9ncmFtSnNvbiIsInBsYXRmb3JtcyIsInBhY2thZ2VzIiwidmVyc2lvbnMiLCJyZWxlYXNlIiwiZ2l0SWdub3JlIiwiY29yZG92YUJ1aWxkIiwid2ViQ29yZG92YSIsImNvcmRvdmFCdWlsZEluZGV4IiwiY29yZG92YUJ1aWxkUHJvZ3JhbUpzb24iLCJ3ZWJDb3Jkb3ZhUHJvZ3JhbUpzb24iLCJwYWNrYWdlRGlyIiwiaW5zdGFsbGVyRGlyIiwic2NhZmZvbGQiLCJwcm9kdWN0aW9uIiwibW9kdWxlIiwiZXhwb3J0cyJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUNBOzs7Ozs7Ozs7O0lBRVFBLEksaUJBQUFBLEk7QUFFUjs7Ozs7SUFJcUJDLEc7OztBQUNqQixlQUFZQyxLQUFaLEVBQW1CQyxNQUFuQixFQUEyQkMsT0FBM0IsRUFBb0M7QUFBQTs7QUFDaEMsU0FBS0EsT0FBTCxHQUFlQSxPQUFmOztBQUVBLFFBQUksS0FBS0MsaUJBQUwsRUFBSixFQUE4QjtBQUMxQkMsY0FBUUMsR0FBUixDQUFZQyxRQUFaLEdBQXVCLFlBQXZCO0FBQ0g7O0FBRUQsU0FBS0MsR0FBTCxHQUFXO0FBQ1BDLGdCQUFVSixRQUFRSSxRQURYO0FBRVBDLFlBQU1MLFFBQVFLO0FBRlAsS0FBWCxDQVBnQyxDQVloQzs7QUFDQSxTQUFLQyxFQUFMLEdBQVU7QUFDTkMsaUJBQVlQLFFBQVFJLFFBQVIsS0FBcUIsT0FEM0I7QUFFTkksZUFBVVIsUUFBUUksUUFBUixLQUFxQixPQUZ6QjtBQUdOSyxhQUFRVCxRQUFRSSxRQUFSLEtBQXFCO0FBSHZCLEtBQVY7QUFNQSxTQUFLTSxLQUFMLEdBQWEsU0FBYjtBQUVBLFNBQUtKLEVBQUwsQ0FBUUssSUFBUixHQUFnQixLQUFLUixHQUFMLENBQVNDLFFBQVQsS0FBc0IsUUFBdEIsR0FBaUMsS0FBakMsR0FBeUMsS0FBS0QsR0FBTCxDQUFTQyxRQUFsRTtBQUNBLFNBQUtFLEVBQUwsQ0FBUU0sSUFBUixHQUFlWixRQUFRQyxHQUFSLENBQWEsS0FBS0ssRUFBTCxDQUFRQyxTQUFSLEdBQW9CLGFBQXBCLEdBQW9DLE1BQWpELENBQWY7QUFDQSxTQUFLRCxFQUFMLENBQVFPLEdBQVIsR0FBYyxZQUFHQyxNQUFILEVBQWQ7QUFFQTs7QUFDQSxTQUFLQyxLQUFMLEdBQWEsRUFBYjtBQUVBOztBQUNBLFNBQUtBLEtBQUwsQ0FBV0MsYUFBWCxHQUEyQjtBQUN2QkMsWUFBTSxjQUFLQyxPQUFMLENBQWFDLFNBQWIsRUFBd0IsSUFBeEI7QUFEaUIsS0FBM0I7QUFJQSxTQUFLSixLQUFMLENBQVdDLGFBQVgsQ0FBeUJJLFFBQXpCLEdBQW9DMUIsS0FBSyxLQUFLcUIsS0FBTCxDQUFXQyxhQUFYLENBQXlCQyxJQUE5QixFQUFvQyxVQUFwQyxDQUFwQztBQUVBOztBQUNBLFNBQUtGLEtBQUwsQ0FBV00sU0FBWCxHQUF1QjtBQUNuQkosWUFBTXJCO0FBRGEsS0FBdkI7QUFJQTs7QUFDQSxTQUFLbUIsS0FBTCxDQUFXTyxPQUFYLEdBQXFCO0FBQ2pCQyxnQkFBVSxVQURPO0FBRWpCTixZQUFNdkIsS0FBSyxLQUFLcUIsS0FBTCxDQUFXTSxTQUFYLENBQXFCSixJQUExQixFQUFnQyxVQUFoQztBQUZXLEtBQXJCO0FBS0EsMkJBQVMsS0FBS0YsS0FBTCxDQUFXTyxPQUFwQixFQUE2QjtBQUN6QkUsZUFBUzlCLEtBQUssS0FBS3FCLEtBQUwsQ0FBV08sT0FBWCxDQUFtQkwsSUFBeEIsRUFBOEIsU0FBOUIsQ0FEZ0I7QUFFekJRLGNBQVEvQixLQUFLLEtBQUtxQixLQUFMLENBQVdPLE9BQVgsQ0FBbUJMLElBQXhCLEVBQThCLFFBQTlCLENBRmlCO0FBR3pCUyxjQUFRaEMsS0FBSyxLQUFLcUIsS0FBTCxDQUFXTyxPQUFYLENBQW1CTCxJQUF4QixFQUE4QixRQUE5QixDQUhpQjtBQUl6QlUsZ0JBQVVqQyxLQUFLLEtBQUtxQixLQUFMLENBQVdPLE9BQVgsQ0FBbUJMLElBQXhCLEVBQThCLGVBQTlCLENBSmU7QUFLekJLLGVBQVM1QixLQUFLLEtBQUtxQixLQUFMLENBQVdPLE9BQVgsQ0FBbUJMLElBQXhCLEVBQThCLFlBQTlCO0FBTGdCLEtBQTdCO0FBUUEsU0FBS0YsS0FBTCxDQUFXTyxPQUFYLENBQW1CTSxZQUFuQixHQUFrQ2xDLEtBQUssS0FBS3FCLEtBQUwsQ0FBV08sT0FBWCxDQUFtQkksTUFBeEIsRUFBZ0Msa0JBQWhDLENBQWxDO0FBQ0EsU0FBS1gsS0FBTCxDQUFXTyxPQUFYLENBQW1CTyxVQUFuQixHQUFnQ25DLEtBQUssS0FBS3FCLEtBQUwsQ0FBV08sT0FBWCxDQUFtQkksTUFBeEIsRUFBZ0MsYUFBaEMsQ0FBaEM7QUFDQSxTQUFLWCxLQUFMLENBQVdPLE9BQVgsQ0FBbUJRLFNBQW5CLEdBQStCcEMsS0FBSyxLQUFLcUIsS0FBTCxDQUFXTyxPQUFYLENBQW1CSSxNQUF4QixFQUFnQyxZQUFoQyxDQUEvQjtBQUVBOztBQUNBLFNBQUtYLEtBQUwsQ0FBV2dCLFdBQVgsR0FBeUI7QUFDckJSLGdCQUFVO0FBRFcsS0FBekI7QUFHQSxTQUFLUixLQUFMLENBQVdnQixXQUFYLENBQXVCZCxJQUF2QixHQUNJdkIsS0FBSyxLQUFLcUIsS0FBTCxDQUFXTSxTQUFYLENBQXFCSixJQUExQixFQUFnQyxTQUFoQyxFQUEyQyxLQUFLRixLQUFMLENBQVdnQixXQUFYLENBQXVCUixRQUFsRSxDQURKO0FBR0EsU0FBS1IsS0FBTCxDQUFXZ0IsV0FBWCxDQUF1QkMsY0FBdkIsR0FDSXRDLEtBQUssS0FBS3FCLEtBQUwsQ0FBV00sU0FBWCxDQUFxQkosSUFBMUIsRUFBZ0MsU0FBaEMsRUFBMkMsdUJBQTNDLENBREo7QUFHQSxTQUFLRixLQUFMLENBQVdnQixXQUFYLENBQXVCRSxvQkFBdkIsR0FDSXZDLEtBQUssS0FBS3FCLEtBQUwsQ0FBV00sU0FBWCxDQUFxQkosSUFBMUIsRUFBZ0MsU0FBaEMsRUFBMkMsaUNBQTNDLENBREo7QUFHQSxTQUFLRixLQUFMLENBQVdnQixXQUFYLENBQXVCRyx1QkFBdkIsR0FDSXhDLEtBQUssS0FBS3FCLEtBQUwsQ0FBV2dCLFdBQVgsQ0FBdUJFLG9CQUE1QixFQUFrRCxNQUFsRCxDQURKO0FBSUEsU0FBS2xCLEtBQUwsQ0FBV2dCLFdBQVgsQ0FBdUJJLE9BQXZCLEdBQ0l6QyxLQUFLLEtBQUtxQixLQUFMLENBQVdnQixXQUFYLENBQXVCZCxJQUE1QixFQUFrQyxLQUFsQyxDQURKO0FBR0EsMkJBQVMsS0FBS0YsS0FBTCxDQUFXZ0IsV0FBcEIsRUFBaUM7QUFDN0JLLFdBQUsxQyxLQUFLLEtBQUtxQixLQUFMLENBQVdnQixXQUFYLENBQXVCSSxPQUE1QixFQUFxQyxRQUFyQyxDQUR3QjtBQUU3QkUsZUFBUzNDLEtBQUssS0FBS3FCLEtBQUwsQ0FBV2dCLFdBQVgsQ0FBdUJJLE9BQTVCLEVBQXFDLFlBQXJDLENBRm9CO0FBRzdCRyxhQUFPNUMsS0FBSyxLQUFLcUIsS0FBTCxDQUFXZ0IsV0FBWCxDQUF1QkksT0FBNUIsRUFBcUMsVUFBckMsQ0FIc0I7QUFJN0JJLGVBQVM3QyxLQUFLLEtBQUtxQixLQUFMLENBQVdnQixXQUFYLENBQXVCSSxPQUE1QixFQUFxQyxZQUFyQyxDQUpvQjtBQUs3QlgsZUFBUzlCLEtBQUssS0FBS3FCLEtBQUwsQ0FBV2dCLFdBQVgsQ0FBdUJJLE9BQTVCLEVBQXFDLFNBQXJDLENBTG9CO0FBTTdCSyxtQkFBYTlDLEtBQUssS0FBS3FCLEtBQUwsQ0FBV2dCLFdBQVgsQ0FBdUJkLElBQTVCLEVBQWtDLGNBQWxDLENBTmdCO0FBTzdCd0IsaUJBQVcvQyxLQUFLLEtBQUtxQixLQUFMLENBQVdnQixXQUFYLENBQXVCZCxJQUE1QixFQUFrQyxXQUFsQyxDQVBrQjtBQVE3QnlCLGVBQVNoRCxLQUFLLEtBQUtxQixLQUFMLENBQVdnQixXQUFYLENBQXVCZCxJQUE1QixFQUFrQyxVQUFsQyxDQVJvQjtBQVM3QlEsY0FBUS9CLEtBQUssS0FBS3FCLEtBQUwsQ0FBV2dCLFdBQVgsQ0FBdUJkLElBQTVCLEVBQWtDLFFBQWxDLENBVHFCO0FBVTdCUyxjQUFRaEMsS0FBSyxLQUFLcUIsS0FBTCxDQUFXZ0IsV0FBWCxDQUF1QmQsSUFBNUIsRUFBa0MsUUFBbEMsQ0FWcUI7QUFXN0IwQixtQkFBYWpELEtBQUssS0FBS3FCLEtBQUwsQ0FBV2dCLFdBQVgsQ0FBdUJkLElBQTVCLEVBQWtDLGNBQWxDLENBWGdCO0FBWTdCVSxnQkFBVWpDLEtBQUssS0FBS3FCLEtBQUwsQ0FBV2dCLFdBQVgsQ0FBdUJkLElBQTVCLEVBQWtDLGVBQWxDLENBWm1CO0FBYTdCSyxlQUFTNUIsS0FBSyxLQUFLcUIsS0FBTCxDQUFXZ0IsV0FBWCxDQUF1QmQsSUFBNUIsRUFBa0MsWUFBbEMsQ0Fib0I7QUFjN0IyQixrQkFBWWxELEtBQUssS0FBS3FCLEtBQUwsQ0FBV2dCLFdBQVgsQ0FBdUJkLElBQTVCLEVBQWtDLFdBQWxDLENBZGlCO0FBZTdCNEIsbUJBQWFuRCxLQUFLLEtBQUtxQixLQUFMLENBQVdnQixXQUFYLENBQXVCZCxJQUE1QixFQUFrQyxjQUFsQyxDQWZnQjtBQWdCN0I2QixrQkFBWXBELEtBQUssS0FBS3FCLEtBQUwsQ0FBV2dCLFdBQVgsQ0FBdUJkLElBQTVCLEVBQWtDLGFBQWxDLENBaEJpQjtBQWlCN0JJLGlCQUFXM0IsS0FBSyxLQUFLcUIsS0FBTCxDQUFXZ0IsV0FBWCxDQUF1QmQsSUFBNUIsRUFBa0MsUUFBbEMsQ0FqQmtCO0FBa0I3QjhCLHNCQUFnQnJELEtBQUssS0FBS3FCLEtBQUwsQ0FBV2dCLFdBQVgsQ0FBdUJkLElBQTVCLEVBQWtDLFFBQWxDLEVBQTRDLFlBQTVDLENBbEJhO0FBbUI3QitCLDRCQUFzQnRELEtBQUssS0FBS3FCLEtBQUwsQ0FBV2dCLFdBQVgsQ0FBdUJkLElBQTVCLEVBQWtDLFFBQWxDLEVBQTRDLGNBQTVDLENBbkJPO0FBb0I3QkcsZ0JBQVUxQixLQUFLLEtBQUtxQixLQUFMLENBQVdnQixXQUFYLENBQXVCZCxJQUE1QixFQUFrQyxVQUFsQztBQXBCbUIsS0FBakM7QUF1QkEsMkJBQVMsS0FBS0YsS0FBTCxDQUFXTSxTQUFwQixFQUErQjtBQUMzQjRCLGlCQUFXdkQsS0FBSyxLQUFLcUIsS0FBTCxDQUFXTSxTQUFYLENBQXFCSixJQUExQixFQUFnQyxTQUFoQyxFQUEyQyxXQUEzQyxDQURnQjtBQUUzQmlDLGdCQUFVeEQsS0FBSyxLQUFLcUIsS0FBTCxDQUFXTSxTQUFYLENBQXFCSixJQUExQixFQUFnQyxTQUFoQyxFQUEyQyxVQUEzQyxDQUZpQjtBQUczQmtDLGdCQUFVekQsS0FBSyxLQUFLcUIsS0FBTCxDQUFXTSxTQUFYLENBQXFCSixJQUExQixFQUFnQyxTQUFoQyxFQUEyQyxVQUEzQyxDQUhpQjtBQUkzQm1DLGVBQVMxRCxLQUFLLEtBQUtxQixLQUFMLENBQVdNLFNBQVgsQ0FBcUJKLElBQTFCLEVBQWdDLFNBQWhDLEVBQTJDLFNBQTNDLENBSmtCO0FBSzNCMEIsbUJBQWFqRCxLQUFLLEtBQUtxQixLQUFMLENBQVdNLFNBQVgsQ0FBcUJKLElBQTFCLEVBQWdDLGNBQWhDLENBTGM7QUFNM0JvQyxpQkFBVzNELEtBQUssS0FBS3FCLEtBQUwsQ0FBV00sU0FBWCxDQUFxQkosSUFBMUIsRUFBZ0MsU0FBaEMsRUFBMkMsWUFBM0MsQ0FOZ0I7QUFPM0JxQyxvQkFBYzVELEtBQ1YsS0FBS3FCLEtBQUwsQ0FBV00sU0FBWCxDQUFxQkosSUFEWCxFQUVWLFNBRlUsRUFHVixPQUhVLEVBSVYsZUFKVSxFQUtWLEtBTFUsRUFNVixhQU5VLENBUGE7QUFlM0JzQyxrQkFBWTdELEtBQ1IsS0FBS3FCLEtBQUwsQ0FBV00sU0FBWCxDQUFxQkosSUFEYixFQUVSLFNBRlEsRUFHUixPQUhRLEVBSVIsT0FKUSxFQUtSLFVBTFEsRUFNUixhQU5RO0FBZmUsS0FBL0I7QUF5QkEsMkJBQVMsS0FBS0YsS0FBTCxDQUFXTSxTQUFwQixFQUErQjtBQUMzQm1DLHlCQUFtQjlELEtBQ2YsS0FBS3FCLEtBQUwsQ0FBV00sU0FBWCxDQUFxQmlDLFlBRE4sRUFDb0IsWUFEcEIsQ0FEUTtBQUkzQkcsK0JBQXlCL0QsS0FDckIsS0FBS3FCLEtBQUwsQ0FBV00sU0FBWCxDQUFxQmlDLFlBREEsRUFDYyxjQURkO0FBSkUsS0FBL0I7QUFTQSwyQkFBUyxLQUFLdkMsS0FBTCxDQUFXTSxTQUFwQixFQUErQjtBQUMzQnFDLDZCQUF1QmhFLEtBQ25CLEtBQUtxQixLQUFMLENBQVdNLFNBQVgsQ0FBcUJrQyxVQURGLEVBQ2MsY0FEZDtBQURJLEtBQS9CO0FBT0E7O0FBQ0EsU0FBS3hDLEtBQUwsQ0FBVzZCLFVBQVgsR0FBd0I7QUFDcEIzQixZQUFNdkIsS0FBSyxLQUFLcUIsS0FBTCxDQUFXZ0IsV0FBWCxDQUF1QmQsSUFBNUIsRUFBa0MsV0FBbEM7QUFEYyxLQUF4QjtBQUlBLDJCQUFTLEtBQUtGLEtBQUwsQ0FBVzZCLFVBQXBCLEVBQWdDO0FBQzVCcEIsZUFBUzlCLEtBQUssS0FBS3FCLEtBQUwsQ0FBVzZCLFVBQVgsQ0FBc0IzQixJQUEzQixFQUFpQyxTQUFqQyxDQURtQjtBQUU1QlUsZ0JBQVVqQyxLQUFLLEtBQUtxQixLQUFMLENBQVc2QixVQUFYLENBQXNCM0IsSUFBM0IsRUFBaUMsZUFBakM7QUFGa0IsS0FBaEM7QUFLQSxTQUFLRixLQUFMLENBQVc0QyxVQUFYLEdBQXdCLGtCQUF4QjtBQUNBLFNBQUs1QyxLQUFMLENBQVc2QyxZQUFYLEdBQTBCLG9CQUExQixDQXpKZ0MsQ0EySmhDOztBQUNBLFNBQUs3QyxLQUFMLENBQVc4QyxRQUFYLEdBQXNCbkUsS0FBS3lCLFNBQUwsRUFBZ0IsSUFBaEIsRUFBc0IsVUFBdEIsQ0FBdEI7QUFDSDtBQUVEOzs7Ozs7Ozt3Q0FJb0I7QUFDaEIsYUFBTyxDQUFDLEVBQUUsZ0JBQWdCLEtBQUtyQixPQUFyQixJQUFnQyxLQUFLQSxPQUFMLENBQWFnRSxVQUEvQyxDQUFSO0FBQ0g7Ozs7Ozs7QUFHTEMsT0FBT0MsT0FBUCxHQUFpQnJFLEdBQWpCO0FBRUE7Ozs7Ozs7Ozs7Ozs7O0FBY0E7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBK0JBOzs7Ozs7O0FBT0E7Ozs7OztBQU1BIiwiZmlsZSI6ImVudi5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBwYXRoIGZyb20gJ3BhdGgnO1xuaW1wb3J0IG9zIGZyb20gJ29zJztcbmltcG9ydCBhc3NpZ25JbiBmcm9tICdsb2Rhc2gvYXNzaWduSW4nO1xuXG5jb25zdCB7IGpvaW4gfSA9IHBhdGg7XG5cbi8qKlxuICogQGNsYXNzXG4gKiBAcHJvcGVydHkge3BhY2thZ2VQYXRoc30gcGF0aHNcbiAqL1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRW52IHtcbiAgICBjb25zdHJ1Y3RvcihpbnB1dCwgb3V0cHV0LCBvcHRpb25zKSB7XG4gICAgICAgIHRoaXMub3B0aW9ucyA9IG9wdGlvbnM7XG5cbiAgICAgICAgaWYgKHRoaXMuaXNQcm9kdWN0aW9uQnVpbGQoKSkge1xuICAgICAgICAgICAgcHJvY2Vzcy5lbnYuTk9ERV9FTlYgPSAncHJvZHVjdGlvbic7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLnN5cyA9IHtcbiAgICAgICAgICAgIHBsYXRmb3JtOiBwcm9jZXNzLnBsYXRmb3JtLFxuICAgICAgICAgICAgYXJjaDogcHJvY2Vzcy5hcmNoXG4gICAgICAgIH07XG5cbiAgICAgICAgLy8gT3BlcmF0aW9uYWwgU3lzdGVtLlxuICAgICAgICB0aGlzLm9zID0ge1xuICAgICAgICAgICAgaXNXaW5kb3dzOiAocHJvY2Vzcy5wbGF0Zm9ybSA9PT0gJ3dpbjMyJyksXG4gICAgICAgICAgICBpc0xpbnV4OiAocHJvY2Vzcy5wbGF0Zm9ybSA9PT0gJ2xpbnV4JyksXG4gICAgICAgICAgICBpc09zeDogKHByb2Nlc3MucGxhdGZvcm0gPT09ICdkYXJ3aW4nKVxuXG4gICAgICAgIH07XG4gICAgICAgIHRoaXMuc3RkaW8gPSAnaW5oZXJpdCc7XG5cbiAgICAgICAgdGhpcy5vcy5uYW1lID0gKHRoaXMuc3lzLnBsYXRmb3JtID09PSAnZGFyd2luJyA/ICdvc3gnIDogdGhpcy5zeXMucGxhdGZvcm0pO1xuICAgICAgICB0aGlzLm9zLmhvbWUgPSBwcm9jZXNzLmVudlsodGhpcy5vcy5pc1dpbmRvd3MgPyAnVVNFUlBST0ZJTEUnIDogJ0hPTUUnKV07XG4gICAgICAgIHRoaXMub3MudG1wID0gb3MudG1wZGlyKCk7XG5cbiAgICAgICAgLyoqIEB0eXBlIHtwYWNrYWdlUGF0aHN9ICovXG4gICAgICAgIHRoaXMucGF0aHMgPSB7fTtcblxuICAgICAgICAvKiogQHR5cGUge21ldGVvckRlc2t0b3BQYXRoc30gKi9cbiAgICAgICAgdGhpcy5wYXRocy5tZXRlb3JEZXNrdG9wID0ge1xuICAgICAgICAgICAgcm9vdDogcGF0aC5yZXNvbHZlKF9fZGlybmFtZSwgJy4uJylcbiAgICAgICAgfTtcblxuICAgICAgICB0aGlzLnBhdGhzLm1ldGVvckRlc2t0b3Auc2tlbGV0b24gPSBqb2luKHRoaXMucGF0aHMubWV0ZW9yRGVza3RvcC5yb290LCAnc2tlbGV0b24nKTtcblxuICAgICAgICAvKiogQHR5cGUge21ldGVvckFwcFBhdGhzfSAqL1xuICAgICAgICB0aGlzLnBhdGhzLm1ldGVvckFwcCA9IHtcbiAgICAgICAgICAgIHJvb3Q6IGlucHV0XG4gICAgICAgIH07XG5cbiAgICAgICAgLyoqIEB0eXBlIHtkZXNrdG9wUGF0aHN9ICovXG4gICAgICAgIHRoaXMucGF0aHMuZGVza3RvcCA9IHtcbiAgICAgICAgICAgIHJvb3ROYW1lOiAnLmRlc2t0b3AnLFxuICAgICAgICAgICAgcm9vdDogam9pbih0aGlzLnBhdGhzLm1ldGVvckFwcC5yb290LCAnLmRlc2t0b3AnKVxuICAgICAgICB9O1xuXG4gICAgICAgIGFzc2lnbkluKHRoaXMucGF0aHMuZGVza3RvcCwge1xuICAgICAgICAgICAgbW9kdWxlczogam9pbih0aGlzLnBhdGhzLmRlc2t0b3Aucm9vdCwgJ21vZHVsZXMnKSxcbiAgICAgICAgICAgIGltcG9ydDogam9pbih0aGlzLnBhdGhzLmRlc2t0b3Aucm9vdCwgJ2ltcG9ydCcpLFxuICAgICAgICAgICAgYXNzZXRzOiBqb2luKHRoaXMucGF0aHMuZGVza3RvcC5yb290LCAnYXNzZXRzJyksXG4gICAgICAgICAgICBzZXR0aW5nczogam9pbih0aGlzLnBhdGhzLmRlc2t0b3Aucm9vdCwgJ3NldHRpbmdzLmpzb24nKSxcbiAgICAgICAgICAgIGRlc2t0b3A6IGpvaW4odGhpcy5wYXRocy5kZXNrdG9wLnJvb3QsICdkZXNrdG9wLmpzJylcbiAgICAgICAgfSk7XG5cbiAgICAgICAgdGhpcy5wYXRocy5kZXNrdG9wLnNwbGFzaFNjcmVlbiA9IGpvaW4odGhpcy5wYXRocy5kZXNrdG9wLmFzc2V0cywgJ3NwbGFzaFNjcmVlbi5wbmcnKTtcbiAgICAgICAgdGhpcy5wYXRocy5kZXNrdG9wLmxvYWRpbmdHaWYgPSBqb2luKHRoaXMucGF0aHMuZGVza3RvcC5hc3NldHMsICdsb2FkaW5nLmdpZicpO1xuICAgICAgICB0aGlzLnBhdGhzLmRlc2t0b3AubWV0ZW9ySWNvID0gam9pbih0aGlzLnBhdGhzLmRlc2t0b3AuYXNzZXRzLCAnbWV0ZW9yLmljbycpO1xuXG4gICAgICAgIC8qKiBAdHlwZSB7ZWxlY3Ryb25BcHBQYXRoc30gKi9cbiAgICAgICAgdGhpcy5wYXRocy5lbGVjdHJvbkFwcCA9IHtcbiAgICAgICAgICAgIHJvb3ROYW1lOiAnZGVza3RvcC1idWlsZCcsXG4gICAgICAgIH07XG4gICAgICAgIHRoaXMucGF0aHMuZWxlY3Ryb25BcHAucm9vdCA9XG4gICAgICAgICAgICBqb2luKHRoaXMucGF0aHMubWV0ZW9yQXBwLnJvb3QsICcubWV0ZW9yJywgdGhpcy5wYXRocy5lbGVjdHJvbkFwcC5yb290TmFtZSk7XG5cbiAgICAgICAgdGhpcy5wYXRocy5lbGVjdHJvbkFwcC50bXBOb2RlTW9kdWxlcyA9XG4gICAgICAgICAgICBqb2luKHRoaXMucGF0aHMubWV0ZW9yQXBwLnJvb3QsICcubWV0ZW9yJywgJy5kZXNrdG9wX25vZGVfbW9kdWxlcycpO1xuXG4gICAgICAgIHRoaXMucGF0aHMuZWxlY3Ryb25BcHAuZXh0cmFjdGVkTm9kZU1vZHVsZXMgPVxuICAgICAgICAgICAgam9pbih0aGlzLnBhdGhzLm1ldGVvckFwcC5yb290LCAnLm1ldGVvcicsICcuZGVza3RvcF9leHRyYWN0ZWRfbm9kZV9tb2R1bGVzJyk7XG5cbiAgICAgICAgdGhpcy5wYXRocy5lbGVjdHJvbkFwcC5leHRyYWN0ZWROb2RlTW9kdWxlc0JpbiA9XG4gICAgICAgICAgICBqb2luKHRoaXMucGF0aHMuZWxlY3Ryb25BcHAuZXh0cmFjdGVkTm9kZU1vZHVsZXMsICcuYmluJyk7XG5cblxuICAgICAgICB0aGlzLnBhdGhzLmVsZWN0cm9uQXBwLmFwcFJvb3QgPVxuICAgICAgICAgICAgam9pbih0aGlzLnBhdGhzLmVsZWN0cm9uQXBwLnJvb3QsICdhcHAnKTtcblxuICAgICAgICBhc3NpZ25Jbih0aGlzLnBhdGhzLmVsZWN0cm9uQXBwLCB7XG4gICAgICAgICAgICBhcHA6IGpvaW4odGhpcy5wYXRocy5lbGVjdHJvbkFwcC5hcHBSb290LCAnYXBwLmpzJyksXG4gICAgICAgICAgICBjb3Jkb3ZhOiBqb2luKHRoaXMucGF0aHMuZWxlY3Ryb25BcHAuYXBwUm9vdCwgJ2NvcmRvdmEuanMnKSxcbiAgICAgICAgICAgIGluZGV4OiBqb2luKHRoaXMucGF0aHMuZWxlY3Ryb25BcHAuYXBwUm9vdCwgJ2luZGV4LmpzJyksXG4gICAgICAgICAgICBwcmVsb2FkOiBqb2luKHRoaXMucGF0aHMuZWxlY3Ryb25BcHAuYXBwUm9vdCwgJ3ByZWxvYWQuanMnKSxcbiAgICAgICAgICAgIG1vZHVsZXM6IGpvaW4odGhpcy5wYXRocy5lbGVjdHJvbkFwcC5hcHBSb290LCAnbW9kdWxlcycpLFxuICAgICAgICAgICAgZGVza3RvcEFzYXI6IGpvaW4odGhpcy5wYXRocy5lbGVjdHJvbkFwcC5yb290LCAnZGVza3RvcC5hc2FyJyksXG4gICAgICAgICAgICBleHRyYWN0ZWQ6IGpvaW4odGhpcy5wYXRocy5lbGVjdHJvbkFwcC5yb290LCAnZXh0cmFjdGVkJyksXG4gICAgICAgICAgICBhcHBBc2FyOiBqb2luKHRoaXMucGF0aHMuZWxlY3Ryb25BcHAucm9vdCwgJ2FwcC5hc2FyJyksXG4gICAgICAgICAgICBpbXBvcnQ6IGpvaW4odGhpcy5wYXRocy5lbGVjdHJvbkFwcC5yb290LCAnaW1wb3J0JyksXG4gICAgICAgICAgICBhc3NldHM6IGpvaW4odGhpcy5wYXRocy5lbGVjdHJvbkFwcC5yb290LCAnYXNzZXRzJyksXG4gICAgICAgICAgICBwYWNrYWdlSnNvbjogam9pbih0aGlzLnBhdGhzLmVsZWN0cm9uQXBwLnJvb3QsICdwYWNrYWdlLmpzb24nKSxcbiAgICAgICAgICAgIHNldHRpbmdzOiBqb2luKHRoaXMucGF0aHMuZWxlY3Ryb25BcHAucm9vdCwgJ3NldHRpbmdzLmpzb24nKSxcbiAgICAgICAgICAgIGRlc2t0b3A6IGpvaW4odGhpcy5wYXRocy5lbGVjdHJvbkFwcC5yb290LCAnZGVza3RvcC5qcycpLFxuICAgICAgICAgICAgZGVza3RvcFRtcDogam9pbih0aGlzLnBhdGhzLmVsZWN0cm9uQXBwLnJvb3QsICdfX2Rlc2t0b3AnKSxcbiAgICAgICAgICAgIG5vZGVNb2R1bGVzOiBqb2luKHRoaXMucGF0aHMuZWxlY3Ryb25BcHAucm9vdCwgJ25vZGVfbW9kdWxlcycpLFxuICAgICAgICAgICAgbWV0ZW9yQXNhcjogam9pbih0aGlzLnBhdGhzLmVsZWN0cm9uQXBwLnJvb3QsICdtZXRlb3IuYXNhcicpLFxuICAgICAgICAgICAgbWV0ZW9yQXBwOiBqb2luKHRoaXMucGF0aHMuZWxlY3Ryb25BcHAucm9vdCwgJ21ldGVvcicpLFxuICAgICAgICAgICAgbWV0ZW9yQXBwSW5kZXg6IGpvaW4odGhpcy5wYXRocy5lbGVjdHJvbkFwcC5yb290LCAnbWV0ZW9yJywgJ2luZGV4Lmh0bWwnKSxcbiAgICAgICAgICAgIG1ldGVvckFwcFByb2dyYW1Kc29uOiBqb2luKHRoaXMucGF0aHMuZWxlY3Ryb25BcHAucm9vdCwgJ21ldGVvcicsICdwcm9ncmFtLmpzb24nKSxcbiAgICAgICAgICAgIHNrZWxldG9uOiBqb2luKHRoaXMucGF0aHMuZWxlY3Ryb25BcHAucm9vdCwgJ3NrZWxldG9uJylcbiAgICAgICAgfSk7XG5cbiAgICAgICAgYXNzaWduSW4odGhpcy5wYXRocy5tZXRlb3JBcHAsIHtcbiAgICAgICAgICAgIHBsYXRmb3Jtczogam9pbih0aGlzLnBhdGhzLm1ldGVvckFwcC5yb290LCAnLm1ldGVvcicsICdwbGF0Zm9ybXMnKSxcbiAgICAgICAgICAgIHBhY2thZ2VzOiBqb2luKHRoaXMucGF0aHMubWV0ZW9yQXBwLnJvb3QsICcubWV0ZW9yJywgJ3BhY2thZ2VzJyksXG4gICAgICAgICAgICB2ZXJzaW9uczogam9pbih0aGlzLnBhdGhzLm1ldGVvckFwcC5yb290LCAnLm1ldGVvcicsICd2ZXJzaW9ucycpLFxuICAgICAgICAgICAgcmVsZWFzZTogam9pbih0aGlzLnBhdGhzLm1ldGVvckFwcC5yb290LCAnLm1ldGVvcicsICdyZWxlYXNlJyksXG4gICAgICAgICAgICBwYWNrYWdlSnNvbjogam9pbih0aGlzLnBhdGhzLm1ldGVvckFwcC5yb290LCAncGFja2FnZS5qc29uJyksXG4gICAgICAgICAgICBnaXRJZ25vcmU6IGpvaW4odGhpcy5wYXRocy5tZXRlb3JBcHAucm9vdCwgJy5tZXRlb3InLCAnLmdpdGlnbm9yZScpLFxuICAgICAgICAgICAgY29yZG92YUJ1aWxkOiBqb2luKFxuICAgICAgICAgICAgICAgIHRoaXMucGF0aHMubWV0ZW9yQXBwLnJvb3QsXG4gICAgICAgICAgICAgICAgJy5tZXRlb3InLFxuICAgICAgICAgICAgICAgICdsb2NhbCcsXG4gICAgICAgICAgICAgICAgJ2NvcmRvdmEtYnVpbGQnLFxuICAgICAgICAgICAgICAgICd3d3cnLFxuICAgICAgICAgICAgICAgICdhcHBsaWNhdGlvbidcbiAgICAgICAgICAgICksXG4gICAgICAgICAgICB3ZWJDb3Jkb3ZhOiBqb2luKFxuICAgICAgICAgICAgICAgIHRoaXMucGF0aHMubWV0ZW9yQXBwLnJvb3QsXG4gICAgICAgICAgICAgICAgJy5tZXRlb3InLFxuICAgICAgICAgICAgICAgICdsb2NhbCcsXG4gICAgICAgICAgICAgICAgJ2J1aWxkJyxcbiAgICAgICAgICAgICAgICAncHJvZ3JhbXMnLFxuICAgICAgICAgICAgICAgICd3ZWIuY29yZG92YSdcbiAgICAgICAgICAgIClcbiAgICAgICAgfSk7XG5cbiAgICAgICAgYXNzaWduSW4odGhpcy5wYXRocy5tZXRlb3JBcHAsIHtcbiAgICAgICAgICAgIGNvcmRvdmFCdWlsZEluZGV4OiBqb2luKFxuICAgICAgICAgICAgICAgIHRoaXMucGF0aHMubWV0ZW9yQXBwLmNvcmRvdmFCdWlsZCwgJ2luZGV4Lmh0bWwnXG4gICAgICAgICAgICApLFxuICAgICAgICAgICAgY29yZG92YUJ1aWxkUHJvZ3JhbUpzb246IGpvaW4oXG4gICAgICAgICAgICAgICAgdGhpcy5wYXRocy5tZXRlb3JBcHAuY29yZG92YUJ1aWxkLCAncHJvZ3JhbS5qc29uJ1xuICAgICAgICAgICAgKVxuICAgICAgICB9KTtcblxuICAgICAgICBhc3NpZ25Jbih0aGlzLnBhdGhzLm1ldGVvckFwcCwge1xuICAgICAgICAgICAgd2ViQ29yZG92YVByb2dyYW1Kc29uOiBqb2luKFxuICAgICAgICAgICAgICAgIHRoaXMucGF0aHMubWV0ZW9yQXBwLndlYkNvcmRvdmEsICdwcm9ncmFtLmpzb24nXG4gICAgICAgICAgICApXG4gICAgICAgIH0pO1xuXG5cbiAgICAgICAgLyoqIEB0eXBlIHtkZXNrdG9wVG1wUGF0aHN9ICovXG4gICAgICAgIHRoaXMucGF0aHMuZGVza3RvcFRtcCA9IHtcbiAgICAgICAgICAgIHJvb3Q6IGpvaW4odGhpcy5wYXRocy5lbGVjdHJvbkFwcC5yb290LCAnX19kZXNrdG9wJyksXG4gICAgICAgIH07XG5cbiAgICAgICAgYXNzaWduSW4odGhpcy5wYXRocy5kZXNrdG9wVG1wLCB7XG4gICAgICAgICAgICBtb2R1bGVzOiBqb2luKHRoaXMucGF0aHMuZGVza3RvcFRtcC5yb290LCAnbW9kdWxlcycpLFxuICAgICAgICAgICAgc2V0dGluZ3M6IGpvaW4odGhpcy5wYXRocy5kZXNrdG9wVG1wLnJvb3QsICdzZXR0aW5ncy5qc29uJylcbiAgICAgICAgfSk7XG5cbiAgICAgICAgdGhpcy5wYXRocy5wYWNrYWdlRGlyID0gJy5kZXNrdG9wLXBhY2thZ2UnO1xuICAgICAgICB0aGlzLnBhdGhzLmluc3RhbGxlckRpciA9ICcuZGVza3RvcC1pbnN0YWxsZXInO1xuXG4gICAgICAgIC8vIFNjYWZmb2xkXG4gICAgICAgIHRoaXMucGF0aHMuc2NhZmZvbGQgPSBqb2luKF9fZGlybmFtZSwgJy4uJywgJ3NjYWZmb2xkJyk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQHJldHVybnMge2Jvb2xlYW58Kn1cbiAgICAgKiBAcHVibGljXG4gICAgICovXG4gICAgaXNQcm9kdWN0aW9uQnVpbGQoKSB7XG4gICAgICAgIHJldHVybiAhISgncHJvZHVjdGlvbicgaW4gdGhpcy5vcHRpb25zICYmIHRoaXMub3B0aW9ucy5wcm9kdWN0aW9uKTtcbiAgICB9XG59XG5cbm1vZHVsZS5leHBvcnRzID0gRW52O1xuXG4vKipcbiAqIEB0eXBlZGVmIHtPYmplY3R9IGRlc2t0b3BQYXRoc1xuICogQHByb3BlcnR5IHtzdHJpbmd9IHJvb3ROYW1lXG4gKiBAcHJvcGVydHkge3N0cmluZ30gcm9vdFxuICogQHByb3BlcnR5IHtzdHJpbmd9IG1vZHVsZXNcbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSBpbXBvcnRcbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSBhc3NldHNcbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSBzZXR0aW5nc1xuICogQHByb3BlcnR5IHtzdHJpbmd9IGRlc2t0b3BcbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSBzcGxhc2hTY3JlZW5cbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSBsb2FkaW5nR2lmXG4gKiBAcHJvcGVydHkge3N0cmluZ30gbWV0ZW9ySWNvXG4gKi9cblxuLyoqXG4gKiBAdHlwZWRlZiB7T2JqZWN0fSBtZXRlb3JBcHBQYXRoc1xuICogQHByb3BlcnR5IHtzdHJpbmd9IHJvb3RcbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSBwbGF0Zm9ybXNcbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSByZWxlYXNlXG4gKiBAcHJvcGVydHkge3N0cmluZ30gcGFja2FnZXNcbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSB2ZXJzaW9uc1xuICogQHByb3BlcnR5IHtzdHJpbmd9IGdpdElnbm9yZVxuICogQHByb3BlcnR5IHtzdHJpbmd9IHBhY2thZ2VKc29uXG4gKiBAcHJvcGVydHkge3N0cmluZ30gY29yZG92YUJ1aWxkXG4gKiBAcHJvcGVydHkge3N0cmluZ30gY29yZG92YUJ1aWxkSW5kZXhcbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSBjb3Jkb3ZhQnVpbGRQcm9ncmFtSnNvblxuICogQHByb3BlcnR5IHtzdHJpbmd9IHdlYkNvcmRvdmFcbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSB3ZWJDb3Jkb3ZhSW5kZXhcbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSB3ZWJDb3Jkb3ZhUHJvZ3JhbUpzb25cbiAqL1xuXG4vKipcbiAqIEB0eXBlZGVmIHtPYmplY3R9IGVsZWN0cm9uQXBwUGF0aHNcbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSByb290TmFtZVxuICogQHByb3BlcnR5IHtzdHJpbmd9IHJvb3RcbiAqIEBwcm9wZXJ0eSB7T2JqZWN0fSBhcHBSb290XG4gKiBAcHJvcGVydHkge3N0cmluZ30gYXBwUm9vdC5jb3Jkb3ZhXG4gKiBAcHJvcGVydHkge3N0cmluZ30gYXBwUm9vdC5pbmRleFxuICogQHByb3BlcnR5IHtzdHJpbmd9IGFwcFJvb3QuYXBwXG4gKiBAcHJvcGVydHkge3N0cmluZ30gYXBwUm9vdC5tb2R1bGVzXG4gKiBAcHJvcGVydHkge3N0cmluZ30gZGVza3RvcEFzYXJcbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSBleHRyYWN0ZWRcbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSBhcHBBc2FyXG4gKiBAcHJvcGVydHkge3N0cmluZ30gcHJlbG9hZFxuICogQHByb3BlcnR5IHtzdHJpbmd9IGltcG9ydFxuICogQHByb3BlcnR5IHtzdHJpbmd9IGFzc2V0c1xuICogQHByb3BlcnR5IHtzdHJpbmd9IGdpdElnbm9yZVxuICogQHByb3BlcnR5IHtzdHJpbmd9IHBhY2thZ2VKc29uXG4gKiBAcHJvcGVydHkge3N0cmluZ30gc2V0dGluZ3NcbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSBkZXNrdG9wXG4gKiBAcHJvcGVydHkge3N0cmluZ30gZGVza3RvcFRtcFxuICogQHByb3BlcnR5IHtzdHJpbmd9IG5vZGVNb2R1bGVzXG4gKiBAcHJvcGVydHkge3N0cmluZ30gbWV0ZW9yQXNhclxuICogQHByb3BlcnR5IHtzdHJpbmd9IG1ldGVvckFwcFxuICogQHByb3BlcnR5IHtzdHJpbmd9IG1ldGVvckFwcEluZGV4XG4gKiBAcHJvcGVydHkge3N0cmluZ30gbWV0ZW9yQXBwUHJvZ3JhbUpzb25cbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSBza2VsZXRvblxuICogQHByb3BlcnR5IHtzdHJpbmd9IHRtcE5vZGVNb2R1bGVzXG4gKiBAcHJvcGVydHkge3N0cmluZ30gZXh0cmFjdGVkTm9kZU1vZHVsZXNcbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSBleHRyYWN0ZWROb2RlTW9kdWxlc0JpblxuICovXG5cbi8qKlxuICogQHR5cGVkZWYge09iamVjdH0gZGVza3RvcFRtcFBhdGhzXG4gKiBAcHJvcGVydHkge3N0cmluZ30gcm9vdFxuICogQHByb3BlcnR5IHtzdHJpbmd9IG1vZHVsZXNcbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSBzZXR0aW5nc1xuICovXG5cbi8qKlxuICogQHR5cGVkZWYge09iamVjdH0gbWV0ZW9yRGVza3RvcFBhdGhzXG4gKiBAcHJvcGVydHkge3N0cmluZ30gcm9vdFxuICogQHByb3BlcnR5IHtzdHJpbmd9IHNrZWxldG9uXG4gKi9cblxuLyoqXG4gKiBAdHlwZWRlZiB7T2JqZWN0fSBwYWNrYWdlUGF0aHNcbiAqIEBwcm9wZXJ0eSB7bWV0ZW9yQXBwUGF0aHN9IG1ldGVvckFwcFxuICogQHByb3BlcnR5IHtkZXNrdG9wUGF0aHN9IGRlc2t0b3BcbiAqIEBwcm9wZXJ0eSB7ZWxlY3Ryb25BcHBQYXRoc30gZWxlY3Ryb25BcHBcbiAqIEBwcm9wZXJ0eSB7ZGVza3RvcFRtcFBhdGhzfSBkZXNrdG9wVG1wXG4gKiBAcHJvcGVydHkge21ldGVvckRlc2t0b3BQYXRoc30gbWV0ZW9yRGVza3RvcFxuICogQHByb3BlcnR5IHtzdHJpbmd9IHBhY2thZ2VEaXJcbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSBzY2FmZm9sZFxuICovXG4iXX0=