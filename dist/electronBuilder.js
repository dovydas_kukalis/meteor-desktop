"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _electronBuilder = require("electron-builder");

var _yarn = require("electron-builder-lib/out/util/yarn");

var _packageDependencies = require("electron-builder-lib/out/util/packageDependencies");

var _shelljs = _interopRequireDefault(require("shelljs"));

var _path = _interopRequireDefault(require("path"));

var _fs = _interopRequireDefault(require("fs"));

var _rimraf = _interopRequireDefault(require("rimraf"));

var _crossSpawn = _interopRequireDefault(require("cross-spawn"));

var _log = _interopRequireDefault(require("./log"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * Promisfied rimraf.
 *
 * @param {string} dirPath - path to the dir to be deleted
 * @param {number} delay - delay the task by ms
 * @returns {Promise<any>}
 */
function removeDir(dirPath) {
  var delay = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      (0, _rimraf.default)(dirPath, {
        maxBusyTries: 100
      }, function (err) {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    }, delay);
  });
}
/**
 * Wrapper for electron-builder.
 */


var InstallerBuilder =
/*#__PURE__*/
function () {
  /**
   * @param {MeteorDesktop} $ - context
   *
   * @constructor
   */
  function InstallerBuilder($) {
    _classCallCheck(this, InstallerBuilder);

    this.log = new _log.default('electronBuilder');
    this.$ = $;
    this.firstPass = true;
    this.lastRebuild = {};
    this.currentContext = null;
  }
  /**
   * Prepares the last rebuild object for electron-builder.
   *
   * @param {string} arch
   * @param {string} platform
   * @returns {Object}
   */


  _createClass(InstallerBuilder, [{
    key: "prepareLastRebuildObject",
    value: function prepareLastRebuildObject(arch) {
      var platform = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : process.platform;
      var productionDeps = (0, _packageDependencies.createLazyProductionDeps)(this.$.env.paths.electronApp.root);
      this.lastRebuild = {
        frameworkInfo: {
          version: this.$.getElectronVersion(),
          useCustomDist: true
        },
        platform,
        arch,
        productionDeps
      };
      return this.lastRebuild;
    }
    /**
     * Calls npm rebuild from electron-builder.
     * @param {string} arch
     * @param {string} platform
     * @param {boolean} install
     * @returns {Promise}
     */

  }, {
    key: "installOrRebuild",
    value: function () {
      var _installOrRebuild2 = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee(arch) {
        var platform,
            install,
            _args = arguments;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                platform = _args.length > 1 && _args[1] !== undefined ? _args[1] : process.platform;
                install = _args.length > 2 && _args[2] !== undefined ? _args[2] : false;
                this.log.debug(`calling installOrRebuild from electron-builder for arch ${arch}`);
                this.prepareLastRebuildObject(arch, platform);
                _context.next = 6;
                return (0, _yarn.installOrRebuild)(this.$.desktop.getSettings().builderOptions || {}, this.$.env.paths.electronApp.root, this.lastRebuild, install);

              case 6:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function installOrRebuild(_x) {
        return _installOrRebuild2.apply(this, arguments);
      };
    }()
    /**
     * Callback invoked before build is made. Ensures that app.asar have the right rebuilt
     * node_modules.
     *
     * @param {Object} context
     * @returns {Promise}
     */

  }, {
    key: "beforeBuild",
    value: function beforeBuild(context) {
      var _this = this;

      this.currentContext = Object.assign({}, context);
      return new Promise(function (resolve, reject) {
        var platformMatches = process.platform === context.platform.nodeName;
        var rebuild = platformMatches && context.arch !== _this.lastRebuild.arch;

        if (!platformMatches) {
          _this.log.warn('skipping dependencies rebuild because platform is different, if you have native ' + 'node modules as your app dependencies you should od the build on the target platform only');
        }

        if (!rebuild) {
          _this.moveNodeModulesOut().catch(function (e) {
            return reject(e);
          }).then(function () {
            return setTimeout(function () {
              return resolve(false);
            }, 2000);
          }); // Timeout helps on Windows to clear the file locks.

        } else {
          // Lets rebuild the node_modules for different arch.
          _this.installOrRebuild(context.arch, context.platform.nodeName).catch(function (e) {
            return reject(e);
          }).then(function () {
            return _this.$.electronApp.installLocalNodeModules(context.arch);
          }).catch(function (e) {
            return reject(e);
          }).then(function () {
            _this.$.electronApp.scaffold.createAppRoot();

            _this.$.electronApp.scaffold.copySkeletonApp();

            return _this.$.electronApp.packSkeletonToAsar([_this.$.env.paths.electronApp.meteorAsar, _this.$.env.paths.electronApp.desktopAsar, _this.$.env.paths.electronApp.extracted]);
          }).catch(function (e) {
            return reject(e);
          }).then(function () {
            return _this.moveNodeModulesOut();
          }).catch(function (e) {
            return reject(e);
          }).then(function () {
            return resolve(false);
          });
        }
      });
    }
    /**
     * Callback to be invoked after packing. Restores node_modules to the .desktop-build.
     * @returns {Promise}
     */

  }, {
    key: "afterPack",
    value: function afterPack(context) {
      var _this2 = this;

      return new Promise(function (resolve, reject) {
        _shelljs.default.config.fatal = true;

        if (_this2.$.utils.exists(_this2.$.env.paths.electronApp.extractedNodeModules)) {
          _this2.log.debug('injecting extracted modules');

          _shelljs.default.cp('-Rf', _this2.$.env.paths.electronApp.extractedNodeModules, _path.default.join(_this2.getPackagedAppPath(context), 'node_modules'));
        }

        _this2.log.debug('moving node_modules back'); // Move node_modules back.


        try {
          _shelljs.default.mv(_this2.$.env.paths.electronApp.tmpNodeModules, _this2.$.env.paths.electronApp.nodeModules);
        } catch (e) {
          reject(e);
          return;
        } finally {
          _shelljs.default.config.reset();
        }

        if (_this2.firstPass) {
          _this2.firstPass = false;
        }

        _this2.log.debug('node_modules moved back');

        _this2.wait().catch(function (e) {
          return reject(e);
        }).then(function () {
          return resolve();
        });
      });
    }
    /**
     * This command kills orphaned MSBuild.exe processes.
     * Sometime after native node_modules compilation they are still writing some logs,
     * prevent node_modules from being deleted.
     */

  }, {
    key: "killMSBuild",
    value: function killMSBuild() {
      var _this3 = this;

      if (this.currentContext.platform.nodeName !== 'win32') {
        return;
      }

      try {
        var out = _crossSpawn.default.sync('wmic', ['process', 'where', 'caption="MSBuild.exe"', 'get', 'processid']).stdout.toString('utf-8').split('\n');

        var regex = new RegExp(/(\d+)/, 'gm'); // No we will check for those with the matching params.

        out.forEach(function (line) {
          var match = regex.exec(line) || false;

          if (match) {
            _this3.log.debug(`killing MSBuild.exe at pid: ${match[1]}`);

            _crossSpawn.default.sync('taskkill', ['/pid', match[1], '/f', '/t']);
          }

          regex.lastIndex = 0;
        });
      } catch (e) {
        this.log.debug('kill MSBuild failed');
      }
    }
    /**
     * Returns the path to packaged app.
     * @returns {string}
     */

  }, {
    key: "getPackagedAppPath",
    value: function getPackagedAppPath() {
      var context = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      switch (this.currentContext.platform.nodeName) {
        case 'darwin':
          return _path.default.join(context.appOutDir, `${context.packager.appInfo.productFilename}.app`, 'Contents', 'Resources', 'app');
          break;

        case 'win32':
          return _path.default.join(this.$.env.options.output, this.$.env.paths.installerDir, `win-${this.currentContext.arch === 'ia32' ? 'ia32-' : ''}unpacked`, 'resources', 'app');
          break;

        default:
          return _path.default.join(this.$.env.options.output, this.$.env.paths.installerDir, `linux-unpacked`, 'resources', 'app');
      }
    }
    /**
     * On Windows it waits for the app.asar in the packed app to be free (no file locks).
     * @returns {*}
     */

  }, {
    key: "wait",
    value: function wait() {
      if (this.currentContext.platform.nodeName !== 'win32') {
        return Promise.resolve();
      }

      var appAsarPath = _path.default.join(this.getPackagedAppPath(), 'app.asar');

      var retries = 0;
      var self = this;
      return new Promise(function (resolve, reject) {
        function check() {
          _fs.default.open(appAsarPath, 'r+', function (err, fd) {
            retries += 1;

            if (err) {
              if (err.code !== 'ENOENT') {
                self.log.debug(`waiting for app.asar to be readable, ${'code' in err ? `currently reading it returns ${err.code}` : ''}`);

                if (retries < 6) {
                  setTimeout(function () {
                    return check();
                  }, 4000);
                } else {
                  reject(`file is locked: ${appAsarPath}`);
                }
              } else {
                resolve();
              }
            } else {
              _fs.default.closeSync(fd);

              resolve();
            }
          });
        }

        check();
      });
    }
    /**
     * Prepares the target object passed to the electron-builder.
     *
     * @returns {Map<Platform, Map<Arch, Array<string>>>}
     */

  }, {
    key: "prepareTargets",
    value: function prepareTargets() {
      var arch = this.$.env.options.ia32 ? 'ia32' : 'x64';
      arch = this.$.env.options.allArchs ? 'all' : arch;
      var targets = [];

      if (this.$.env.options.win) {
        targets.push(_electronBuilder.Platform.WINDOWS);
      }

      if (this.$.env.options.linux) {
        targets.push(_electronBuilder.Platform.LINUX);
      }

      if (this.$.env.options.mac) {
        targets.push(_electronBuilder.Platform.MAC);
      }

      if (targets.length === 0) {
        if (this.$.env.os.isWindows) {
          targets.push(_electronBuilder.Platform.WINDOWS);
        } else if (this.$.env.os.isLinux) {
          targets.push(_electronBuilder.Platform.LINUX);
        } else {
          targets.push(_electronBuilder.Platform.MAC);
        }
      }

      return (0, _electronBuilder.createTargets)(targets, null, arch);
    }
  }, {
    key: "build",
    value: function () {
      var _build2 = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2() {
        var settings, builderOptions;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                settings = this.$.desktop.getSettings();

                if (!('builderOptions' in settings)) {
                  this.log.error('no builderOptions in settings.json, aborting');
                  process.exit(1);
                }

                builderOptions = Object.assign({}, settings.builderOptions);
                builderOptions.asar = false;
                builderOptions.npmRebuild = true;
                builderOptions.beforeBuild = this.beforeBuild.bind(this);
                builderOptions.afterPack = this.afterPack.bind(this);
                builderOptions.electronVersion = this.$.getElectronVersion();
                builderOptions.directories = {
                  app: this.$.env.paths.electronApp.root,
                  output: _path.default.join(this.$.env.options.output, this.$.env.paths.installerDir)
                };
                _context2.prev = 9;
                this.log.debug('calling build from electron-builder');
                _context2.next = 13;
                return (0, _electronBuilder.build)(Object.assign({
                  targets: this.prepareTargets(),
                  config: builderOptions
                }, settings.builderCliOptions));

              case 13:
                if (this.$.utils.exists(this.$.env.paths.electronApp.extractedNodeModules)) {
                  _shelljs.default.rm('-rf', this.$.env.paths.electronApp.extractedNodeModules);
                }

                _context2.next = 19;
                break;

              case 16:
                _context2.prev = 16;
                _context2.t0 = _context2["catch"](9);
                this.log.error('error while building installer: ', _context2.t0);

              case 19:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[9, 16]]);
      }));

      return function build() {
        return _build2.apply(this, arguments);
      };
    }()
    /**
     * Moves node_modules out of the app because while the app will be packaged
     * we do not want it to be there.
     * @returns {Promise<any>}
     */

  }, {
    key: "moveNodeModulesOut",
    value: function moveNodeModulesOut() {
      var _this4 = this;

      return new Promise(function (resolve, reject) {
        _this4.log.debug('moving node_modules out, because we have them already in' + ' app.asar');

        _this4.killMSBuild();

        removeDir(_this4.$.env.paths.electronApp.tmpNodeModules).catch(function (e) {
          return reject(e);
        }).then(function () {
          _shelljs.default.config.fatal = true;
          _shelljs.default.config.verbose = true;

          try {
            _shelljs.default.mv(_this4.$.env.paths.electronApp.nodeModules, _this4.$.env.paths.electronApp.tmpNodeModules);

            _shelljs.default.config.reset();

            return _this4.wait();
          } catch (e) {
            _shelljs.default.config.reset();

            return Promise.reject(e);
          }
        }).catch(function (e) {
          return reject(e);
        }).then(function () {
          return removeDir(_this4.$.env.paths.electronApp.nodeModules, 1000);
        }).catch(function (e) {
          return reject(e);
        }).then(function () {
          return _this4.wait();
        }).catch(reject).then(resolve);
      });
    }
  }]);

  return InstallerBuilder;
}();

exports.default = InstallerBuilder;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL2xpYi9lbGVjdHJvbkJ1aWxkZXIuanMiXSwibmFtZXMiOlsicmVtb3ZlRGlyIiwiZGlyUGF0aCIsImRlbGF5IiwiUHJvbWlzZSIsInJlc29sdmUiLCJyZWplY3QiLCJzZXRUaW1lb3V0IiwibWF4QnVzeVRyaWVzIiwiZXJyIiwiSW5zdGFsbGVyQnVpbGRlciIsIiQiLCJsb2ciLCJmaXJzdFBhc3MiLCJsYXN0UmVidWlsZCIsImN1cnJlbnRDb250ZXh0IiwiYXJjaCIsInBsYXRmb3JtIiwicHJvY2VzcyIsInByb2R1Y3Rpb25EZXBzIiwiZW52IiwicGF0aHMiLCJlbGVjdHJvbkFwcCIsInJvb3QiLCJmcmFtZXdvcmtJbmZvIiwidmVyc2lvbiIsImdldEVsZWN0cm9uVmVyc2lvbiIsInVzZUN1c3RvbURpc3QiLCJpbnN0YWxsIiwiZGVidWciLCJwcmVwYXJlTGFzdFJlYnVpbGRPYmplY3QiLCJkZXNrdG9wIiwiZ2V0U2V0dGluZ3MiLCJidWlsZGVyT3B0aW9ucyIsImNvbnRleHQiLCJPYmplY3QiLCJhc3NpZ24iLCJwbGF0Zm9ybU1hdGNoZXMiLCJub2RlTmFtZSIsInJlYnVpbGQiLCJ3YXJuIiwibW92ZU5vZGVNb2R1bGVzT3V0IiwiY2F0Y2giLCJlIiwidGhlbiIsImluc3RhbGxPclJlYnVpbGQiLCJpbnN0YWxsTG9jYWxOb2RlTW9kdWxlcyIsInNjYWZmb2xkIiwiY3JlYXRlQXBwUm9vdCIsImNvcHlTa2VsZXRvbkFwcCIsInBhY2tTa2VsZXRvblRvQXNhciIsIm1ldGVvckFzYXIiLCJkZXNrdG9wQXNhciIsImV4dHJhY3RlZCIsImNvbmZpZyIsImZhdGFsIiwidXRpbHMiLCJleGlzdHMiLCJleHRyYWN0ZWROb2RlTW9kdWxlcyIsImNwIiwiam9pbiIsImdldFBhY2thZ2VkQXBwUGF0aCIsIm12IiwidG1wTm9kZU1vZHVsZXMiLCJub2RlTW9kdWxlcyIsInJlc2V0Iiwid2FpdCIsIm91dCIsInN5bmMiLCJzdGRvdXQiLCJ0b1N0cmluZyIsInNwbGl0IiwicmVnZXgiLCJSZWdFeHAiLCJmb3JFYWNoIiwibGluZSIsIm1hdGNoIiwiZXhlYyIsImxhc3RJbmRleCIsImFwcE91dERpciIsInBhY2thZ2VyIiwiYXBwSW5mbyIsInByb2R1Y3RGaWxlbmFtZSIsIm9wdGlvbnMiLCJvdXRwdXQiLCJpbnN0YWxsZXJEaXIiLCJhcHBBc2FyUGF0aCIsInJldHJpZXMiLCJzZWxmIiwiY2hlY2siLCJvcGVuIiwiZmQiLCJjb2RlIiwiY2xvc2VTeW5jIiwiaWEzMiIsImFsbEFyY2hzIiwidGFyZ2V0cyIsIndpbiIsInB1c2giLCJXSU5ET1dTIiwibGludXgiLCJMSU5VWCIsIm1hYyIsIk1BQyIsImxlbmd0aCIsIm9zIiwiaXNXaW5kb3dzIiwiaXNMaW51eCIsInNldHRpbmdzIiwiZXJyb3IiLCJleGl0IiwiYXNhciIsIm5wbVJlYnVpbGQiLCJiZWZvcmVCdWlsZCIsImJpbmQiLCJhZnRlclBhY2siLCJlbGVjdHJvblZlcnNpb24iLCJkaXJlY3RvcmllcyIsImFwcCIsInByZXBhcmVUYXJnZXRzIiwiYnVpbGRlckNsaU9wdGlvbnMiLCJybSIsImtpbGxNU0J1aWxkIiwidmVyYm9zZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOzs7Ozs7Ozs7Ozs7QUFFQTs7Ozs7OztBQU9BLFNBQVNBLFNBQVQsQ0FBbUJDLE9BQW5CLEVBQXVDO0FBQUEsTUFBWEMsS0FBVyx1RUFBSCxDQUFHO0FBQ25DLFNBQU8sSUFBSUMsT0FBSixDQUFZLFVBQUNDLE9BQUQsRUFBVUMsTUFBVixFQUFxQjtBQUNwQ0MsZUFBVyxZQUFNO0FBQ2IsMkJBQU9MLE9BQVAsRUFBZ0I7QUFDWk0sc0JBQWM7QUFERixPQUFoQixFQUVHLFVBQUNDLEdBQUQsRUFBUztBQUNSLFlBQUlBLEdBQUosRUFBUztBQUNMSCxpQkFBT0csR0FBUDtBQUNILFNBRkQsTUFFTztBQUNISjtBQUNIO0FBQ0osT0FSRDtBQVNILEtBVkQsRUFVR0YsS0FWSDtBQVdILEdBWk0sQ0FBUDtBQWFIO0FBR0Q7Ozs7O0lBR3FCTyxnQjs7O0FBQ2pCOzs7OztBQUtBLDRCQUFZQyxDQUFaLEVBQWU7QUFBQTs7QUFDWCxTQUFLQyxHQUFMLEdBQVcsaUJBQVEsaUJBQVIsQ0FBWDtBQUNBLFNBQUtELENBQUwsR0FBU0EsQ0FBVDtBQUNBLFNBQUtFLFNBQUwsR0FBaUIsSUFBakI7QUFDQSxTQUFLQyxXQUFMLEdBQW1CLEVBQW5CO0FBQ0EsU0FBS0MsY0FBTCxHQUFzQixJQUF0QjtBQUNIO0FBRUQ7Ozs7Ozs7Ozs7OzZDQU95QkMsSSxFQUFtQztBQUFBLFVBQTdCQyxRQUE2Qix1RUFBbEJDLFFBQVFELFFBQVU7QUFDeEQsVUFBTUUsaUJBQWlCLG1EQUF5QixLQUFLUixDQUFMLENBQU9TLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkJDLElBQXRELENBQXZCO0FBQ0EsV0FBS1QsV0FBTCxHQUFtQjtBQUNmVSx1QkFBZTtBQUFFQyxtQkFBUyxLQUFLZCxDQUFMLENBQU9lLGtCQUFQLEVBQVg7QUFBd0NDLHlCQUFlO0FBQXZELFNBREE7QUFFZlYsZ0JBRmU7QUFHZkQsWUFIZTtBQUlmRztBQUplLE9BQW5CO0FBTUEsYUFBTyxLQUFLTCxXQUFaO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozs7OzsrQ0FPdUJFLEk7Ozs7Ozs7O0FBQU1DLHdCLDJEQUFXQyxRQUFRRCxRO0FBQVVXLHVCLDJEQUFVLEs7QUFDaEUscUJBQUtoQixHQUFMLENBQVNpQixLQUFULENBQWdCLDJEQUEwRGIsSUFBSyxFQUEvRTtBQUNBLHFCQUFLYyx3QkFBTCxDQUE4QmQsSUFBOUIsRUFBb0NDLFFBQXBDOzt1QkFDTSw0QkFBaUIsS0FBS04sQ0FBTCxDQUFPb0IsT0FBUCxDQUFlQyxXQUFmLEdBQTZCQyxjQUE3QixJQUErQyxFQUFoRSxFQUNGLEtBQUt0QixDQUFMLENBQU9TLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkJDLElBRDNCLEVBQ2lDLEtBQUtULFdBRHRDLEVBQ21EYyxPQURuRCxDOzs7Ozs7Ozs7Ozs7OztBQUlWOzs7Ozs7Ozs7O2dDQU9ZTSxPLEVBQVM7QUFBQTs7QUFDakIsV0FBS25CLGNBQUwsR0FBc0JvQixPQUFPQyxNQUFQLENBQWMsRUFBZCxFQUFrQkYsT0FBbEIsQ0FBdEI7QUFDQSxhQUFPLElBQUk5QixPQUFKLENBQVksVUFBQ0MsT0FBRCxFQUFVQyxNQUFWLEVBQXFCO0FBQ3BDLFlBQU0rQixrQkFBa0JuQixRQUFRRCxRQUFSLEtBQXFCaUIsUUFBUWpCLFFBQVIsQ0FBaUJxQixRQUE5RDtBQUNBLFlBQU1DLFVBQVVGLG1CQUFtQkgsUUFBUWxCLElBQVIsS0FBaUIsTUFBS0YsV0FBTCxDQUFpQkUsSUFBckU7O0FBQ0EsWUFBSSxDQUFDcUIsZUFBTCxFQUFzQjtBQUNsQixnQkFBS3pCLEdBQUwsQ0FBUzRCLElBQVQsQ0FBYyxxRkFDViwyRkFESjtBQUVIOztBQUVELFlBQUksQ0FBQ0QsT0FBTCxFQUFjO0FBQ1YsZ0JBQUtFLGtCQUFMLEdBQ0tDLEtBREwsQ0FDVztBQUFBLG1CQUFLcEMsT0FBT3FDLENBQVAsQ0FBTDtBQUFBLFdBRFgsRUFFS0MsSUFGTCxDQUVVO0FBQUEsbUJBQU1yQyxXQUFXO0FBQUEscUJBQU1GLFFBQVEsS0FBUixDQUFOO0FBQUEsYUFBWCxFQUFpQyxJQUFqQyxDQUFOO0FBQUEsV0FGVixFQURVLENBSVY7O0FBQ0gsU0FMRCxNQUtPO0FBQ0g7QUFDQSxnQkFBS3dDLGdCQUFMLENBQXNCWCxRQUFRbEIsSUFBOUIsRUFBb0NrQixRQUFRakIsUUFBUixDQUFpQnFCLFFBQXJELEVBQ0tJLEtBREwsQ0FDVztBQUFBLG1CQUFLcEMsT0FBT3FDLENBQVAsQ0FBTDtBQUFBLFdBRFgsRUFFS0MsSUFGTCxDQUVVO0FBQUEsbUJBQU0sTUFBS2pDLENBQUwsQ0FBT1csV0FBUCxDQUFtQndCLHVCQUFuQixDQUEyQ1osUUFBUWxCLElBQW5ELENBQU47QUFBQSxXQUZWLEVBR0swQixLQUhMLENBR1c7QUFBQSxtQkFBS3BDLE9BQU9xQyxDQUFQLENBQUw7QUFBQSxXQUhYLEVBSUtDLElBSkwsQ0FJVSxZQUFNO0FBQ1Isa0JBQUtqQyxDQUFMLENBQU9XLFdBQVAsQ0FBbUJ5QixRQUFuQixDQUE0QkMsYUFBNUI7O0FBQ0Esa0JBQUtyQyxDQUFMLENBQU9XLFdBQVAsQ0FBbUJ5QixRQUFuQixDQUE0QkUsZUFBNUI7O0FBQ0EsbUJBQU8sTUFBS3RDLENBQUwsQ0FBT1csV0FBUCxDQUFtQjRCLGtCQUFuQixDQUNILENBQ0ksTUFBS3ZDLENBQUwsQ0FBT1MsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxXQUFqQixDQUE2QjZCLFVBRGpDLEVBRUksTUFBS3hDLENBQUwsQ0FBT1MsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxXQUFqQixDQUE2QjhCLFdBRmpDLEVBR0ksTUFBS3pDLENBQUwsQ0FBT1MsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxXQUFqQixDQUE2QitCLFNBSGpDLENBREcsQ0FBUDtBQU9ILFdBZEwsRUFlS1gsS0FmTCxDQWVXO0FBQUEsbUJBQUtwQyxPQUFPcUMsQ0FBUCxDQUFMO0FBQUEsV0FmWCxFQWdCS0MsSUFoQkwsQ0FnQlU7QUFBQSxtQkFBTSxNQUFLSCxrQkFBTCxFQUFOO0FBQUEsV0FoQlYsRUFpQktDLEtBakJMLENBaUJXO0FBQUEsbUJBQUtwQyxPQUFPcUMsQ0FBUCxDQUFMO0FBQUEsV0FqQlgsRUFrQktDLElBbEJMLENBa0JVO0FBQUEsbUJBQU12QyxRQUFRLEtBQVIsQ0FBTjtBQUFBLFdBbEJWO0FBbUJIO0FBQ0osT0FuQ00sQ0FBUDtBQW9DSDtBQUVEOzs7Ozs7OzhCQUlVNkIsTyxFQUFTO0FBQUE7O0FBQ2YsYUFBTyxJQUFJOUIsT0FBSixDQUFZLFVBQUNDLE9BQUQsRUFBVUMsTUFBVixFQUFxQjtBQUNwQyx5QkFBTWdELE1BQU4sQ0FBYUMsS0FBYixHQUFxQixJQUFyQjs7QUFFQSxZQUFJLE9BQUs1QyxDQUFMLENBQU82QyxLQUFQLENBQWFDLE1BQWIsQ0FBb0IsT0FBSzlDLENBQUwsQ0FBT1MsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxXQUFqQixDQUE2Qm9DLG9CQUFqRCxDQUFKLEVBQTRFO0FBQ3hFLGlCQUFLOUMsR0FBTCxDQUFTaUIsS0FBVCxDQUFlLDZCQUFmOztBQUNBLDJCQUFNOEIsRUFBTixDQUNJLEtBREosRUFFSSxPQUFLaEQsQ0FBTCxDQUFPUyxHQUFQLENBQVdDLEtBQVgsQ0FBaUJDLFdBQWpCLENBQTZCb0Msb0JBRmpDLEVBR0ksY0FBS0UsSUFBTCxDQUFVLE9BQUtDLGtCQUFMLENBQXdCM0IsT0FBeEIsQ0FBVixFQUE0QyxjQUE1QyxDQUhKO0FBS0g7O0FBRUQsZUFBS3RCLEdBQUwsQ0FBU2lCLEtBQVQsQ0FBZSwwQkFBZixFQVpvQyxDQWFwQzs7O0FBRUEsWUFBSTtBQUNBLDJCQUFNaUMsRUFBTixDQUNJLE9BQUtuRCxDQUFMLENBQU9TLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkJ5QyxjQURqQyxFQUVJLE9BQUtwRCxDQUFMLENBQU9TLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkIwQyxXQUZqQztBQUlILFNBTEQsQ0FLRSxPQUFPckIsQ0FBUCxFQUFVO0FBQ1JyQyxpQkFBT3FDLENBQVA7QUFDQTtBQUNILFNBUkQsU0FRVTtBQUNOLDJCQUFNVyxNQUFOLENBQWFXLEtBQWI7QUFDSDs7QUFFRCxZQUFJLE9BQUtwRCxTQUFULEVBQW9CO0FBQ2hCLGlCQUFLQSxTQUFMLEdBQWlCLEtBQWpCO0FBQ0g7O0FBQ0QsZUFBS0QsR0FBTCxDQUFTaUIsS0FBVCxDQUFlLHlCQUFmOztBQUVBLGVBQUtxQyxJQUFMLEdBQ0t4QixLQURMLENBQ1c7QUFBQSxpQkFBS3BDLE9BQU9xQyxDQUFQLENBQUw7QUFBQSxTQURYLEVBRUtDLElBRkwsQ0FFVTtBQUFBLGlCQUFNdkMsU0FBTjtBQUFBLFNBRlY7QUFHSCxPQW5DTSxDQUFQO0FBb0NIO0FBRUQ7Ozs7Ozs7O2tDQUtjO0FBQUE7O0FBQ1YsVUFBSSxLQUFLVSxjQUFMLENBQW9CRSxRQUFwQixDQUE2QnFCLFFBQTdCLEtBQTBDLE9BQTlDLEVBQXVEO0FBQ25EO0FBQ0g7O0FBQ0QsVUFBSTtBQUNBLFlBQU02QixNQUFNLG9CQUNQQyxJQURPLENBRUosTUFGSSxFQUdKLENBQUMsU0FBRCxFQUFZLE9BQVosRUFBcUIsdUJBQXJCLEVBQThDLEtBQTlDLEVBQXFELFdBQXJELENBSEksRUFLUEMsTUFMTyxDQUtBQyxRQUxBLENBS1MsT0FMVCxFQU1QQyxLQU5PLENBTUQsSUFOQyxDQUFaOztBQVFBLFlBQU1DLFFBQVEsSUFBSUMsTUFBSixDQUFXLE9BQVgsRUFBb0IsSUFBcEIsQ0FBZCxDQVRBLENBVUE7O0FBQ0FOLFlBQUlPLE9BQUosQ0FBWSxVQUFDQyxJQUFELEVBQVU7QUFDbEIsY0FBTUMsUUFBUUosTUFBTUssSUFBTixDQUFXRixJQUFYLEtBQW9CLEtBQWxDOztBQUNBLGNBQUlDLEtBQUosRUFBVztBQUNQLG1CQUFLaEUsR0FBTCxDQUFTaUIsS0FBVCxDQUFnQiwrQkFBOEIrQyxNQUFNLENBQU4sQ0FBUyxFQUF2RDs7QUFDQSxnQ0FBTVIsSUFBTixDQUFXLFVBQVgsRUFBdUIsQ0FBQyxNQUFELEVBQVNRLE1BQU0sQ0FBTixDQUFULEVBQW1CLElBQW5CLEVBQXlCLElBQXpCLENBQXZCO0FBQ0g7O0FBQ0RKLGdCQUFNTSxTQUFOLEdBQWtCLENBQWxCO0FBQ0gsU0FQRDtBQVFILE9BbkJELENBbUJFLE9BQU9uQyxDQUFQLEVBQVU7QUFDUixhQUFLL0IsR0FBTCxDQUFTaUIsS0FBVCxDQUFlLHFCQUFmO0FBQ0g7QUFDSjtBQUVEOzs7Ozs7O3lDQUlpQztBQUFBLFVBQWRLLE9BQWMsdUVBQUosRUFBSTs7QUFDN0IsY0FBUSxLQUFLbkIsY0FBTCxDQUFvQkUsUUFBcEIsQ0FBNkJxQixRQUFyQztBQUNJLGFBQUssUUFBTDtBQUNJLGlCQUFPLGNBQUtzQixJQUFMLENBQ0gxQixRQUFRNkMsU0FETCxFQUVGLEdBQUU3QyxRQUFROEMsUUFBUixDQUFpQkMsT0FBakIsQ0FBeUJDLGVBQWdCLE1BRnpDLEVBR0gsVUFIRyxFQUdTLFdBSFQsRUFHc0IsS0FIdEIsQ0FBUDtBQUtBOztBQUNKLGFBQUssT0FBTDtBQUNJLGlCQUFPLGNBQUt0QixJQUFMLENBQ0gsS0FBS2pELENBQUwsQ0FBT1MsR0FBUCxDQUFXK0QsT0FBWCxDQUFtQkMsTUFEaEIsRUFFSCxLQUFLekUsQ0FBTCxDQUFPUyxHQUFQLENBQVdDLEtBQVgsQ0FBaUJnRSxZQUZkLEVBR0YsT0FBTSxLQUFLdEUsY0FBTCxDQUFvQkMsSUFBcEIsS0FBNkIsTUFBN0IsR0FBc0MsT0FBdEMsR0FBZ0QsRUFBRyxVQUh2RCxFQUlILFdBSkcsRUFJVSxLQUpWLENBQVA7QUFNQTs7QUFDSjtBQUNJLGlCQUFPLGNBQUs0QyxJQUFMLENBQ0gsS0FBS2pELENBQUwsQ0FBT1MsR0FBUCxDQUFXK0QsT0FBWCxDQUFtQkMsTUFEaEIsRUFFSCxLQUFLekUsQ0FBTCxDQUFPUyxHQUFQLENBQVdDLEtBQVgsQ0FBaUJnRSxZQUZkLEVBR0YsZ0JBSEUsRUFJSCxXQUpHLEVBSVUsS0FKVixDQUFQO0FBakJSO0FBd0JIO0FBRUQ7Ozs7Ozs7MkJBSU87QUFDSCxVQUFJLEtBQUt0RSxjQUFMLENBQW9CRSxRQUFwQixDQUE2QnFCLFFBQTdCLEtBQTBDLE9BQTlDLEVBQXVEO0FBQ25ELGVBQU9sQyxRQUFRQyxPQUFSLEVBQVA7QUFDSDs7QUFDRCxVQUFNaUYsY0FBYyxjQUFLMUIsSUFBTCxDQUNoQixLQUFLQyxrQkFBTCxFQURnQixFQUVoQixVQUZnQixDQUFwQjs7QUFJQSxVQUFJMEIsVUFBVSxDQUFkO0FBQ0EsVUFBTUMsT0FBTyxJQUFiO0FBQ0EsYUFBTyxJQUFJcEYsT0FBSixDQUFZLFVBQUNDLE9BQUQsRUFBVUMsTUFBVixFQUFxQjtBQUNwQyxpQkFBU21GLEtBQVQsR0FBaUI7QUFDYixzQkFBR0MsSUFBSCxDQUFRSixXQUFSLEVBQXFCLElBQXJCLEVBQTJCLFVBQUM3RSxHQUFELEVBQU1rRixFQUFOLEVBQWE7QUFDcENKLHVCQUFXLENBQVg7O0FBQ0EsZ0JBQUk5RSxHQUFKLEVBQVM7QUFDTCxrQkFBSUEsSUFBSW1GLElBQUosS0FBYSxRQUFqQixFQUEyQjtBQUN2QkoscUJBQUs1RSxHQUFMLENBQVNpQixLQUFULENBQWdCLHdDQUF1QyxVQUFVcEIsR0FBVixHQUFpQixnQ0FBK0JBLElBQUltRixJQUFLLEVBQXpELEdBQTZELEVBQUcsRUFBdkg7O0FBQ0Esb0JBQUlMLFVBQVUsQ0FBZCxFQUFpQjtBQUNiaEYsNkJBQVc7QUFBQSwyQkFBTWtGLE9BQU47QUFBQSxtQkFBWCxFQUEwQixJQUExQjtBQUNILGlCQUZELE1BRU87QUFDSG5GLHlCQUFRLG1CQUFrQmdGLFdBQVksRUFBdEM7QUFDSDtBQUNKLGVBUEQsTUFPTztBQUNIakY7QUFDSDtBQUNKLGFBWEQsTUFXTztBQUNILDBCQUFHd0YsU0FBSCxDQUFhRixFQUFiOztBQUNBdEY7QUFDSDtBQUNKLFdBakJEO0FBa0JIOztBQUNEb0Y7QUFDSCxPQXRCTSxDQUFQO0FBdUJIO0FBRUQ7Ozs7Ozs7O3FDQUtpQjtBQUNiLFVBQUl6RSxPQUFPLEtBQUtMLENBQUwsQ0FBT1MsR0FBUCxDQUFXK0QsT0FBWCxDQUFtQlcsSUFBbkIsR0FBMEIsTUFBMUIsR0FBbUMsS0FBOUM7QUFDQTlFLGFBQU8sS0FBS0wsQ0FBTCxDQUFPUyxHQUFQLENBQVcrRCxPQUFYLENBQW1CWSxRQUFuQixHQUE4QixLQUE5QixHQUFzQy9FLElBQTdDO0FBRUEsVUFBTWdGLFVBQVUsRUFBaEI7O0FBRUEsVUFBSSxLQUFLckYsQ0FBTCxDQUFPUyxHQUFQLENBQVcrRCxPQUFYLENBQW1CYyxHQUF2QixFQUE0QjtBQUN4QkQsZ0JBQVFFLElBQVIsQ0FBYSwwQkFBU0MsT0FBdEI7QUFDSDs7QUFDRCxVQUFJLEtBQUt4RixDQUFMLENBQU9TLEdBQVAsQ0FBVytELE9BQVgsQ0FBbUJpQixLQUF2QixFQUE4QjtBQUMxQkosZ0JBQVFFLElBQVIsQ0FBYSwwQkFBU0csS0FBdEI7QUFDSDs7QUFDRCxVQUFJLEtBQUsxRixDQUFMLENBQU9TLEdBQVAsQ0FBVytELE9BQVgsQ0FBbUJtQixHQUF2QixFQUE0QjtBQUN4Qk4sZ0JBQVFFLElBQVIsQ0FBYSwwQkFBU0ssR0FBdEI7QUFDSDs7QUFFRCxVQUFJUCxRQUFRUSxNQUFSLEtBQW1CLENBQXZCLEVBQTBCO0FBQ3RCLFlBQUksS0FBSzdGLENBQUwsQ0FBT1MsR0FBUCxDQUFXcUYsRUFBWCxDQUFjQyxTQUFsQixFQUE2QjtBQUN6QlYsa0JBQVFFLElBQVIsQ0FBYSwwQkFBU0MsT0FBdEI7QUFDSCxTQUZELE1BRU8sSUFBSSxLQUFLeEYsQ0FBTCxDQUFPUyxHQUFQLENBQVdxRixFQUFYLENBQWNFLE9BQWxCLEVBQTJCO0FBQzlCWCxrQkFBUUUsSUFBUixDQUFhLDBCQUFTRyxLQUF0QjtBQUNILFNBRk0sTUFFQTtBQUNITCxrQkFBUUUsSUFBUixDQUFhLDBCQUFTSyxHQUF0QjtBQUNIO0FBQ0o7O0FBQ0QsYUFBTyxvQ0FBY1AsT0FBZCxFQUF1QixJQUF2QixFQUE2QmhGLElBQTdCLENBQVA7QUFDSDs7Ozs7Ozs7Ozs7O0FBR1M0Rix3QixHQUFXLEtBQUtqRyxDQUFMLENBQU9vQixPQUFQLENBQWVDLFdBQWYsRTs7QUFDakIsb0JBQUksRUFBRSxvQkFBb0I0RSxRQUF0QixDQUFKLEVBQXFDO0FBQ2pDLHVCQUFLaEcsR0FBTCxDQUFTaUcsS0FBVCxDQUNJLDhDQURKO0FBR0EzRiwwQkFBUTRGLElBQVIsQ0FBYSxDQUFiO0FBQ0g7O0FBRUs3RSw4QixHQUFpQkUsT0FBT0MsTUFBUCxDQUFjLEVBQWQsRUFBa0J3RSxTQUFTM0UsY0FBM0IsQztBQUV2QkEsK0JBQWU4RSxJQUFmLEdBQXNCLEtBQXRCO0FBQ0E5RSwrQkFBZStFLFVBQWYsR0FBNEIsSUFBNUI7QUFFQS9FLCtCQUFlZ0YsV0FBZixHQUE2QixLQUFLQSxXQUFMLENBQWlCQyxJQUFqQixDQUFzQixJQUF0QixDQUE3QjtBQUNBakYsK0JBQWVrRixTQUFmLEdBQTJCLEtBQUtBLFNBQUwsQ0FBZUQsSUFBZixDQUFvQixJQUFwQixDQUEzQjtBQUNBakYsK0JBQWVtRixlQUFmLEdBQWlDLEtBQUt6RyxDQUFMLENBQU9lLGtCQUFQLEVBQWpDO0FBRUFPLCtCQUFlb0YsV0FBZixHQUE2QjtBQUN6QkMsdUJBQUssS0FBSzNHLENBQUwsQ0FBT1MsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxXQUFqQixDQUE2QkMsSUFEVDtBQUV6QjZELDBCQUFRLGNBQUt4QixJQUFMLENBQVUsS0FBS2pELENBQUwsQ0FBT1MsR0FBUCxDQUFXK0QsT0FBWCxDQUFtQkMsTUFBN0IsRUFBcUMsS0FBS3pFLENBQUwsQ0FBT1MsR0FBUCxDQUFXQyxLQUFYLENBQWlCZ0UsWUFBdEQ7QUFGaUIsaUJBQTdCOztBQU1JLHFCQUFLekUsR0FBTCxDQUFTaUIsS0FBVCxDQUFlLHFDQUFmOzt1QkFDTSw0QkFBTU0sT0FBT0MsTUFBUCxDQUFjO0FBQ3RCNEQsMkJBQVMsS0FBS3VCLGNBQUwsRUFEYTtBQUV0QmpFLDBCQUFRckI7QUFGYyxpQkFBZCxFQUdUMkUsU0FBU1ksaUJBSEEsQ0FBTixDOzs7QUFLTixvQkFBSSxLQUFLN0csQ0FBTCxDQUFPNkMsS0FBUCxDQUFhQyxNQUFiLENBQW9CLEtBQUs5QyxDQUFMLENBQU9TLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkJvQyxvQkFBakQsQ0FBSixFQUE0RTtBQUN4RSxtQ0FBTStELEVBQU4sQ0FBUyxLQUFULEVBQWdCLEtBQUs5RyxDQUFMLENBQU9TLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkJvQyxvQkFBN0M7QUFDSDs7Ozs7Ozs7QUFFRCxxQkFBSzlDLEdBQUwsQ0FBU2lHLEtBQVQsQ0FBZSxrQ0FBZjs7Ozs7Ozs7Ozs7Ozs7QUFJUjs7Ozs7Ozs7eUNBS3FCO0FBQUE7O0FBQ2pCLGFBQU8sSUFBSXpHLE9BQUosQ0FBWSxVQUFDQyxPQUFELEVBQVVDLE1BQVYsRUFBcUI7QUFDcEMsZUFBS00sR0FBTCxDQUFTaUIsS0FBVCxDQUFlLDZEQUNYLFdBREo7O0FBRUEsZUFBSzZGLFdBQUw7O0FBQ0F6SCxrQkFBVSxPQUFLVSxDQUFMLENBQU9TLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkJ5QyxjQUF2QyxFQUNLckIsS0FETCxDQUNXO0FBQUEsaUJBQUtwQyxPQUFPcUMsQ0FBUCxDQUFMO0FBQUEsU0FEWCxFQUVLQyxJQUZMLENBRVUsWUFBTTtBQUNSLDJCQUFNVSxNQUFOLENBQWFDLEtBQWIsR0FBcUIsSUFBckI7QUFDQSwyQkFBTUQsTUFBTixDQUFhcUUsT0FBYixHQUF1QixJQUF2Qjs7QUFDQSxjQUFJO0FBQ0EsNkJBQU03RCxFQUFOLENBQ0ksT0FBS25ELENBQUwsQ0FBT1MsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxXQUFqQixDQUE2QjBDLFdBRGpDLEVBRUksT0FBS3JELENBQUwsQ0FBT1MsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxXQUFqQixDQUE2QnlDLGNBRmpDOztBQUlBLDZCQUFNVCxNQUFOLENBQWFXLEtBQWI7O0FBQ0EsbUJBQU8sT0FBS0MsSUFBTCxFQUFQO0FBQ0gsV0FQRCxDQU9FLE9BQU92QixDQUFQLEVBQVU7QUFDUiw2QkFBTVcsTUFBTixDQUFhVyxLQUFiOztBQUNBLG1CQUFPN0QsUUFBUUUsTUFBUixDQUFlcUMsQ0FBZixDQUFQO0FBQ0g7QUFDSixTQWhCTCxFQWlCS0QsS0FqQkwsQ0FpQlc7QUFBQSxpQkFBS3BDLE9BQU9xQyxDQUFQLENBQUw7QUFBQSxTQWpCWCxFQWtCS0MsSUFsQkwsQ0FrQlU7QUFBQSxpQkFBTTNDLFVBQVUsT0FBS1UsQ0FBTCxDQUFPUyxHQUFQLENBQVdDLEtBQVgsQ0FBaUJDLFdBQWpCLENBQTZCMEMsV0FBdkMsRUFBb0QsSUFBcEQsQ0FBTjtBQUFBLFNBbEJWLEVBbUJLdEIsS0FuQkwsQ0FtQlc7QUFBQSxpQkFBS3BDLE9BQU9xQyxDQUFQLENBQUw7QUFBQSxTQW5CWCxFQW9CS0MsSUFwQkwsQ0FvQlU7QUFBQSxpQkFBTSxPQUFLc0IsSUFBTCxFQUFOO0FBQUEsU0FwQlYsRUFxQkt4QixLQXJCTCxDQXFCV3BDLE1BckJYLEVBc0JLc0MsSUF0QkwsQ0FzQlV2QyxPQXRCVjtBQXVCSCxPQTNCTSxDQUFQO0FBNEJIIiwiZmlsZSI6ImVsZWN0cm9uQnVpbGRlci5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGJ1aWxkLCBQbGF0Zm9ybSwgY3JlYXRlVGFyZ2V0cyB9IGZyb20gJ2VsZWN0cm9uLWJ1aWxkZXInO1xuaW1wb3J0IHsgaW5zdGFsbE9yUmVidWlsZCB9IGZyb20gJ2VsZWN0cm9uLWJ1aWxkZXItbGliL291dC91dGlsL3lhcm4nO1xuaW1wb3J0IHsgY3JlYXRlTGF6eVByb2R1Y3Rpb25EZXBzIH0gZnJvbSAnZWxlY3Ryb24tYnVpbGRlci1saWIvb3V0L3V0aWwvcGFja2FnZURlcGVuZGVuY2llcyc7XG5pbXBvcnQgc2hlbGwgZnJvbSAnc2hlbGxqcyc7XG5pbXBvcnQgcGF0aCBmcm9tICdwYXRoJztcbmltcG9ydCBmcyBmcm9tICdmcyc7XG5pbXBvcnQgcmltcmFmIGZyb20gJ3JpbXJhZic7XG5pbXBvcnQgc3Bhd24gZnJvbSAnY3Jvc3Mtc3Bhd24nO1xuaW1wb3J0IExvZyBmcm9tICcuL2xvZyc7XG5cbi8qKlxuICogUHJvbWlzZmllZCByaW1yYWYuXG4gKlxuICogQHBhcmFtIHtzdHJpbmd9IGRpclBhdGggLSBwYXRoIHRvIHRoZSBkaXIgdG8gYmUgZGVsZXRlZFxuICogQHBhcmFtIHtudW1iZXJ9IGRlbGF5IC0gZGVsYXkgdGhlIHRhc2sgYnkgbXNcbiAqIEByZXR1cm5zIHtQcm9taXNlPGFueT59XG4gKi9cbmZ1bmN0aW9uIHJlbW92ZURpcihkaXJQYXRoLCBkZWxheSA9IDApIHtcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgIHJpbXJhZihkaXJQYXRoLCB7XG4gICAgICAgICAgICAgICAgbWF4QnVzeVRyaWVzOiAxMDBcbiAgICAgICAgICAgIH0sIChlcnIpID0+IHtcbiAgICAgICAgICAgICAgICBpZiAoZXJyKSB7XG4gICAgICAgICAgICAgICAgICAgIHJlamVjdChlcnIpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSwgZGVsYXkpO1xuICAgIH0pO1xufVxuXG5cbi8qKlxuICogV3JhcHBlciBmb3IgZWxlY3Ryb24tYnVpbGRlci5cbiAqL1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgSW5zdGFsbGVyQnVpbGRlciB7XG4gICAgLyoqXG4gICAgICogQHBhcmFtIHtNZXRlb3JEZXNrdG9wfSAkIC0gY29udGV4dFxuICAgICAqXG4gICAgICogQGNvbnN0cnVjdG9yXG4gICAgICovXG4gICAgY29uc3RydWN0b3IoJCkge1xuICAgICAgICB0aGlzLmxvZyA9IG5ldyBMb2coJ2VsZWN0cm9uQnVpbGRlcicpO1xuICAgICAgICB0aGlzLiQgPSAkO1xuICAgICAgICB0aGlzLmZpcnN0UGFzcyA9IHRydWU7XG4gICAgICAgIHRoaXMubGFzdFJlYnVpbGQgPSB7fTtcbiAgICAgICAgdGhpcy5jdXJyZW50Q29udGV4dCA9IG51bGw7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogUHJlcGFyZXMgdGhlIGxhc3QgcmVidWlsZCBvYmplY3QgZm9yIGVsZWN0cm9uLWJ1aWxkZXIuXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gYXJjaFxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBwbGF0Zm9ybVxuICAgICAqIEByZXR1cm5zIHtPYmplY3R9XG4gICAgICovXG4gICAgcHJlcGFyZUxhc3RSZWJ1aWxkT2JqZWN0KGFyY2gsIHBsYXRmb3JtID0gcHJvY2Vzcy5wbGF0Zm9ybSkge1xuICAgICAgICBjb25zdCBwcm9kdWN0aW9uRGVwcyA9IGNyZWF0ZUxhenlQcm9kdWN0aW9uRGVwcyh0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLnJvb3QpO1xuICAgICAgICB0aGlzLmxhc3RSZWJ1aWxkID0ge1xuICAgICAgICAgICAgZnJhbWV3b3JrSW5mbzogeyB2ZXJzaW9uOiB0aGlzLiQuZ2V0RWxlY3Ryb25WZXJzaW9uKCksIHVzZUN1c3RvbURpc3Q6IHRydWUgfSxcbiAgICAgICAgICAgIHBsYXRmb3JtLFxuICAgICAgICAgICAgYXJjaCxcbiAgICAgICAgICAgIHByb2R1Y3Rpb25EZXBzXG4gICAgICAgIH07XG4gICAgICAgIHJldHVybiB0aGlzLmxhc3RSZWJ1aWxkO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIENhbGxzIG5wbSByZWJ1aWxkIGZyb20gZWxlY3Ryb24tYnVpbGRlci5cbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gYXJjaFxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBwbGF0Zm9ybVxuICAgICAqIEBwYXJhbSB7Ym9vbGVhbn0gaW5zdGFsbFxuICAgICAqIEByZXR1cm5zIHtQcm9taXNlfVxuICAgICAqL1xuICAgIGFzeW5jIGluc3RhbGxPclJlYnVpbGQoYXJjaCwgcGxhdGZvcm0gPSBwcm9jZXNzLnBsYXRmb3JtLCBpbnN0YWxsID0gZmFsc2UpIHtcbiAgICAgICAgdGhpcy5sb2cuZGVidWcoYGNhbGxpbmcgaW5zdGFsbE9yUmVidWlsZCBmcm9tIGVsZWN0cm9uLWJ1aWxkZXIgZm9yIGFyY2ggJHthcmNofWApO1xuICAgICAgICB0aGlzLnByZXBhcmVMYXN0UmVidWlsZE9iamVjdChhcmNoLCBwbGF0Zm9ybSk7XG4gICAgICAgIGF3YWl0IGluc3RhbGxPclJlYnVpbGQodGhpcy4kLmRlc2t0b3AuZ2V0U2V0dGluZ3MoKS5idWlsZGVyT3B0aW9ucyB8fCB7fSxcbiAgICAgICAgICAgIHRoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAucm9vdCwgdGhpcy5sYXN0UmVidWlsZCwgaW5zdGFsbCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQ2FsbGJhY2sgaW52b2tlZCBiZWZvcmUgYnVpbGQgaXMgbWFkZS4gRW5zdXJlcyB0aGF0IGFwcC5hc2FyIGhhdmUgdGhlIHJpZ2h0IHJlYnVpbHRcbiAgICAgKiBub2RlX21vZHVsZXMuXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gY29udGV4dFxuICAgICAqIEByZXR1cm5zIHtQcm9taXNlfVxuICAgICAqL1xuICAgIGJlZm9yZUJ1aWxkKGNvbnRleHQpIHtcbiAgICAgICAgdGhpcy5jdXJyZW50Q29udGV4dCA9IE9iamVjdC5hc3NpZ24oe30sIGNvbnRleHQpO1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICAgICAgY29uc3QgcGxhdGZvcm1NYXRjaGVzID0gcHJvY2Vzcy5wbGF0Zm9ybSA9PT0gY29udGV4dC5wbGF0Zm9ybS5ub2RlTmFtZTtcbiAgICAgICAgICAgIGNvbnN0IHJlYnVpbGQgPSBwbGF0Zm9ybU1hdGNoZXMgJiYgY29udGV4dC5hcmNoICE9PSB0aGlzLmxhc3RSZWJ1aWxkLmFyY2g7XG4gICAgICAgICAgICBpZiAoIXBsYXRmb3JtTWF0Y2hlcykge1xuICAgICAgICAgICAgICAgIHRoaXMubG9nLndhcm4oJ3NraXBwaW5nIGRlcGVuZGVuY2llcyByZWJ1aWxkIGJlY2F1c2UgcGxhdGZvcm0gaXMgZGlmZmVyZW50LCBpZiB5b3UgaGF2ZSBuYXRpdmUgJyArXG4gICAgICAgICAgICAgICAgICAgICdub2RlIG1vZHVsZXMgYXMgeW91ciBhcHAgZGVwZW5kZW5jaWVzIHlvdSBzaG91bGQgb2QgdGhlIGJ1aWxkIG9uIHRoZSB0YXJnZXQgcGxhdGZvcm0gb25seScpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoIXJlYnVpbGQpIHtcbiAgICAgICAgICAgICAgICB0aGlzLm1vdmVOb2RlTW9kdWxlc091dCgpXG4gICAgICAgICAgICAgICAgICAgIC5jYXRjaChlID0+IHJlamVjdChlKSlcbiAgICAgICAgICAgICAgICAgICAgLnRoZW4oKCkgPT4gc2V0VGltZW91dCgoKSA9PiByZXNvbHZlKGZhbHNlKSwgMjAwMCkpO1xuICAgICAgICAgICAgICAgIC8vIFRpbWVvdXQgaGVscHMgb24gV2luZG93cyB0byBjbGVhciB0aGUgZmlsZSBsb2Nrcy5cbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgLy8gTGV0cyByZWJ1aWxkIHRoZSBub2RlX21vZHVsZXMgZm9yIGRpZmZlcmVudCBhcmNoLlxuICAgICAgICAgICAgICAgIHRoaXMuaW5zdGFsbE9yUmVidWlsZChjb250ZXh0LmFyY2gsIGNvbnRleHQucGxhdGZvcm0ubm9kZU5hbWUpXG4gICAgICAgICAgICAgICAgICAgIC5jYXRjaChlID0+IHJlamVjdChlKSlcbiAgICAgICAgICAgICAgICAgICAgLnRoZW4oKCkgPT4gdGhpcy4kLmVsZWN0cm9uQXBwLmluc3RhbGxMb2NhbE5vZGVNb2R1bGVzKGNvbnRleHQuYXJjaCkpXG4gICAgICAgICAgICAgICAgICAgIC5jYXRjaChlID0+IHJlamVjdChlKSlcbiAgICAgICAgICAgICAgICAgICAgLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kLmVsZWN0cm9uQXBwLnNjYWZmb2xkLmNyZWF0ZUFwcFJvb3QoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJC5lbGVjdHJvbkFwcC5zY2FmZm9sZC5jb3B5U2tlbGV0b25BcHAoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLiQuZWxlY3Ryb25BcHAucGFja1NrZWxldG9uVG9Bc2FyKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5tZXRlb3JBc2FyLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLmRlc2t0b3BBc2FyLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLmV4dHJhY3RlZFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIC5jYXRjaChlID0+IHJlamVjdChlKSlcbiAgICAgICAgICAgICAgICAgICAgLnRoZW4oKCkgPT4gdGhpcy5tb3ZlTm9kZU1vZHVsZXNPdXQoKSlcbiAgICAgICAgICAgICAgICAgICAgLmNhdGNoKGUgPT4gcmVqZWN0KGUpKVxuICAgICAgICAgICAgICAgICAgICAudGhlbigoKSA9PiByZXNvbHZlKGZhbHNlKSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIENhbGxiYWNrIHRvIGJlIGludm9rZWQgYWZ0ZXIgcGFja2luZy4gUmVzdG9yZXMgbm9kZV9tb2R1bGVzIHRvIHRoZSAuZGVza3RvcC1idWlsZC5cbiAgICAgKiBAcmV0dXJucyB7UHJvbWlzZX1cbiAgICAgKi9cbiAgICBhZnRlclBhY2soY29udGV4dCkge1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICAgICAgc2hlbGwuY29uZmlnLmZhdGFsID0gdHJ1ZTtcblxuICAgICAgICAgICAgaWYgKHRoaXMuJC51dGlscy5leGlzdHModGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5leHRyYWN0ZWROb2RlTW9kdWxlcykpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmxvZy5kZWJ1ZygnaW5qZWN0aW5nIGV4dHJhY3RlZCBtb2R1bGVzJyk7XG4gICAgICAgICAgICAgICAgc2hlbGwuY3AoXG4gICAgICAgICAgICAgICAgICAgICctUmYnLFxuICAgICAgICAgICAgICAgICAgICB0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLmV4dHJhY3RlZE5vZGVNb2R1bGVzLFxuICAgICAgICAgICAgICAgICAgICBwYXRoLmpvaW4odGhpcy5nZXRQYWNrYWdlZEFwcFBhdGgoY29udGV4dCksICdub2RlX21vZHVsZXMnKVxuICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHRoaXMubG9nLmRlYnVnKCdtb3Zpbmcgbm9kZV9tb2R1bGVzIGJhY2snKTtcbiAgICAgICAgICAgIC8vIE1vdmUgbm9kZV9tb2R1bGVzIGJhY2suXG5cbiAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgc2hlbGwubXYoXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAudG1wTm9kZU1vZHVsZXMsXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAubm9kZU1vZHVsZXNcbiAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgICAgIHJlamVjdChlKTtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9IGZpbmFsbHkge1xuICAgICAgICAgICAgICAgIHNoZWxsLmNvbmZpZy5yZXNldCgpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodGhpcy5maXJzdFBhc3MpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmZpcnN0UGFzcyA9IGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5sb2cuZGVidWcoJ25vZGVfbW9kdWxlcyBtb3ZlZCBiYWNrJyk7XG5cbiAgICAgICAgICAgIHRoaXMud2FpdCgpXG4gICAgICAgICAgICAgICAgLmNhdGNoKGUgPT4gcmVqZWN0KGUpKVxuICAgICAgICAgICAgICAgIC50aGVuKCgpID0+IHJlc29sdmUoKSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFRoaXMgY29tbWFuZCBraWxscyBvcnBoYW5lZCBNU0J1aWxkLmV4ZSBwcm9jZXNzZXMuXG4gICAgICogU29tZXRpbWUgYWZ0ZXIgbmF0aXZlIG5vZGVfbW9kdWxlcyBjb21waWxhdGlvbiB0aGV5IGFyZSBzdGlsbCB3cml0aW5nIHNvbWUgbG9ncyxcbiAgICAgKiBwcmV2ZW50IG5vZGVfbW9kdWxlcyBmcm9tIGJlaW5nIGRlbGV0ZWQuXG4gICAgICovXG4gICAga2lsbE1TQnVpbGQoKSB7XG4gICAgICAgIGlmICh0aGlzLmN1cnJlbnRDb250ZXh0LnBsYXRmb3JtLm5vZGVOYW1lICE9PSAnd2luMzInKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGNvbnN0IG91dCA9IHNwYXduXG4gICAgICAgICAgICAgICAgLnN5bmMoXG4gICAgICAgICAgICAgICAgICAgICd3bWljJyxcbiAgICAgICAgICAgICAgICAgICAgWydwcm9jZXNzJywgJ3doZXJlJywgJ2NhcHRpb249XCJNU0J1aWxkLmV4ZVwiJywgJ2dldCcsICdwcm9jZXNzaWQnXVxuICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAuc3Rkb3V0LnRvU3RyaW5nKCd1dGYtOCcpXG4gICAgICAgICAgICAgICAgLnNwbGl0KCdcXG4nKTtcblxuICAgICAgICAgICAgY29uc3QgcmVnZXggPSBuZXcgUmVnRXhwKC8oXFxkKykvLCAnZ20nKTtcbiAgICAgICAgICAgIC8vIE5vIHdlIHdpbGwgY2hlY2sgZm9yIHRob3NlIHdpdGggdGhlIG1hdGNoaW5nIHBhcmFtcy5cbiAgICAgICAgICAgIG91dC5mb3JFYWNoKChsaW5lKSA9PiB7XG4gICAgICAgICAgICAgICAgY29uc3QgbWF0Y2ggPSByZWdleC5leGVjKGxpbmUpIHx8IGZhbHNlO1xuICAgICAgICAgICAgICAgIGlmIChtYXRjaCkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvZy5kZWJ1Zyhga2lsbGluZyBNU0J1aWxkLmV4ZSBhdCBwaWQ6ICR7bWF0Y2hbMV19YCk7XG4gICAgICAgICAgICAgICAgICAgIHNwYXduLnN5bmMoJ3Rhc2traWxsJywgWycvcGlkJywgbWF0Y2hbMV0sICcvZicsICcvdCddKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgcmVnZXgubGFzdEluZGV4ID0gMDtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgICB0aGlzLmxvZy5kZWJ1Zygna2lsbCBNU0J1aWxkIGZhaWxlZCcpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0aGUgcGF0aCB0byBwYWNrYWdlZCBhcHAuXG4gICAgICogQHJldHVybnMge3N0cmluZ31cbiAgICAgKi9cbiAgICBnZXRQYWNrYWdlZEFwcFBhdGgoY29udGV4dCA9IHt9KSB7XG4gICAgICAgIHN3aXRjaCAodGhpcy5jdXJyZW50Q29udGV4dC5wbGF0Zm9ybS5ub2RlTmFtZSkge1xuICAgICAgICAgICAgY2FzZSAnZGFyd2luJzpcbiAgICAgICAgICAgICAgICByZXR1cm4gcGF0aC5qb2luKFxuICAgICAgICAgICAgICAgICAgICBjb250ZXh0LmFwcE91dERpcixcbiAgICAgICAgICAgICAgICAgICAgYCR7Y29udGV4dC5wYWNrYWdlci5hcHBJbmZvLnByb2R1Y3RGaWxlbmFtZX0uYXBwYCxcbiAgICAgICAgICAgICAgICAgICAgJ0NvbnRlbnRzJywgJ1Jlc291cmNlcycsICdhcHAnXG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgJ3dpbjMyJzpcbiAgICAgICAgICAgICAgICByZXR1cm4gcGF0aC5qb2luKFxuICAgICAgICAgICAgICAgICAgICB0aGlzLiQuZW52Lm9wdGlvbnMub3V0cHV0LFxuICAgICAgICAgICAgICAgICAgICB0aGlzLiQuZW52LnBhdGhzLmluc3RhbGxlckRpcixcbiAgICAgICAgICAgICAgICAgICAgYHdpbi0ke3RoaXMuY3VycmVudENvbnRleHQuYXJjaCA9PT0gJ2lhMzInID8gJ2lhMzItJyA6ICcnfXVucGFja2VkYCxcbiAgICAgICAgICAgICAgICAgICAgJ3Jlc291cmNlcycsICdhcHAnXG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgcmV0dXJuIHBhdGguam9pbihcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kLmVudi5vcHRpb25zLm91dHB1dCxcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kLmVudi5wYXRocy5pbnN0YWxsZXJEaXIsXG4gICAgICAgICAgICAgICAgICAgIGBsaW51eC11bnBhY2tlZGAsXG4gICAgICAgICAgICAgICAgICAgICdyZXNvdXJjZXMnLCAnYXBwJ1xuICAgICAgICAgICAgICAgICk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBPbiBXaW5kb3dzIGl0IHdhaXRzIGZvciB0aGUgYXBwLmFzYXIgaW4gdGhlIHBhY2tlZCBhcHAgdG8gYmUgZnJlZSAobm8gZmlsZSBsb2NrcykuXG4gICAgICogQHJldHVybnMgeyp9XG4gICAgICovXG4gICAgd2FpdCgpIHtcbiAgICAgICAgaWYgKHRoaXMuY3VycmVudENvbnRleHQucGxhdGZvcm0ubm9kZU5hbWUgIT09ICd3aW4zMicpIHtcbiAgICAgICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoKTtcbiAgICAgICAgfVxuICAgICAgICBjb25zdCBhcHBBc2FyUGF0aCA9IHBhdGguam9pbihcbiAgICAgICAgICAgIHRoaXMuZ2V0UGFja2FnZWRBcHBQYXRoKCksXG4gICAgICAgICAgICAnYXBwLmFzYXInXG4gICAgICAgICk7XG4gICAgICAgIGxldCByZXRyaWVzID0gMDtcbiAgICAgICAgY29uc3Qgc2VsZiA9IHRoaXM7XG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICAgICAgICBmdW5jdGlvbiBjaGVjaygpIHtcbiAgICAgICAgICAgICAgICBmcy5vcGVuKGFwcEFzYXJQYXRoLCAncisnLCAoZXJyLCBmZCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICByZXRyaWVzICs9IDE7XG4gICAgICAgICAgICAgICAgICAgIGlmIChlcnIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChlcnIuY29kZSAhPT0gJ0VOT0VOVCcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWxmLmxvZy5kZWJ1Zyhgd2FpdGluZyBmb3IgYXBwLmFzYXIgdG8gYmUgcmVhZGFibGUsICR7J2NvZGUnIGluIGVyciA/IGBjdXJyZW50bHkgcmVhZGluZyBpdCByZXR1cm5zICR7ZXJyLmNvZGV9YCA6ICcnfWApO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXRyaWVzIDwgNikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IGNoZWNrKCksIDQwMDApO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlamVjdChgZmlsZSBpcyBsb2NrZWQ6ICR7YXBwQXNhclBhdGh9YCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBmcy5jbG9zZVN5bmMoZmQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSgpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjaGVjaygpO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBQcmVwYXJlcyB0aGUgdGFyZ2V0IG9iamVjdCBwYXNzZWQgdG8gdGhlIGVsZWN0cm9uLWJ1aWxkZXIuXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7TWFwPFBsYXRmb3JtLCBNYXA8QXJjaCwgQXJyYXk8c3RyaW5nPj4+fVxuICAgICAqL1xuICAgIHByZXBhcmVUYXJnZXRzKCkge1xuICAgICAgICBsZXQgYXJjaCA9IHRoaXMuJC5lbnYub3B0aW9ucy5pYTMyID8gJ2lhMzInIDogJ3g2NCc7XG4gICAgICAgIGFyY2ggPSB0aGlzLiQuZW52Lm9wdGlvbnMuYWxsQXJjaHMgPyAnYWxsJyA6IGFyY2g7XG5cbiAgICAgICAgY29uc3QgdGFyZ2V0cyA9IFtdO1xuXG4gICAgICAgIGlmICh0aGlzLiQuZW52Lm9wdGlvbnMud2luKSB7XG4gICAgICAgICAgICB0YXJnZXRzLnB1c2goUGxhdGZvcm0uV0lORE9XUyk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMuJC5lbnYub3B0aW9ucy5saW51eCkge1xuICAgICAgICAgICAgdGFyZ2V0cy5wdXNoKFBsYXRmb3JtLkxJTlVYKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAodGhpcy4kLmVudi5vcHRpb25zLm1hYykge1xuICAgICAgICAgICAgdGFyZ2V0cy5wdXNoKFBsYXRmb3JtLk1BQyk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGFyZ2V0cy5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICAgIGlmICh0aGlzLiQuZW52Lm9zLmlzV2luZG93cykge1xuICAgICAgICAgICAgICAgIHRhcmdldHMucHVzaChQbGF0Zm9ybS5XSU5ET1dTKTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy4kLmVudi5vcy5pc0xpbnV4KSB7XG4gICAgICAgICAgICAgICAgdGFyZ2V0cy5wdXNoKFBsYXRmb3JtLkxJTlVYKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGFyZ2V0cy5wdXNoKFBsYXRmb3JtLk1BQyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGNyZWF0ZVRhcmdldHModGFyZ2V0cywgbnVsbCwgYXJjaCk7XG4gICAgfVxuXG4gICAgYXN5bmMgYnVpbGQoKSB7XG4gICAgICAgIGNvbnN0IHNldHRpbmdzID0gdGhpcy4kLmRlc2t0b3AuZ2V0U2V0dGluZ3MoKTtcbiAgICAgICAgaWYgKCEoJ2J1aWxkZXJPcHRpb25zJyBpbiBzZXR0aW5ncykpIHtcbiAgICAgICAgICAgIHRoaXMubG9nLmVycm9yKFxuICAgICAgICAgICAgICAgICdubyBidWlsZGVyT3B0aW9ucyBpbiBzZXR0aW5ncy5qc29uLCBhYm9ydGluZydcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgICBwcm9jZXNzLmV4aXQoMSk7XG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCBidWlsZGVyT3B0aW9ucyA9IE9iamVjdC5hc3NpZ24oe30sIHNldHRpbmdzLmJ1aWxkZXJPcHRpb25zKTtcblxuICAgICAgICBidWlsZGVyT3B0aW9ucy5hc2FyID0gZmFsc2U7XG4gICAgICAgIGJ1aWxkZXJPcHRpb25zLm5wbVJlYnVpbGQgPSB0cnVlO1xuXG4gICAgICAgIGJ1aWxkZXJPcHRpb25zLmJlZm9yZUJ1aWxkID0gdGhpcy5iZWZvcmVCdWlsZC5iaW5kKHRoaXMpO1xuICAgICAgICBidWlsZGVyT3B0aW9ucy5hZnRlclBhY2sgPSB0aGlzLmFmdGVyUGFjay5iaW5kKHRoaXMpO1xuICAgICAgICBidWlsZGVyT3B0aW9ucy5lbGVjdHJvblZlcnNpb24gPSB0aGlzLiQuZ2V0RWxlY3Ryb25WZXJzaW9uKCk7XG5cbiAgICAgICAgYnVpbGRlck9wdGlvbnMuZGlyZWN0b3JpZXMgPSB7XG4gICAgICAgICAgICBhcHA6IHRoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAucm9vdCxcbiAgICAgICAgICAgIG91dHB1dDogcGF0aC5qb2luKHRoaXMuJC5lbnYub3B0aW9ucy5vdXRwdXQsIHRoaXMuJC5lbnYucGF0aHMuaW5zdGFsbGVyRGlyKVxuICAgICAgICB9O1xuXG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICB0aGlzLmxvZy5kZWJ1ZygnY2FsbGluZyBidWlsZCBmcm9tIGVsZWN0cm9uLWJ1aWxkZXInKTtcbiAgICAgICAgICAgIGF3YWl0IGJ1aWxkKE9iamVjdC5hc3NpZ24oe1xuICAgICAgICAgICAgICAgIHRhcmdldHM6IHRoaXMucHJlcGFyZVRhcmdldHMoKSxcbiAgICAgICAgICAgICAgICBjb25maWc6IGJ1aWxkZXJPcHRpb25zXG4gICAgICAgICAgICB9LCBzZXR0aW5ncy5idWlsZGVyQ2xpT3B0aW9ucykpO1xuXG4gICAgICAgICAgICBpZiAodGhpcy4kLnV0aWxzLmV4aXN0cyh0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLmV4dHJhY3RlZE5vZGVNb2R1bGVzKSkge1xuICAgICAgICAgICAgICAgIHNoZWxsLnJtKCctcmYnLCB0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLmV4dHJhY3RlZE5vZGVNb2R1bGVzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgdGhpcy5sb2cuZXJyb3IoJ2Vycm9yIHdoaWxlIGJ1aWxkaW5nIGluc3RhbGxlcjogJywgZSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBNb3ZlcyBub2RlX21vZHVsZXMgb3V0IG9mIHRoZSBhcHAgYmVjYXVzZSB3aGlsZSB0aGUgYXBwIHdpbGwgYmUgcGFja2FnZWRcbiAgICAgKiB3ZSBkbyBub3Qgd2FudCBpdCB0byBiZSB0aGVyZS5cbiAgICAgKiBAcmV0dXJucyB7UHJvbWlzZTxhbnk+fVxuICAgICAqL1xuICAgIG1vdmVOb2RlTW9kdWxlc091dCgpIHtcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgICAgICAgIHRoaXMubG9nLmRlYnVnKCdtb3Zpbmcgbm9kZV9tb2R1bGVzIG91dCwgYmVjYXVzZSB3ZSBoYXZlIHRoZW0gYWxyZWFkeSBpbicgK1xuICAgICAgICAgICAgICAgICcgYXBwLmFzYXInKTtcbiAgICAgICAgICAgIHRoaXMua2lsbE1TQnVpbGQoKTtcbiAgICAgICAgICAgIHJlbW92ZURpcih0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLnRtcE5vZGVNb2R1bGVzKVxuICAgICAgICAgICAgICAgIC5jYXRjaChlID0+IHJlamVjdChlKSlcbiAgICAgICAgICAgICAgICAudGhlbigoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHNoZWxsLmNvbmZpZy5mYXRhbCA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgIHNoZWxsLmNvbmZpZy52ZXJib3NlID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNoZWxsLm12KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAubm9kZU1vZHVsZXMsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC50bXBOb2RlTW9kdWxlc1xuICAgICAgICAgICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNoZWxsLmNvbmZpZy5yZXNldCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMud2FpdCgpO1xuICAgICAgICAgICAgICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzaGVsbC5jb25maWcucmVzZXQoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLmNhdGNoKGUgPT4gcmVqZWN0KGUpKVxuICAgICAgICAgICAgICAgIC50aGVuKCgpID0+IHJlbW92ZURpcih0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLm5vZGVNb2R1bGVzLCAxMDAwKSlcbiAgICAgICAgICAgICAgICAuY2F0Y2goZSA9PiByZWplY3QoZSkpXG4gICAgICAgICAgICAgICAgLnRoZW4oKCkgPT4gdGhpcy53YWl0KCkpXG4gICAgICAgICAgICAgICAgLmNhdGNoKHJlamVjdClcbiAgICAgICAgICAgICAgICAudGhlbihyZXNvbHZlKTtcbiAgICAgICAgfSk7XG4gICAgfVxufVxuIl19