"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _crossSpawn = _interopRequireDefault(require("cross-spawn"));

var _electron = _interopRequireDefault(require("electron"));

var _log = _interopRequireDefault(require("./log"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * Simple Electron runner. Runs the project with the bin provided by the 'electron' package.
 * @class
 */
var Electron =
/*#__PURE__*/
function () {
  function Electron($) {
    _classCallCheck(this, Electron);

    this.log = new _log.default('electron');
    this.$ = $;
  }

  _createClass(Electron, [{
    key: "run",
    value: function run() {
      // Until: https://github.com/electron-userland/electron-prebuilt/pull/118
      var _process = process,
          env = _process.env;
      env.ELECTRON_ENV = 'development';
      var child = (0, _crossSpawn.default)(_electron.default, ['.'], {
        cwd: this.$.env.paths.electronApp.root,
        env
      }); // TODO: check if we can configure piping in spawn options

      child.stdout.on('data', function (chunk) {
        process.stdout.write(chunk);
      });
      child.stderr.on('data', function (chunk) {
        process.stderr.write(chunk);
      });
    }
  }]);

  return Electron;
}();

exports.default = Electron;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL2xpYi9lbGVjdHJvbi5qcyJdLCJuYW1lcyI6WyJFbGVjdHJvbiIsIiQiLCJsb2ciLCJwcm9jZXNzIiwiZW52IiwiRUxFQ1RST05fRU5WIiwiY2hpbGQiLCJjd2QiLCJwYXRocyIsImVsZWN0cm9uQXBwIiwicm9vdCIsInN0ZG91dCIsIm9uIiwiY2h1bmsiLCJ3cml0ZSIsInN0ZGVyciJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUVBOzs7Ozs7Ozs7O0FBRUE7Ozs7SUFJcUJBLFE7OztBQUNqQixvQkFBWUMsQ0FBWixFQUFlO0FBQUE7O0FBQ1gsU0FBS0MsR0FBTCxHQUFXLGlCQUFRLFVBQVIsQ0FBWDtBQUNBLFNBQUtELENBQUwsR0FBU0EsQ0FBVDtBQUNIOzs7OzBCQUVLO0FBQ0Y7QUFERSxxQkFFY0UsT0FGZDtBQUFBLFVBRU1DLEdBRk4sWUFFTUEsR0FGTjtBQUdGQSxVQUFJQyxZQUFKLEdBQW1CLGFBQW5CO0FBRUEsVUFBTUMsUUFBUSw0Q0FBc0IsQ0FBQyxHQUFELENBQXRCLEVBQTZCO0FBQ3ZDQyxhQUFLLEtBQUtOLENBQUwsQ0FBT0csR0FBUCxDQUFXSSxLQUFYLENBQWlCQyxXQUFqQixDQUE2QkMsSUFESztBQUV2Q047QUFGdUMsT0FBN0IsQ0FBZCxDQUxFLENBVUY7O0FBQ0FFLFlBQU1LLE1BQU4sQ0FBYUMsRUFBYixDQUFnQixNQUFoQixFQUF3QixVQUFDQyxLQUFELEVBQVc7QUFDL0JWLGdCQUFRUSxNQUFSLENBQWVHLEtBQWYsQ0FBcUJELEtBQXJCO0FBQ0gsT0FGRDtBQUdBUCxZQUFNUyxNQUFOLENBQWFILEVBQWIsQ0FBZ0IsTUFBaEIsRUFBd0IsVUFBQ0MsS0FBRCxFQUFXO0FBQy9CVixnQkFBUVksTUFBUixDQUFlRCxLQUFmLENBQXFCRCxLQUFyQjtBQUNILE9BRkQ7QUFHSCIsImZpbGUiOiJlbGVjdHJvbi5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBzcGF3biBmcm9tICdjcm9zcy1zcGF3bic7XG5pbXBvcnQgcGF0aFRvRWxlY3Ryb24gZnJvbSAnZWxlY3Ryb24nO1xuXG5pbXBvcnQgTG9nIGZyb20gJy4vbG9nJztcblxuLyoqXG4gKiBTaW1wbGUgRWxlY3Ryb24gcnVubmVyLiBSdW5zIHRoZSBwcm9qZWN0IHdpdGggdGhlIGJpbiBwcm92aWRlZCBieSB0aGUgJ2VsZWN0cm9uJyBwYWNrYWdlLlxuICogQGNsYXNzXG4gKi9cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEVsZWN0cm9uIHtcbiAgICBjb25zdHJ1Y3RvcigkKSB7XG4gICAgICAgIHRoaXMubG9nID0gbmV3IExvZygnZWxlY3Ryb24nKTtcbiAgICAgICAgdGhpcy4kID0gJDtcbiAgICB9XG5cbiAgICBydW4oKSB7XG4gICAgICAgIC8vIFVudGlsOiBodHRwczovL2dpdGh1Yi5jb20vZWxlY3Ryb24tdXNlcmxhbmQvZWxlY3Ryb24tcHJlYnVpbHQvcHVsbC8xMThcbiAgICAgICAgY29uc3QgeyBlbnYgfSA9IHByb2Nlc3M7XG4gICAgICAgIGVudi5FTEVDVFJPTl9FTlYgPSAnZGV2ZWxvcG1lbnQnO1xuXG4gICAgICAgIGNvbnN0IGNoaWxkID0gc3Bhd24ocGF0aFRvRWxlY3Ryb24sIFsnLiddLCB7XG4gICAgICAgICAgICBjd2Q6IHRoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAucm9vdCxcbiAgICAgICAgICAgIGVudlxuICAgICAgICB9KTtcblxuICAgICAgICAvLyBUT0RPOiBjaGVjayBpZiB3ZSBjYW4gY29uZmlndXJlIHBpcGluZyBpbiBzcGF3biBvcHRpb25zXG4gICAgICAgIGNoaWxkLnN0ZG91dC5vbignZGF0YScsIChjaHVuaykgPT4ge1xuICAgICAgICAgICAgcHJvY2Vzcy5zdGRvdXQud3JpdGUoY2h1bmspO1xuICAgICAgICB9KTtcbiAgICAgICAgY2hpbGQuc3RkZXJyLm9uKCdkYXRhJywgKGNodW5rKSA9PiB7XG4gICAgICAgICAgICBwcm9jZXNzLnN0ZGVyci53cml0ZShjaHVuayk7XG4gICAgICAgIH0pO1xuICAgIH1cbn1cbiJdfQ==