"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _runtime = _interopRequireDefault(require("regenerator-runtime/runtime"));

var _fs = _interopRequireDefault(require("fs"));

var _del = _interopRequireDefault(require("del"));

var _shelljs = _interopRequireDefault(require("shelljs"));

var _path = _interopRequireDefault(require("path"));

var _log = _interopRequireDefault(require("./log"));

var _skeletonDependencies = _interopRequireDefault(require("./skeletonDependencies"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var join = _path.default.join;
_shelljs.default.config.fatal = true;
/**
 * Represents the .desktop dir scaffold.
 */

var ElectronAppScaffold =
/*#__PURE__*/
function () {
  /**
   * @param {MeteorDesktop} $ - context
   * @constructor
   */
  function ElectronAppScaffold($) {
    _classCallCheck(this, ElectronAppScaffold);

    this.log = new _log.default('electronAppScaffold');
    this.$ = $;
    this.packageJson = {
      name: 'MyMeteorApp',
      main: this.$.env.isProductionBuild() ? 'app.asar/index.js' : 'app/index.js',
      dependencies: Object.assign({}, _skeletonDependencies.default)
    };

    if (!this.$.env.isProductionBuild()) {
      this.packageJson.dependencies.devtron = '1.4.0';
      this.packageJson.dependencies['electron-debug'] = '1.1.0';
    }
  }
  /**
   * Just a public getter from the default package.json object.
   * @returns {Object}
   */


  _createClass(ElectronAppScaffold, [{
    key: "getDefaultPackageJson",
    value: function getDefaultPackageJson() {
      return Object.assign({}, this.packageJson);
    }
    /**
     * Clear the electron app. Removes everything except the node_modules which would be a waste
     * to delete. Later `npm prune` will keep it clear.
     */

  }, {
    key: "clear",
    value: function clear() {
      if (!this.$.utils.exists(this.$.env.paths.electronApp.root)) {
        this.log.verbose(`creating ${this.$.env.paths.electronApp.rootName}`);

        _shelljs.default.mkdir(this.$.env.paths.electronApp.root);
      }

      return (0, _del.default)([`${this.$.env.paths.electronApp.root}${_path.default.sep}*`, `!${this.$.env.paths.electronApp.nodeModules}`]);
    }
    /**
     * Just copies the Skeleton App into the electron app.
     */

  }, {
    key: "copySkeletonApp",
    value: function copySkeletonApp() {
      this.log.verbose('copying skeleton app');

      try {
        _shelljs.default.cp('-rf', join(this.$.env.paths.meteorDesktop.skeleton, '*'), this.$.env.paths.electronApp.appRoot + _path.default.sep);
      } catch (e) {
        this.log.error('error while copying skeleton app:', e);
        process.exit(1);
      }
    }
    /**
     * After clearing the electron app path, copies a fresh skeleton.
     */

  }, {
    key: "make",
    value: function () {
      var _make = _asyncToGenerator(
      /*#__PURE__*/
      _runtime.default.mark(function _callee() {
        return _runtime.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                this.log.verbose(`clearing ${this.$.env.paths.electronApp.rootName}`);
                _context.next = 4;
                return this.clear();

              case 4:
                _context.next = 10;
                break;

              case 6:
                _context.prev = 6;
                _context.t0 = _context["catch"](0);
                this.log.error(`error while removing ${this.$.env.paths.electronApp.root}: `, _context.t0);
                process.exit(1);

              case 10:
                this.createAppRoot();
                this.copySkeletonApp(); // TODO: hey, wait, .gitignore is not needed - right?

                /*
                this.log.debug('creating .gitignore');
                fs.writeFileSync(this.$.env.paths.electronApp.gitIgnore, [
                    'node_modules'
                ].join('\n'));
                */

                this.log.verbose('writing package.json');

                _fs.default.writeFileSync(this.$.env.paths.electronApp.packageJson, JSON.stringify(this.packageJson, null, 2));

              case 14:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 6]]);
      }));

      return function make() {
        return _make.apply(this, arguments);
      };
    }()
    /**
     * Creates the app directory in the electron app.
     */

  }, {
    key: "createAppRoot",
    value: function createAppRoot() {
      try {
        this.log.verbose(`creating ${this.$.env.paths.electronApp.appRoot}`);

        _fs.default.mkdirSync(this.$.env.paths.electronApp.appRoot);
      } catch (e) {
        if (e.code !== 'EEXIST') {
          this.log.error(`error while creating dir: ${this.$.env.paths.electronApp.appRoot}: `, e);
          process.exit(1);
        }
      }
    }
  }]);

  return ElectronAppScaffold;
}();

exports.default = ElectronAppScaffold;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL2xpYi9lbGVjdHJvbkFwcFNjYWZmb2xkLmpzIl0sIm5hbWVzIjpbImpvaW4iLCJjb25maWciLCJmYXRhbCIsIkVsZWN0cm9uQXBwU2NhZmZvbGQiLCIkIiwibG9nIiwicGFja2FnZUpzb24iLCJuYW1lIiwibWFpbiIsImVudiIsImlzUHJvZHVjdGlvbkJ1aWxkIiwiZGVwZW5kZW5jaWVzIiwiT2JqZWN0IiwiYXNzaWduIiwiZGV2dHJvbiIsInV0aWxzIiwiZXhpc3RzIiwicGF0aHMiLCJlbGVjdHJvbkFwcCIsInJvb3QiLCJ2ZXJib3NlIiwicm9vdE5hbWUiLCJta2RpciIsInNlcCIsIm5vZGVNb2R1bGVzIiwiY3AiLCJtZXRlb3JEZXNrdG9wIiwic2tlbGV0b24iLCJhcHBSb290IiwiZSIsImVycm9yIiwicHJvY2VzcyIsImV4aXQiLCJjbGVhciIsImNyZWF0ZUFwcFJvb3QiLCJjb3B5U2tlbGV0b25BcHAiLCJ3cml0ZUZpbGVTeW5jIiwiSlNPTiIsInN0cmluZ2lmeSIsIm1rZGlyU3luYyIsImNvZGUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFDQTs7Ozs7Ozs7Ozs7O0lBRVFBLEksaUJBQUFBLEk7QUFDUixpQkFBTUMsTUFBTixDQUFhQyxLQUFiLEdBQXFCLElBQXJCO0FBRUE7Ozs7SUFHcUJDLG1COzs7QUFDakI7Ozs7QUFJQSwrQkFBWUMsQ0FBWixFQUFlO0FBQUE7O0FBQ1gsU0FBS0MsR0FBTCxHQUFXLGlCQUFRLHFCQUFSLENBQVg7QUFDQSxTQUFLRCxDQUFMLEdBQVNBLENBQVQ7QUFFQSxTQUFLRSxXQUFMLEdBQW1CO0FBQ2ZDLFlBQU0sYUFEUztBQUVmQyxZQUFPLEtBQUtKLENBQUwsQ0FBT0ssR0FBUCxDQUFXQyxpQkFBWCxFQUFELEdBQ0YsbUJBREUsR0FDb0IsY0FIWDtBQUlmQyxvQkFBY0MsT0FBT0MsTUFBUCxDQUFjLEVBQWQ7QUFKQyxLQUFuQjs7QUFPQSxRQUFJLENBQUMsS0FBS1QsQ0FBTCxDQUFPSyxHQUFQLENBQVdDLGlCQUFYLEVBQUwsRUFBcUM7QUFDakMsV0FBS0osV0FBTCxDQUFpQkssWUFBakIsQ0FBOEJHLE9BQTlCLEdBQXdDLE9BQXhDO0FBQ0EsV0FBS1IsV0FBTCxDQUFpQkssWUFBakIsQ0FBOEIsZ0JBQTlCLElBQWtELE9BQWxEO0FBQ0g7QUFDSjtBQUVEOzs7Ozs7Ozs0Q0FJd0I7QUFDcEIsYUFBT0MsT0FBT0MsTUFBUCxDQUFjLEVBQWQsRUFBa0IsS0FBS1AsV0FBdkIsQ0FBUDtBQUNIO0FBRUQ7Ozs7Ozs7NEJBSVE7QUFDSixVQUFJLENBQUMsS0FBS0YsQ0FBTCxDQUFPVyxLQUFQLENBQWFDLE1BQWIsQ0FBb0IsS0FBS1osQ0FBTCxDQUFPSyxHQUFQLENBQVdRLEtBQVgsQ0FBaUJDLFdBQWpCLENBQTZCQyxJQUFqRCxDQUFMLEVBQTZEO0FBQ3pELGFBQUtkLEdBQUwsQ0FBU2UsT0FBVCxDQUFrQixZQUFXLEtBQUtoQixDQUFMLENBQU9LLEdBQVAsQ0FBV1EsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkJHLFFBQVMsRUFBbkU7O0FBQ0EseUJBQU1DLEtBQU4sQ0FBWSxLQUFLbEIsQ0FBTCxDQUFPSyxHQUFQLENBQVdRLEtBQVgsQ0FBaUJDLFdBQWpCLENBQTZCQyxJQUF6QztBQUNIOztBQUVELGFBQU8sa0JBQUksQ0FDTixHQUFFLEtBQUtmLENBQUwsQ0FBT0ssR0FBUCxDQUFXUSxLQUFYLENBQWlCQyxXQUFqQixDQUE2QkMsSUFBSyxHQUFFLGNBQUtJLEdBQUksR0FEekMsRUFFTixJQUFHLEtBQUtuQixDQUFMLENBQU9LLEdBQVAsQ0FBV1EsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkJNLFdBQVksRUFGdEMsQ0FBSixDQUFQO0FBSUg7QUFFRDs7Ozs7O3NDQUdrQjtBQUNkLFdBQUtuQixHQUFMLENBQVNlLE9BQVQsQ0FBaUIsc0JBQWpCOztBQUNBLFVBQUk7QUFDQSx5QkFBTUssRUFBTixDQUNJLEtBREosRUFFSXpCLEtBQUssS0FBS0ksQ0FBTCxDQUFPSyxHQUFQLENBQVdRLEtBQVgsQ0FBaUJTLGFBQWpCLENBQStCQyxRQUFwQyxFQUE4QyxHQUE5QyxDQUZKLEVBR0ksS0FBS3ZCLENBQUwsQ0FBT0ssR0FBUCxDQUFXUSxLQUFYLENBQWlCQyxXQUFqQixDQUE2QlUsT0FBN0IsR0FBdUMsY0FBS0wsR0FIaEQ7QUFLSCxPQU5ELENBTUUsT0FBT00sQ0FBUCxFQUFVO0FBQ1IsYUFBS3hCLEdBQUwsQ0FBU3lCLEtBQVQsQ0FBZSxtQ0FBZixFQUFvREQsQ0FBcEQ7QUFDQUUsZ0JBQVFDLElBQVIsQ0FBYSxDQUFiO0FBQ0g7QUFDSjtBQUVEOzs7Ozs7Ozs7Ozs7Ozs7QUFLUSxxQkFBSzNCLEdBQUwsQ0FBU2UsT0FBVCxDQUFrQixZQUFXLEtBQUtoQixDQUFMLENBQU9LLEdBQVAsQ0FBV1EsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkJHLFFBQVMsRUFBbkU7O3VCQUNNLEtBQUtZLEtBQUwsRTs7Ozs7Ozs7O0FBRU4scUJBQUs1QixHQUFMLENBQVN5QixLQUFULENBQ0ssd0JBQXVCLEtBQUsxQixDQUFMLENBQU9LLEdBQVAsQ0FBV1EsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkJDLElBQUssSUFEOUQ7QUFHQVksd0JBQVFDLElBQVIsQ0FBYSxDQUFiOzs7QUFHSixxQkFBS0UsYUFBTDtBQUVBLHFCQUFLQyxlQUFMLEcsQ0FFQTs7QUFDQTs7Ozs7OztBQU1BLHFCQUFLOUIsR0FBTCxDQUFTZSxPQUFULENBQWlCLHNCQUFqQjs7QUFDQSw0QkFBR2dCLGFBQUgsQ0FDSSxLQUFLaEMsQ0FBTCxDQUFPSyxHQUFQLENBQVdRLEtBQVgsQ0FBaUJDLFdBQWpCLENBQTZCWixXQURqQyxFQUM4QytCLEtBQUtDLFNBQUwsQ0FBZSxLQUFLaEMsV0FBcEIsRUFBaUMsSUFBakMsRUFBdUMsQ0FBdkMsQ0FEOUM7Ozs7Ozs7Ozs7Ozs7O0FBS0o7Ozs7OztvQ0FHZ0I7QUFDWixVQUFJO0FBQ0EsYUFBS0QsR0FBTCxDQUFTZSxPQUFULENBQWtCLFlBQVcsS0FBS2hCLENBQUwsQ0FBT0ssR0FBUCxDQUFXUSxLQUFYLENBQWlCQyxXQUFqQixDQUE2QlUsT0FBUSxFQUFsRTs7QUFDQSxvQkFBR1csU0FBSCxDQUFhLEtBQUtuQyxDQUFMLENBQU9LLEdBQVAsQ0FBV1EsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkJVLE9BQTFDO0FBQ0gsT0FIRCxDQUdFLE9BQU9DLENBQVAsRUFBVTtBQUNSLFlBQUlBLEVBQUVXLElBQUYsS0FBVyxRQUFmLEVBQXlCO0FBQ3JCLGVBQUtuQyxHQUFMLENBQVN5QixLQUFULENBQ0ssNkJBQTRCLEtBQUsxQixDQUFMLENBQU9LLEdBQVAsQ0FBV1EsS0FBWCxDQUFpQkMsV0FBakIsQ0FBNkJVLE9BQVEsSUFEdEUsRUFDMkVDLENBRDNFO0FBR0FFLGtCQUFRQyxJQUFSLENBQWEsQ0FBYjtBQUNIO0FBQ0o7QUFDSiIsImZpbGUiOiJlbGVjdHJvbkFwcFNjYWZmb2xkLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHJlZ2VuZXJhdG9yUnVudGltZSBmcm9tICdyZWdlbmVyYXRvci1ydW50aW1lL3J1bnRpbWUnO1xuaW1wb3J0IGZzIGZyb20gJ2ZzJztcbmltcG9ydCBkZWwgZnJvbSAnZGVsJztcbmltcG9ydCBzaGVsbCBmcm9tICdzaGVsbGpzJztcbmltcG9ydCBwYXRoIGZyb20gJ3BhdGgnO1xuXG5pbXBvcnQgTG9nIGZyb20gJy4vbG9nJztcbmltcG9ydCBkZXBlbmRlbmNpZXMgZnJvbSAnLi9za2VsZXRvbkRlcGVuZGVuY2llcyc7XG5cbmNvbnN0IHsgam9pbiB9ID0gcGF0aDtcbnNoZWxsLmNvbmZpZy5mYXRhbCA9IHRydWU7XG5cbi8qKlxuICogUmVwcmVzZW50cyB0aGUgLmRlc2t0b3AgZGlyIHNjYWZmb2xkLlxuICovXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBFbGVjdHJvbkFwcFNjYWZmb2xkIHtcbiAgICAvKipcbiAgICAgKiBAcGFyYW0ge01ldGVvckRlc2t0b3B9ICQgLSBjb250ZXh0XG4gICAgICogQGNvbnN0cnVjdG9yXG4gICAgICovXG4gICAgY29uc3RydWN0b3IoJCkge1xuICAgICAgICB0aGlzLmxvZyA9IG5ldyBMb2coJ2VsZWN0cm9uQXBwU2NhZmZvbGQnKTtcbiAgICAgICAgdGhpcy4kID0gJDtcblxuICAgICAgICB0aGlzLnBhY2thZ2VKc29uID0ge1xuICAgICAgICAgICAgbmFtZTogJ015TWV0ZW9yQXBwJyxcbiAgICAgICAgICAgIG1haW46ICh0aGlzLiQuZW52LmlzUHJvZHVjdGlvbkJ1aWxkKCkpID9cbiAgICAgICAgICAgICAgICAnYXBwLmFzYXIvaW5kZXguanMnIDogJ2FwcC9pbmRleC5qcycsXG4gICAgICAgICAgICBkZXBlbmRlbmNpZXM6IE9iamVjdC5hc3NpZ24oe30sIGRlcGVuZGVuY2llcylcbiAgICAgICAgfTtcblxuICAgICAgICBpZiAoIXRoaXMuJC5lbnYuaXNQcm9kdWN0aW9uQnVpbGQoKSkge1xuICAgICAgICAgICAgdGhpcy5wYWNrYWdlSnNvbi5kZXBlbmRlbmNpZXMuZGV2dHJvbiA9ICcxLjQuMCc7XG4gICAgICAgICAgICB0aGlzLnBhY2thZ2VKc29uLmRlcGVuZGVuY2llc1snZWxlY3Ryb24tZGVidWcnXSA9ICcxLjEuMCc7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBKdXN0IGEgcHVibGljIGdldHRlciBmcm9tIHRoZSBkZWZhdWx0IHBhY2thZ2UuanNvbiBvYmplY3QuXG4gICAgICogQHJldHVybnMge09iamVjdH1cbiAgICAgKi9cbiAgICBnZXREZWZhdWx0UGFja2FnZUpzb24oKSB7XG4gICAgICAgIHJldHVybiBPYmplY3QuYXNzaWduKHt9LCB0aGlzLnBhY2thZ2VKc29uKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBDbGVhciB0aGUgZWxlY3Ryb24gYXBwLiBSZW1vdmVzIGV2ZXJ5dGhpbmcgZXhjZXB0IHRoZSBub2RlX21vZHVsZXMgd2hpY2ggd291bGQgYmUgYSB3YXN0ZVxuICAgICAqIHRvIGRlbGV0ZS4gTGF0ZXIgYG5wbSBwcnVuZWAgd2lsbCBrZWVwIGl0IGNsZWFyLlxuICAgICAqL1xuICAgIGNsZWFyKCkge1xuICAgICAgICBpZiAoIXRoaXMuJC51dGlscy5leGlzdHModGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5yb290KSkge1xuICAgICAgICAgICAgdGhpcy5sb2cudmVyYm9zZShgY3JlYXRpbmcgJHt0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLnJvb3ROYW1lfWApO1xuICAgICAgICAgICAgc2hlbGwubWtkaXIodGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5yb290KTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBkZWwoW1xuICAgICAgICAgICAgYCR7dGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5yb290fSR7cGF0aC5zZXB9KmAsXG4gICAgICAgICAgICBgISR7dGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5ub2RlTW9kdWxlc31gXG4gICAgICAgIF0pO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEp1c3QgY29waWVzIHRoZSBTa2VsZXRvbiBBcHAgaW50byB0aGUgZWxlY3Ryb24gYXBwLlxuICAgICAqL1xuICAgIGNvcHlTa2VsZXRvbkFwcCgpIHtcbiAgICAgICAgdGhpcy5sb2cudmVyYm9zZSgnY29weWluZyBza2VsZXRvbiBhcHAnKTtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIHNoZWxsLmNwKFxuICAgICAgICAgICAgICAgICctcmYnLFxuICAgICAgICAgICAgICAgIGpvaW4odGhpcy4kLmVudi5wYXRocy5tZXRlb3JEZXNrdG9wLnNrZWxldG9uLCAnKicpLFxuICAgICAgICAgICAgICAgIHRoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAuYXBwUm9vdCArIHBhdGguc2VwXG4gICAgICAgICAgICApO1xuICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgICB0aGlzLmxvZy5lcnJvcignZXJyb3Igd2hpbGUgY29weWluZyBza2VsZXRvbiBhcHA6JywgZSk7XG4gICAgICAgICAgICBwcm9jZXNzLmV4aXQoMSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBBZnRlciBjbGVhcmluZyB0aGUgZWxlY3Ryb24gYXBwIHBhdGgsIGNvcGllcyBhIGZyZXNoIHNrZWxldG9uLlxuICAgICAqL1xuICAgIGFzeW5jIG1ha2UoKSB7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICB0aGlzLmxvZy52ZXJib3NlKGBjbGVhcmluZyAke3RoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAucm9vdE5hbWV9YCk7XG4gICAgICAgICAgICBhd2FpdCB0aGlzLmNsZWFyKCk7XG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgIHRoaXMubG9nLmVycm9yKFxuICAgICAgICAgICAgICAgIGBlcnJvciB3aGlsZSByZW1vdmluZyAke3RoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAucm9vdH06IGAsIGVcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgICBwcm9jZXNzLmV4aXQoMSk7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLmNyZWF0ZUFwcFJvb3QoKTtcblxuICAgICAgICB0aGlzLmNvcHlTa2VsZXRvbkFwcCgpO1xuXG4gICAgICAgIC8vIFRPRE86IGhleSwgd2FpdCwgLmdpdGlnbm9yZSBpcyBub3QgbmVlZGVkIC0gcmlnaHQ/XG4gICAgICAgIC8qXG4gICAgICAgIHRoaXMubG9nLmRlYnVnKCdjcmVhdGluZyAuZ2l0aWdub3JlJyk7XG4gICAgICAgIGZzLndyaXRlRmlsZVN5bmModGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5naXRJZ25vcmUsIFtcbiAgICAgICAgICAgICdub2RlX21vZHVsZXMnXG4gICAgICAgIF0uam9pbignXFxuJykpO1xuICAgICAgICAqL1xuICAgICAgICB0aGlzLmxvZy52ZXJib3NlKCd3cml0aW5nIHBhY2thZ2UuanNvbicpO1xuICAgICAgICBmcy53cml0ZUZpbGVTeW5jKFxuICAgICAgICAgICAgdGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5wYWNrYWdlSnNvbiwgSlNPTi5zdHJpbmdpZnkodGhpcy5wYWNrYWdlSnNvbiwgbnVsbCwgMilcbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIHRoZSBhcHAgZGlyZWN0b3J5IGluIHRoZSBlbGVjdHJvbiBhcHAuXG4gICAgICovXG4gICAgY3JlYXRlQXBwUm9vdCgpIHtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIHRoaXMubG9nLnZlcmJvc2UoYGNyZWF0aW5nICR7dGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5hcHBSb290fWApO1xuICAgICAgICAgICAgZnMubWtkaXJTeW5jKHRoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAuYXBwUm9vdCk7XG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgIGlmIChlLmNvZGUgIT09ICdFRVhJU1QnKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5sb2cuZXJyb3IoXG4gICAgICAgICAgICAgICAgICAgIGBlcnJvciB3aGlsZSBjcmVhdGluZyBkaXI6ICR7dGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5hcHBSb290fTogYCwgZVxuICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgcHJvY2Vzcy5leGl0KDEpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxufVxuIl19