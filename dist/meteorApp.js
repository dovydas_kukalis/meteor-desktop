"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _fs = _interopRequireDefault(require("fs"));

var _crossSpawn = _interopRequireDefault(require("cross-spawn"));

var _semver = _interopRequireDefault(require("semver"));

var _shelljs = _interopRequireDefault(require("shelljs"));

var _path = _interopRequireDefault(require("path"));

var _singleLineLog = _interopRequireDefault(require("single-line-log"));

var _asar = _interopRequireDefault(require("asar"));

var _nodeFetch = _interopRequireDefault(require("node-fetch"));

var _isDesktopInjector = _interopRequireDefault(require("../skeleton/modules/autoupdate/isDesktopInjector"));

var _log = _interopRequireDefault(require("./log"));

var _meteorManager = _interopRequireDefault(require("./meteorManager"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _slicedToArray(arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return _sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var join = _path.default.join;
var sll = _singleLineLog.default.stdout; // TODO: refactor all strategy ifs to one place

/**
 * Represents the Meteor app.
 * @property {MeteorDesktop} $
 * @class
 */

var MeteorApp =
/*#__PURE__*/
function () {
  /**
   * @param {MeteorDesktop} $ - context
   * @constructor
   */
  function MeteorApp($) {
    _classCallCheck(this, MeteorApp);

    this.log = new _log.default('meteorApp');
    this.$ = $;
    this.meteorManager = new _meteorManager.default($);
    this.mobilePlatform = null;
    this.oldManifest = null;
    this.injector = new _isDesktopInjector.default();
    this.matcher = new RegExp('__meteor_runtime_config__ = JSON.parse\\(decodeURIComponent\\("([^"]*)"\\)\\)');
    this.replacer = new RegExp('(__meteor_runtime_config__ = JSON.parse\\(decodeURIComponent\\()"([^"]*)"(\\)\\))');
    this.meteorVersion = null;
    this.indexHTMLstrategy = null;
    this.indexHTMLStrategies = {
      INDEX_FROM_CORDOVA_BUILD: 1,
      INDEX_FROM_RUNNING_SERVER: 2
    };
  }
  /**
   * Ensures that required packages are added to the Meteor app.
   */


  _createClass(MeteorApp, [{
    key: "ensureDesktopHCPPackages",
    value: function () {
      var _ensureDesktopHCPPackages = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee() {
        var _this = this;

        var desktopHCPPackages, packagesWithVersion;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                desktopHCPPackages = ['omega:meteor-desktop-watcher', 'omega:meteor-desktop-bundler'];

                if (!this.$.desktop.getSettings().desktopHCP) {
                  _context.next = 14;
                  break;
                }

                this.log.verbose('desktopHCP is enabled, checking for required packages');
                packagesWithVersion = desktopHCPPackages.map(function (packageName) {
                  return `${packageName}@${_this.$.getVersion()}`;
                });
                _context.prev = 4;
                _context.next = 7;
                return this.meteorManager.ensurePackages(desktopHCPPackages, packagesWithVersion, 'desktopHCP');

              case 7:
                _context.next = 12;
                break;

              case 9:
                _context.prev = 9;
                _context.t0 = _context["catch"](4);
                throw new Error(_context.t0);

              case 12:
                _context.next = 24;
                break;

              case 14:
                this.log.verbose('desktopHCP is not enabled, removing required packages');
                _context.prev = 15;

                if (!this.meteorManager.checkPackages(desktopHCPPackages)) {
                  _context.next = 19;
                  break;
                }

                _context.next = 19;
                return this.meteorManager.deletePackages(desktopHCPPackages, 'desktopHCP');

              case 19:
                _context.next = 24;
                break;

              case 21:
                _context.prev = 21;
                _context.t1 = _context["catch"](15);
                throw new Error(_context.t1);

              case 24:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[4, 9], [15, 21]]);
      }));

      return function ensureDesktopHCPPackages() {
        return _ensureDesktopHCPPackages.apply(this, arguments);
      };
    }()
    /**
     * Adds entry to .meteor/.gitignore if necessary.
     */

  }, {
    key: "updateGitIgnore",
    value: function updateGitIgnore() {
      this.log.verbose('updating .meteor/.gitignore'); // Lets read the .meteor/.gitignore and filter out blank lines.

      var gitIgnore = _fs.default.readFileSync(this.$.env.paths.meteorApp.gitIgnore, 'UTF-8').split('\n').filter(function (ignoredPath) {
        return ignoredPath.trim() !== '';
      });

      if (!~gitIgnore.indexOf(this.$.env.paths.electronApp.rootName)) {
        this.log.verbose(`adding ${this.$.env.paths.electronApp.rootName} to .meteor/.gitignore`);
        gitIgnore.push(this.$.env.paths.electronApp.rootName);

        _fs.default.writeFileSync(this.$.env.paths.meteorApp.gitIgnore, gitIgnore.join('\n'), 'UTF-8');
      }
    }
    /**
     * Reads the Meteor release version used in the app.
     * @returns {string}
     */

  }, {
    key: "getMeteorRelease",
    value: function getMeteorRelease() {
      var release = _fs.default.readFileSync(this.$.env.paths.meteorApp.release, 'UTF-8').replace(/\r/gm, '').split('\n')[0];

      var _release$split = release.split('@');

      var _release$split2 = _slicedToArray(_release$split, 2);

      release = _release$split2[1];

      // We do not care if it is beta.
      if (~release.indexOf('-')) {
        var _release$split3 = release.split('-');

        var _release$split4 = _slicedToArray(_release$split3, 1);

        release = _release$split4[0];
      }

      return release;
    }
    /**
     * Cast Meteor release to semver version.
     * @returns {string}
     */

  }, {
    key: "castMeteorReleaseToSemver",
    value: function castMeteorReleaseToSemver() {
      return `${this.getMeteorRelease()}.0.0`.match(/(^\d+\.\d+\.\d+)/gmi)[0];
    }
    /**
     * Validate meteor version against a versionRange.
     * @param {string} versionRange - semver version range
     */

  }, {
    key: "checkMeteorVersion",
    value: function checkMeteorVersion(versionRange) {
      var release = this.castMeteorReleaseToSemver();

      if (!_semver.default.satisfies(release, versionRange)) {
        if (this.$.env.options.skipMobileBuild) {
          this.log.error(`wrong meteor version (${release}) in project - only ` + `${versionRange} is supported`);
        } else {
          this.log.error(`wrong meteor version (${release}) in project - only ` + `${versionRange} is supported for automatic meteor builds (you can always ` + 'try with `--skip-mobile-build` if you are using meteor >= 1.2.1');
        }

        process.exit(1);
      }
    }
    /**
     * Decides which strategy to use while trying to get client build out of Meteor project.
     * @returns {number}
     */

  }, {
    key: "chooseStrategy",
    value: function chooseStrategy() {
      if (this.$.env.options.forceCordovaBuild) {
        return this.indexHTMLStrategies.INDEX_FROM_CORDOVA_BUILD;
      }

      var release = this.castMeteorReleaseToSemver();

      if (_semver.default.satisfies(release, '> 1.3.4')) {
        return this.indexHTMLStrategies.INDEX_FROM_RUNNING_SERVER;
      }

      if (_semver.default.satisfies(release, '1.3.4')) {
        var explodedVersion = this.getMeteorRelease().split('.');

        if (explodedVersion.length >= 4) {
          if (explodedVersion[3] > 1) {
            return this.indexHTMLStrategies.INDEX_FROM_RUNNING_SERVER;
          }

          return this.indexHTMLStrategies.INDEX_FROM_CORDOVA_BUILD;
        }
      }

      return this.indexHTMLStrategies.INDEX_FROM_CORDOVA_BUILD;
    }
    /**
     * Checks required preconditions.
     * - Meteor version
     * - is mobile platform added
     */

  }, {
    key: "checkPreconditions",
    value: function () {
      var _checkPreconditions = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2() {
        var platforms;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                if (this.$.env.options.skipMobileBuild) {
                  this.checkMeteorVersion('>= 1.2.1');
                } else {
                  this.checkMeteorVersion('>= 1.3.3');
                  this.indexHTMLstrategy = this.chooseStrategy();

                  if (this.indexHTMLstrategy === this.indexHTMLStrategies.INDEX_FROM_CORDOVA_BUILD) {
                    this.log.debug('meteor version is < 1.3.4.2 so the index.html from cordova-build will' + ' be used');
                  } else {
                    this.log.debug('meteor version is >= 1.3.4.2 so the index.html will be downloaded ' + 'from __cordova/index.html');
                  }
                }

                if (this.$.env.options.skipMobileBuild) {
                  _context2.next = 15;
                  break;
                }

                platforms = _fs.default.readFileSync(this.$.env.paths.meteorApp.platforms, 'UTF-8');

                if (!(!~platforms.indexOf('android') && !~platforms.indexOf('ios'))) {
                  _context2.next = 15;
                  break;
                }

                if (!this.$.env.options.android) {
                  this.mobilePlatform = 'ios';
                } else {
                  this.mobilePlatform = 'android';
                }

                this.log.warn(`no mobile target detected - will add '${this.mobilePlatform}' ` + 'just to get a mobile build');
                _context2.prev = 6;
                _context2.next = 9;
                return this.addMobilePlatform(this.mobilePlatform);

              case 9:
                _context2.next = 15;
                break;

              case 11:
                _context2.prev = 11;
                _context2.t0 = _context2["catch"](6);
                this.log.error('failed to add a mobile platform - please try to do it manually');
                process.exit(1);

              case 15:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[6, 11]]);
      }));

      return function checkPreconditions() {
        return _checkPreconditions.apply(this, arguments);
      };
    }()
    /**
     * Tries to add a mobile platform to meteor project.
     * @param {string} platform - platform to add
     * @returns {Promise}
     */

  }, {
    key: "addMobilePlatform",
    value: function addMobilePlatform(platform) {
      var _this2 = this;

      return new Promise(function (resolve, reject) {
        _this2.log.verbose(`adding mobile platform: ${platform}`);

        (0, _crossSpawn.default)('meteor', ['add-platform', platform], {
          cwd: _this2.$.env.paths.meteorApp.root,
          stdio: _this2.$.env.stdio
        }).on('exit', function () {
          var platforms = _fs.default.readFileSync(_this2.$.env.paths.meteorApp.platforms, 'UTF-8');

          if (!~platforms.indexOf('android') && !~platforms.indexOf('ios')) {
            reject();
          } else {
            resolve();
          }
        });
      });
    }
    /**
     * Tries to remove a mobile platform from meteor project.
     * @param {string} platform - platform to remove
     * @returns {Promise}
     */

  }, {
    key: "removeMobilePlatform",
    value: function removeMobilePlatform(platform) {
      var _this3 = this;

      return new Promise(function (resolve, reject) {
        _this3.log.verbose(`removing mobile platform: ${platform}`);

        (0, _crossSpawn.default)('meteor', ['remove-platform', platform], {
          cwd: _this3.$.env.paths.meteorApp.root,
          stdio: _this3.$.env.stdio,
          env: Object.assign({
            METEOR_PRETTY_OUTPUT: 0
          }, process.env)
        }).on('exit', function () {
          var platforms = _fs.default.readFileSync(_this3.$.env.paths.meteorApp.platforms, 'UTF-8');

          if (~platforms.indexOf(platform)) {
            reject();
          } else {
            resolve();
          }
        });
      });
    }
    /**
     * Just checks for index.html and program.json existence.
     * @returns {boolean}
     */

  }, {
    key: "isCordovaBuildReady",
    value: function isCordovaBuildReady() {
      if (this.indexHTMLstrategy === this.indexHTMLStrategies.INDEX_FROM_CORDOVA_BUILD) {
        return this.$.utils.exists(this.$.env.paths.meteorApp.cordovaBuildIndex) && this.$.utils.exists(this.$.env.paths.meteorApp.cordovaBuildProgramJson) && (!this.oldManifest || this.oldManifest && this.oldManifest !== _fs.default.readFileSync(this.$.env.paths.meteorApp.cordovaBuildProgramJson, 'UTF-8'));
      }

      return this.$.utils.exists(this.$.env.paths.meteorApp.webCordovaProgramJson) && (!this.oldManifest || this.oldManifest && this.oldManifest !== _fs.default.readFileSync(this.$.env.paths.meteorApp.webCordovaProgramJson, 'UTF-8'));
    }
    /**
     * Fetches index.html from running project.
     * @returns {Promise.<*>}
     */

  }, {
    key: "acquireIndex",
    value: function () {
      var _acquireIndex = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee3() {
        var port, res, text;
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                port = this.$.env.options.port ? this.$.env.options.port : 3080;
                this.log.info('acquiring index.html');
                _context3.next = 4;
                return (0, _nodeFetch.default)(`http://127.0.0.1:${port}/__cordova/index.html`);

              case 4:
                res = _context3.sent;
                _context3.next = 7;
                return res.text();

              case 7:
                text = _context3.sent;

                if (!~text.indexOf('src="/cordova.js"')) {
                  _context3.next = 10;
                  break;
                }

                return _context3.abrupt("return", text);

              case 10:
                return _context3.abrupt("return", false);

              case 11:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      return function acquireIndex() {
        return _acquireIndex.apply(this, arguments);
      };
    }()
    /**
     * Fetches mainfest.json from running project.
     * @returns {Promise.<void>}
     */

  }, {
    key: "acquireManifest",
    value: function () {
      var _acquireManifest = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee4() {
        var port, res, text;
        return regeneratorRuntime.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                port = this.$.env.options.port ? this.$.env.options.port : 3080;
                this.log.info('acquiring manifest.json');
                _context4.next = 4;
                return (0, _nodeFetch.default)(`http://127.0.0.1:${port}/__cordova/manifest.json?meteor_dont_serve_index=true`);

              case 4:
                res = _context4.sent;
                _context4.next = 7;
                return res.text();

              case 7:
                text = _context4.sent;
                return _context4.abrupt("return", JSON.parse(text));

              case 9:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      return function acquireManifest() {
        return _acquireManifest.apply(this, arguments);
      };
    }()
    /**
     * Tries to get a mobile build from meteor app.
     * In case of failure leaves a meteor.log.
     * A lot of stuff is happening here - but the main aim is to get a mobile build from
     * .meteor/local/cordova-build/www/application and exit as soon as possible.
     *
     * @returns {Promise}
     */

  }, {
    key: "buildMobileTarget",
    value: function buildMobileTarget() {
      var _this4 = this;

      var programJson = this.indexHTMLstrategy === this.indexHTMLStrategies.INDEX_FROM_CORDOVA_BUILD ? this.$.env.paths.meteorApp.cordovaBuildProgramJson : this.$.env.paths.meteorApp.webCordovaProgramJson;

      if (this.$.utils.exists(programJson)) {
        this.oldManifest = _fs.default.readFileSync(programJson, 'UTF-8');
      }

      return new Promise(function (resolve, reject) {
        var self = _this4;
        var log = '';
        var desiredExit = false;
        var buildTimeout = null;
        var errorTimeout = null;
        var messageTimeout = null;
        var killTimeout = null;
        var cordovaCheckInterval = null;
        var portProblem = false;

        function windowsKill(pid) {
          self.log.debug(`killing pid: ${pid}`);

          _crossSpawn.default.sync('taskkill', ['/pid', pid, '/f', '/t']); // We will look for other process which might have been created outside the
          // process tree.
          // Lets list all node.exe processes.


          var out = _crossSpawn.default.sync('wmic', ['process', 'where', 'caption="node.exe"', 'get', 'commandline,processid']).stdout.toString('utf-8').split('\n');

          var args = self.prepareArguments(); // Lets mount regex.

          var regexV1 = new RegExp(`${args.join('\\s+')}\\s+(\\d+)`, 'gm');
          var regexV2 = new RegExp(`"${args.join('"\\s+"')}"\\s+(\\d+)`, 'gm'); // No we will check for those with the matching params.

          out.forEach(function (line) {
            var match = regexV1.exec(line) || regexV2.exec(line) || false;

            if (match) {
              self.log.debug(`killing pid: ${match[1]}`);

              _crossSpawn.default.sync('taskkill', ['/pid', match[1], '/f', '/t']);
            }

            regexV1.lastIndex = 0;
            regexV2.lastIndex = 0;
          });
        }

        function writeLog() {
          _fs.default.writeFileSync('meteor.log', log, 'UTF-8');
        }

        function clearTimeoutsAndIntervals() {
          clearInterval(cordovaCheckInterval);
          clearTimeout(buildTimeout);
          clearTimeout(errorTimeout);
          clearTimeout(messageTimeout);
          clearTimeout(killTimeout);
        }

        var args = _this4.prepareArguments();

        _this4.log.info(`running "meteor ${args.join(' ')}"... this might take a while`); // Lets spawn meteor.


        var child = (0, _crossSpawn.default)('meteor', args, {
          env: Object.assign({
            METEOR_PRETTY_OUTPUT: 0,
            METEOR_NO_RELEASE_CHECK: 1
          }, process.env),
          cwd: _this4.$.env.paths.meteorApp.root
        }, {
          shell: true
        }); // Kills the currently running meteor command.

        function kill() {
          sll('');
          child.kill('SIGKILL');

          if (self.$.env.os.isWindows) {
            windowsKill(child.pid);
          }
        }

        function exit() {
          killTimeout = setTimeout(function () {
            clearTimeoutsAndIntervals();
            desiredExit = true;
            kill();
            resolve();
          }, 500);
        }

        function copyBuild() {
          self.copyBuild().then(function () {
            exit();
          }).catch(function () {
            clearTimeoutsAndIntervals();
            kill();
            writeLog();
            reject('copy');
          });
        }

        cordovaCheckInterval = setInterval(function () {
          // Check if we already have cordova-build ready.
          if (_this4.isCordovaBuildReady()) {
            // If so, then exit immediately.
            if (_this4.indexHTMLstrategy === _this4.indexHTMLStrategies.INDEX_FROM_CORDOVA_BUILD) {
              copyBuild();
            }
          }
        }, 1000);
        child.stderr.on('data', function (chunk) {
          var line = chunk.toString('UTF-8');
          log += `${line}\n`;

          if (errorTimeout) {
            clearTimeout(errorTimeout);
          } // Do not exit if this is the warning for using --production.
          // Output exceeds -> https://github.com/meteor/meteor/issues/8592


          if (!~line.indexOf('--production') && !~line.indexOf('Output exceeds ')) {
            // We will exit 1s after last error in stderr.
            errorTimeout = setTimeout(function () {
              clearTimeoutsAndIntervals();
              kill();
              writeLog();
              reject('error');
            }, 1000);
          }
        });
        child.stdout.on('data', function (chunk) {
          var line = chunk.toString('UTF-8');

          if (!desiredExit && line.trim().replace(/[\n\r\t\v\f]+/gm, '') !== '') {
            var linesToDisplay = line.trim().split('\n\r'); // Only display last line from the chunk.

            var sanitizedLine = linesToDisplay.pop().replace(/[\n\r\t\v\f]+/gm, '');
            sll(sanitizedLine);
          }

          log += `${line}\n`;

          if (~line.indexOf('after_platform_add')) {
            sll('');

            _this4.log.info('done... 10%');
          }

          if (~line.indexOf('Local package version')) {
            if (messageTimeout) {
              clearTimeout(messageTimeout);
            }

            messageTimeout = setTimeout(function () {
              sll('');

              _this4.log.info('building in progress...');
            }, 1500);
          }

          if (~line.indexOf('Preparing Cordova project')) {
            sll('');

            _this4.log.info('done... 60%');
          }

          if (~line.indexOf('Can\'t listen on port')) {
            portProblem = true;
          }

          if (~line.indexOf('Your application has errors')) {
            if (errorTimeout) {
              clearTimeout(errorTimeout);
            }

            errorTimeout = setTimeout(function () {
              clearTimeoutsAndIntervals();
              kill();
              writeLog();
              reject('errorInApp');
            }, 1000);
          }

          if (~line.indexOf('App running at')) {
            copyBuild();
          }
        }); // When Meteor exits

        child.on('exit', function () {
          sll('');
          clearTimeoutsAndIntervals();

          if (!desiredExit) {
            writeLog();

            if (portProblem) {
              reject('port');
            } else {
              reject('exit');
            }
          }
        });
        buildTimeout = setTimeout(function () {
          kill();
          writeLog();
          reject('timeout');
        }, _this4.$.env.options.buildTimeout ? _this4.$.env.options.buildTimeout * 1000 : 600000);
      });
    }
    /**
     * Replaces the DDP url that was used originally when Meteor was building the client.
     * @param {string} indexHtml - path to index.html from the client
     */

  }, {
    key: "updateDdpUrl",
    value: function updateDdpUrl(indexHtml) {
      var content;
      var runtimeConfig;

      try {
        content = _fs.default.readFileSync(indexHtml, 'UTF-8');
      } catch (e) {
        this.log.error(`error loading index.html file: ${e.message}`);
        process.exit(1);
      }

      if (!this.matcher.test(content)) {
        this.log.error('could not find runtime config in index file');
        process.exit(1);
      }

      try {
        var matches = content.match(this.matcher);
        runtimeConfig = JSON.parse(decodeURIComponent(matches[1]));
      } catch (e) {
        this.log.error('could not find runtime config in index file');
        process.exit(1);
      }

      if (this.$.env.options.ddpUrl.substr(-1, 1) !== '/') {
        this.$.env.options.ddpUrl += '/';
      }

      runtimeConfig.ROOT_URL = this.$.env.options.ddpUrl;
      runtimeConfig.DDP_DEFAULT_CONNECTION_URL = this.$.env.options.ddpUrl;
      content = content.replace(this.replacer, `$1"${encodeURIComponent(JSON.stringify(runtimeConfig))}"$3`);

      try {
        _fs.default.writeFileSync(indexHtml, content);
      } catch (e) {
        this.log.error(`error writing index.html file: ${e.message}`);
        process.exit(1);
      }

      this.log.info('successfully updated ddp string in the runtime config of a mobile build' + ` to ${this.$.env.options.ddpUrl}`);
    }
    /**
     * Prepares the arguments passed to `meteor` command.
     * @returns {string[]}
     */

  }, {
    key: "prepareArguments",
    value: function prepareArguments() {
      var args = ['run', '--verbose', `--mobile-server=${this.$.env.options.ddpUrl}`];

      if (this.$.env.isProductionBuild()) {
        args.push('--production');
      }

      args.push('-p');

      if (this.$.env.options.port) {
        args.push(this.$.env.options.port);
      } else {
        args.push('3080');
      }

      if (this.$.env.options.meteorSettings) {
        args.push('--settings', this.$.env.options.meteorSettings);
      }

      return args;
    }
    /**
     * Validates the mobile build and copies it into electron app.
     */

  }, {
    key: "copyBuild",
    value: function () {
      var _copyBuild = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee5() {
        var prefix, copyPathPostfix, indexHtml, cordovaBuild, cordovaBuildIndex, cordovaBuildProgramJson, programJson;
        return regeneratorRuntime.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                this.log.debug('clearing build dir');
                _context5.prev = 1;
                _context5.next = 4;
                return this.$.utils.rmWithRetries('-rf', this.$.env.paths.electronApp.meteorApp);

              case 4:
                _context5.next = 9;
                break;

              case 6:
                _context5.prev = 6;
                _context5.t0 = _context5["catch"](1);
                throw new Error(_context5.t0);

              case 9:
                prefix = 'cordovaBuild';
                copyPathPostfix = '';

                if (!(this.indexHTMLstrategy === this.indexHTMLStrategies.INDEX_FROM_RUNNING_SERVER)) {
                  _context5.next = 27;
                  break;
                }

                prefix = 'webCordova';
                copyPathPostfix = `${_path.default.sep}*`;
                _context5.prev = 14;

                _fs.default.mkdir(this.$.env.paths.electronApp.meteorApp);

                _context5.next = 18;
                return this.acquireIndex();

              case 18:
                indexHtml = _context5.sent;

                _fs.default.writeFileSync(this.$.env.paths.electronApp.meteorAppIndex, indexHtml);

                this.log.info('successfully downloaded index.html from running meteor app');
                _context5.next = 27;
                break;

              case 23:
                _context5.prev = 23;
                _context5.t1 = _context5["catch"](14);
                this.log.error('error while trying to download index.html for web.cordova, ' + 'be sure that you are running a mobile target or with' + ' --mobile-server: ', _context5.t1);
                throw _context5.t1;

              case 27:
                cordovaBuild = this.$.env.paths.meteorApp[prefix];
                cordovaBuildIndex = this.$.env.paths.meteorApp.cordovaBuildIndex;
                cordovaBuildProgramJson = this.$.env.paths.meteorApp[`${prefix}ProgramJson`];

                if (this.$.utils.exists(cordovaBuild)) {
                  _context5.next = 34;
                  break;
                }

                this.log.error(`no mobile build found at ${cordovaBuild}`);
                this.log.error('are you sure you did run meteor with --mobile-server?');
                throw new Error('required file not present');

              case 34:
                if (this.$.utils.exists(cordovaBuildProgramJson)) {
                  _context5.next = 38;
                  break;
                }

                this.log.error('no program.json found in mobile build found at ' + `${cordovaBuild}`);
                this.log.error('are you sure you did run meteor with --mobile-server?');
                throw new Error('required file not present');

              case 38:
                if (!(this.indexHTMLstrategy !== this.indexHTMLStrategies.INDEX_FROM_RUNNING_SERVER)) {
                  _context5.next = 43;
                  break;
                }

                if (this.$.utils.exists(cordovaBuildIndex)) {
                  _context5.next = 43;
                  break;
                }

                this.log.error('no index.html found in cordova build found at ' + `${cordovaBuild}`);
                this.log.error('are you sure you did run meteor with --mobile-server?');
                throw new Error('required file not present');

              case 43:
                this.log.verbose('copying mobile build');

                _shelljs.default.cp('-R', `${cordovaBuild}${copyPathPostfix}`, this.$.env.paths.electronApp.meteorApp); // Because of various permission problems here we try to clear te path by clearing
                // all possible restrictions.


                _shelljs.default.chmod('-R', '777', this.$.env.paths.electronApp.meteorApp);

                if (this.$.env.os.isWindows) {
                  _shelljs.default.exec(`attrib -r ${this.$.env.paths.electronApp.meteorApp}${_path.default.sep}*.* /s`);
                }

                if (!(this.indexHTMLstrategy === this.indexHTMLStrategies.INDEX_FROM_RUNNING_SERVER)) {
                  _context5.next = 60;
                  break;
                }

                _context5.prev = 48;
                _context5.next = 51;
                return this.acquireManifest();

              case 51:
                programJson = _context5.sent;

                _fs.default.writeFileSync(this.$.env.paths.electronApp.meteorAppProgramJson, JSON.stringify(programJson, null, 4));

                this.log.info('successfully downloaded manifest.json from running meteor app');
                _context5.next = 60;
                break;

              case 56:
                _context5.prev = 56;
                _context5.t2 = _context5["catch"](48);
                this.log.error('error while trying to download manifest.json for web.cordova,' + ' be sure that you are running a mobile target or with' + ' --mobile-server: ', _context5.t2);
                throw _context5.t2;

              case 60:
                this.log.info('mobile build copied to electron app');
                this.log.debug('copy cordova.js to meteor build');

                _shelljs.default.cp(join(__dirname, '..', 'skeleton', 'cordova.js'), this.$.env.paths.electronApp.meteorApp);

              case 63:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this, [[1, 6], [14, 23], [48, 56]]);
      }));

      return function copyBuild() {
        return _copyBuild.apply(this, arguments);
      };
    }()
    /**
     * Injects Meteor.isDesktop
     */

  }, {
    key: "injectIsDesktop",
    value: function injectIsDesktop() {
      var _this5 = this;

      this.log.info('injecting isDesktop');
      var manifestJsonPath = this.$.env.paths.meteorApp.cordovaBuildProgramJson;

      if (this.indexHTMLstrategy === this.indexHTMLStrategies.INDEX_FROM_RUNNING_SERVER) {
        manifestJsonPath = this.$.env.paths.meteorApp.webCordovaProgramJson;
      }

      try {
        var _JSON$parse = JSON.parse(_fs.default.readFileSync(manifestJsonPath, 'UTF-8')),
            manifest = _JSON$parse.manifest;

        var injected = false;
        var injectedStartupDidComplete = false;
        var result = null; // We will search in every .js file in the manifest.
        // We could probably detect whether this is a dev or production build and only search in
        // the correct files, but for now this should be fine.

        manifest.forEach(function (file) {
          var fileContents; // Hacky way of setting isDesktop.

          if (file.type === 'js') {
            fileContents = _fs.default.readFileSync(join(_this5.$.env.paths.electronApp.meteorApp, file.path), 'UTF-8');
            result = _this5.injector.processFileContents(fileContents);
            var _result = result;
            fileContents = _result.fileContents;
            injectedStartupDidComplete = result.injectedStartupDidComplete ? true : injectedStartupDidComplete;
            injected = result.injected ? true : injected;

            _fs.default.writeFileSync(join(_this5.$.env.paths.electronApp.meteorApp, file.path), fileContents);
          }
        });

        if (!injected) {
          this.log.error('error injecting isDesktop global var.');
          process.exit(1);
        }

        if (!injectedStartupDidComplete) {
          this.log.error('error injecting isDesktop for startupDidComplete');
          process.exit(1);
        }
      } catch (e) {
        this.log.error('error occurred while injecting isDesktop: ', e);
        process.exit(1);
      }

      this.log.info('injected successfully');
    }
    /**
     * Builds, modifies and copies the meteor app to electron app.
     */

  }, {
    key: "build",
    value: function () {
      var _build = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee6() {
        return regeneratorRuntime.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                this.log.info('checking for any mobile platform');
                _context6.prev = 1;
                _context6.next = 4;
                return this.checkPreconditions();

              case 4:
                _context6.next = 10;
                break;

              case 6:
                _context6.prev = 6;
                _context6.t0 = _context6["catch"](1);
                this.log.error('error occurred during checking preconditions: ', _context6.t0);
                process.exit(1);

              case 10:
                this.log.info('building meteor app');

                if (this.$.env.options.skipMobileBuild) {
                  _context6.next = 41;
                  break;
                }

                _context6.prev = 12;
                _context6.next = 15;
                return this.buildMobileTarget();

              case 15:
                _context6.next = 39;
                break;

              case 17:
                _context6.prev = 17;
                _context6.t1 = _context6["catch"](12);
                _context6.t2 = _context6.t1;
                _context6.next = _context6.t2 === 'timeout' ? 22 : _context6.t2 === 'error' ? 24 : _context6.t2 === 'errorInApp' ? 26 : _context6.t2 === 'port' ? 28 : _context6.t2 === 'exit' ? 30 : _context6.t2 === 'copy' ? 32 : 34;
                break;

              case 22:
                this.log.error('timeout while building, log has been written to meteor.log');
                return _context6.abrupt("break", 35);

              case 24:
                this.log.error('some errors were reported during build, check meteor.log for more' + ' info');
                return _context6.abrupt("break", 35);

              case 26:
                this.log.error('your meteor app has errors - look into meteor.log for more' + ' info');
                return _context6.abrupt("break", 35);

              case 28:
                this.log.error('your port 3080 is currently used (you probably have this or other ' + 'meteor project running?), use `-t` or `--meteor-port` to use ' + 'different port while building');
                return _context6.abrupt("break", 35);

              case 30:
                this.log.error('meteor cmd exited unexpectedly, log has been written to meteor.log');
                return _context6.abrupt("break", 35);

              case 32:
                this.log.error('error encountered when copying the build');
                return _context6.abrupt("break", 35);

              case 34:
                this.log.error('error occurred during building mobile target', _context6.t1);

              case 35:
                if (!this.mobilePlatform) {
                  _context6.next = 38;
                  break;
                }

                _context6.next = 38;
                return this.removeMobilePlatform(this.mobilePlatform);

              case 38:
                process.exit(1);

              case 39:
                _context6.next = 50;
                break;

              case 41:
                this.indexHTMLstrategy = this.chooseStrategy();
                _context6.prev = 42;
                _context6.next = 45;
                return this.copyBuild();

              case 45:
                _context6.next = 50;
                break;

              case 47:
                _context6.prev = 47;
                _context6.t3 = _context6["catch"](42);
                process.exit(1);

              case 50:
                this.injectIsDesktop();
                this.changeDdpUrl();
                _context6.prev = 52;
                _context6.next = 55;
                return this.packToAsar();

              case 55:
                _context6.next = 61;
                break;

              case 57:
                _context6.prev = 57;
                _context6.t4 = _context6["catch"](52);
                this.log.error('error while packing meteor app to asar');
                process.exit(1);

              case 61:
                this.log.info('meteor build finished');

                if (!this.mobilePlatform) {
                  _context6.next = 65;
                  break;
                }

                _context6.next = 65;
                return this.removeMobilePlatform(this.mobilePlatform);

              case 65:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this, [[1, 6], [12, 17], [42, 47], [52, 57]]);
      }));

      return function build() {
        return _build.apply(this, arguments);
      };
    }()
  }, {
    key: "changeDdpUrl",
    value: function changeDdpUrl() {
      if (this.$.env.options.ddpUrl !== null) {
        try {
          this.updateDdpUrl(this.$.env.paths.electronApp.meteorAppIndex);
        } catch (e) {
          this.log.error(`error while trying to change the ddp url: ${e.message}`);
        }
      }
    }
  }, {
    key: "packToAsar",
    value: function packToAsar() {
      var _this6 = this;

      this.log.info('packing meteor app to asar archive');
      return new Promise(function (resolve, reject) {
        return _asar.default.createPackage(_this6.$.env.paths.electronApp.meteorApp, _path.default.join(_this6.$.env.paths.electronApp.root, 'meteor.asar'), function () {
          // On Windows some files might still be blocked. Giving a tick for them to be
          // ready for deletion.
          setImmediate(function () {
            _this6.log.verbose('clearing meteor app after packing');

            _this6.$.utils.rmWithRetries('-rf', _this6.$.env.paths.electronApp.meteorApp).then(function () {
              resolve();
            }).catch(function (e) {
              reject(e);
            });
          });
        });
      });
    }
  }]);

  return MeteorApp;
}();

exports.default = MeteorApp;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL2xpYi9tZXRlb3JBcHAuanMiXSwibmFtZXMiOlsiam9pbiIsInNsbCIsInN0ZG91dCIsIk1ldGVvckFwcCIsIiQiLCJsb2ciLCJtZXRlb3JNYW5hZ2VyIiwibW9iaWxlUGxhdGZvcm0iLCJvbGRNYW5pZmVzdCIsImluamVjdG9yIiwibWF0Y2hlciIsIlJlZ0V4cCIsInJlcGxhY2VyIiwibWV0ZW9yVmVyc2lvbiIsImluZGV4SFRNTHN0cmF0ZWd5IiwiaW5kZXhIVE1MU3RyYXRlZ2llcyIsIklOREVYX0ZST01fQ09SRE9WQV9CVUlMRCIsIklOREVYX0ZST01fUlVOTklOR19TRVJWRVIiLCJkZXNrdG9wSENQUGFja2FnZXMiLCJkZXNrdG9wIiwiZ2V0U2V0dGluZ3MiLCJkZXNrdG9wSENQIiwidmVyYm9zZSIsInBhY2thZ2VzV2l0aFZlcnNpb24iLCJtYXAiLCJwYWNrYWdlTmFtZSIsImdldFZlcnNpb24iLCJlbnN1cmVQYWNrYWdlcyIsIkVycm9yIiwiY2hlY2tQYWNrYWdlcyIsImRlbGV0ZVBhY2thZ2VzIiwiZ2l0SWdub3JlIiwicmVhZEZpbGVTeW5jIiwiZW52IiwicGF0aHMiLCJtZXRlb3JBcHAiLCJzcGxpdCIsImZpbHRlciIsImlnbm9yZWRQYXRoIiwidHJpbSIsImluZGV4T2YiLCJlbGVjdHJvbkFwcCIsInJvb3ROYW1lIiwicHVzaCIsIndyaXRlRmlsZVN5bmMiLCJyZWxlYXNlIiwicmVwbGFjZSIsImdldE1ldGVvclJlbGVhc2UiLCJtYXRjaCIsInZlcnNpb25SYW5nZSIsImNhc3RNZXRlb3JSZWxlYXNlVG9TZW12ZXIiLCJzYXRpc2ZpZXMiLCJvcHRpb25zIiwic2tpcE1vYmlsZUJ1aWxkIiwiZXJyb3IiLCJwcm9jZXNzIiwiZXhpdCIsImZvcmNlQ29yZG92YUJ1aWxkIiwiZXhwbG9kZWRWZXJzaW9uIiwibGVuZ3RoIiwiY2hlY2tNZXRlb3JWZXJzaW9uIiwiY2hvb3NlU3RyYXRlZ3kiLCJkZWJ1ZyIsInBsYXRmb3JtcyIsImFuZHJvaWQiLCJ3YXJuIiwiYWRkTW9iaWxlUGxhdGZvcm0iLCJwbGF0Zm9ybSIsIlByb21pc2UiLCJyZXNvbHZlIiwicmVqZWN0IiwiY3dkIiwicm9vdCIsInN0ZGlvIiwib24iLCJPYmplY3QiLCJhc3NpZ24iLCJNRVRFT1JfUFJFVFRZX09VVFBVVCIsInV0aWxzIiwiZXhpc3RzIiwiY29yZG92YUJ1aWxkSW5kZXgiLCJjb3Jkb3ZhQnVpbGRQcm9ncmFtSnNvbiIsIndlYkNvcmRvdmFQcm9ncmFtSnNvbiIsInBvcnQiLCJpbmZvIiwicmVzIiwidGV4dCIsIkpTT04iLCJwYXJzZSIsInByb2dyYW1Kc29uIiwic2VsZiIsImRlc2lyZWRFeGl0IiwiYnVpbGRUaW1lb3V0IiwiZXJyb3JUaW1lb3V0IiwibWVzc2FnZVRpbWVvdXQiLCJraWxsVGltZW91dCIsImNvcmRvdmFDaGVja0ludGVydmFsIiwicG9ydFByb2JsZW0iLCJ3aW5kb3dzS2lsbCIsInBpZCIsInN5bmMiLCJvdXQiLCJ0b1N0cmluZyIsImFyZ3MiLCJwcmVwYXJlQXJndW1lbnRzIiwicmVnZXhWMSIsInJlZ2V4VjIiLCJmb3JFYWNoIiwibGluZSIsImV4ZWMiLCJsYXN0SW5kZXgiLCJ3cml0ZUxvZyIsImNsZWFyVGltZW91dHNBbmRJbnRlcnZhbHMiLCJjbGVhckludGVydmFsIiwiY2xlYXJUaW1lb3V0IiwiY2hpbGQiLCJNRVRFT1JfTk9fUkVMRUFTRV9DSEVDSyIsInNoZWxsIiwia2lsbCIsIm9zIiwiaXNXaW5kb3dzIiwic2V0VGltZW91dCIsImNvcHlCdWlsZCIsInRoZW4iLCJjYXRjaCIsInNldEludGVydmFsIiwiaXNDb3Jkb3ZhQnVpbGRSZWFkeSIsInN0ZGVyciIsImNodW5rIiwibGluZXNUb0Rpc3BsYXkiLCJzYW5pdGl6ZWRMaW5lIiwicG9wIiwiaW5kZXhIdG1sIiwiY29udGVudCIsInJ1bnRpbWVDb25maWciLCJlIiwibWVzc2FnZSIsInRlc3QiLCJtYXRjaGVzIiwiZGVjb2RlVVJJQ29tcG9uZW50IiwiZGRwVXJsIiwic3Vic3RyIiwiUk9PVF9VUkwiLCJERFBfREVGQVVMVF9DT05ORUNUSU9OX1VSTCIsImVuY29kZVVSSUNvbXBvbmVudCIsInN0cmluZ2lmeSIsImlzUHJvZHVjdGlvbkJ1aWxkIiwibWV0ZW9yU2V0dGluZ3MiLCJybVdpdGhSZXRyaWVzIiwicHJlZml4IiwiY29weVBhdGhQb3N0Zml4Iiwic2VwIiwibWtkaXIiLCJhY3F1aXJlSW5kZXgiLCJtZXRlb3JBcHBJbmRleCIsImNvcmRvdmFCdWlsZCIsImNwIiwiY2htb2QiLCJhY3F1aXJlTWFuaWZlc3QiLCJtZXRlb3JBcHBQcm9ncmFtSnNvbiIsIl9fZGlybmFtZSIsIm1hbmlmZXN0SnNvblBhdGgiLCJtYW5pZmVzdCIsImluamVjdGVkIiwiaW5qZWN0ZWRTdGFydHVwRGlkQ29tcGxldGUiLCJyZXN1bHQiLCJmaWxlIiwiZmlsZUNvbnRlbnRzIiwidHlwZSIsInBhdGgiLCJwcm9jZXNzRmlsZUNvbnRlbnRzIiwiY2hlY2tQcmVjb25kaXRpb25zIiwiYnVpbGRNb2JpbGVUYXJnZXQiLCJyZW1vdmVNb2JpbGVQbGF0Zm9ybSIsImluamVjdElzRGVza3RvcCIsImNoYW5nZURkcFVybCIsInBhY2tUb0FzYXIiLCJ1cGRhdGVEZHBVcmwiLCJjcmVhdGVQYWNrYWdlIiwic2V0SW1tZWRpYXRlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBRUE7O0FBQ0E7O0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7SUFFUUEsSSxpQkFBQUEsSTtBQUNSLElBQU1DLE1BQU0sdUJBQWNDLE1BQTFCLEMsQ0FFQTs7QUFFQTs7Ozs7O0lBS3FCQyxTOzs7QUFDakI7Ozs7QUFJQSxxQkFBWUMsQ0FBWixFQUFlO0FBQUE7O0FBQ1gsU0FBS0MsR0FBTCxHQUFXLGlCQUFRLFdBQVIsQ0FBWDtBQUNBLFNBQUtELENBQUwsR0FBU0EsQ0FBVDtBQUNBLFNBQUtFLGFBQUwsR0FBcUIsMkJBQWtCRixDQUFsQixDQUFyQjtBQUNBLFNBQUtHLGNBQUwsR0FBc0IsSUFBdEI7QUFDQSxTQUFLQyxXQUFMLEdBQW1CLElBQW5CO0FBQ0EsU0FBS0MsUUFBTCxHQUFnQixnQ0FBaEI7QUFDQSxTQUFLQyxPQUFMLEdBQWUsSUFBSUMsTUFBSixDQUNYLCtFQURXLENBQWY7QUFHQSxTQUFLQyxRQUFMLEdBQWdCLElBQUlELE1BQUosQ0FDWixtRkFEWSxDQUFoQjtBQUdBLFNBQUtFLGFBQUwsR0FBcUIsSUFBckI7QUFDQSxTQUFLQyxpQkFBTCxHQUF5QixJQUF6QjtBQUVBLFNBQUtDLG1CQUFMLEdBQTJCO0FBQ3ZCQyxnQ0FBMEIsQ0FESDtBQUV2QkMsaUNBQTJCO0FBRkosS0FBM0I7QUFJSDtBQUVEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFJVUMsa0MsR0FBcUIsQ0FBQyw4QkFBRCxFQUFpQyw4QkFBakMsQzs7cUJBQ3ZCLEtBQUtkLENBQUwsQ0FBT2UsT0FBUCxDQUFlQyxXQUFmLEdBQTZCQyxVOzs7OztBQUM3QixxQkFBS2hCLEdBQUwsQ0FBU2lCLE9BQVQsQ0FBaUIsdURBQWpCO0FBRU1DLG1DLEdBQXNCTCxtQkFBbUJNLEdBQW5CLENBQXVCO0FBQUEseUJBQWdCLEdBQUVDLFdBQVksSUFBRyxNQUFLckIsQ0FBTCxDQUFPc0IsVUFBUCxFQUFvQixFQUFyRDtBQUFBLGlCQUF2QixDOzs7dUJBR2xCLEtBQUtwQixhQUFMLENBQW1CcUIsY0FBbkIsQ0FBa0NULGtCQUFsQyxFQUFzREssbUJBQXRELEVBQTJFLFlBQTNFLEM7Ozs7Ozs7OztzQkFFQSxJQUFJSyxLQUFKLGE7Ozs7Ozs7QUFHVixxQkFBS3ZCLEdBQUwsQ0FBU2lCLE9BQVQsQ0FBaUIsdURBQWpCOzs7cUJBR1EsS0FBS2hCLGFBQUwsQ0FBbUJ1QixhQUFuQixDQUFpQ1gsa0JBQWpDLEM7Ozs7Ozt1QkFDTSxLQUFLWixhQUFMLENBQW1Cd0IsY0FBbkIsQ0FBa0NaLGtCQUFsQyxFQUFzRCxZQUF0RCxDOzs7Ozs7Ozs7c0JBR0osSUFBSVUsS0FBSixhOzs7Ozs7Ozs7Ozs7OztBQUtsQjs7Ozs7O3NDQUdrQjtBQUNkLFdBQUt2QixHQUFMLENBQVNpQixPQUFULENBQWlCLDZCQUFqQixFQURjLENBRWQ7O0FBQ0EsVUFBTVMsWUFBWSxZQUFHQyxZQUFILENBQWdCLEtBQUs1QixDQUFMLENBQU82QixHQUFQLENBQVdDLEtBQVgsQ0FBaUJDLFNBQWpCLENBQTJCSixTQUEzQyxFQUFzRCxPQUF0RCxFQUNiSyxLQURhLENBQ1AsSUFETyxFQUNEQyxNQURDLENBQ007QUFBQSxlQUFlQyxZQUFZQyxJQUFaLE9BQXVCLEVBQXRDO0FBQUEsT0FETixDQUFsQjs7QUFHQSxVQUFJLENBQUMsQ0FBQ1IsVUFBVVMsT0FBVixDQUFrQixLQUFLcEMsQ0FBTCxDQUFPNkIsR0FBUCxDQUFXQyxLQUFYLENBQWlCTyxXQUFqQixDQUE2QkMsUUFBL0MsQ0FBTixFQUFnRTtBQUM1RCxhQUFLckMsR0FBTCxDQUFTaUIsT0FBVCxDQUFrQixVQUFTLEtBQUtsQixDQUFMLENBQU82QixHQUFQLENBQVdDLEtBQVgsQ0FBaUJPLFdBQWpCLENBQTZCQyxRQUFTLHdCQUFqRTtBQUNBWCxrQkFBVVksSUFBVixDQUFlLEtBQUt2QyxDQUFMLENBQU82QixHQUFQLENBQVdDLEtBQVgsQ0FBaUJPLFdBQWpCLENBQTZCQyxRQUE1Qzs7QUFFQSxvQkFBR0UsYUFBSCxDQUFpQixLQUFLeEMsQ0FBTCxDQUFPNkIsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxTQUFqQixDQUEyQkosU0FBNUMsRUFBdURBLFVBQVUvQixJQUFWLENBQWUsSUFBZixDQUF2RCxFQUE2RSxPQUE3RTtBQUNIO0FBQ0o7QUFFRDs7Ozs7Ozt1Q0FJbUI7QUFDZixVQUFJNkMsVUFBVSxZQUFHYixZQUFILENBQWdCLEtBQUs1QixDQUFMLENBQU82QixHQUFQLENBQVdDLEtBQVgsQ0FBaUJDLFNBQWpCLENBQTJCVSxPQUEzQyxFQUFvRCxPQUFwRCxFQUNUQyxPQURTLENBQ0QsTUFEQyxFQUNPLEVBRFAsRUFFVFYsS0FGUyxDQUVILElBRkcsRUFFRyxDQUZILENBQWQ7O0FBRGUsMkJBSUFTLFFBQVFULEtBQVIsQ0FBYyxHQUFkLENBSkE7O0FBQUE7O0FBSVhTLGFBSlc7O0FBS2Y7QUFDQSxVQUFJLENBQUNBLFFBQVFMLE9BQVIsQ0FBZ0IsR0FBaEIsQ0FBTCxFQUEyQjtBQUFBLDhCQUNWSyxRQUFRVCxLQUFSLENBQWMsR0FBZCxDQURVOztBQUFBOztBQUNyQlMsZUFEcUI7QUFFMUI7O0FBQ0QsYUFBT0EsT0FBUDtBQUNIO0FBRUQ7Ozs7Ozs7Z0RBSTRCO0FBQ3hCLGFBQVEsR0FBRSxLQUFLRSxnQkFBTCxFQUF3QixNQUEzQixDQUFpQ0MsS0FBakMsQ0FBdUMscUJBQXZDLEVBQThELENBQTlELENBQVA7QUFDSDtBQUVEOzs7Ozs7O3VDQUltQkMsWSxFQUFjO0FBQzdCLFVBQU1KLFVBQVUsS0FBS0sseUJBQUwsRUFBaEI7O0FBQ0EsVUFBSSxDQUFDLGdCQUFPQyxTQUFQLENBQWlCTixPQUFqQixFQUEwQkksWUFBMUIsQ0FBTCxFQUE4QztBQUMxQyxZQUFJLEtBQUs3QyxDQUFMLENBQU82QixHQUFQLENBQVdtQixPQUFYLENBQW1CQyxlQUF2QixFQUF3QztBQUNwQyxlQUFLaEQsR0FBTCxDQUFTaUQsS0FBVCxDQUFnQix5QkFBd0JULE9BQVEsc0JBQWpDLEdBQ1YsR0FBRUksWUFBYSxlQURwQjtBQUVILFNBSEQsTUFHTztBQUNILGVBQUs1QyxHQUFMLENBQVNpRCxLQUFULENBQWdCLHlCQUF3QlQsT0FBUSxzQkFBakMsR0FDVixHQUFFSSxZQUFhLDREQURMLEdBRVgsaUVBRko7QUFHSDs7QUFDRE0sZ0JBQVFDLElBQVIsQ0FBYSxDQUFiO0FBQ0g7QUFDSjtBQUVEOzs7Ozs7O3FDQUlpQjtBQUNiLFVBQUksS0FBS3BELENBQUwsQ0FBTzZCLEdBQVAsQ0FBV21CLE9BQVgsQ0FBbUJLLGlCQUF2QixFQUEwQztBQUN0QyxlQUFPLEtBQUsxQyxtQkFBTCxDQUF5QkMsd0JBQWhDO0FBQ0g7O0FBRUQsVUFBTTZCLFVBQVUsS0FBS0sseUJBQUwsRUFBaEI7O0FBQ0EsVUFBSSxnQkFBT0MsU0FBUCxDQUFpQk4sT0FBakIsRUFBMEIsU0FBMUIsQ0FBSixFQUEwQztBQUN0QyxlQUFPLEtBQUs5QixtQkFBTCxDQUF5QkUseUJBQWhDO0FBQ0g7O0FBQ0QsVUFBSSxnQkFBT2tDLFNBQVAsQ0FBaUJOLE9BQWpCLEVBQTBCLE9BQTFCLENBQUosRUFBd0M7QUFDcEMsWUFBTWEsa0JBQWtCLEtBQUtYLGdCQUFMLEdBQXdCWCxLQUF4QixDQUE4QixHQUE5QixDQUF4Qjs7QUFDQSxZQUFJc0IsZ0JBQWdCQyxNQUFoQixJQUEwQixDQUE5QixFQUFpQztBQUM3QixjQUFJRCxnQkFBZ0IsQ0FBaEIsSUFBcUIsQ0FBekIsRUFBNEI7QUFDeEIsbUJBQU8sS0FBSzNDLG1CQUFMLENBQXlCRSx5QkFBaEM7QUFDSDs7QUFDRCxpQkFBTyxLQUFLRixtQkFBTCxDQUF5QkMsd0JBQWhDO0FBQ0g7QUFDSjs7QUFDRCxhQUFPLEtBQUtELG1CQUFMLENBQXlCQyx3QkFBaEM7QUFDSDtBQUVEOzs7Ozs7Ozs7Ozs7Ozs7OztBQU1JLG9CQUFJLEtBQUtaLENBQUwsQ0FBTzZCLEdBQVAsQ0FBV21CLE9BQVgsQ0FBbUJDLGVBQXZCLEVBQXdDO0FBQ3BDLHVCQUFLTyxrQkFBTCxDQUF3QixVQUF4QjtBQUNILGlCQUZELE1BRU87QUFDSCx1QkFBS0Esa0JBQUwsQ0FBd0IsVUFBeEI7QUFDQSx1QkFBSzlDLGlCQUFMLEdBQXlCLEtBQUsrQyxjQUFMLEVBQXpCOztBQUNBLHNCQUFJLEtBQUsvQyxpQkFBTCxLQUEyQixLQUFLQyxtQkFBTCxDQUF5QkMsd0JBQXhELEVBQWtGO0FBQzlFLHlCQUFLWCxHQUFMLENBQVN5RCxLQUFULENBQ0ksMEVBQ0EsVUFGSjtBQUlILG1CQUxELE1BS087QUFDSCx5QkFBS3pELEdBQUwsQ0FBU3lELEtBQVQsQ0FDSSx1RUFDQSwyQkFGSjtBQUlIO0FBQ0o7O29CQUVJLEtBQUsxRCxDQUFMLENBQU82QixHQUFQLENBQVdtQixPQUFYLENBQW1CQyxlOzs7OztBQUNkVSx5QixHQUFZLFlBQUcvQixZQUFILENBQWdCLEtBQUs1QixDQUFMLENBQU82QixHQUFQLENBQVdDLEtBQVgsQ0FBaUJDLFNBQWpCLENBQTJCNEIsU0FBM0MsRUFBc0QsT0FBdEQsQzs7c0JBQ2QsQ0FBQyxDQUFDQSxVQUFVdkIsT0FBVixDQUFrQixTQUFsQixDQUFGLElBQWtDLENBQUMsQ0FBQ3VCLFVBQVV2QixPQUFWLENBQWtCLEtBQWxCLEM7Ozs7O0FBQ3BDLG9CQUFJLENBQUMsS0FBS3BDLENBQUwsQ0FBTzZCLEdBQVAsQ0FBV21CLE9BQVgsQ0FBbUJZLE9BQXhCLEVBQWlDO0FBQzdCLHVCQUFLekQsY0FBTCxHQUFzQixLQUF0QjtBQUNILGlCQUZELE1BRU87QUFDSCx1QkFBS0EsY0FBTCxHQUFzQixTQUF0QjtBQUNIOztBQUNELHFCQUFLRixHQUFMLENBQVM0RCxJQUFULENBQWUseUNBQXdDLEtBQUsxRCxjQUFlLElBQTdELEdBQ1YsNEJBREo7Ozt1QkFHVSxLQUFLMkQsaUJBQUwsQ0FBdUIsS0FBSzNELGNBQTVCLEM7Ozs7Ozs7OztBQUVOLHFCQUFLRixHQUFMLENBQVNpRCxLQUFULENBQWUsZ0VBQWY7QUFDQUMsd0JBQVFDLElBQVIsQ0FBYSxDQUFiOzs7Ozs7Ozs7Ozs7OztBQU1oQjs7Ozs7Ozs7c0NBS2tCVyxRLEVBQVU7QUFBQTs7QUFDeEIsYUFBTyxJQUFJQyxPQUFKLENBQVksVUFBQ0MsT0FBRCxFQUFVQyxNQUFWLEVBQXFCO0FBQ3BDLGVBQUtqRSxHQUFMLENBQVNpQixPQUFULENBQWtCLDJCQUEwQjZDLFFBQVMsRUFBckQ7O0FBQ0EsaUNBQU0sUUFBTixFQUFnQixDQUFDLGNBQUQsRUFBaUJBLFFBQWpCLENBQWhCLEVBQTRDO0FBQ3hDSSxlQUFLLE9BQUtuRSxDQUFMLENBQU82QixHQUFQLENBQVdDLEtBQVgsQ0FBaUJDLFNBQWpCLENBQTJCcUMsSUFEUTtBQUV4Q0MsaUJBQU8sT0FBS3JFLENBQUwsQ0FBTzZCLEdBQVAsQ0FBV3dDO0FBRnNCLFNBQTVDLEVBR0dDLEVBSEgsQ0FHTSxNQUhOLEVBR2MsWUFBTTtBQUNoQixjQUFNWCxZQUFZLFlBQUcvQixZQUFILENBQWdCLE9BQUs1QixDQUFMLENBQU82QixHQUFQLENBQVdDLEtBQVgsQ0FBaUJDLFNBQWpCLENBQTJCNEIsU0FBM0MsRUFBc0QsT0FBdEQsQ0FBbEI7O0FBQ0EsY0FBSSxDQUFDLENBQUNBLFVBQVV2QixPQUFWLENBQWtCLFNBQWxCLENBQUYsSUFBa0MsQ0FBQyxDQUFDdUIsVUFBVXZCLE9BQVYsQ0FBa0IsS0FBbEIsQ0FBeEMsRUFBa0U7QUFDOUQ4QjtBQUNILFdBRkQsTUFFTztBQUNIRDtBQUNIO0FBQ0osU0FWRDtBQVdILE9BYk0sQ0FBUDtBQWNIO0FBRUQ7Ozs7Ozs7O3lDQUtxQkYsUSxFQUFVO0FBQUE7O0FBQzNCLGFBQU8sSUFBSUMsT0FBSixDQUFZLFVBQUNDLE9BQUQsRUFBVUMsTUFBVixFQUFxQjtBQUNwQyxlQUFLakUsR0FBTCxDQUFTaUIsT0FBVCxDQUFrQiw2QkFBNEI2QyxRQUFTLEVBQXZEOztBQUNBLGlDQUFNLFFBQU4sRUFBZ0IsQ0FBQyxpQkFBRCxFQUFvQkEsUUFBcEIsQ0FBaEIsRUFBK0M7QUFDM0NJLGVBQUssT0FBS25FLENBQUwsQ0FBTzZCLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsU0FBakIsQ0FBMkJxQyxJQURXO0FBRTNDQyxpQkFBTyxPQUFLckUsQ0FBTCxDQUFPNkIsR0FBUCxDQUFXd0MsS0FGeUI7QUFHM0N4QyxlQUFLMEMsT0FBT0MsTUFBUCxDQUFjO0FBQUVDLGtDQUFzQjtBQUF4QixXQUFkLEVBQTJDdEIsUUFBUXRCLEdBQW5EO0FBSHNDLFNBQS9DLEVBSUd5QyxFQUpILENBSU0sTUFKTixFQUljLFlBQU07QUFDaEIsY0FBTVgsWUFBWSxZQUFHL0IsWUFBSCxDQUFnQixPQUFLNUIsQ0FBTCxDQUFPNkIsR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxTQUFqQixDQUEyQjRCLFNBQTNDLEVBQXNELE9BQXRELENBQWxCOztBQUNBLGNBQUksQ0FBQ0EsVUFBVXZCLE9BQVYsQ0FBa0IyQixRQUFsQixDQUFMLEVBQWtDO0FBQzlCRztBQUNILFdBRkQsTUFFTztBQUNIRDtBQUNIO0FBQ0osU0FYRDtBQVlILE9BZE0sQ0FBUDtBQWVIO0FBRUQ7Ozs7Ozs7MENBSXNCO0FBQ2xCLFVBQUksS0FBS3ZELGlCQUFMLEtBQTJCLEtBQUtDLG1CQUFMLENBQXlCQyx3QkFBeEQsRUFBa0Y7QUFDOUUsZUFBTyxLQUFLWixDQUFMLENBQU8wRSxLQUFQLENBQWFDLE1BQWIsQ0FBb0IsS0FBSzNFLENBQUwsQ0FBTzZCLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsU0FBakIsQ0FBMkI2QyxpQkFBL0MsS0FDSCxLQUFLNUUsQ0FBTCxDQUFPMEUsS0FBUCxDQUFhQyxNQUFiLENBQW9CLEtBQUszRSxDQUFMLENBQU82QixHQUFQLENBQVdDLEtBQVgsQ0FBaUJDLFNBQWpCLENBQTJCOEMsdUJBQS9DLENBREcsS0FHQyxDQUFDLEtBQUt6RSxXQUFOLElBQ0MsS0FBS0EsV0FBTCxJQUNHLEtBQUtBLFdBQUwsS0FBcUIsWUFBR3dCLFlBQUgsQ0FDakIsS0FBSzVCLENBQUwsQ0FBTzZCLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsU0FBakIsQ0FBMkI4Qyx1QkFEVixFQUNtQyxPQURuQyxDQUwxQixDQUFQO0FBVUg7O0FBQ0QsYUFBTyxLQUFLN0UsQ0FBTCxDQUFPMEUsS0FBUCxDQUFhQyxNQUFiLENBQW9CLEtBQUszRSxDQUFMLENBQU82QixHQUFQLENBQVdDLEtBQVgsQ0FBaUJDLFNBQWpCLENBQTJCK0MscUJBQS9DLE1BRUMsQ0FBQyxLQUFLMUUsV0FBTixJQUNDLEtBQUtBLFdBQUwsSUFDRyxLQUFLQSxXQUFMLEtBQXFCLFlBQUd3QixZQUFILENBQ2pCLEtBQUs1QixDQUFMLENBQU82QixHQUFQLENBQVdDLEtBQVgsQ0FBaUJDLFNBQWpCLENBQTJCK0MscUJBRFYsRUFDaUMsT0FEakMsQ0FKMUIsQ0FBUDtBQVNIO0FBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7QUFLVUMsb0IsR0FBUSxLQUFLL0UsQ0FBTCxDQUFPNkIsR0FBUCxDQUFXbUIsT0FBWCxDQUFtQitCLElBQXBCLEdBQTRCLEtBQUsvRSxDQUFMLENBQU82QixHQUFQLENBQVdtQixPQUFYLENBQW1CK0IsSUFBL0MsR0FBc0QsSTtBQUNuRSxxQkFBSzlFLEdBQUwsQ0FBUytFLElBQVQsQ0FBYyxzQkFBZDs7dUJBQ2tCLHdCQUFPLG9CQUFtQkQsSUFBSyx1QkFBL0IsQzs7O0FBQVpFLG1COzt1QkFDYUEsSUFBSUMsSUFBSixFOzs7QUFBYkEsb0I7O3FCQUVGLENBQUNBLEtBQUs5QyxPQUFMLENBQWEsbUJBQWIsQzs7Ozs7a0RBQ004QyxJOzs7a0RBRUosSzs7Ozs7Ozs7Ozs7Ozs7QUFHWDs7Ozs7Ozs7Ozs7Ozs7OztBQUtVSCxvQixHQUFRLEtBQUsvRSxDQUFMLENBQU82QixHQUFQLENBQVdtQixPQUFYLENBQW1CK0IsSUFBcEIsR0FBNEIsS0FBSy9FLENBQUwsQ0FBTzZCLEdBQVAsQ0FBV21CLE9BQVgsQ0FBbUIrQixJQUEvQyxHQUFzRCxJO0FBQ25FLHFCQUFLOUUsR0FBTCxDQUFTK0UsSUFBVCxDQUFjLHlCQUFkOzt1QkFDa0Isd0JBQ2Isb0JBQW1CRCxJQUFLLHVEQURYLEM7OztBQUFaRSxtQjs7dUJBR2FBLElBQUlDLElBQUosRTs7O0FBQWJBLG9CO2tEQUNDQyxLQUFLQyxLQUFMLENBQVdGLElBQVgsQzs7Ozs7Ozs7Ozs7Ozs7QUFHWDs7Ozs7Ozs7Ozs7d0NBUW9CO0FBQUE7O0FBQ2hCLFVBQU1HLGNBQ0QsS0FBSzNFLGlCQUFMLEtBQTJCLEtBQUtDLG1CQUFMLENBQXlCQyx3QkFBckQsR0FDSSxLQUFLWixDQUFMLENBQU82QixHQUFQLENBQVdDLEtBQVgsQ0FBaUJDLFNBQWpCLENBQTJCOEMsdUJBRC9CLEdBRUksS0FBSzdFLENBQUwsQ0FBTzZCLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsU0FBakIsQ0FBMkIrQyxxQkFIbkM7O0FBS0EsVUFBSSxLQUFLOUUsQ0FBTCxDQUFPMEUsS0FBUCxDQUFhQyxNQUFiLENBQW9CVSxXQUFwQixDQUFKLEVBQXNDO0FBQ2xDLGFBQUtqRixXQUFMLEdBQW1CLFlBQUd3QixZQUFILENBQWdCeUQsV0FBaEIsRUFBNkIsT0FBN0IsQ0FBbkI7QUFDSDs7QUFFRCxhQUFPLElBQUlyQixPQUFKLENBQVksVUFBQ0MsT0FBRCxFQUFVQyxNQUFWLEVBQXFCO0FBQ3BDLFlBQU1vQixPQUFPLE1BQWI7QUFDQSxZQUFJckYsTUFBTSxFQUFWO0FBQ0EsWUFBSXNGLGNBQWMsS0FBbEI7QUFDQSxZQUFJQyxlQUFlLElBQW5CO0FBQ0EsWUFBSUMsZUFBZSxJQUFuQjtBQUNBLFlBQUlDLGlCQUFpQixJQUFyQjtBQUNBLFlBQUlDLGNBQWMsSUFBbEI7QUFDQSxZQUFJQyx1QkFBdUIsSUFBM0I7QUFDQSxZQUFJQyxjQUFjLEtBQWxCOztBQUVBLGlCQUFTQyxXQUFULENBQXFCQyxHQUFyQixFQUEwQjtBQUN0QlQsZUFBS3JGLEdBQUwsQ0FBU3lELEtBQVQsQ0FBZ0IsZ0JBQWVxQyxHQUFJLEVBQW5DOztBQUNBLDhCQUFNQyxJQUFOLENBQVcsVUFBWCxFQUF1QixDQUFDLE1BQUQsRUFBU0QsR0FBVCxFQUFjLElBQWQsRUFBb0IsSUFBcEIsQ0FBdkIsRUFGc0IsQ0FJdEI7QUFDQTtBQUNBOzs7QUFFQSxjQUFNRSxNQUFNLG9CQUNQRCxJQURPLENBRUosTUFGSSxFQUdKLENBQUMsU0FBRCxFQUFZLE9BQVosRUFBcUIsb0JBQXJCLEVBQTJDLEtBQTNDLEVBQWtELHVCQUFsRCxDQUhJLEVBS1BsRyxNQUxPLENBS0FvRyxRQUxBLENBS1MsT0FMVCxFQU1QbEUsS0FOTyxDQU1ELElBTkMsQ0FBWjs7QUFPQSxjQUFNbUUsT0FBT2IsS0FBS2MsZ0JBQUwsRUFBYixDQWZzQixDQWdCdEI7O0FBQ0EsY0FBTUMsVUFBVSxJQUFJOUYsTUFBSixDQUFZLEdBQUU0RixLQUFLdkcsSUFBTCxDQUFVLE1BQVYsQ0FBa0IsWUFBaEMsRUFBNkMsSUFBN0MsQ0FBaEI7QUFDQSxjQUFNMEcsVUFBVSxJQUFJL0YsTUFBSixDQUFZLElBQUc0RixLQUFLdkcsSUFBTCxDQUFVLFFBQVYsQ0FBb0IsYUFBbkMsRUFBaUQsSUFBakQsQ0FBaEIsQ0FsQnNCLENBbUJ0Qjs7QUFDQXFHLGNBQUlNLE9BQUosQ0FBWSxVQUFDQyxJQUFELEVBQVU7QUFDbEIsZ0JBQU01RCxRQUFReUQsUUFBUUksSUFBUixDQUFhRCxJQUFiLEtBQXNCRixRQUFRRyxJQUFSLENBQWFELElBQWIsQ0FBdEIsSUFBNEMsS0FBMUQ7O0FBQ0EsZ0JBQUk1RCxLQUFKLEVBQVc7QUFDUDBDLG1CQUFLckYsR0FBTCxDQUFTeUQsS0FBVCxDQUFnQixnQkFBZWQsTUFBTSxDQUFOLENBQVMsRUFBeEM7O0FBQ0Esa0NBQU1vRCxJQUFOLENBQVcsVUFBWCxFQUF1QixDQUFDLE1BQUQsRUFBU3BELE1BQU0sQ0FBTixDQUFULEVBQW1CLElBQW5CLEVBQXlCLElBQXpCLENBQXZCO0FBQ0g7O0FBQ0R5RCxvQkFBUUssU0FBUixHQUFvQixDQUFwQjtBQUNBSixvQkFBUUksU0FBUixHQUFvQixDQUFwQjtBQUNILFdBUkQ7QUFTSDs7QUFFRCxpQkFBU0MsUUFBVCxHQUFvQjtBQUNoQixzQkFBR25FLGFBQUgsQ0FBaUIsWUFBakIsRUFBK0J2QyxHQUEvQixFQUFvQyxPQUFwQztBQUNIOztBQUVELGlCQUFTMkcseUJBQVQsR0FBcUM7QUFDakNDLHdCQUFjakIsb0JBQWQ7QUFDQWtCLHVCQUFhdEIsWUFBYjtBQUNBc0IsdUJBQWFyQixZQUFiO0FBQ0FxQix1QkFBYXBCLGNBQWI7QUFDQW9CLHVCQUFhbkIsV0FBYjtBQUNIOztBQUVELFlBQU1RLE9BQU8sT0FBS0MsZ0JBQUwsRUFBYjs7QUFFQSxlQUFLbkcsR0FBTCxDQUFTK0UsSUFBVCxDQUFlLG1CQUFrQm1CLEtBQUt2RyxJQUFMLENBQVUsR0FBVixDQUFlLDhCQUFoRCxFQXhEb0MsQ0EwRHBDOzs7QUFDQSxZQUFNbUgsUUFBUSx5QkFDVixRQURVLEVBRVZaLElBRlUsRUFHVjtBQUNJdEUsZUFBSzBDLE9BQU9DLE1BQVAsQ0FDRDtBQUFFQyxrQ0FBc0IsQ0FBeEI7QUFBMkJ1QyxxQ0FBeUI7QUFBcEQsV0FEQyxFQUN3RDdELFFBQVF0QixHQURoRSxDQURUO0FBSUlzQyxlQUFLLE9BQUtuRSxDQUFMLENBQU82QixHQUFQLENBQVdDLEtBQVgsQ0FBaUJDLFNBQWpCLENBQTJCcUM7QUFKcEMsU0FIVSxFQVNWO0FBQUU2QyxpQkFBTztBQUFULFNBVFUsQ0FBZCxDQTNEb0MsQ0F1RXBDOztBQUNBLGlCQUFTQyxJQUFULEdBQWdCO0FBQ1pySCxjQUFJLEVBQUo7QUFDQWtILGdCQUFNRyxJQUFOLENBQVcsU0FBWDs7QUFDQSxjQUFJNUIsS0FBS3RGLENBQUwsQ0FBTzZCLEdBQVAsQ0FBV3NGLEVBQVgsQ0FBY0MsU0FBbEIsRUFBNkI7QUFDekJ0Qix3QkFBWWlCLE1BQU1oQixHQUFsQjtBQUNIO0FBQ0o7O0FBRUQsaUJBQVMzQyxJQUFULEdBQWdCO0FBQ1p1Qyx3QkFBYzBCLFdBQVcsWUFBTTtBQUMzQlQ7QUFDQXJCLDBCQUFjLElBQWQ7QUFDQTJCO0FBQ0FqRDtBQUNILFdBTGEsRUFLWCxHQUxXLENBQWQ7QUFNSDs7QUFFRCxpQkFBU3FELFNBQVQsR0FBcUI7QUFDakJoQyxlQUFLZ0MsU0FBTCxHQUFpQkMsSUFBakIsQ0FBc0IsWUFBTTtBQUN4Qm5FO0FBQ0gsV0FGRCxFQUVHb0UsS0FGSCxDQUVTLFlBQU07QUFDWFo7QUFDQU07QUFDQVA7QUFDQXpDLG1CQUFPLE1BQVA7QUFDSCxXQVBEO0FBUUg7O0FBRUQwQiwrQkFBdUI2QixZQUFZLFlBQU07QUFDckM7QUFDQSxjQUFJLE9BQUtDLG1CQUFMLEVBQUosRUFBZ0M7QUFDNUI7QUFDQSxnQkFBSSxPQUFLaEgsaUJBQUwsS0FDQSxPQUFLQyxtQkFBTCxDQUF5QkMsd0JBRDdCLEVBQ3VEO0FBQ25EMEc7QUFDSDtBQUNKO0FBQ0osU0FUc0IsRUFTcEIsSUFUb0IsQ0FBdkI7QUFXQVAsY0FBTVksTUFBTixDQUFhckQsRUFBYixDQUFnQixNQUFoQixFQUF3QixVQUFDc0QsS0FBRCxFQUFXO0FBQy9CLGNBQU1wQixPQUFPb0IsTUFBTTFCLFFBQU4sQ0FBZSxPQUFmLENBQWI7QUFDQWpHLGlCQUFRLEdBQUV1RyxJQUFLLElBQWY7O0FBQ0EsY0FBSWYsWUFBSixFQUFrQjtBQUNkcUIseUJBQWFyQixZQUFiO0FBQ0gsV0FMOEIsQ0FNL0I7QUFDQTs7O0FBQ0EsY0FBSSxDQUFDLENBQUNlLEtBQUtwRSxPQUFMLENBQWEsY0FBYixDQUFGLElBQWtDLENBQUMsQ0FBQ29FLEtBQUtwRSxPQUFMLENBQWEsaUJBQWIsQ0FBeEMsRUFBeUU7QUFDckU7QUFDQXFELDJCQUFlNEIsV0FBVyxZQUFNO0FBQzVCVDtBQUNBTTtBQUNBUDtBQUNBekMscUJBQU8sT0FBUDtBQUNILGFBTGMsRUFLWixJQUxZLENBQWY7QUFNSDtBQUNKLFNBakJEO0FBbUJBNkMsY0FBTWpILE1BQU4sQ0FBYXdFLEVBQWIsQ0FBZ0IsTUFBaEIsRUFBd0IsVUFBQ3NELEtBQUQsRUFBVztBQUMvQixjQUFNcEIsT0FBT29CLE1BQU0xQixRQUFOLENBQWUsT0FBZixDQUFiOztBQUNBLGNBQUksQ0FBQ1gsV0FBRCxJQUFnQmlCLEtBQUtyRSxJQUFMLEdBQVlPLE9BQVosQ0FBb0IsaUJBQXBCLEVBQXVDLEVBQXZDLE1BQStDLEVBQW5FLEVBQXVFO0FBQ25FLGdCQUFNbUYsaUJBQWlCckIsS0FBS3JFLElBQUwsR0FDbEJILEtBRGtCLENBQ1osTUFEWSxDQUF2QixDQURtRSxDQUduRTs7QUFDQSxnQkFBTThGLGdCQUFnQkQsZUFBZUUsR0FBZixHQUFxQnJGLE9BQXJCLENBQTZCLGlCQUE3QixFQUFnRCxFQUFoRCxDQUF0QjtBQUNBN0MsZ0JBQUlpSSxhQUFKO0FBQ0g7O0FBQ0Q3SCxpQkFBUSxHQUFFdUcsSUFBSyxJQUFmOztBQUNBLGNBQUksQ0FBQ0EsS0FBS3BFLE9BQUwsQ0FBYSxvQkFBYixDQUFMLEVBQXlDO0FBQ3JDdkMsZ0JBQUksRUFBSjs7QUFDQSxtQkFBS0ksR0FBTCxDQUFTK0UsSUFBVCxDQUFjLGFBQWQ7QUFDSDs7QUFFRCxjQUFJLENBQUN3QixLQUFLcEUsT0FBTCxDQUFhLHVCQUFiLENBQUwsRUFBNEM7QUFDeEMsZ0JBQUlzRCxjQUFKLEVBQW9CO0FBQ2hCb0IsMkJBQWFwQixjQUFiO0FBQ0g7O0FBQ0RBLDZCQUFpQjJCLFdBQVcsWUFBTTtBQUM5QnhILGtCQUFJLEVBQUo7O0FBQ0EscUJBQUtJLEdBQUwsQ0FBUytFLElBQVQsQ0FBYyx5QkFBZDtBQUNILGFBSGdCLEVBR2QsSUFIYyxDQUFqQjtBQUlIOztBQUVELGNBQUksQ0FBQ3dCLEtBQUtwRSxPQUFMLENBQWEsMkJBQWIsQ0FBTCxFQUFnRDtBQUM1Q3ZDLGdCQUFJLEVBQUo7O0FBQ0EsbUJBQUtJLEdBQUwsQ0FBUytFLElBQVQsQ0FBYyxhQUFkO0FBQ0g7O0FBRUQsY0FBSSxDQUFDd0IsS0FBS3BFLE9BQUwsQ0FBYSx1QkFBYixDQUFMLEVBQTRDO0FBQ3hDeUQsMEJBQWMsSUFBZDtBQUNIOztBQUVELGNBQUksQ0FBQ1csS0FBS3BFLE9BQUwsQ0FBYSw2QkFBYixDQUFMLEVBQWtEO0FBQzlDLGdCQUFJcUQsWUFBSixFQUFrQjtBQUNkcUIsMkJBQWFyQixZQUFiO0FBQ0g7O0FBQ0RBLDJCQUFlNEIsV0FBVyxZQUFNO0FBQzVCVDtBQUNBTTtBQUNBUDtBQUNBekMscUJBQU8sWUFBUDtBQUNILGFBTGMsRUFLWixJQUxZLENBQWY7QUFNSDs7QUFFRCxjQUFJLENBQUNzQyxLQUFLcEUsT0FBTCxDQUFhLGdCQUFiLENBQUwsRUFBcUM7QUFDakNrRjtBQUNIO0FBQ0osU0FqREQsRUFsSW9DLENBcUxwQzs7QUFDQVAsY0FBTXpDLEVBQU4sQ0FBUyxNQUFULEVBQWlCLFlBQU07QUFDbkJ6RSxjQUFJLEVBQUo7QUFDQStHOztBQUNBLGNBQUksQ0FBQ3JCLFdBQUwsRUFBa0I7QUFDZG9COztBQUNBLGdCQUFJZCxXQUFKLEVBQWlCO0FBQ2IzQixxQkFBTyxNQUFQO0FBQ0gsYUFGRCxNQUVPO0FBQ0hBLHFCQUFPLE1BQVA7QUFDSDtBQUNKO0FBQ0osU0FYRDtBQWFBc0IsdUJBQWU2QixXQUFXLFlBQU07QUFDNUJIO0FBQ0FQO0FBQ0F6QyxpQkFBTyxTQUFQO0FBQ0gsU0FKYyxFQUlaLE9BQUtsRSxDQUFMLENBQU82QixHQUFQLENBQVdtQixPQUFYLENBQW1Cd0MsWUFBbkIsR0FBa0MsT0FBS3hGLENBQUwsQ0FBTzZCLEdBQVAsQ0FBV21CLE9BQVgsQ0FBbUJ3QyxZQUFuQixHQUFrQyxJQUFwRSxHQUEyRSxNQUovRCxDQUFmO0FBS0gsT0F4TU0sQ0FBUDtBQXlNSDtBQUVEOzs7Ozs7O2lDQUlhd0MsUyxFQUFXO0FBQ3BCLFVBQUlDLE9BQUo7QUFDQSxVQUFJQyxhQUFKOztBQUVBLFVBQUk7QUFDQUQsa0JBQVUsWUFBR3JHLFlBQUgsQ0FBZ0JvRyxTQUFoQixFQUEyQixPQUEzQixDQUFWO0FBQ0gsT0FGRCxDQUVFLE9BQU9HLENBQVAsRUFBVTtBQUNSLGFBQUtsSSxHQUFMLENBQVNpRCxLQUFULENBQWdCLGtDQUFpQ2lGLEVBQUVDLE9BQVEsRUFBM0Q7QUFDQWpGLGdCQUFRQyxJQUFSLENBQWEsQ0FBYjtBQUNIOztBQUNELFVBQUksQ0FBQyxLQUFLOUMsT0FBTCxDQUFhK0gsSUFBYixDQUFrQkosT0FBbEIsQ0FBTCxFQUFpQztBQUM3QixhQUFLaEksR0FBTCxDQUFTaUQsS0FBVCxDQUFlLDZDQUFmO0FBQ0FDLGdCQUFRQyxJQUFSLENBQWEsQ0FBYjtBQUNIOztBQUVELFVBQUk7QUFDQSxZQUFNa0YsVUFBVUwsUUFBUXJGLEtBQVIsQ0FBYyxLQUFLdEMsT0FBbkIsQ0FBaEI7QUFDQTRILHdCQUFnQi9DLEtBQUtDLEtBQUwsQ0FBV21ELG1CQUFtQkQsUUFBUSxDQUFSLENBQW5CLENBQVgsQ0FBaEI7QUFDSCxPQUhELENBR0UsT0FBT0gsQ0FBUCxFQUFVO0FBQ1IsYUFBS2xJLEdBQUwsQ0FBU2lELEtBQVQsQ0FBZSw2Q0FBZjtBQUNBQyxnQkFBUUMsSUFBUixDQUFhLENBQWI7QUFDSDs7QUFFRCxVQUFJLEtBQUtwRCxDQUFMLENBQU82QixHQUFQLENBQVdtQixPQUFYLENBQW1Cd0YsTUFBbkIsQ0FBMEJDLE1BQTFCLENBQWlDLENBQUMsQ0FBbEMsRUFBcUMsQ0FBckMsTUFBNEMsR0FBaEQsRUFBcUQ7QUFDakQsYUFBS3pJLENBQUwsQ0FBTzZCLEdBQVAsQ0FBV21CLE9BQVgsQ0FBbUJ3RixNQUFuQixJQUE2QixHQUE3QjtBQUNIOztBQUVETixvQkFBY1EsUUFBZCxHQUF5QixLQUFLMUksQ0FBTCxDQUFPNkIsR0FBUCxDQUFXbUIsT0FBWCxDQUFtQndGLE1BQTVDO0FBQ0FOLG9CQUFjUywwQkFBZCxHQUEyQyxLQUFLM0ksQ0FBTCxDQUFPNkIsR0FBUCxDQUFXbUIsT0FBWCxDQUFtQndGLE1BQTlEO0FBRUFQLGdCQUFVQSxRQUFRdkYsT0FBUixDQUNOLEtBQUtsQyxRQURDLEVBQ1UsTUFBS29JLG1CQUFtQnpELEtBQUswRCxTQUFMLENBQWVYLGFBQWYsQ0FBbkIsQ0FBa0QsS0FEakUsQ0FBVjs7QUFJQSxVQUFJO0FBQ0Esb0JBQUcxRixhQUFILENBQWlCd0YsU0FBakIsRUFBNEJDLE9BQTVCO0FBQ0gsT0FGRCxDQUVFLE9BQU9FLENBQVAsRUFBVTtBQUNSLGFBQUtsSSxHQUFMLENBQVNpRCxLQUFULENBQWdCLGtDQUFpQ2lGLEVBQUVDLE9BQVEsRUFBM0Q7QUFDQWpGLGdCQUFRQyxJQUFSLENBQWEsQ0FBYjtBQUNIOztBQUNELFdBQUtuRCxHQUFMLENBQVMrRSxJQUFULENBQWMsNEVBQ1QsT0FBTSxLQUFLaEYsQ0FBTCxDQUFPNkIsR0FBUCxDQUFXbUIsT0FBWCxDQUFtQndGLE1BQU8sRUFEckM7QUFFSDtBQUVEOzs7Ozs7O3VDQUltQjtBQUNmLFVBQU1yQyxPQUFPLENBQUMsS0FBRCxFQUFRLFdBQVIsRUFBc0IsbUJBQWtCLEtBQUtuRyxDQUFMLENBQU82QixHQUFQLENBQVdtQixPQUFYLENBQW1Cd0YsTUFBTyxFQUFsRSxDQUFiOztBQUNBLFVBQUksS0FBS3hJLENBQUwsQ0FBTzZCLEdBQVAsQ0FBV2lILGlCQUFYLEVBQUosRUFBb0M7QUFDaEMzQyxhQUFLNUQsSUFBTCxDQUFVLGNBQVY7QUFDSDs7QUFDRDRELFdBQUs1RCxJQUFMLENBQVUsSUFBVjs7QUFDQSxVQUFJLEtBQUt2QyxDQUFMLENBQU82QixHQUFQLENBQVdtQixPQUFYLENBQW1CK0IsSUFBdkIsRUFBNkI7QUFDekJvQixhQUFLNUQsSUFBTCxDQUFVLEtBQUt2QyxDQUFMLENBQU82QixHQUFQLENBQVdtQixPQUFYLENBQW1CK0IsSUFBN0I7QUFDSCxPQUZELE1BRU87QUFDSG9CLGFBQUs1RCxJQUFMLENBQVUsTUFBVjtBQUNIOztBQUNELFVBQUksS0FBS3ZDLENBQUwsQ0FBTzZCLEdBQVAsQ0FBV21CLE9BQVgsQ0FBbUIrRixjQUF2QixFQUF1QztBQUNuQzVDLGFBQUs1RCxJQUFMLENBQVUsWUFBVixFQUF3QixLQUFLdkMsQ0FBTCxDQUFPNkIsR0FBUCxDQUFXbUIsT0FBWCxDQUFtQitGLGNBQTNDO0FBQ0g7O0FBQ0QsYUFBTzVDLElBQVA7QUFDSDtBQUVEOzs7Ozs7Ozs7Ozs7Ozs7QUFJSSxxQkFBS2xHLEdBQUwsQ0FBU3lELEtBQVQsQ0FBZSxvQkFBZjs7O3VCQUVVLEtBQUsxRCxDQUFMLENBQU8wRSxLQUFQLENBQWFzRSxhQUFiLENBQTJCLEtBQTNCLEVBQWtDLEtBQUtoSixDQUFMLENBQU82QixHQUFQLENBQVdDLEtBQVgsQ0FBaUJPLFdBQWpCLENBQTZCTixTQUEvRCxDOzs7Ozs7Ozs7c0JBRUEsSUFBSVAsS0FBSixjOzs7QUFHTnlILHNCLEdBQVMsYztBQUNUQywrQixHQUFrQixFOztzQkFFbEIsS0FBS3hJLGlCQUFMLEtBQTJCLEtBQUtDLG1CQUFMLENBQXlCRSx5Qjs7Ozs7QUFDcERvSSx5QkFBUyxZQUFUO0FBQ0FDLGtDQUFtQixHQUFFLGNBQUtDLEdBQUksR0FBOUI7OztBQUdJLDRCQUFHQyxLQUFILENBQVMsS0FBS3BKLENBQUwsQ0FBTzZCLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQk8sV0FBakIsQ0FBNkJOLFNBQXRDOzs7dUJBQ2tCLEtBQUtzSCxZQUFMLEU7OztBQUFsQnJCLHlCOztBQUNBLDRCQUFHeEYsYUFBSCxDQUFpQixLQUFLeEMsQ0FBTCxDQUFPNkIsR0FBUCxDQUFXQyxLQUFYLENBQWlCTyxXQUFqQixDQUE2QmlILGNBQTlDLEVBQThEdEIsU0FBOUQ7O0FBQ0EscUJBQUsvSCxHQUFMLENBQVMrRSxJQUFULENBQWMsNERBQWQ7Ozs7Ozs7QUFFQSxxQkFBSy9FLEdBQUwsQ0FBU2lELEtBQVQsQ0FBZSxnRUFDWCxzREFEVyxHQUVYLG9CQUZKOzs7O0FBT0ZxRyw0QixHQUFlLEtBQUt2SixDQUFMLENBQU82QixHQUFQLENBQVdDLEtBQVgsQ0FBaUJDLFNBQWpCLENBQTJCa0gsTUFBM0IsQztBQUNickUsaUMsR0FBc0IsS0FBSzVFLENBQUwsQ0FBTzZCLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsUyxDQUF2QzZDLGlCO0FBQ0ZDLHVDLEdBQTBCLEtBQUs3RSxDQUFMLENBQU82QixHQUFQLENBQVdDLEtBQVgsQ0FBaUJDLFNBQWpCLENBQTRCLEdBQUVrSCxNQUFPLGFBQXJDLEM7O29CQUUzQixLQUFLakosQ0FBTCxDQUFPMEUsS0FBUCxDQUFhQyxNQUFiLENBQW9CNEUsWUFBcEIsQzs7Ozs7QUFDRCxxQkFBS3RKLEdBQUwsQ0FBU2lELEtBQVQsQ0FBZ0IsNEJBQTJCcUcsWUFBYSxFQUF4RDtBQUNBLHFCQUFLdEosR0FBTCxDQUFTaUQsS0FBVCxDQUFlLHVEQUFmO3NCQUNNLElBQUkxQixLQUFKLENBQVUsMkJBQVYsQzs7O29CQUdMLEtBQUt4QixDQUFMLENBQU8wRSxLQUFQLENBQWFDLE1BQWIsQ0FBb0JFLHVCQUFwQixDOzs7OztBQUNELHFCQUFLNUUsR0FBTCxDQUFTaUQsS0FBVCxDQUFlLG9EQUNWLEdBQUVxRyxZQUFhLEVBRHBCO0FBRUEscUJBQUt0SixHQUFMLENBQVNpRCxLQUFULENBQWUsdURBQWY7c0JBQ00sSUFBSTFCLEtBQUosQ0FBVSwyQkFBVixDOzs7c0JBR04sS0FBS2QsaUJBQUwsS0FBMkIsS0FBS0MsbUJBQUwsQ0FBeUJFLHlCOzs7OztvQkFDL0MsS0FBS2IsQ0FBTCxDQUFPMEUsS0FBUCxDQUFhQyxNQUFiLENBQW9CQyxpQkFBcEIsQzs7Ozs7QUFDRCxxQkFBSzNFLEdBQUwsQ0FBU2lELEtBQVQsQ0FBZSxtREFDVixHQUFFcUcsWUFBYSxFQURwQjtBQUVBLHFCQUFLdEosR0FBTCxDQUFTaUQsS0FBVCxDQUFlLHVEQUFmO3NCQUNNLElBQUkxQixLQUFKLENBQVUsMkJBQVYsQzs7O0FBSWQscUJBQUt2QixHQUFMLENBQVNpQixPQUFULENBQWlCLHNCQUFqQjs7QUFDQSxpQ0FBTXNJLEVBQU4sQ0FDSSxJQURKLEVBQ1csR0FBRUQsWUFBYSxHQUFFTCxlQUFnQixFQUQ1QyxFQUMrQyxLQUFLbEosQ0FBTCxDQUFPNkIsR0FBUCxDQUFXQyxLQUFYLENBQWlCTyxXQUFqQixDQUE2Qk4sU0FENUUsRSxDQUlBO0FBQ0E7OztBQUNBLGlDQUFNMEgsS0FBTixDQUNJLElBREosRUFDVSxLQURWLEVBQ2lCLEtBQUt6SixDQUFMLENBQU82QixHQUFQLENBQVdDLEtBQVgsQ0FBaUJPLFdBQWpCLENBQTZCTixTQUQ5Qzs7QUFHQSxvQkFBSSxLQUFLL0IsQ0FBTCxDQUFPNkIsR0FBUCxDQUFXc0YsRUFBWCxDQUFjQyxTQUFsQixFQUE2QjtBQUN6QixtQ0FBTVgsSUFBTixDQUFZLGFBQVksS0FBS3pHLENBQUwsQ0FBTzZCLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQk8sV0FBakIsQ0FBNkJOLFNBQVUsR0FBRSxjQUFLb0gsR0FBSSxRQUExRTtBQUNIOztzQkFFRyxLQUFLekksaUJBQUwsS0FBMkIsS0FBS0MsbUJBQUwsQ0FBeUJFLHlCOzs7Ozs7O3VCQUc1QixLQUFLNkksZUFBTCxFOzs7QUFBcEJyRSwyQjs7QUFDQSw0QkFBRzdDLGFBQUgsQ0FDSSxLQUFLeEMsQ0FBTCxDQUFPNkIsR0FBUCxDQUFXQyxLQUFYLENBQWlCTyxXQUFqQixDQUE2QnNILG9CQURqQyxFQUVJeEUsS0FBSzBELFNBQUwsQ0FBZXhELFdBQWYsRUFBNEIsSUFBNUIsRUFBa0MsQ0FBbEMsQ0FGSjs7QUFJQSxxQkFBS3BGLEdBQUwsQ0FBUytFLElBQVQsQ0FBYywrREFBZDs7Ozs7OztBQUVBLHFCQUFLL0UsR0FBTCxDQUFTaUQsS0FBVCxDQUFlLGtFQUNYLHVEQURXLEdBRVgsb0JBRko7Ozs7QUFPUixxQkFBS2pELEdBQUwsQ0FBUytFLElBQVQsQ0FBYyxxQ0FBZDtBQUVBLHFCQUFLL0UsR0FBTCxDQUFTeUQsS0FBVCxDQUFlLGlDQUFmOztBQUNBLGlDQUFNOEYsRUFBTixDQUNJNUosS0FBS2dLLFNBQUwsRUFBZ0IsSUFBaEIsRUFBc0IsVUFBdEIsRUFBa0MsWUFBbEMsQ0FESixFQUVJLEtBQUs1SixDQUFMLENBQU82QixHQUFQLENBQVdDLEtBQVgsQ0FBaUJPLFdBQWpCLENBQTZCTixTQUZqQzs7Ozs7Ozs7Ozs7Ozs7QUFNSjs7Ozs7O3NDQUdrQjtBQUFBOztBQUNkLFdBQUs5QixHQUFMLENBQVMrRSxJQUFULENBQWMscUJBQWQ7QUFFQSxVQUFJNkUsbUJBQW1CLEtBQUs3SixDQUFMLENBQU82QixHQUFQLENBQVdDLEtBQVgsQ0FBaUJDLFNBQWpCLENBQTJCOEMsdUJBQWxEOztBQUNBLFVBQUksS0FBS25FLGlCQUFMLEtBQTJCLEtBQUtDLG1CQUFMLENBQXlCRSx5QkFBeEQsRUFBbUY7QUFDL0VnSiwyQkFBbUIsS0FBSzdKLENBQUwsQ0FBTzZCLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsU0FBakIsQ0FBMkIrQyxxQkFBOUM7QUFDSDs7QUFFRCxVQUFJO0FBQUEsMEJBQ3FCSyxLQUFLQyxLQUFMLENBQ2pCLFlBQUd4RCxZQUFILENBQWdCaUksZ0JBQWhCLEVBQWtDLE9BQWxDLENBRGlCLENBRHJCO0FBQUEsWUFDUUMsUUFEUixlQUNRQSxRQURSOztBQUlBLFlBQUlDLFdBQVcsS0FBZjtBQUNBLFlBQUlDLDZCQUE2QixLQUFqQztBQUNBLFlBQUlDLFNBQVMsSUFBYixDQU5BLENBUUE7QUFDQTtBQUNBOztBQUNBSCxpQkFBU3ZELE9BQVQsQ0FBaUIsVUFBQzJELElBQUQsRUFBVTtBQUN2QixjQUFJQyxZQUFKLENBRHVCLENBRXZCOztBQUNBLGNBQUlELEtBQUtFLElBQUwsS0FBYyxJQUFsQixFQUF3QjtBQUNwQkQsMkJBQWUsWUFBR3ZJLFlBQUgsQ0FDWGhDLEtBQUssT0FBS0ksQ0FBTCxDQUFPNkIsR0FBUCxDQUFXQyxLQUFYLENBQWlCTyxXQUFqQixDQUE2Qk4sU0FBbEMsRUFBNkNtSSxLQUFLRyxJQUFsRCxDQURXLEVBRVgsT0FGVyxDQUFmO0FBSUFKLHFCQUFTLE9BQUs1SixRQUFMLENBQWNpSyxtQkFBZCxDQUFrQ0gsWUFBbEMsQ0FBVDtBQUxvQiwwQkFPQUYsTUFQQTtBQU9qQkUsd0JBUGlCLFdBT2pCQSxZQVBpQjtBQVFwQkgseUNBQ0lDLE9BQU9ELDBCQUFQLEdBQW9DLElBQXBDLEdBQTJDQSwwQkFEL0M7QUFFQUQsdUJBQVdFLE9BQU9GLFFBQVAsR0FBa0IsSUFBbEIsR0FBeUJBLFFBQXBDOztBQUVBLHdCQUFHdkgsYUFBSCxDQUNJNUMsS0FBSyxPQUFLSSxDQUFMLENBQU82QixHQUFQLENBQVdDLEtBQVgsQ0FBaUJPLFdBQWpCLENBQTZCTixTQUFsQyxFQUE2Q21JLEtBQUtHLElBQWxELENBREosRUFDNkRGLFlBRDdEO0FBR0g7QUFDSixTQW5CRDs7QUFxQkEsWUFBSSxDQUFDSixRQUFMLEVBQWU7QUFDWCxlQUFLOUosR0FBTCxDQUFTaUQsS0FBVCxDQUFlLHVDQUFmO0FBQ0FDLGtCQUFRQyxJQUFSLENBQWEsQ0FBYjtBQUNIOztBQUNELFlBQUksQ0FBQzRHLDBCQUFMLEVBQWlDO0FBQzdCLGVBQUsvSixHQUFMLENBQVNpRCxLQUFULENBQWUsa0RBQWY7QUFDQUMsa0JBQVFDLElBQVIsQ0FBYSxDQUFiO0FBQ0g7QUFDSixPQXhDRCxDQXdDRSxPQUFPK0UsQ0FBUCxFQUFVO0FBQ1IsYUFBS2xJLEdBQUwsQ0FBU2lELEtBQVQsQ0FBZSw0Q0FBZixFQUE2RGlGLENBQTdEO0FBQ0FoRixnQkFBUUMsSUFBUixDQUFhLENBQWI7QUFDSDs7QUFDRCxXQUFLbkQsR0FBTCxDQUFTK0UsSUFBVCxDQUFjLHVCQUFkO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozs7Ozs7QUFJSSxxQkFBSy9FLEdBQUwsQ0FBUytFLElBQVQsQ0FBYyxrQ0FBZDs7O3VCQUVVLEtBQUt1RixrQkFBTCxFOzs7Ozs7Ozs7QUFFTixxQkFBS3RLLEdBQUwsQ0FBU2lELEtBQVQsQ0FBZSxnREFBZjtBQUNBQyx3QkFBUUMsSUFBUixDQUFhLENBQWI7OztBQUdKLHFCQUFLbkQsR0FBTCxDQUFTK0UsSUFBVCxDQUFjLHFCQUFkOztvQkFFSyxLQUFLaEYsQ0FBTCxDQUFPNkIsR0FBUCxDQUFXbUIsT0FBWCxDQUFtQkMsZTs7Ozs7Ozt1QkFFVixLQUFLdUgsaUJBQUwsRTs7Ozs7Ozs7OztrREFHRyxTLHlCQUtBLE8seUJBTUEsWSx5QkFNQSxNLHlCQU9BLE0seUJBS0EsTTs7OztBQTVCRCxxQkFBS3ZLLEdBQUwsQ0FBU2lELEtBQVQsQ0FDSSw0REFESjs7OztBQUtBLHFCQUFLakQsR0FBTCxDQUFTaUQsS0FBVCxDQUNJLHNFQUNBLE9BRko7Ozs7QUFNQSxxQkFBS2pELEdBQUwsQ0FBU2lELEtBQVQsQ0FDSSwrREFDQSxPQUZKOzs7O0FBTUEscUJBQUtqRCxHQUFMLENBQVNpRCxLQUFULENBQ0ksdUVBQ0EsK0RBREEsR0FFQSwrQkFISjs7OztBQU9BLHFCQUFLakQsR0FBTCxDQUFTaUQsS0FBVCxDQUNJLG9FQURKOzs7O0FBS0EscUJBQUtqRCxHQUFMLENBQVNpRCxLQUFULENBQ0ksMENBREo7Ozs7QUFLQSxxQkFBS2pELEdBQUwsQ0FBU2lELEtBQVQsQ0FBZSw4Q0FBZjs7O3FCQUVKLEtBQUsvQyxjOzs7Ozs7dUJBQ0MsS0FBS3NLLG9CQUFMLENBQTBCLEtBQUt0SyxjQUEvQixDOzs7QUFFVmdELHdCQUFRQyxJQUFSLENBQWEsQ0FBYjs7Ozs7OztBQUdKLHFCQUFLMUMsaUJBQUwsR0FBeUIsS0FBSytDLGNBQUwsRUFBekI7Ozt1QkFFVSxLQUFLNkQsU0FBTCxFOzs7Ozs7Ozs7QUFFTm5FLHdCQUFRQyxJQUFSLENBQWEsQ0FBYjs7O0FBSVIscUJBQUtzSCxlQUFMO0FBRUEscUJBQUtDLFlBQUw7Ozt1QkFHVSxLQUFLQyxVQUFMLEU7Ozs7Ozs7OztBQUVOLHFCQUFLM0ssR0FBTCxDQUFTaUQsS0FBVCxDQUFlLHdDQUFmO0FBQ0FDLHdCQUFRQyxJQUFSLENBQWEsQ0FBYjs7O0FBR0oscUJBQUtuRCxHQUFMLENBQVMrRSxJQUFULENBQWMsdUJBQWQ7O3FCQUVJLEtBQUs3RSxjOzs7Ozs7dUJBQ0MsS0FBS3NLLG9CQUFMLENBQTBCLEtBQUt0SyxjQUEvQixDOzs7Ozs7Ozs7Ozs7Ozs7O21DQUlDO0FBQ1gsVUFBSSxLQUFLSCxDQUFMLENBQU82QixHQUFQLENBQVdtQixPQUFYLENBQW1Cd0YsTUFBbkIsS0FBOEIsSUFBbEMsRUFBd0M7QUFDcEMsWUFBSTtBQUNBLGVBQUtxQyxZQUFMLENBQWtCLEtBQUs3SyxDQUFMLENBQU82QixHQUFQLENBQVdDLEtBQVgsQ0FBaUJPLFdBQWpCLENBQTZCaUgsY0FBL0M7QUFDSCxTQUZELENBRUUsT0FBT25CLENBQVAsRUFBVTtBQUNSLGVBQUtsSSxHQUFMLENBQVNpRCxLQUFULENBQWdCLDZDQUE0Q2lGLEVBQUVDLE9BQVEsRUFBdEU7QUFDSDtBQUNKO0FBQ0o7OztpQ0FFWTtBQUFBOztBQUNULFdBQUtuSSxHQUFMLENBQVMrRSxJQUFULENBQWMsb0NBQWQ7QUFDQSxhQUFPLElBQUloQixPQUFKLENBQVksVUFBQ0MsT0FBRCxFQUFVQyxNQUFWO0FBQUEsZUFDZixjQUFLNEcsYUFBTCxDQUNJLE9BQUs5SyxDQUFMLENBQU82QixHQUFQLENBQVdDLEtBQVgsQ0FBaUJPLFdBQWpCLENBQTZCTixTQURqQyxFQUVJLGNBQUtuQyxJQUFMLENBQVUsT0FBS0ksQ0FBTCxDQUFPNkIsR0FBUCxDQUFXQyxLQUFYLENBQWlCTyxXQUFqQixDQUE2QitCLElBQXZDLEVBQTZDLGFBQTdDLENBRkosRUFHSSxZQUFNO0FBQ0Y7QUFDQTtBQUNBMkcsdUJBQWEsWUFBTTtBQUNmLG1CQUFLOUssR0FBTCxDQUFTaUIsT0FBVCxDQUFpQixtQ0FBakI7O0FBQ0EsbUJBQUtsQixDQUFMLENBQU8wRSxLQUFQLENBQ0tzRSxhQURMLENBQ21CLEtBRG5CLEVBQzBCLE9BQUtoSixDQUFMLENBQU82QixHQUFQLENBQVdDLEtBQVgsQ0FBaUJPLFdBQWpCLENBQTZCTixTQUR2RCxFQUVLd0YsSUFGTCxDQUVVLFlBQU07QUFDUnREO0FBQ0gsYUFKTCxFQUtLdUQsS0FMTCxDQUtXLFVBQUNXLENBQUQsRUFBTztBQUNWakUscUJBQU9pRSxDQUFQO0FBQ0gsYUFQTDtBQVFILFdBVkQ7QUFXSCxTQWpCTCxDQURlO0FBQUEsT0FBWixDQUFQO0FBb0JIIiwiZmlsZSI6Im1ldGVvckFwcC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBmcyBmcm9tICdmcyc7XG5pbXBvcnQgc3Bhd24gZnJvbSAnY3Jvc3Mtc3Bhd24nO1xuaW1wb3J0IHNlbXZlciBmcm9tICdzZW12ZXInO1xuaW1wb3J0IHNoZWxsIGZyb20gJ3NoZWxsanMnO1xuaW1wb3J0IHBhdGggZnJvbSAncGF0aCc7XG5pbXBvcnQgc2luZ2xlTGluZUxvZyBmcm9tICdzaW5nbGUtbGluZS1sb2cnO1xuaW1wb3J0IGFzYXIgZnJvbSAnYXNhcic7XG5pbXBvcnQgZmV0Y2ggZnJvbSAnbm9kZS1mZXRjaCc7XG5cbmltcG9ydCBJc0Rlc2t0b3BJbmplY3RvciBmcm9tICcuLi9za2VsZXRvbi9tb2R1bGVzL2F1dG91cGRhdGUvaXNEZXNrdG9wSW5qZWN0b3InO1xuaW1wb3J0IExvZyBmcm9tICcuL2xvZyc7XG5pbXBvcnQgTWV0ZW9yTWFuYWdlciBmcm9tICcuL21ldGVvck1hbmFnZXInO1xuXG5jb25zdCB7IGpvaW4gfSA9IHBhdGg7XG5jb25zdCBzbGwgPSBzaW5nbGVMaW5lTG9nLnN0ZG91dDtcblxuLy8gVE9ETzogcmVmYWN0b3IgYWxsIHN0cmF0ZWd5IGlmcyB0byBvbmUgcGxhY2VcblxuLyoqXG4gKiBSZXByZXNlbnRzIHRoZSBNZXRlb3IgYXBwLlxuICogQHByb3BlcnR5IHtNZXRlb3JEZXNrdG9wfSAkXG4gKiBAY2xhc3NcbiAqL1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTWV0ZW9yQXBwIHtcbiAgICAvKipcbiAgICAgKiBAcGFyYW0ge01ldGVvckRlc2t0b3B9ICQgLSBjb250ZXh0XG4gICAgICogQGNvbnN0cnVjdG9yXG4gICAgICovXG4gICAgY29uc3RydWN0b3IoJCkge1xuICAgICAgICB0aGlzLmxvZyA9IG5ldyBMb2coJ21ldGVvckFwcCcpO1xuICAgICAgICB0aGlzLiQgPSAkO1xuICAgICAgICB0aGlzLm1ldGVvck1hbmFnZXIgPSBuZXcgTWV0ZW9yTWFuYWdlcigkKTtcbiAgICAgICAgdGhpcy5tb2JpbGVQbGF0Zm9ybSA9IG51bGw7XG4gICAgICAgIHRoaXMub2xkTWFuaWZlc3QgPSBudWxsO1xuICAgICAgICB0aGlzLmluamVjdG9yID0gbmV3IElzRGVza3RvcEluamVjdG9yKCk7XG4gICAgICAgIHRoaXMubWF0Y2hlciA9IG5ldyBSZWdFeHAoXG4gICAgICAgICAgICAnX19tZXRlb3JfcnVudGltZV9jb25maWdfXyA9IEpTT04ucGFyc2VcXFxcKGRlY29kZVVSSUNvbXBvbmVudFxcXFwoXCIoW15cIl0qKVwiXFxcXClcXFxcKSdcbiAgICAgICAgKTtcbiAgICAgICAgdGhpcy5yZXBsYWNlciA9IG5ldyBSZWdFeHAoXG4gICAgICAgICAgICAnKF9fbWV0ZW9yX3J1bnRpbWVfY29uZmlnX18gPSBKU09OLnBhcnNlXFxcXChkZWNvZGVVUklDb21wb25lbnRcXFxcKClcIihbXlwiXSopXCIoXFxcXClcXFxcKSknXG4gICAgICAgICk7XG4gICAgICAgIHRoaXMubWV0ZW9yVmVyc2lvbiA9IG51bGw7XG4gICAgICAgIHRoaXMuaW5kZXhIVE1Mc3RyYXRlZ3kgPSBudWxsO1xuXG4gICAgICAgIHRoaXMuaW5kZXhIVE1MU3RyYXRlZ2llcyA9IHtcbiAgICAgICAgICAgIElOREVYX0ZST01fQ09SRE9WQV9CVUlMRDogMSxcbiAgICAgICAgICAgIElOREVYX0ZST01fUlVOTklOR19TRVJWRVI6IDJcbiAgICAgICAgfTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBFbnN1cmVzIHRoYXQgcmVxdWlyZWQgcGFja2FnZXMgYXJlIGFkZGVkIHRvIHRoZSBNZXRlb3IgYXBwLlxuICAgICAqL1xuICAgIGFzeW5jIGVuc3VyZURlc2t0b3BIQ1BQYWNrYWdlcygpIHtcbiAgICAgICAgY29uc3QgZGVza3RvcEhDUFBhY2thZ2VzID0gWydvbWVnYTptZXRlb3ItZGVza3RvcC13YXRjaGVyJywgJ29tZWdhOm1ldGVvci1kZXNrdG9wLWJ1bmRsZXInXTtcbiAgICAgICAgaWYgKHRoaXMuJC5kZXNrdG9wLmdldFNldHRpbmdzKCkuZGVza3RvcEhDUCkge1xuICAgICAgICAgICAgdGhpcy5sb2cudmVyYm9zZSgnZGVza3RvcEhDUCBpcyBlbmFibGVkLCBjaGVja2luZyBmb3IgcmVxdWlyZWQgcGFja2FnZXMnKTtcblxuICAgICAgICAgICAgY29uc3QgcGFja2FnZXNXaXRoVmVyc2lvbiA9IGRlc2t0b3BIQ1BQYWNrYWdlcy5tYXAocGFja2FnZU5hbWUgPT4gYCR7cGFja2FnZU5hbWV9QCR7dGhpcy4kLmdldFZlcnNpb24oKX1gKTtcblxuICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLm1ldGVvck1hbmFnZXIuZW5zdXJlUGFja2FnZXMoZGVza3RvcEhDUFBhY2thZ2VzLCBwYWNrYWdlc1dpdGhWZXJzaW9uLCAnZGVza3RvcEhDUCcpO1xuICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMubG9nLnZlcmJvc2UoJ2Rlc2t0b3BIQ1AgaXMgbm90IGVuYWJsZWQsIHJlbW92aW5nIHJlcXVpcmVkIHBhY2thZ2VzJyk7XG5cbiAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgaWYgKHRoaXMubWV0ZW9yTWFuYWdlci5jaGVja1BhY2thZ2VzKGRlc2t0b3BIQ1BQYWNrYWdlcykpIHtcbiAgICAgICAgICAgICAgICAgICAgYXdhaXQgdGhpcy5tZXRlb3JNYW5hZ2VyLmRlbGV0ZVBhY2thZ2VzKGRlc2t0b3BIQ1BQYWNrYWdlcywgJ2Rlc2t0b3BIQ1AnKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKGUpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQWRkcyBlbnRyeSB0byAubWV0ZW9yLy5naXRpZ25vcmUgaWYgbmVjZXNzYXJ5LlxuICAgICAqL1xuICAgIHVwZGF0ZUdpdElnbm9yZSgpIHtcbiAgICAgICAgdGhpcy5sb2cudmVyYm9zZSgndXBkYXRpbmcgLm1ldGVvci8uZ2l0aWdub3JlJyk7XG4gICAgICAgIC8vIExldHMgcmVhZCB0aGUgLm1ldGVvci8uZ2l0aWdub3JlIGFuZCBmaWx0ZXIgb3V0IGJsYW5rIGxpbmVzLlxuICAgICAgICBjb25zdCBnaXRJZ25vcmUgPSBmcy5yZWFkRmlsZVN5bmModGhpcy4kLmVudi5wYXRocy5tZXRlb3JBcHAuZ2l0SWdub3JlLCAnVVRGLTgnKVxuICAgICAgICAgICAgLnNwbGl0KCdcXG4nKS5maWx0ZXIoaWdub3JlZFBhdGggPT4gaWdub3JlZFBhdGgudHJpbSgpICE9PSAnJyk7XG5cbiAgICAgICAgaWYgKCF+Z2l0SWdub3JlLmluZGV4T2YodGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5yb290TmFtZSkpIHtcbiAgICAgICAgICAgIHRoaXMubG9nLnZlcmJvc2UoYGFkZGluZyAke3RoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAucm9vdE5hbWV9IHRvIC5tZXRlb3IvLmdpdGlnbm9yZWApO1xuICAgICAgICAgICAgZ2l0SWdub3JlLnB1c2godGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5yb290TmFtZSk7XG5cbiAgICAgICAgICAgIGZzLndyaXRlRmlsZVN5bmModGhpcy4kLmVudi5wYXRocy5tZXRlb3JBcHAuZ2l0SWdub3JlLCBnaXRJZ25vcmUuam9pbignXFxuJyksICdVVEYtOCcpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogUmVhZHMgdGhlIE1ldGVvciByZWxlYXNlIHZlcnNpb24gdXNlZCBpbiB0aGUgYXBwLlxuICAgICAqIEByZXR1cm5zIHtzdHJpbmd9XG4gICAgICovXG4gICAgZ2V0TWV0ZW9yUmVsZWFzZSgpIHtcbiAgICAgICAgbGV0IHJlbGVhc2UgPSBmcy5yZWFkRmlsZVN5bmModGhpcy4kLmVudi5wYXRocy5tZXRlb3JBcHAucmVsZWFzZSwgJ1VURi04JylcbiAgICAgICAgICAgIC5yZXBsYWNlKC9cXHIvZ20sICcnKVxuICAgICAgICAgICAgLnNwbGl0KCdcXG4nKVswXTtcbiAgICAgICAgKFssIHJlbGVhc2VdID0gcmVsZWFzZS5zcGxpdCgnQCcpKTtcbiAgICAgICAgLy8gV2UgZG8gbm90IGNhcmUgaWYgaXQgaXMgYmV0YS5cbiAgICAgICAgaWYgKH5yZWxlYXNlLmluZGV4T2YoJy0nKSkge1xuICAgICAgICAgICAgKFtyZWxlYXNlXSA9IHJlbGVhc2Uuc3BsaXQoJy0nKSk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHJlbGVhc2U7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQ2FzdCBNZXRlb3IgcmVsZWFzZSB0byBzZW12ZXIgdmVyc2lvbi5cbiAgICAgKiBAcmV0dXJucyB7c3RyaW5nfVxuICAgICAqL1xuICAgIGNhc3RNZXRlb3JSZWxlYXNlVG9TZW12ZXIoKSB7XG4gICAgICAgIHJldHVybiBgJHt0aGlzLmdldE1ldGVvclJlbGVhc2UoKX0uMC4wYC5tYXRjaCgvKF5cXGQrXFwuXFxkK1xcLlxcZCspL2dtaSlbMF07XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogVmFsaWRhdGUgbWV0ZW9yIHZlcnNpb24gYWdhaW5zdCBhIHZlcnNpb25SYW5nZS5cbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gdmVyc2lvblJhbmdlIC0gc2VtdmVyIHZlcnNpb24gcmFuZ2VcbiAgICAgKi9cbiAgICBjaGVja01ldGVvclZlcnNpb24odmVyc2lvblJhbmdlKSB7XG4gICAgICAgIGNvbnN0IHJlbGVhc2UgPSB0aGlzLmNhc3RNZXRlb3JSZWxlYXNlVG9TZW12ZXIoKTtcbiAgICAgICAgaWYgKCFzZW12ZXIuc2F0aXNmaWVzKHJlbGVhc2UsIHZlcnNpb25SYW5nZSkpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLiQuZW52Lm9wdGlvbnMuc2tpcE1vYmlsZUJ1aWxkKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5sb2cuZXJyb3IoYHdyb25nIG1ldGVvciB2ZXJzaW9uICgke3JlbGVhc2V9KSBpbiBwcm9qZWN0IC0gb25seSBgICtcbiAgICAgICAgICAgICAgICAgICAgYCR7dmVyc2lvblJhbmdlfSBpcyBzdXBwb3J0ZWRgKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy5sb2cuZXJyb3IoYHdyb25nIG1ldGVvciB2ZXJzaW9uICgke3JlbGVhc2V9KSBpbiBwcm9qZWN0IC0gb25seSBgICtcbiAgICAgICAgICAgICAgICAgICAgYCR7dmVyc2lvblJhbmdlfSBpcyBzdXBwb3J0ZWQgZm9yIGF1dG9tYXRpYyBtZXRlb3IgYnVpbGRzICh5b3UgY2FuIGFsd2F5cyBgICtcbiAgICAgICAgICAgICAgICAgICAgJ3RyeSB3aXRoIGAtLXNraXAtbW9iaWxlLWJ1aWxkYCBpZiB5b3UgYXJlIHVzaW5nIG1ldGVvciA+PSAxLjIuMScpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcHJvY2Vzcy5leGl0KDEpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogRGVjaWRlcyB3aGljaCBzdHJhdGVneSB0byB1c2Ugd2hpbGUgdHJ5aW5nIHRvIGdldCBjbGllbnQgYnVpbGQgb3V0IG9mIE1ldGVvciBwcm9qZWN0LlxuICAgICAqIEByZXR1cm5zIHtudW1iZXJ9XG4gICAgICovXG4gICAgY2hvb3NlU3RyYXRlZ3koKSB7XG4gICAgICAgIGlmICh0aGlzLiQuZW52Lm9wdGlvbnMuZm9yY2VDb3Jkb3ZhQnVpbGQpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmluZGV4SFRNTFN0cmF0ZWdpZXMuSU5ERVhfRlJPTV9DT1JET1ZBX0JVSUxEO1xuICAgICAgICB9XG5cbiAgICAgICAgY29uc3QgcmVsZWFzZSA9IHRoaXMuY2FzdE1ldGVvclJlbGVhc2VUb1NlbXZlcigpO1xuICAgICAgICBpZiAoc2VtdmVyLnNhdGlzZmllcyhyZWxlYXNlLCAnPiAxLjMuNCcpKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5pbmRleEhUTUxTdHJhdGVnaWVzLklOREVYX0ZST01fUlVOTklOR19TRVJWRVI7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHNlbXZlci5zYXRpc2ZpZXMocmVsZWFzZSwgJzEuMy40JykpIHtcbiAgICAgICAgICAgIGNvbnN0IGV4cGxvZGVkVmVyc2lvbiA9IHRoaXMuZ2V0TWV0ZW9yUmVsZWFzZSgpLnNwbGl0KCcuJyk7XG4gICAgICAgICAgICBpZiAoZXhwbG9kZWRWZXJzaW9uLmxlbmd0aCA+PSA0KSB7XG4gICAgICAgICAgICAgICAgaWYgKGV4cGxvZGVkVmVyc2lvblszXSA+IDEpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuaW5kZXhIVE1MU3RyYXRlZ2llcy5JTkRFWF9GUk9NX1JVTk5JTkdfU0VSVkVSO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5pbmRleEhUTUxTdHJhdGVnaWVzLklOREVYX0ZST01fQ09SRE9WQV9CVUlMRDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5pbmRleEhUTUxTdHJhdGVnaWVzLklOREVYX0ZST01fQ09SRE9WQV9CVUlMRDtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBDaGVja3MgcmVxdWlyZWQgcHJlY29uZGl0aW9ucy5cbiAgICAgKiAtIE1ldGVvciB2ZXJzaW9uXG4gICAgICogLSBpcyBtb2JpbGUgcGxhdGZvcm0gYWRkZWRcbiAgICAgKi9cbiAgICBhc3luYyBjaGVja1ByZWNvbmRpdGlvbnMoKSB7XG4gICAgICAgIGlmICh0aGlzLiQuZW52Lm9wdGlvbnMuc2tpcE1vYmlsZUJ1aWxkKSB7XG4gICAgICAgICAgICB0aGlzLmNoZWNrTWV0ZW9yVmVyc2lvbignPj0gMS4yLjEnKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuY2hlY2tNZXRlb3JWZXJzaW9uKCc+PSAxLjMuMycpO1xuICAgICAgICAgICAgdGhpcy5pbmRleEhUTUxzdHJhdGVneSA9IHRoaXMuY2hvb3NlU3RyYXRlZ3koKTtcbiAgICAgICAgICAgIGlmICh0aGlzLmluZGV4SFRNTHN0cmF0ZWd5ID09PSB0aGlzLmluZGV4SFRNTFN0cmF0ZWdpZXMuSU5ERVhfRlJPTV9DT1JET1ZBX0JVSUxEKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5sb2cuZGVidWcoXG4gICAgICAgICAgICAgICAgICAgICdtZXRlb3IgdmVyc2lvbiBpcyA8IDEuMy40LjIgc28gdGhlIGluZGV4Lmh0bWwgZnJvbSBjb3Jkb3ZhLWJ1aWxkIHdpbGwnICtcbiAgICAgICAgICAgICAgICAgICAgJyBiZSB1c2VkJ1xuICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMubG9nLmRlYnVnKFxuICAgICAgICAgICAgICAgICAgICAnbWV0ZW9yIHZlcnNpb24gaXMgPj0gMS4zLjQuMiBzbyB0aGUgaW5kZXguaHRtbCB3aWxsIGJlIGRvd25sb2FkZWQgJyArXG4gICAgICAgICAgICAgICAgICAgICdmcm9tIF9fY29yZG92YS9pbmRleC5odG1sJ1xuICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoIXRoaXMuJC5lbnYub3B0aW9ucy5za2lwTW9iaWxlQnVpbGQpIHtcbiAgICAgICAgICAgIGNvbnN0IHBsYXRmb3JtcyA9IGZzLnJlYWRGaWxlU3luYyh0aGlzLiQuZW52LnBhdGhzLm1ldGVvckFwcC5wbGF0Zm9ybXMsICdVVEYtOCcpO1xuICAgICAgICAgICAgaWYgKCF+cGxhdGZvcm1zLmluZGV4T2YoJ2FuZHJvaWQnKSAmJiAhfnBsYXRmb3Jtcy5pbmRleE9mKCdpb3MnKSkge1xuICAgICAgICAgICAgICAgIGlmICghdGhpcy4kLmVudi5vcHRpb25zLmFuZHJvaWQpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5tb2JpbGVQbGF0Zm9ybSA9ICdpb3MnO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMubW9iaWxlUGxhdGZvcm0gPSAnYW5kcm9pZCc7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHRoaXMubG9nLndhcm4oYG5vIG1vYmlsZSB0YXJnZXQgZGV0ZWN0ZWQgLSB3aWxsIGFkZCAnJHt0aGlzLm1vYmlsZVBsYXRmb3JtfScgYCArXG4gICAgICAgICAgICAgICAgICAgICdqdXN0IHRvIGdldCBhIG1vYmlsZSBidWlsZCcpO1xuICAgICAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMuYWRkTW9iaWxlUGxhdGZvcm0odGhpcy5tb2JpbGVQbGF0Zm9ybSk7XG4gICAgICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvZy5lcnJvcignZmFpbGVkIHRvIGFkZCBhIG1vYmlsZSBwbGF0Zm9ybSAtIHBsZWFzZSB0cnkgdG8gZG8gaXQgbWFudWFsbHknKTtcbiAgICAgICAgICAgICAgICAgICAgcHJvY2Vzcy5leGl0KDEpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFRyaWVzIHRvIGFkZCBhIG1vYmlsZSBwbGF0Zm9ybSB0byBtZXRlb3IgcHJvamVjdC5cbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gcGxhdGZvcm0gLSBwbGF0Zm9ybSB0byBhZGRcbiAgICAgKiBAcmV0dXJucyB7UHJvbWlzZX1cbiAgICAgKi9cbiAgICBhZGRNb2JpbGVQbGF0Zm9ybShwbGF0Zm9ybSkge1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5sb2cudmVyYm9zZShgYWRkaW5nIG1vYmlsZSBwbGF0Zm9ybTogJHtwbGF0Zm9ybX1gKTtcbiAgICAgICAgICAgIHNwYXduKCdtZXRlb3InLCBbJ2FkZC1wbGF0Zm9ybScsIHBsYXRmb3JtXSwge1xuICAgICAgICAgICAgICAgIGN3ZDogdGhpcy4kLmVudi5wYXRocy5tZXRlb3JBcHAucm9vdCxcbiAgICAgICAgICAgICAgICBzdGRpbzogdGhpcy4kLmVudi5zdGRpb1xuICAgICAgICAgICAgfSkub24oJ2V4aXQnLCAoKSA9PiB7XG4gICAgICAgICAgICAgICAgY29uc3QgcGxhdGZvcm1zID0gZnMucmVhZEZpbGVTeW5jKHRoaXMuJC5lbnYucGF0aHMubWV0ZW9yQXBwLnBsYXRmb3JtcywgJ1VURi04Jyk7XG4gICAgICAgICAgICAgICAgaWYgKCF+cGxhdGZvcm1zLmluZGV4T2YoJ2FuZHJvaWQnKSAmJiAhfnBsYXRmb3Jtcy5pbmRleE9mKCdpb3MnKSkge1xuICAgICAgICAgICAgICAgICAgICByZWplY3QoKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFRyaWVzIHRvIHJlbW92ZSBhIG1vYmlsZSBwbGF0Zm9ybSBmcm9tIG1ldGVvciBwcm9qZWN0LlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBwbGF0Zm9ybSAtIHBsYXRmb3JtIHRvIHJlbW92ZVxuICAgICAqIEByZXR1cm5zIHtQcm9taXNlfVxuICAgICAqL1xuICAgIHJlbW92ZU1vYmlsZVBsYXRmb3JtKHBsYXRmb3JtKSB7XG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICAgICAgICB0aGlzLmxvZy52ZXJib3NlKGByZW1vdmluZyBtb2JpbGUgcGxhdGZvcm06ICR7cGxhdGZvcm19YCk7XG4gICAgICAgICAgICBzcGF3bignbWV0ZW9yJywgWydyZW1vdmUtcGxhdGZvcm0nLCBwbGF0Zm9ybV0sIHtcbiAgICAgICAgICAgICAgICBjd2Q6IHRoaXMuJC5lbnYucGF0aHMubWV0ZW9yQXBwLnJvb3QsXG4gICAgICAgICAgICAgICAgc3RkaW86IHRoaXMuJC5lbnYuc3RkaW8sXG4gICAgICAgICAgICAgICAgZW52OiBPYmplY3QuYXNzaWduKHsgTUVURU9SX1BSRVRUWV9PVVRQVVQ6IDAgfSwgcHJvY2Vzcy5lbnYpXG4gICAgICAgICAgICB9KS5vbignZXhpdCcsICgpID0+IHtcbiAgICAgICAgICAgICAgICBjb25zdCBwbGF0Zm9ybXMgPSBmcy5yZWFkRmlsZVN5bmModGhpcy4kLmVudi5wYXRocy5tZXRlb3JBcHAucGxhdGZvcm1zLCAnVVRGLTgnKTtcbiAgICAgICAgICAgICAgICBpZiAofnBsYXRmb3Jtcy5pbmRleE9mKHBsYXRmb3JtKSkge1xuICAgICAgICAgICAgICAgICAgICByZWplY3QoKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEp1c3QgY2hlY2tzIGZvciBpbmRleC5odG1sIGFuZCBwcm9ncmFtLmpzb24gZXhpc3RlbmNlLlxuICAgICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgICAqL1xuICAgIGlzQ29yZG92YUJ1aWxkUmVhZHkoKSB7XG4gICAgICAgIGlmICh0aGlzLmluZGV4SFRNTHN0cmF0ZWd5ID09PSB0aGlzLmluZGV4SFRNTFN0cmF0ZWdpZXMuSU5ERVhfRlJPTV9DT1JET1ZBX0JVSUxEKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy4kLnV0aWxzLmV4aXN0cyh0aGlzLiQuZW52LnBhdGhzLm1ldGVvckFwcC5jb3Jkb3ZhQnVpbGRJbmRleCkgJiZcbiAgICAgICAgICAgICAgICB0aGlzLiQudXRpbHMuZXhpc3RzKHRoaXMuJC5lbnYucGF0aHMubWV0ZW9yQXBwLmNvcmRvdmFCdWlsZFByb2dyYW1Kc29uKSAmJlxuICAgICAgICAgICAgICAgIChcbiAgICAgICAgICAgICAgICAgICAgIXRoaXMub2xkTWFuaWZlc3QgfHxcbiAgICAgICAgICAgICAgICAgICAgKHRoaXMub2xkTWFuaWZlc3QgJiZcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub2xkTWFuaWZlc3QgIT09IGZzLnJlYWRGaWxlU3luYyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiQuZW52LnBhdGhzLm1ldGVvckFwcC5jb3Jkb3ZhQnVpbGRQcm9ncmFtSnNvbiwgJ1VURi04J1xuICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy4kLnV0aWxzLmV4aXN0cyh0aGlzLiQuZW52LnBhdGhzLm1ldGVvckFwcC53ZWJDb3Jkb3ZhUHJvZ3JhbUpzb24pICYmXG4gICAgICAgICAgICAoXG4gICAgICAgICAgICAgICAgIXRoaXMub2xkTWFuaWZlc3QgfHxcbiAgICAgICAgICAgICAgICAodGhpcy5vbGRNYW5pZmVzdCAmJlxuICAgICAgICAgICAgICAgICAgICB0aGlzLm9sZE1hbmlmZXN0ICE9PSBmcy5yZWFkRmlsZVN5bmMoXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiQuZW52LnBhdGhzLm1ldGVvckFwcC53ZWJDb3Jkb3ZhUHJvZ3JhbUpzb24sICdVVEYtOCdcbiAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogRmV0Y2hlcyBpbmRleC5odG1sIGZyb20gcnVubmluZyBwcm9qZWN0LlxuICAgICAqIEByZXR1cm5zIHtQcm9taXNlLjwqPn1cbiAgICAgKi9cbiAgICBhc3luYyBhY3F1aXJlSW5kZXgoKSB7XG4gICAgICAgIGNvbnN0IHBvcnQgPSAodGhpcy4kLmVudi5vcHRpb25zLnBvcnQpID8gdGhpcy4kLmVudi5vcHRpb25zLnBvcnQgOiAzMDgwO1xuICAgICAgICB0aGlzLmxvZy5pbmZvKCdhY3F1aXJpbmcgaW5kZXguaHRtbCcpO1xuICAgICAgICBjb25zdCByZXMgPSBhd2FpdCBmZXRjaChgaHR0cDovLzEyNy4wLjAuMToke3BvcnR9L19fY29yZG92YS9pbmRleC5odG1sYCk7XG4gICAgICAgIGNvbnN0IHRleHQgPSBhd2FpdCByZXMudGV4dCgpO1xuICAgICAgICAvLyBTaW1wbGUgdGVzdCBpZiB3ZSByZWFsbHkgZG93bmxvYWQgaW5kZXguaHRtbCBmb3Igd2ViLmNvcmRvdmEuXG4gICAgICAgIGlmICh+dGV4dC5pbmRleE9mKCdzcmM9XCIvY29yZG92YS5qc1wiJykpIHtcbiAgICAgICAgICAgIHJldHVybiB0ZXh0O1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBGZXRjaGVzIG1haW5mZXN0Lmpzb24gZnJvbSBydW5uaW5nIHByb2plY3QuXG4gICAgICogQHJldHVybnMge1Byb21pc2UuPHZvaWQ+fVxuICAgICAqL1xuICAgIGFzeW5jIGFjcXVpcmVNYW5pZmVzdCgpIHtcbiAgICAgICAgY29uc3QgcG9ydCA9ICh0aGlzLiQuZW52Lm9wdGlvbnMucG9ydCkgPyB0aGlzLiQuZW52Lm9wdGlvbnMucG9ydCA6IDMwODA7XG4gICAgICAgIHRoaXMubG9nLmluZm8oJ2FjcXVpcmluZyBtYW5pZmVzdC5qc29uJyk7XG4gICAgICAgIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKFxuICAgICAgICAgICAgYGh0dHA6Ly8xMjcuMC4wLjE6JHtwb3J0fS9fX2NvcmRvdmEvbWFuaWZlc3QuanNvbj9tZXRlb3JfZG9udF9zZXJ2ZV9pbmRleD10cnVlYFxuICAgICAgICApO1xuICAgICAgICBjb25zdCB0ZXh0ID0gYXdhaXQgcmVzLnRleHQoKTtcbiAgICAgICAgcmV0dXJuIEpTT04ucGFyc2UodGV4dCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogVHJpZXMgdG8gZ2V0IGEgbW9iaWxlIGJ1aWxkIGZyb20gbWV0ZW9yIGFwcC5cbiAgICAgKiBJbiBjYXNlIG9mIGZhaWx1cmUgbGVhdmVzIGEgbWV0ZW9yLmxvZy5cbiAgICAgKiBBIGxvdCBvZiBzdHVmZiBpcyBoYXBwZW5pbmcgaGVyZSAtIGJ1dCB0aGUgbWFpbiBhaW0gaXMgdG8gZ2V0IGEgbW9iaWxlIGJ1aWxkIGZyb21cbiAgICAgKiAubWV0ZW9yL2xvY2FsL2NvcmRvdmEtYnVpbGQvd3d3L2FwcGxpY2F0aW9uIGFuZCBleGl0IGFzIHNvb24gYXMgcG9zc2libGUuXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7UHJvbWlzZX1cbiAgICAgKi9cbiAgICBidWlsZE1vYmlsZVRhcmdldCgpIHtcbiAgICAgICAgY29uc3QgcHJvZ3JhbUpzb24gPVxuICAgICAgICAgICAgKHRoaXMuaW5kZXhIVE1Mc3RyYXRlZ3kgPT09IHRoaXMuaW5kZXhIVE1MU3RyYXRlZ2llcy5JTkRFWF9GUk9NX0NPUkRPVkFfQlVJTEQpID9cbiAgICAgICAgICAgICAgICB0aGlzLiQuZW52LnBhdGhzLm1ldGVvckFwcC5jb3Jkb3ZhQnVpbGRQcm9ncmFtSnNvbiA6XG4gICAgICAgICAgICAgICAgdGhpcy4kLmVudi5wYXRocy5tZXRlb3JBcHAud2ViQ29yZG92YVByb2dyYW1Kc29uO1xuXG4gICAgICAgIGlmICh0aGlzLiQudXRpbHMuZXhpc3RzKHByb2dyYW1Kc29uKSkge1xuICAgICAgICAgICAgdGhpcy5vbGRNYW5pZmVzdCA9IGZzLnJlYWRGaWxlU3luYyhwcm9ncmFtSnNvbiwgJ1VURi04Jyk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICAgICAgY29uc3Qgc2VsZiA9IHRoaXM7XG4gICAgICAgICAgICBsZXQgbG9nID0gJyc7XG4gICAgICAgICAgICBsZXQgZGVzaXJlZEV4aXQgPSBmYWxzZTtcbiAgICAgICAgICAgIGxldCBidWlsZFRpbWVvdXQgPSBudWxsO1xuICAgICAgICAgICAgbGV0IGVycm9yVGltZW91dCA9IG51bGw7XG4gICAgICAgICAgICBsZXQgbWVzc2FnZVRpbWVvdXQgPSBudWxsO1xuICAgICAgICAgICAgbGV0IGtpbGxUaW1lb3V0ID0gbnVsbDtcbiAgICAgICAgICAgIGxldCBjb3Jkb3ZhQ2hlY2tJbnRlcnZhbCA9IG51bGw7XG4gICAgICAgICAgICBsZXQgcG9ydFByb2JsZW0gPSBmYWxzZTtcblxuICAgICAgICAgICAgZnVuY3Rpb24gd2luZG93c0tpbGwocGlkKSB7XG4gICAgICAgICAgICAgICAgc2VsZi5sb2cuZGVidWcoYGtpbGxpbmcgcGlkOiAke3BpZH1gKTtcbiAgICAgICAgICAgICAgICBzcGF3bi5zeW5jKCd0YXNra2lsbCcsIFsnL3BpZCcsIHBpZCwgJy9mJywgJy90J10pO1xuXG4gICAgICAgICAgICAgICAgLy8gV2Ugd2lsbCBsb29rIGZvciBvdGhlciBwcm9jZXNzIHdoaWNoIG1pZ2h0IGhhdmUgYmVlbiBjcmVhdGVkIG91dHNpZGUgdGhlXG4gICAgICAgICAgICAgICAgLy8gcHJvY2VzcyB0cmVlLlxuICAgICAgICAgICAgICAgIC8vIExldHMgbGlzdCBhbGwgbm9kZS5leGUgcHJvY2Vzc2VzLlxuXG4gICAgICAgICAgICAgICAgY29uc3Qgb3V0ID0gc3Bhd25cbiAgICAgICAgICAgICAgICAgICAgLnN5bmMoXG4gICAgICAgICAgICAgICAgICAgICAgICAnd21pYycsXG4gICAgICAgICAgICAgICAgICAgICAgICBbJ3Byb2Nlc3MnLCAnd2hlcmUnLCAnY2FwdGlvbj1cIm5vZGUuZXhlXCInLCAnZ2V0JywgJ2NvbW1hbmRsaW5lLHByb2Nlc3NpZCddXG4gICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgLnN0ZG91dC50b1N0cmluZygndXRmLTgnKVxuICAgICAgICAgICAgICAgICAgICAuc3BsaXQoJ1xcbicpO1xuICAgICAgICAgICAgICAgIGNvbnN0IGFyZ3MgPSBzZWxmLnByZXBhcmVBcmd1bWVudHMoKTtcbiAgICAgICAgICAgICAgICAvLyBMZXRzIG1vdW50IHJlZ2V4LlxuICAgICAgICAgICAgICAgIGNvbnN0IHJlZ2V4VjEgPSBuZXcgUmVnRXhwKGAke2FyZ3Muam9pbignXFxcXHMrJyl9XFxcXHMrKFxcXFxkKylgLCAnZ20nKTtcbiAgICAgICAgICAgICAgICBjb25zdCByZWdleFYyID0gbmV3IFJlZ0V4cChgXCIke2FyZ3Muam9pbignXCJcXFxccytcIicpfVwiXFxcXHMrKFxcXFxkKylgLCAnZ20nKTtcbiAgICAgICAgICAgICAgICAvLyBObyB3ZSB3aWxsIGNoZWNrIGZvciB0aG9zZSB3aXRoIHRoZSBtYXRjaGluZyBwYXJhbXMuXG4gICAgICAgICAgICAgICAgb3V0LmZvckVhY2goKGxpbmUpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgbWF0Y2ggPSByZWdleFYxLmV4ZWMobGluZSkgfHwgcmVnZXhWMi5leGVjKGxpbmUpIHx8IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICBpZiAobWF0Y2gpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYubG9nLmRlYnVnKGBraWxsaW5nIHBpZDogJHttYXRjaFsxXX1gKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNwYXduLnN5bmMoJ3Rhc2traWxsJywgWycvcGlkJywgbWF0Y2hbMV0sICcvZicsICcvdCddKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICByZWdleFYxLmxhc3RJbmRleCA9IDA7XG4gICAgICAgICAgICAgICAgICAgIHJlZ2V4VjIubGFzdEluZGV4ID0gMDtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgZnVuY3Rpb24gd3JpdGVMb2coKSB7XG4gICAgICAgICAgICAgICAgZnMud3JpdGVGaWxlU3luYygnbWV0ZW9yLmxvZycsIGxvZywgJ1VURi04Jyk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGZ1bmN0aW9uIGNsZWFyVGltZW91dHNBbmRJbnRlcnZhbHMoKSB7XG4gICAgICAgICAgICAgICAgY2xlYXJJbnRlcnZhbChjb3Jkb3ZhQ2hlY2tJbnRlcnZhbCk7XG4gICAgICAgICAgICAgICAgY2xlYXJUaW1lb3V0KGJ1aWxkVGltZW91dCk7XG4gICAgICAgICAgICAgICAgY2xlYXJUaW1lb3V0KGVycm9yVGltZW91dCk7XG4gICAgICAgICAgICAgICAgY2xlYXJUaW1lb3V0KG1lc3NhZ2VUaW1lb3V0KTtcbiAgICAgICAgICAgICAgICBjbGVhclRpbWVvdXQoa2lsbFRpbWVvdXQpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBjb25zdCBhcmdzID0gdGhpcy5wcmVwYXJlQXJndW1lbnRzKCk7XG5cbiAgICAgICAgICAgIHRoaXMubG9nLmluZm8oYHJ1bm5pbmcgXCJtZXRlb3IgJHthcmdzLmpvaW4oJyAnKX1cIi4uLiB0aGlzIG1pZ2h0IHRha2UgYSB3aGlsZWApO1xuXG4gICAgICAgICAgICAvLyBMZXRzIHNwYXduIG1ldGVvci5cbiAgICAgICAgICAgIGNvbnN0IGNoaWxkID0gc3Bhd24oXG4gICAgICAgICAgICAgICAgJ21ldGVvcicsXG4gICAgICAgICAgICAgICAgYXJncyxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGVudjogT2JqZWN0LmFzc2lnbihcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgTUVURU9SX1BSRVRUWV9PVVRQVVQ6IDAsIE1FVEVPUl9OT19SRUxFQVNFX0NIRUNLOiAxIH0sIHByb2Nlc3MuZW52XG4gICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgIGN3ZDogdGhpcy4kLmVudi5wYXRocy5tZXRlb3JBcHAucm9vdFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgeyBzaGVsbDogdHJ1ZSB9XG4gICAgICAgICAgICApO1xuXG4gICAgICAgICAgICAvLyBLaWxscyB0aGUgY3VycmVudGx5IHJ1bm5pbmcgbWV0ZW9yIGNvbW1hbmQuXG4gICAgICAgICAgICBmdW5jdGlvbiBraWxsKCkge1xuICAgICAgICAgICAgICAgIHNsbCgnJyk7XG4gICAgICAgICAgICAgICAgY2hpbGQua2lsbCgnU0lHS0lMTCcpO1xuICAgICAgICAgICAgICAgIGlmIChzZWxmLiQuZW52Lm9zLmlzV2luZG93cykge1xuICAgICAgICAgICAgICAgICAgICB3aW5kb3dzS2lsbChjaGlsZC5waWQpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgZnVuY3Rpb24gZXhpdCgpIHtcbiAgICAgICAgICAgICAgICBraWxsVGltZW91dCA9IHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBjbGVhclRpbWVvdXRzQW5kSW50ZXJ2YWxzKCk7XG4gICAgICAgICAgICAgICAgICAgIGRlc2lyZWRFeGl0ID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAga2lsbCgpO1xuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKCk7XG4gICAgICAgICAgICAgICAgfSwgNTAwKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgZnVuY3Rpb24gY29weUJ1aWxkKCkge1xuICAgICAgICAgICAgICAgIHNlbGYuY29weUJ1aWxkKCkudGhlbigoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGV4aXQoKTtcbiAgICAgICAgICAgICAgICB9KS5jYXRjaCgoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGNsZWFyVGltZW91dHNBbmRJbnRlcnZhbHMoKTtcbiAgICAgICAgICAgICAgICAgICAga2lsbCgpO1xuICAgICAgICAgICAgICAgICAgICB3cml0ZUxvZygpO1xuICAgICAgICAgICAgICAgICAgICByZWplY3QoJ2NvcHknKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgY29yZG92YUNoZWNrSW50ZXJ2YWwgPSBzZXRJbnRlcnZhbCgoKSA9PiB7XG4gICAgICAgICAgICAgICAgLy8gQ2hlY2sgaWYgd2UgYWxyZWFkeSBoYXZlIGNvcmRvdmEtYnVpbGQgcmVhZHkuXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuaXNDb3Jkb3ZhQnVpbGRSZWFkeSgpKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIElmIHNvLCB0aGVuIGV4aXQgaW1tZWRpYXRlbHkuXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmluZGV4SFRNTHN0cmF0ZWd5ID09PVxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pbmRleEhUTUxTdHJhdGVnaWVzLklOREVYX0ZST01fQ09SRE9WQV9CVUlMRCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29weUJ1aWxkKCk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LCAxMDAwKTtcblxuICAgICAgICAgICAgY2hpbGQuc3RkZXJyLm9uKCdkYXRhJywgKGNodW5rKSA9PiB7XG4gICAgICAgICAgICAgICAgY29uc3QgbGluZSA9IGNodW5rLnRvU3RyaW5nKCdVVEYtOCcpO1xuICAgICAgICAgICAgICAgIGxvZyArPSBgJHtsaW5lfVxcbmA7XG4gICAgICAgICAgICAgICAgaWYgKGVycm9yVGltZW91dCkge1xuICAgICAgICAgICAgICAgICAgICBjbGVhclRpbWVvdXQoZXJyb3JUaW1lb3V0KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLy8gRG8gbm90IGV4aXQgaWYgdGhpcyBpcyB0aGUgd2FybmluZyBmb3IgdXNpbmcgLS1wcm9kdWN0aW9uLlxuICAgICAgICAgICAgICAgIC8vIE91dHB1dCBleGNlZWRzIC0+IGh0dHBzOi8vZ2l0aHViLmNvbS9tZXRlb3IvbWV0ZW9yL2lzc3Vlcy84NTkyXG4gICAgICAgICAgICAgICAgaWYgKCF+bGluZS5pbmRleE9mKCctLXByb2R1Y3Rpb24nKSAmJiAhfmxpbmUuaW5kZXhPZignT3V0cHV0IGV4Y2VlZHMgJykpIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gV2Ugd2lsbCBleGl0IDFzIGFmdGVyIGxhc3QgZXJyb3IgaW4gc3RkZXJyLlxuICAgICAgICAgICAgICAgICAgICBlcnJvclRpbWVvdXQgPSBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsZWFyVGltZW91dHNBbmRJbnRlcnZhbHMoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGtpbGwoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHdyaXRlTG9nKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICByZWplY3QoJ2Vycm9yJyk7XG4gICAgICAgICAgICAgICAgICAgIH0sIDEwMDApO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICBjaGlsZC5zdGRvdXQub24oJ2RhdGEnLCAoY2h1bmspID0+IHtcbiAgICAgICAgICAgICAgICBjb25zdCBsaW5lID0gY2h1bmsudG9TdHJpbmcoJ1VURi04Jyk7XG4gICAgICAgICAgICAgICAgaWYgKCFkZXNpcmVkRXhpdCAmJiBsaW5lLnRyaW0oKS5yZXBsYWNlKC9bXFxuXFxyXFx0XFx2XFxmXSsvZ20sICcnKSAhPT0gJycpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgbGluZXNUb0Rpc3BsYXkgPSBsaW5lLnRyaW0oKVxuICAgICAgICAgICAgICAgICAgICAgICAgLnNwbGl0KCdcXG5cXHInKTtcbiAgICAgICAgICAgICAgICAgICAgLy8gT25seSBkaXNwbGF5IGxhc3QgbGluZSBmcm9tIHRoZSBjaHVuay5cbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgc2FuaXRpemVkTGluZSA9IGxpbmVzVG9EaXNwbGF5LnBvcCgpLnJlcGxhY2UoL1tcXG5cXHJcXHRcXHZcXGZdKy9nbSwgJycpO1xuICAgICAgICAgICAgICAgICAgICBzbGwoc2FuaXRpemVkTGluZSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGxvZyArPSBgJHtsaW5lfVxcbmA7XG4gICAgICAgICAgICAgICAgaWYgKH5saW5lLmluZGV4T2YoJ2FmdGVyX3BsYXRmb3JtX2FkZCcpKSB7XG4gICAgICAgICAgICAgICAgICAgIHNsbCgnJyk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9nLmluZm8oJ2RvbmUuLi4gMTAlJyk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKH5saW5lLmluZGV4T2YoJ0xvY2FsIHBhY2thZ2UgdmVyc2lvbicpKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChtZXNzYWdlVGltZW91dCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgY2xlYXJUaW1lb3V0KG1lc3NhZ2VUaW1lb3V0KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlVGltZW91dCA9IHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgc2xsKCcnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9nLmluZm8oJ2J1aWxkaW5nIGluIHByb2dyZXNzLi4uJyk7XG4gICAgICAgICAgICAgICAgICAgIH0sIDE1MDApO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmICh+bGluZS5pbmRleE9mKCdQcmVwYXJpbmcgQ29yZG92YSBwcm9qZWN0JykpIHtcbiAgICAgICAgICAgICAgICAgICAgc2xsKCcnKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2cuaW5mbygnZG9uZS4uLiA2MCUnKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZiAofmxpbmUuaW5kZXhPZignQ2FuXFwndCBsaXN0ZW4gb24gcG9ydCcpKSB7XG4gICAgICAgICAgICAgICAgICAgIHBvcnRQcm9ibGVtID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZiAofmxpbmUuaW5kZXhPZignWW91ciBhcHBsaWNhdGlvbiBoYXMgZXJyb3JzJykpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGVycm9yVGltZW91dCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgY2xlYXJUaW1lb3V0KGVycm9yVGltZW91dCk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgZXJyb3JUaW1lb3V0ID0gc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjbGVhclRpbWVvdXRzQW5kSW50ZXJ2YWxzKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBraWxsKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB3cml0ZUxvZygpO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVqZWN0KCdlcnJvckluQXBwJyk7XG4gICAgICAgICAgICAgICAgICAgIH0sIDEwMDApO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmICh+bGluZS5pbmRleE9mKCdBcHAgcnVubmluZyBhdCcpKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvcHlCdWlsZCgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAvLyBXaGVuIE1ldGVvciBleGl0c1xuICAgICAgICAgICAgY2hpbGQub24oJ2V4aXQnLCAoKSA9PiB7XG4gICAgICAgICAgICAgICAgc2xsKCcnKTtcbiAgICAgICAgICAgICAgICBjbGVhclRpbWVvdXRzQW5kSW50ZXJ2YWxzKCk7XG4gICAgICAgICAgICAgICAgaWYgKCFkZXNpcmVkRXhpdCkge1xuICAgICAgICAgICAgICAgICAgICB3cml0ZUxvZygpO1xuICAgICAgICAgICAgICAgICAgICBpZiAocG9ydFByb2JsZW0pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlamVjdCgncG9ydCcpO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVqZWN0KCdleGl0Jyk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgYnVpbGRUaW1lb3V0ID0gc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgICAgICAga2lsbCgpO1xuICAgICAgICAgICAgICAgIHdyaXRlTG9nKCk7XG4gICAgICAgICAgICAgICAgcmVqZWN0KCd0aW1lb3V0Jyk7XG4gICAgICAgICAgICB9LCB0aGlzLiQuZW52Lm9wdGlvbnMuYnVpbGRUaW1lb3V0ID8gdGhpcy4kLmVudi5vcHRpb25zLmJ1aWxkVGltZW91dCAqIDEwMDAgOiA2MDAwMDApO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBSZXBsYWNlcyB0aGUgRERQIHVybCB0aGF0IHdhcyB1c2VkIG9yaWdpbmFsbHkgd2hlbiBNZXRlb3Igd2FzIGJ1aWxkaW5nIHRoZSBjbGllbnQuXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGluZGV4SHRtbCAtIHBhdGggdG8gaW5kZXguaHRtbCBmcm9tIHRoZSBjbGllbnRcbiAgICAgKi9cbiAgICB1cGRhdGVEZHBVcmwoaW5kZXhIdG1sKSB7XG4gICAgICAgIGxldCBjb250ZW50O1xuICAgICAgICBsZXQgcnVudGltZUNvbmZpZztcblxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgY29udGVudCA9IGZzLnJlYWRGaWxlU3luYyhpbmRleEh0bWwsICdVVEYtOCcpO1xuICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgICB0aGlzLmxvZy5lcnJvcihgZXJyb3IgbG9hZGluZyBpbmRleC5odG1sIGZpbGU6ICR7ZS5tZXNzYWdlfWApO1xuICAgICAgICAgICAgcHJvY2Vzcy5leGl0KDEpO1xuICAgICAgICB9XG4gICAgICAgIGlmICghdGhpcy5tYXRjaGVyLnRlc3QoY29udGVudCkpIHtcbiAgICAgICAgICAgIHRoaXMubG9nLmVycm9yKCdjb3VsZCBub3QgZmluZCBydW50aW1lIGNvbmZpZyBpbiBpbmRleCBmaWxlJyk7XG4gICAgICAgICAgICBwcm9jZXNzLmV4aXQoMSk7XG4gICAgICAgIH1cblxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgY29uc3QgbWF0Y2hlcyA9IGNvbnRlbnQubWF0Y2godGhpcy5tYXRjaGVyKTtcbiAgICAgICAgICAgIHJ1bnRpbWVDb25maWcgPSBKU09OLnBhcnNlKGRlY29kZVVSSUNvbXBvbmVudChtYXRjaGVzWzFdKSk7XG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgIHRoaXMubG9nLmVycm9yKCdjb3VsZCBub3QgZmluZCBydW50aW1lIGNvbmZpZyBpbiBpbmRleCBmaWxlJyk7XG4gICAgICAgICAgICBwcm9jZXNzLmV4aXQoMSk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy4kLmVudi5vcHRpb25zLmRkcFVybC5zdWJzdHIoLTEsIDEpICE9PSAnLycpIHtcbiAgICAgICAgICAgIHRoaXMuJC5lbnYub3B0aW9ucy5kZHBVcmwgKz0gJy8nO1xuICAgICAgICB9XG5cbiAgICAgICAgcnVudGltZUNvbmZpZy5ST09UX1VSTCA9IHRoaXMuJC5lbnYub3B0aW9ucy5kZHBVcmw7XG4gICAgICAgIHJ1bnRpbWVDb25maWcuRERQX0RFRkFVTFRfQ09OTkVDVElPTl9VUkwgPSB0aGlzLiQuZW52Lm9wdGlvbnMuZGRwVXJsO1xuXG4gICAgICAgIGNvbnRlbnQgPSBjb250ZW50LnJlcGxhY2UoXG4gICAgICAgICAgICB0aGlzLnJlcGxhY2VyLCBgJDFcIiR7ZW5jb2RlVVJJQ29tcG9uZW50KEpTT04uc3RyaW5naWZ5KHJ1bnRpbWVDb25maWcpKX1cIiQzYFxuICAgICAgICApO1xuXG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBmcy53cml0ZUZpbGVTeW5jKGluZGV4SHRtbCwgY29udGVudCk7XG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgIHRoaXMubG9nLmVycm9yKGBlcnJvciB3cml0aW5nIGluZGV4Lmh0bWwgZmlsZTogJHtlLm1lc3NhZ2V9YCk7XG4gICAgICAgICAgICBwcm9jZXNzLmV4aXQoMSk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5sb2cuaW5mbygnc3VjY2Vzc2Z1bGx5IHVwZGF0ZWQgZGRwIHN0cmluZyBpbiB0aGUgcnVudGltZSBjb25maWcgb2YgYSBtb2JpbGUgYnVpbGQnICtcbiAgICAgICAgICAgIGAgdG8gJHt0aGlzLiQuZW52Lm9wdGlvbnMuZGRwVXJsfWApO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFByZXBhcmVzIHRoZSBhcmd1bWVudHMgcGFzc2VkIHRvIGBtZXRlb3JgIGNvbW1hbmQuXG4gICAgICogQHJldHVybnMge3N0cmluZ1tdfVxuICAgICAqL1xuICAgIHByZXBhcmVBcmd1bWVudHMoKSB7XG4gICAgICAgIGNvbnN0IGFyZ3MgPSBbJ3J1bicsICctLXZlcmJvc2UnLCBgLS1tb2JpbGUtc2VydmVyPSR7dGhpcy4kLmVudi5vcHRpb25zLmRkcFVybH1gXTtcbiAgICAgICAgaWYgKHRoaXMuJC5lbnYuaXNQcm9kdWN0aW9uQnVpbGQoKSkge1xuICAgICAgICAgICAgYXJncy5wdXNoKCctLXByb2R1Y3Rpb24nKTtcbiAgICAgICAgfVxuICAgICAgICBhcmdzLnB1c2goJy1wJyk7XG4gICAgICAgIGlmICh0aGlzLiQuZW52Lm9wdGlvbnMucG9ydCkge1xuICAgICAgICAgICAgYXJncy5wdXNoKHRoaXMuJC5lbnYub3B0aW9ucy5wb3J0KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGFyZ3MucHVzaCgnMzA4MCcpO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLiQuZW52Lm9wdGlvbnMubWV0ZW9yU2V0dGluZ3MpIHtcbiAgICAgICAgICAgIGFyZ3MucHVzaCgnLS1zZXR0aW5ncycsIHRoaXMuJC5lbnYub3B0aW9ucy5tZXRlb3JTZXR0aW5ncyk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGFyZ3M7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogVmFsaWRhdGVzIHRoZSBtb2JpbGUgYnVpbGQgYW5kIGNvcGllcyBpdCBpbnRvIGVsZWN0cm9uIGFwcC5cbiAgICAgKi9cbiAgICBhc3luYyBjb3B5QnVpbGQoKSB7XG4gICAgICAgIHRoaXMubG9nLmRlYnVnKCdjbGVhcmluZyBidWlsZCBkaXInKTtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGF3YWl0IHRoaXMuJC51dGlscy5ybVdpdGhSZXRyaWVzKCctcmYnLCB0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLm1ldGVvckFwcCk7XG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGxldCBwcmVmaXggPSAnY29yZG92YUJ1aWxkJztcbiAgICAgICAgbGV0IGNvcHlQYXRoUG9zdGZpeCA9ICcnO1xuXG4gICAgICAgIGlmICh0aGlzLmluZGV4SFRNTHN0cmF0ZWd5ID09PSB0aGlzLmluZGV4SFRNTFN0cmF0ZWdpZXMuSU5ERVhfRlJPTV9SVU5OSU5HX1NFUlZFUikge1xuICAgICAgICAgICAgcHJlZml4ID0gJ3dlYkNvcmRvdmEnO1xuICAgICAgICAgICAgY29weVBhdGhQb3N0Zml4ID0gYCR7cGF0aC5zZXB9KmA7XG4gICAgICAgICAgICBsZXQgaW5kZXhIdG1sO1xuICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICBmcy5ta2Rpcih0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLm1ldGVvckFwcCk7XG4gICAgICAgICAgICAgICAgaW5kZXhIdG1sID0gYXdhaXQgdGhpcy5hY3F1aXJlSW5kZXgoKTtcbiAgICAgICAgICAgICAgICBmcy53cml0ZUZpbGVTeW5jKHRoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAubWV0ZW9yQXBwSW5kZXgsIGluZGV4SHRtbCk7XG4gICAgICAgICAgICAgICAgdGhpcy5sb2cuaW5mbygnc3VjY2Vzc2Z1bGx5IGRvd25sb2FkZWQgaW5kZXguaHRtbCBmcm9tIHJ1bm5pbmcgbWV0ZW9yIGFwcCcpO1xuICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgICAgIHRoaXMubG9nLmVycm9yKCdlcnJvciB3aGlsZSB0cnlpbmcgdG8gZG93bmxvYWQgaW5kZXguaHRtbCBmb3Igd2ViLmNvcmRvdmEsICcgK1xuICAgICAgICAgICAgICAgICAgICAnYmUgc3VyZSB0aGF0IHlvdSBhcmUgcnVubmluZyBhIG1vYmlsZSB0YXJnZXQgb3Igd2l0aCcgK1xuICAgICAgICAgICAgICAgICAgICAnIC0tbW9iaWxlLXNlcnZlcjogJywgZSk7XG4gICAgICAgICAgICAgICAgdGhyb3cgZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IGNvcmRvdmFCdWlsZCA9IHRoaXMuJC5lbnYucGF0aHMubWV0ZW9yQXBwW3ByZWZpeF07XG4gICAgICAgIGNvbnN0IHsgY29yZG92YUJ1aWxkSW5kZXggfSA9IHRoaXMuJC5lbnYucGF0aHMubWV0ZW9yQXBwO1xuICAgICAgICBjb25zdCBjb3Jkb3ZhQnVpbGRQcm9ncmFtSnNvbiA9IHRoaXMuJC5lbnYucGF0aHMubWV0ZW9yQXBwW2Ake3ByZWZpeH1Qcm9ncmFtSnNvbmBdO1xuXG4gICAgICAgIGlmICghdGhpcy4kLnV0aWxzLmV4aXN0cyhjb3Jkb3ZhQnVpbGQpKSB7XG4gICAgICAgICAgICB0aGlzLmxvZy5lcnJvcihgbm8gbW9iaWxlIGJ1aWxkIGZvdW5kIGF0ICR7Y29yZG92YUJ1aWxkfWApO1xuICAgICAgICAgICAgdGhpcy5sb2cuZXJyb3IoJ2FyZSB5b3Ugc3VyZSB5b3UgZGlkIHJ1biBtZXRlb3Igd2l0aCAtLW1vYmlsZS1zZXJ2ZXI/Jyk7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ3JlcXVpcmVkIGZpbGUgbm90IHByZXNlbnQnKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICghdGhpcy4kLnV0aWxzLmV4aXN0cyhjb3Jkb3ZhQnVpbGRQcm9ncmFtSnNvbikpIHtcbiAgICAgICAgICAgIHRoaXMubG9nLmVycm9yKCdubyBwcm9ncmFtLmpzb24gZm91bmQgaW4gbW9iaWxlIGJ1aWxkIGZvdW5kIGF0ICcgK1xuICAgICAgICAgICAgICAgIGAke2NvcmRvdmFCdWlsZH1gKTtcbiAgICAgICAgICAgIHRoaXMubG9nLmVycm9yKCdhcmUgeW91IHN1cmUgeW91IGRpZCBydW4gbWV0ZW9yIHdpdGggLS1tb2JpbGUtc2VydmVyPycpO1xuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdyZXF1aXJlZCBmaWxlIG5vdCBwcmVzZW50Jyk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy5pbmRleEhUTUxzdHJhdGVneSAhPT0gdGhpcy5pbmRleEhUTUxTdHJhdGVnaWVzLklOREVYX0ZST01fUlVOTklOR19TRVJWRVIpIHtcbiAgICAgICAgICAgIGlmICghdGhpcy4kLnV0aWxzLmV4aXN0cyhjb3Jkb3ZhQnVpbGRJbmRleCkpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmxvZy5lcnJvcignbm8gaW5kZXguaHRtbCBmb3VuZCBpbiBjb3Jkb3ZhIGJ1aWxkIGZvdW5kIGF0ICcgK1xuICAgICAgICAgICAgICAgICAgICBgJHtjb3Jkb3ZhQnVpbGR9YCk7XG4gICAgICAgICAgICAgICAgdGhpcy5sb2cuZXJyb3IoJ2FyZSB5b3Ugc3VyZSB5b3UgZGlkIHJ1biBtZXRlb3Igd2l0aCAtLW1vYmlsZS1zZXJ2ZXI/Jyk7XG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdyZXF1aXJlZCBmaWxlIG5vdCBwcmVzZW50Jyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLmxvZy52ZXJib3NlKCdjb3B5aW5nIG1vYmlsZSBidWlsZCcpO1xuICAgICAgICBzaGVsbC5jcChcbiAgICAgICAgICAgICctUicsIGAke2NvcmRvdmFCdWlsZH0ke2NvcHlQYXRoUG9zdGZpeH1gLCB0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLm1ldGVvckFwcFxuICAgICAgICApO1xuXG4gICAgICAgIC8vIEJlY2F1c2Ugb2YgdmFyaW91cyBwZXJtaXNzaW9uIHByb2JsZW1zIGhlcmUgd2UgdHJ5IHRvIGNsZWFyIHRlIHBhdGggYnkgY2xlYXJpbmdcbiAgICAgICAgLy8gYWxsIHBvc3NpYmxlIHJlc3RyaWN0aW9ucy5cbiAgICAgICAgc2hlbGwuY2htb2QoXG4gICAgICAgICAgICAnLVInLCAnNzc3JywgdGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5tZXRlb3JBcHBcbiAgICAgICAgKTtcbiAgICAgICAgaWYgKHRoaXMuJC5lbnYub3MuaXNXaW5kb3dzKSB7XG4gICAgICAgICAgICBzaGVsbC5leGVjKGBhdHRyaWIgLXIgJHt0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLm1ldGVvckFwcH0ke3BhdGguc2VwfSouKiAvc2ApO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHRoaXMuaW5kZXhIVE1Mc3RyYXRlZ3kgPT09IHRoaXMuaW5kZXhIVE1MU3RyYXRlZ2llcy5JTkRFWF9GUk9NX1JVTk5JTkdfU0VSVkVSKSB7XG4gICAgICAgICAgICBsZXQgcHJvZ3JhbUpzb247XG4gICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgIHByb2dyYW1Kc29uID0gYXdhaXQgdGhpcy5hY3F1aXJlTWFuaWZlc3QoKTtcbiAgICAgICAgICAgICAgICBmcy53cml0ZUZpbGVTeW5jKFxuICAgICAgICAgICAgICAgICAgICB0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLm1ldGVvckFwcFByb2dyYW1Kc29uLFxuICAgICAgICAgICAgICAgICAgICBKU09OLnN0cmluZ2lmeShwcm9ncmFtSnNvbiwgbnVsbCwgNClcbiAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgIHRoaXMubG9nLmluZm8oJ3N1Y2Nlc3NmdWxseSBkb3dubG9hZGVkIG1hbmlmZXN0Lmpzb24gZnJvbSBydW5uaW5nIG1ldGVvciBhcHAnKTtcbiAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmxvZy5lcnJvcignZXJyb3Igd2hpbGUgdHJ5aW5nIHRvIGRvd25sb2FkIG1hbmlmZXN0Lmpzb24gZm9yIHdlYi5jb3Jkb3ZhLCcgK1xuICAgICAgICAgICAgICAgICAgICAnIGJlIHN1cmUgdGhhdCB5b3UgYXJlIHJ1bm5pbmcgYSBtb2JpbGUgdGFyZ2V0IG9yIHdpdGgnICtcbiAgICAgICAgICAgICAgICAgICAgJyAtLW1vYmlsZS1zZXJ2ZXI6ICcsIGUpO1xuICAgICAgICAgICAgICAgIHRocm93IGU7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLmxvZy5pbmZvKCdtb2JpbGUgYnVpbGQgY29waWVkIHRvIGVsZWN0cm9uIGFwcCcpO1xuXG4gICAgICAgIHRoaXMubG9nLmRlYnVnKCdjb3B5IGNvcmRvdmEuanMgdG8gbWV0ZW9yIGJ1aWxkJyk7XG4gICAgICAgIHNoZWxsLmNwKFxuICAgICAgICAgICAgam9pbihfX2Rpcm5hbWUsICcuLicsICdza2VsZXRvbicsICdjb3Jkb3ZhLmpzJyksXG4gICAgICAgICAgICB0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLm1ldGVvckFwcFxuICAgICAgICApO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEluamVjdHMgTWV0ZW9yLmlzRGVza3RvcFxuICAgICAqL1xuICAgIGluamVjdElzRGVza3RvcCgpIHtcbiAgICAgICAgdGhpcy5sb2cuaW5mbygnaW5qZWN0aW5nIGlzRGVza3RvcCcpO1xuXG4gICAgICAgIGxldCBtYW5pZmVzdEpzb25QYXRoID0gdGhpcy4kLmVudi5wYXRocy5tZXRlb3JBcHAuY29yZG92YUJ1aWxkUHJvZ3JhbUpzb247XG4gICAgICAgIGlmICh0aGlzLmluZGV4SFRNTHN0cmF0ZWd5ID09PSB0aGlzLmluZGV4SFRNTFN0cmF0ZWdpZXMuSU5ERVhfRlJPTV9SVU5OSU5HX1NFUlZFUikge1xuICAgICAgICAgICAgbWFuaWZlc3RKc29uUGF0aCA9IHRoaXMuJC5lbnYucGF0aHMubWV0ZW9yQXBwLndlYkNvcmRvdmFQcm9ncmFtSnNvbjtcbiAgICAgICAgfVxuXG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBjb25zdCB7IG1hbmlmZXN0IH0gPSBKU09OLnBhcnNlKFxuICAgICAgICAgICAgICAgIGZzLnJlYWRGaWxlU3luYyhtYW5pZmVzdEpzb25QYXRoLCAnVVRGLTgnKVxuICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIGxldCBpbmplY3RlZCA9IGZhbHNlO1xuICAgICAgICAgICAgbGV0IGluamVjdGVkU3RhcnR1cERpZENvbXBsZXRlID0gZmFsc2U7XG4gICAgICAgICAgICBsZXQgcmVzdWx0ID0gbnVsbDtcblxuICAgICAgICAgICAgLy8gV2Ugd2lsbCBzZWFyY2ggaW4gZXZlcnkgLmpzIGZpbGUgaW4gdGhlIG1hbmlmZXN0LlxuICAgICAgICAgICAgLy8gV2UgY291bGQgcHJvYmFibHkgZGV0ZWN0IHdoZXRoZXIgdGhpcyBpcyBhIGRldiBvciBwcm9kdWN0aW9uIGJ1aWxkIGFuZCBvbmx5IHNlYXJjaCBpblxuICAgICAgICAgICAgLy8gdGhlIGNvcnJlY3QgZmlsZXMsIGJ1dCBmb3Igbm93IHRoaXMgc2hvdWxkIGJlIGZpbmUuXG4gICAgICAgICAgICBtYW5pZmVzdC5mb3JFYWNoKChmaWxlKSA9PiB7XG4gICAgICAgICAgICAgICAgbGV0IGZpbGVDb250ZW50cztcbiAgICAgICAgICAgICAgICAvLyBIYWNreSB3YXkgb2Ygc2V0dGluZyBpc0Rlc2t0b3AuXG4gICAgICAgICAgICAgICAgaWYgKGZpbGUudHlwZSA9PT0gJ2pzJykge1xuICAgICAgICAgICAgICAgICAgICBmaWxlQ29udGVudHMgPSBmcy5yZWFkRmlsZVN5bmMoXG4gICAgICAgICAgICAgICAgICAgICAgICBqb2luKHRoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAubWV0ZW9yQXBwLCBmaWxlLnBhdGgpLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ1VURi04J1xuICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSB0aGlzLmluamVjdG9yLnByb2Nlc3NGaWxlQ29udGVudHMoZmlsZUNvbnRlbnRzKTtcblxuICAgICAgICAgICAgICAgICAgICAoeyBmaWxlQ29udGVudHMgfSA9IHJlc3VsdCk7XG4gICAgICAgICAgICAgICAgICAgIGluamVjdGVkU3RhcnR1cERpZENvbXBsZXRlID1cbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdC5pbmplY3RlZFN0YXJ0dXBEaWRDb21wbGV0ZSA/IHRydWUgOiBpbmplY3RlZFN0YXJ0dXBEaWRDb21wbGV0ZTtcbiAgICAgICAgICAgICAgICAgICAgaW5qZWN0ZWQgPSByZXN1bHQuaW5qZWN0ZWQgPyB0cnVlIDogaW5qZWN0ZWQ7XG5cbiAgICAgICAgICAgICAgICAgICAgZnMud3JpdGVGaWxlU3luYyhcbiAgICAgICAgICAgICAgICAgICAgICAgIGpvaW4odGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5tZXRlb3JBcHAsIGZpbGUucGF0aCksIGZpbGVDb250ZW50c1xuICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICBpZiAoIWluamVjdGVkKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5sb2cuZXJyb3IoJ2Vycm9yIGluamVjdGluZyBpc0Rlc2t0b3AgZ2xvYmFsIHZhci4nKTtcbiAgICAgICAgICAgICAgICBwcm9jZXNzLmV4aXQoMSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoIWluamVjdGVkU3RhcnR1cERpZENvbXBsZXRlKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5sb2cuZXJyb3IoJ2Vycm9yIGluamVjdGluZyBpc0Rlc2t0b3AgZm9yIHN0YXJ0dXBEaWRDb21wbGV0ZScpO1xuICAgICAgICAgICAgICAgIHByb2Nlc3MuZXhpdCgxKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgdGhpcy5sb2cuZXJyb3IoJ2Vycm9yIG9jY3VycmVkIHdoaWxlIGluamVjdGluZyBpc0Rlc2t0b3A6ICcsIGUpO1xuICAgICAgICAgICAgcHJvY2Vzcy5leGl0KDEpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMubG9nLmluZm8oJ2luamVjdGVkIHN1Y2Nlc3NmdWxseScpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEJ1aWxkcywgbW9kaWZpZXMgYW5kIGNvcGllcyB0aGUgbWV0ZW9yIGFwcCB0byBlbGVjdHJvbiBhcHAuXG4gICAgICovXG4gICAgYXN5bmMgYnVpbGQoKSB7XG4gICAgICAgIHRoaXMubG9nLmluZm8oJ2NoZWNraW5nIGZvciBhbnkgbW9iaWxlIHBsYXRmb3JtJyk7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBhd2FpdCB0aGlzLmNoZWNrUHJlY29uZGl0aW9ucygpO1xuICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgICB0aGlzLmxvZy5lcnJvcignZXJyb3Igb2NjdXJyZWQgZHVyaW5nIGNoZWNraW5nIHByZWNvbmRpdGlvbnM6ICcsIGUpO1xuICAgICAgICAgICAgcHJvY2Vzcy5leGl0KDEpO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5sb2cuaW5mbygnYnVpbGRpbmcgbWV0ZW9yIGFwcCcpO1xuXG4gICAgICAgIGlmICghdGhpcy4kLmVudi5vcHRpb25zLnNraXBNb2JpbGVCdWlsZCkge1xuICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLmJ1aWxkTW9iaWxlVGFyZ2V0KCk7XG4gICAgICAgICAgICB9IGNhdGNoIChyZWFzb24pIHtcbiAgICAgICAgICAgICAgICBzd2l0Y2ggKHJlYXNvbikge1xuICAgICAgICAgICAgICAgICAgICBjYXNlICd0aW1lb3V0JzpcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9nLmVycm9yKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICd0aW1lb3V0IHdoaWxlIGJ1aWxkaW5nLCBsb2cgaGFzIGJlZW4gd3JpdHRlbiB0byBtZXRlb3IubG9nJ1xuICAgICAgICAgICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICBjYXNlICdlcnJvcic6XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvZy5lcnJvcihcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnc29tZSBlcnJvcnMgd2VyZSByZXBvcnRlZCBkdXJpbmcgYnVpbGQsIGNoZWNrIG1ldGVvci5sb2cgZm9yIG1vcmUnICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnIGluZm8nXG4gICAgICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ2Vycm9ySW5BcHAnOlxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2cuZXJyb3IoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ3lvdXIgbWV0ZW9yIGFwcCBoYXMgZXJyb3JzIC0gbG9vayBpbnRvIG1ldGVvci5sb2cgZm9yIG1vcmUnICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnIGluZm8nXG4gICAgICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ3BvcnQnOlxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2cuZXJyb3IoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ3lvdXIgcG9ydCAzMDgwIGlzIGN1cnJlbnRseSB1c2VkICh5b3UgcHJvYmFibHkgaGF2ZSB0aGlzIG9yIG90aGVyICcgK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdtZXRlb3IgcHJvamVjdCBydW5uaW5nPyksIHVzZSBgLXRgIG9yIGAtLW1ldGVvci1wb3J0YCB0byB1c2UgJyArXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2RpZmZlcmVudCBwb3J0IHdoaWxlIGJ1aWxkaW5nJ1xuICAgICAgICAgICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICBjYXNlICdleGl0JzpcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9nLmVycm9yKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdtZXRlb3IgY21kIGV4aXRlZCB1bmV4cGVjdGVkbHksIGxvZyBoYXMgYmVlbiB3cml0dGVuIHRvIG1ldGVvci5sb2cnXG4gICAgICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ2NvcHknOlxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2cuZXJyb3IoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2Vycm9yIGVuY291bnRlcmVkIHdoZW4gY29weWluZyB0aGUgYnVpbGQnXG4gICAgICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvZy5lcnJvcignZXJyb3Igb2NjdXJyZWQgZHVyaW5nIGJ1aWxkaW5nIG1vYmlsZSB0YXJnZXQnLCByZWFzb24pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBpZiAodGhpcy5tb2JpbGVQbGF0Zm9ybSkge1xuICAgICAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLnJlbW92ZU1vYmlsZVBsYXRmb3JtKHRoaXMubW9iaWxlUGxhdGZvcm0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBwcm9jZXNzLmV4aXQoMSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLmluZGV4SFRNTHN0cmF0ZWd5ID0gdGhpcy5jaG9vc2VTdHJhdGVneSgpO1xuICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLmNvcHlCdWlsZCgpO1xuICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgICAgIHByb2Nlc3MuZXhpdCgxKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuaW5qZWN0SXNEZXNrdG9wKCk7XG5cbiAgICAgICAgdGhpcy5jaGFuZ2VEZHBVcmwoKTtcblxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgYXdhaXQgdGhpcy5wYWNrVG9Bc2FyKCk7XG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgIHRoaXMubG9nLmVycm9yKCdlcnJvciB3aGlsZSBwYWNraW5nIG1ldGVvciBhcHAgdG8gYXNhcicpO1xuICAgICAgICAgICAgcHJvY2Vzcy5leGl0KDEpO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5sb2cuaW5mbygnbWV0ZW9yIGJ1aWxkIGZpbmlzaGVkJyk7XG5cbiAgICAgICAgaWYgKHRoaXMubW9iaWxlUGxhdGZvcm0pIHtcbiAgICAgICAgICAgIGF3YWl0IHRoaXMucmVtb3ZlTW9iaWxlUGxhdGZvcm0odGhpcy5tb2JpbGVQbGF0Zm9ybSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBjaGFuZ2VEZHBVcmwoKSB7XG4gICAgICAgIGlmICh0aGlzLiQuZW52Lm9wdGlvbnMuZGRwVXJsICE9PSBudWxsKSB7XG4gICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgIHRoaXMudXBkYXRlRGRwVXJsKHRoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAubWV0ZW9yQXBwSW5kZXgpO1xuICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgICAgIHRoaXMubG9nLmVycm9yKGBlcnJvciB3aGlsZSB0cnlpbmcgdG8gY2hhbmdlIHRoZSBkZHAgdXJsOiAke2UubWVzc2FnZX1gKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIHBhY2tUb0FzYXIoKSB7XG4gICAgICAgIHRoaXMubG9nLmluZm8oJ3BhY2tpbmcgbWV0ZW9yIGFwcCB0byBhc2FyIGFyY2hpdmUnKTtcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+XG4gICAgICAgICAgICBhc2FyLmNyZWF0ZVBhY2thZ2UoXG4gICAgICAgICAgICAgICAgdGhpcy4kLmVudi5wYXRocy5lbGVjdHJvbkFwcC5tZXRlb3JBcHAsXG4gICAgICAgICAgICAgICAgcGF0aC5qb2luKHRoaXMuJC5lbnYucGF0aHMuZWxlY3Ryb25BcHAucm9vdCwgJ21ldGVvci5hc2FyJyksXG4gICAgICAgICAgICAgICAgKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAvLyBPbiBXaW5kb3dzIHNvbWUgZmlsZXMgbWlnaHQgc3RpbGwgYmUgYmxvY2tlZC4gR2l2aW5nIGEgdGljayBmb3IgdGhlbSB0byBiZVxuICAgICAgICAgICAgICAgICAgICAvLyByZWFkeSBmb3IgZGVsZXRpb24uXG4gICAgICAgICAgICAgICAgICAgIHNldEltbWVkaWF0ZSgoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvZy52ZXJib3NlKCdjbGVhcmluZyBtZXRlb3IgYXBwIGFmdGVyIHBhY2tpbmcnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJC51dGlsc1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5ybVdpdGhSZXRyaWVzKCctcmYnLCB0aGlzLiQuZW52LnBhdGhzLmVsZWN0cm9uQXBwLm1ldGVvckFwcClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAudGhlbigoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc29sdmUoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5jYXRjaCgoZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZWplY3QoZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICkpO1xuICAgIH1cbn1cbiJdfQ==