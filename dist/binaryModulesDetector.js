"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _path = _interopRequireDefault(require("path"));

var _isbinaryfile = _interopRequireDefault(require("isbinaryfile"));

var _shelljs = _interopRequireDefault(require("shelljs"));

var _log = _interopRequireDefault(require("./log"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

_shelljs.default.config.fatal = true;
/**
 * Experimental module for detecting modules containing binary files.
 * Based on the same functionality from electron-builder.
 *
 * @property {MeteorDesktop} $
 * @class
 */

var BinaryModulesDetector =
/*#__PURE__*/
function () {
  /**
   * @constructor
   */
  function BinaryModulesDetector(nodeModulesPath) {
    _classCallCheck(this, BinaryModulesDetector);

    this.log = new _log.default('binaryModulesDetector');
    this.nodeModulesPath = nodeModulesPath;
  } // TODO: make asynchronous


  _createClass(BinaryModulesDetector, [{
    key: "detect",
    value: function detect() {
      var _this = this;

      this.log.verbose('detecting node modules with binary files');

      var files = _shelljs.default.ls('-RAl', this.nodeModulesPath);

      var extract = [];
      files.forEach(function (file) {
        var pathSplit = file.name.split(_path.default.posix.sep);
        var dir = pathSplit[0];
        var filename = pathSplit.pop();

        if (extract.indexOf(dir) === -1 && !_this.isIgnored(dir, filename)) {
          if (file.isFile()) {
            var shouldUnpack = false;

            if (file.name.endsWith('.dll') || file.name.endsWith('.exe') || file.name.endsWith('.dylib')) {
              shouldUnpack = true;
            } else if (_path.default.extname(file.name) === '') {
              shouldUnpack = _isbinaryfile.default.sync(_path.default.join(_this.nodeModulesPath, file.name));
            }

            if (shouldUnpack) {
              _this.log.debug(`binary file: ${file.name}`);

              extract.push(dir);
            }
          }
        }
      });

      if (extract.length > 0) {
        this.log.verbose(`detected modules to be extracted: ${extract.join(', ')}`);
      }

      return extract;
    }
  }, {
    key: "isIgnored",
    value: function isIgnored(dir, filename) {
      return dir === '.bin' || filename === '.DS_Store' || filename === 'LICENSE' || filename === 'README';
    }
  }]);

  return BinaryModulesDetector;
}();

exports.default = BinaryModulesDetector;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL2xpYi9iaW5hcnlNb2R1bGVzRGV0ZWN0b3IuanMiXSwibmFtZXMiOlsiY29uZmlnIiwiZmF0YWwiLCJCaW5hcnlNb2R1bGVzRGV0ZWN0b3IiLCJub2RlTW9kdWxlc1BhdGgiLCJsb2ciLCJ2ZXJib3NlIiwiZmlsZXMiLCJscyIsImV4dHJhY3QiLCJmb3JFYWNoIiwiZmlsZSIsInBhdGhTcGxpdCIsIm5hbWUiLCJzcGxpdCIsInBvc2l4Iiwic2VwIiwiZGlyIiwiZmlsZW5hbWUiLCJwb3AiLCJpbmRleE9mIiwiaXNJZ25vcmVkIiwiaXNGaWxlIiwic2hvdWxkVW5wYWNrIiwiZW5kc1dpdGgiLCJleHRuYW1lIiwic3luYyIsImpvaW4iLCJkZWJ1ZyIsInB1c2giLCJsZW5ndGgiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFFQTs7Ozs7Ozs7OztBQUVBLGlCQUFNQSxNQUFOLENBQWFDLEtBQWIsR0FBcUIsSUFBckI7QUFDQTs7Ozs7Ozs7SUFPcUJDLHFCOzs7QUFDakI7OztBQUdBLGlDQUFZQyxlQUFaLEVBQTZCO0FBQUE7O0FBQ3pCLFNBQUtDLEdBQUwsR0FBVyxpQkFBUSx1QkFBUixDQUFYO0FBQ0EsU0FBS0QsZUFBTCxHQUF1QkEsZUFBdkI7QUFDSCxHLENBRUQ7Ozs7OzZCQUNTO0FBQUE7O0FBQ0wsV0FBS0MsR0FBTCxDQUFTQyxPQUFULENBQWlCLDBDQUFqQjs7QUFDQSxVQUFNQyxRQUFRLGlCQUFNQyxFQUFOLENBQVMsTUFBVCxFQUFpQixLQUFLSixlQUF0QixDQUFkOztBQUVBLFVBQU1LLFVBQVUsRUFBaEI7QUFFQUYsWUFBTUcsT0FBTixDQUFjLFVBQUNDLElBQUQsRUFBVTtBQUNwQixZQUFNQyxZQUFZRCxLQUFLRSxJQUFMLENBQVVDLEtBQVYsQ0FBZ0IsY0FBS0MsS0FBTCxDQUFXQyxHQUEzQixDQUFsQjtBQUNBLFlBQU1DLE1BQU1MLFVBQVUsQ0FBVixDQUFaO0FBQ0EsWUFBTU0sV0FBV04sVUFBVU8sR0FBVixFQUFqQjs7QUFFQSxZQUFJVixRQUFRVyxPQUFSLENBQWdCSCxHQUFoQixNQUF5QixDQUFDLENBQTFCLElBQStCLENBQUMsTUFBS0ksU0FBTCxDQUFlSixHQUFmLEVBQW9CQyxRQUFwQixDQUFwQyxFQUFtRTtBQUMvRCxjQUFJUCxLQUFLVyxNQUFMLEVBQUosRUFBbUI7QUFDZixnQkFBSUMsZUFBZSxLQUFuQjs7QUFDQSxnQkFBSVosS0FBS0UsSUFBTCxDQUFVVyxRQUFWLENBQW1CLE1BQW5CLEtBQThCYixLQUFLRSxJQUFMLENBQVVXLFFBQVYsQ0FBbUIsTUFBbkIsQ0FBOUIsSUFBNERiLEtBQUtFLElBQUwsQ0FBVVcsUUFBVixDQUFtQixRQUFuQixDQUFoRSxFQUE4RjtBQUMxRkQsNkJBQWUsSUFBZjtBQUNILGFBRkQsTUFFTyxJQUFJLGNBQUtFLE9BQUwsQ0FBYWQsS0FBS0UsSUFBbEIsTUFBNEIsRUFBaEMsRUFBb0M7QUFDdkNVLDZCQUNJLHNCQUFhRyxJQUFiLENBQWtCLGNBQUtDLElBQUwsQ0FBVSxNQUFLdkIsZUFBZixFQUFnQ08sS0FBS0UsSUFBckMsQ0FBbEIsQ0FESjtBQUVIOztBQUNELGdCQUFJVSxZQUFKLEVBQWtCO0FBQ2Qsb0JBQUtsQixHQUFMLENBQVN1QixLQUFULENBQWdCLGdCQUFlakIsS0FBS0UsSUFBSyxFQUF6Qzs7QUFDQUosc0JBQVFvQixJQUFSLENBQWFaLEdBQWI7QUFDSDtBQUNKO0FBQ0o7QUFDSixPQXBCRDs7QUFxQkEsVUFBSVIsUUFBUXFCLE1BQVIsR0FBaUIsQ0FBckIsRUFBd0I7QUFDcEIsYUFBS3pCLEdBQUwsQ0FBU0MsT0FBVCxDQUFrQixxQ0FBb0NHLFFBQVFrQixJQUFSLENBQWEsSUFBYixDQUFtQixFQUF6RTtBQUNIOztBQUNELGFBQU9sQixPQUFQO0FBQ0g7Ozs4QkFFU1EsRyxFQUFLQyxRLEVBQVU7QUFDckIsYUFBT0QsUUFBUSxNQUFSLElBQWtCQyxhQUFhLFdBQS9CLElBQThDQSxhQUFhLFNBQTNELElBQXdFQSxhQUFhLFFBQTVGO0FBQ0giLCJmaWxlIjoiYmluYXJ5TW9kdWxlc0RldGVjdG9yLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHBhdGggZnJvbSAncGF0aCc7XG5pbXBvcnQgaXNCaW5hcnlGaWxlIGZyb20gJ2lzYmluYXJ5ZmlsZSc7XG5pbXBvcnQgc2hlbGwgZnJvbSAnc2hlbGxqcyc7XG5cbmltcG9ydCBMb2cgZnJvbSAnLi9sb2cnO1xuXG5zaGVsbC5jb25maWcuZmF0YWwgPSB0cnVlO1xuLyoqXG4gKiBFeHBlcmltZW50YWwgbW9kdWxlIGZvciBkZXRlY3RpbmcgbW9kdWxlcyBjb250YWluaW5nIGJpbmFyeSBmaWxlcy5cbiAqIEJhc2VkIG9uIHRoZSBzYW1lIGZ1bmN0aW9uYWxpdHkgZnJvbSBlbGVjdHJvbi1idWlsZGVyLlxuICpcbiAqIEBwcm9wZXJ0eSB7TWV0ZW9yRGVza3RvcH0gJFxuICogQGNsYXNzXG4gKi9cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEJpbmFyeU1vZHVsZXNEZXRlY3RvciB7XG4gICAgLyoqXG4gICAgICogQGNvbnN0cnVjdG9yXG4gICAgICovXG4gICAgY29uc3RydWN0b3Iobm9kZU1vZHVsZXNQYXRoKSB7XG4gICAgICAgIHRoaXMubG9nID0gbmV3IExvZygnYmluYXJ5TW9kdWxlc0RldGVjdG9yJyk7XG4gICAgICAgIHRoaXMubm9kZU1vZHVsZXNQYXRoID0gbm9kZU1vZHVsZXNQYXRoO1xuICAgIH1cblxuICAgIC8vIFRPRE86IG1ha2UgYXN5bmNocm9ub3VzXG4gICAgZGV0ZWN0KCkge1xuICAgICAgICB0aGlzLmxvZy52ZXJib3NlKCdkZXRlY3Rpbmcgbm9kZSBtb2R1bGVzIHdpdGggYmluYXJ5IGZpbGVzJyk7XG4gICAgICAgIGNvbnN0IGZpbGVzID0gc2hlbGwubHMoJy1SQWwnLCB0aGlzLm5vZGVNb2R1bGVzUGF0aCk7XG5cbiAgICAgICAgY29uc3QgZXh0cmFjdCA9IFtdO1xuXG4gICAgICAgIGZpbGVzLmZvckVhY2goKGZpbGUpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHBhdGhTcGxpdCA9IGZpbGUubmFtZS5zcGxpdChwYXRoLnBvc2l4LnNlcCk7XG4gICAgICAgICAgICBjb25zdCBkaXIgPSBwYXRoU3BsaXRbMF07XG4gICAgICAgICAgICBjb25zdCBmaWxlbmFtZSA9IHBhdGhTcGxpdC5wb3AoKTtcblxuICAgICAgICAgICAgaWYgKGV4dHJhY3QuaW5kZXhPZihkaXIpID09PSAtMSAmJiAhdGhpcy5pc0lnbm9yZWQoZGlyLCBmaWxlbmFtZSkpIHtcbiAgICAgICAgICAgICAgICBpZiAoZmlsZS5pc0ZpbGUoKSkge1xuICAgICAgICAgICAgICAgICAgICBsZXQgc2hvdWxkVW5wYWNrID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgIGlmIChmaWxlLm5hbWUuZW5kc1dpdGgoJy5kbGwnKSB8fCBmaWxlLm5hbWUuZW5kc1dpdGgoJy5leGUnKSB8fCBmaWxlLm5hbWUuZW5kc1dpdGgoJy5keWxpYicpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzaG91bGRVbnBhY2sgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHBhdGguZXh0bmFtZShmaWxlLm5hbWUpID09PSAnJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgc2hvdWxkVW5wYWNrID1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc0JpbmFyeUZpbGUuc3luYyhwYXRoLmpvaW4odGhpcy5ub2RlTW9kdWxlc1BhdGgsIGZpbGUubmFtZSkpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGlmIChzaG91bGRVbnBhY2spIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9nLmRlYnVnKGBiaW5hcnkgZmlsZTogJHtmaWxlLm5hbWV9YCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBleHRyYWN0LnB1c2goZGlyKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICAgIGlmIChleHRyYWN0Lmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgIHRoaXMubG9nLnZlcmJvc2UoYGRldGVjdGVkIG1vZHVsZXMgdG8gYmUgZXh0cmFjdGVkOiAke2V4dHJhY3Quam9pbignLCAnKX1gKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZXh0cmFjdDtcbiAgICB9XG5cbiAgICBpc0lnbm9yZWQoZGlyLCBmaWxlbmFtZSkge1xuICAgICAgICByZXR1cm4gZGlyID09PSAnLmJpbicgfHwgZmlsZW5hbWUgPT09ICcuRFNfU3RvcmUnIHx8IGZpbGVuYW1lID09PSAnTElDRU5TRScgfHwgZmlsZW5hbWUgPT09ICdSRUFETUUnO1xuICAgIH1cbn1cbiJdfQ==