"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = _exports;

var _runtime = _interopRequireDefault(require("regenerator-runtime/runtime"));

var _fs = _interopRequireDefault(require("fs"));

var _path = _interopRequireDefault(require("path"));

var _shelljs = _interopRequireDefault(require("shelljs"));

var _env = _interopRequireDefault(require("./env"));

var _electron = _interopRequireDefault(require("./electron"));

var _log = _interopRequireDefault(require("./log"));

var _desktop = _interopRequireDefault(require("./desktop"));

var _electronApp = _interopRequireDefault(require("./electronApp"));

var _meteorApp = _interopRequireDefault(require("./meteorApp"));

var _electronBuilder = _interopRequireDefault(require("./electronBuilder"));

var _packager = _interopRequireDefault(require("./packager"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

_shelljs.default.config.fatal = true;
/**
 * Exists
 * @param {string} pathToCheck
 * @returns {boolean}
 */

function exists(pathToCheck) {
  try {
    _fs.default.accessSync(pathToCheck);

    return true;
  } catch (e) {
    return false;
  }
}
/**
 * Exists
 * @param {string} pathToCheck
 * @returns {boolean}
 */


function symlinkExists(pathToCheck) {
  try {
    _fs.default.readlinkSync(pathToCheck);

    return true;
  } catch (e) {
    return false;
  }
}
/**
 * Simple wrapper for shelljs.rm with additional retries in case of failure.
 * It is useful when something is concurrently reading the dir you want to remove.
 */


function rmWithRetries() {
  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  var retries = 0;
  return new Promise(function (resolve, reject) {
    function rm() {
      for (var _len2 = arguments.length, rmArgs = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        rmArgs[_key2] = arguments[_key2];
      }

      try {
        _shelljs.default.config.fatal = true;

        _shelljs.default.rm.apply(_shelljs.default, rmArgs);

        _shelljs.default.config.reset();

        resolve();
      } catch (e) {
        retries += 1;

        if (retries < 5) {
          setTimeout(function () {
            rm.apply(void 0, rmArgs);
          }, 100);
        } else {
          _shelljs.default.config.reset();

          reject(e);
        }
      }
    }

    rm.apply(void 0, args);
  });
}
/**
 * Main entity.
 * @class
 * @property {Env} env
 * @property {Electron} electron
 * @property {InstallerBuilder} installerBuilder
 * @property {ElectronApp} electronApp
 * @property {Desktop} desktop
 * @property {MeteorApp} meteorApp
 */


var MeteorDesktop =
/*#__PURE__*/
function () {
  /**
   * @param {string} input        - Meteor app dir
   * @param {string} output       - output dir for bundle/package/installer
   * @param {Object} options      - options from cli.js
   * @param {Object} dependencies - dependencies object
   * @constructor
   */
  function MeteorDesktop(input, output, options, dependencies) {
    _classCallCheck(this, MeteorDesktop);

    var Log = dependencies.log;
    this.log = new Log('index');
    this.version = this.getVersion();
    this.log.info('initializing');
    this.env = new _env.default(input, output, options);
    this.electron = new _electron.default(this);
    this.electronBuilder = new _electronBuilder.default(this);
    this.electronApp = new _electronApp.default(this);
    this.desktop = new _desktop.default(this);
    this.meteorApp = new _meteorApp.default(this);
    this.packager = new _packager.default(this);
    this.utils = {
      exists,
      rmWithRetries,
      symlinkExists
    };
  }
  /**
   * Tries to read the version from our own package.json.
   *
   * @returns {string}
   */


  _createClass(MeteorDesktop, [{
    key: "getVersion",
    value: function getVersion() {
      if (this.version) {
        return this.version;
      }

      var version = null;

      try {
        var _JSON$parse = JSON.parse(_fs.default.readFileSync(_path.default.join(__dirname, '..', 'package.json'), 'UTF-8'));

        version = _JSON$parse.version;
      } catch (e) {
        this.log.error(`error while trying to read ${_path.default.join(__dirname, 'package.json')}`, e);
        process.exit(1);
      }

      return version;
    }
    /**
     * Tries to read the version from our own package.json.
     *
     * @returns {string}
     */

  }, {
    key: "getElectronVersion",
    value: function getElectronVersion() {
      var version = null;

      try {
        version = JSON.parse(_fs.default.readFileSync(_path.default.join(__dirname, '..', 'package.json'), 'UTF-8')).dependencies.electron;
      } catch (e) {
        this.log.error(`error while trying to read ${_path.default.join(__dirname, 'package.json')}` + 'or the electron version from it', e);
        process.exit(1);
      }

      return version;
    }
  }, {
    key: "init",
    value: function init() {
      this.desktop.scaffold();
      this.meteorApp.updateGitIgnore();
    }
  }, {
    key: "buildInstaller",
    value: function () {
      var _buildInstaller = _asyncToGenerator(
      /*#__PURE__*/
      _runtime.default.mark(function _callee() {
        return _runtime.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                this.env.options.installerBuild = true;
                _context.next = 3;
                return this.electronApp.build();

              case 3:
                _context.prev = 3;
                _context.next = 6;
                return this.electronBuilder.build();

              case 6:
                _context.next = 11;
                break;

              case 8:
                _context.prev = 8;
                _context.t0 = _context["catch"](3);
                this.log.error('error occurred while building installer', _context.t0);

              case 11:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[3, 8]]);
      }));

      return function buildInstaller() {
        return _buildInstaller.apply(this, arguments);
      };
    }()
  }, {
    key: "run",
    value: function () {
      var _run = _asyncToGenerator(
      /*#__PURE__*/
      _runtime.default.mark(function _callee2() {
        return _runtime.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return this.electronApp.build(true);

              case 2:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      return function run() {
        return _run.apply(this, arguments);
      };
    }()
  }, {
    key: "build",
    value: function () {
      var _build = _asyncToGenerator(
      /*#__PURE__*/
      _runtime.default.mark(function _callee3() {
        return _runtime.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return this.electronApp.build();

              case 2:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      return function build() {
        return _build.apply(this, arguments);
      };
    }()
  }, {
    key: "justRun",
    value: function justRun() {
      this.electron.run();
    }
  }, {
    key: "runPackager",
    value: function () {
      var _runPackager = _asyncToGenerator(
      /*#__PURE__*/
      _runtime.default.mark(function _callee4() {
        var _this = this;

        return _runtime.default.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.next = 2;
                return this.electronApp.build();

              case 2:
                this.packager.packageApp().catch(function (e) {
                  _this.log.error(`while trying to build a package an error occurred: ${e}`);
                });

              case 3:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      return function runPackager() {
        return _runPackager.apply(this, arguments);
      };
    }()
  }]);

  return MeteorDesktop;
}();

function _exports(input, output, options) {
  var _ref = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {
    log: _log.default
  },
      _ref$log = _ref.log,
      log = _ref$log === void 0 ? _log.default : _ref$log;

  return new MeteorDesktop(input, output, options, {
    log
  });
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL2xpYi9pbmRleC5qcyJdLCJuYW1lcyI6WyJjb25maWciLCJmYXRhbCIsImV4aXN0cyIsInBhdGhUb0NoZWNrIiwiYWNjZXNzU3luYyIsImUiLCJzeW1saW5rRXhpc3RzIiwicmVhZGxpbmtTeW5jIiwicm1XaXRoUmV0cmllcyIsImFyZ3MiLCJyZXRyaWVzIiwiUHJvbWlzZSIsInJlc29sdmUiLCJyZWplY3QiLCJybSIsInJtQXJncyIsInJlc2V0Iiwic2V0VGltZW91dCIsIk1ldGVvckRlc2t0b3AiLCJpbnB1dCIsIm91dHB1dCIsIm9wdGlvbnMiLCJkZXBlbmRlbmNpZXMiLCJMb2ciLCJsb2ciLCJ2ZXJzaW9uIiwiZ2V0VmVyc2lvbiIsImluZm8iLCJlbnYiLCJlbGVjdHJvbiIsImVsZWN0cm9uQnVpbGRlciIsImVsZWN0cm9uQXBwIiwiZGVza3RvcCIsIm1ldGVvckFwcCIsInBhY2thZ2VyIiwidXRpbHMiLCJKU09OIiwicGFyc2UiLCJyZWFkRmlsZVN5bmMiLCJqb2luIiwiX19kaXJuYW1lIiwiZXJyb3IiLCJwcm9jZXNzIiwiZXhpdCIsInNjYWZmb2xkIiwidXBkYXRlR2l0SWdub3JlIiwiaW5zdGFsbGVyQnVpbGQiLCJidWlsZCIsInJ1biIsInBhY2thZ2VBcHAiLCJjYXRjaCIsImV4cG9ydHMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7Ozs7Ozs7Ozs7O0FBRUEsaUJBQU1BLE1BQU4sQ0FBYUMsS0FBYixHQUFxQixJQUFyQjtBQUVBOzs7Ozs7QUFLQSxTQUFTQyxNQUFULENBQWdCQyxXQUFoQixFQUE2QjtBQUN6QixNQUFJO0FBQ0EsZ0JBQUdDLFVBQUgsQ0FBY0QsV0FBZDs7QUFDQSxXQUFPLElBQVA7QUFDSCxHQUhELENBR0UsT0FBT0UsQ0FBUCxFQUFVO0FBQ1IsV0FBTyxLQUFQO0FBQ0g7QUFDSjtBQUdEOzs7Ozs7O0FBS0EsU0FBU0MsYUFBVCxDQUF1QkgsV0FBdkIsRUFBb0M7QUFDaEMsTUFBSTtBQUNBLGdCQUFHSSxZQUFILENBQWdCSixXQUFoQjs7QUFDQSxXQUFPLElBQVA7QUFDSCxHQUhELENBR0UsT0FBT0UsQ0FBUCxFQUFVO0FBQ1IsV0FBTyxLQUFQO0FBQ0g7QUFDSjtBQUVEOzs7Ozs7QUFJQSxTQUFTRyxhQUFULEdBQWdDO0FBQUEsb0NBQU5DLElBQU07QUFBTkEsUUFBTTtBQUFBOztBQUM1QixNQUFJQyxVQUFVLENBQWQ7QUFDQSxTQUFPLElBQUlDLE9BQUosQ0FBWSxVQUFDQyxPQUFELEVBQVVDLE1BQVYsRUFBcUI7QUFDcEMsYUFBU0MsRUFBVCxHQUF1QjtBQUFBLHlDQUFSQyxNQUFRO0FBQVJBLGNBQVE7QUFBQTs7QUFDbkIsVUFBSTtBQUNBLHlCQUFNZixNQUFOLENBQWFDLEtBQWIsR0FBcUIsSUFBckI7O0FBQ0EseUJBQU1hLEVBQU4seUJBQVlDLE1BQVo7O0FBQ0EseUJBQU1mLE1BQU4sQ0FBYWdCLEtBQWI7O0FBQ0FKO0FBQ0gsT0FMRCxDQUtFLE9BQU9QLENBQVAsRUFBVTtBQUNSSyxtQkFBVyxDQUFYOztBQUNBLFlBQUlBLFVBQVUsQ0FBZCxFQUFpQjtBQUNiTyxxQkFBVyxZQUFNO0FBQ2JILDZCQUFNQyxNQUFOO0FBQ0gsV0FGRCxFQUVHLEdBRkg7QUFHSCxTQUpELE1BSU87QUFDSCwyQkFBTWYsTUFBTixDQUFhZ0IsS0FBYjs7QUFDQUgsaUJBQU9SLENBQVA7QUFDSDtBQUNKO0FBQ0o7O0FBQ0RTLHFCQUFNTCxJQUFOO0FBQ0gsR0FwQk0sQ0FBUDtBQXFCSDtBQUVEOzs7Ozs7Ozs7Ozs7SUFVTVMsYTs7O0FBQ0Y7Ozs7Ozs7QUFPQSx5QkFBWUMsS0FBWixFQUFtQkMsTUFBbkIsRUFBMkJDLE9BQTNCLEVBQW9DQyxZQUFwQyxFQUFrRDtBQUFBOztBQUM5QyxRQUFNQyxNQUFNRCxhQUFhRSxHQUF6QjtBQUNBLFNBQUtBLEdBQUwsR0FBVyxJQUFJRCxHQUFKLENBQVEsT0FBUixDQUFYO0FBQ0EsU0FBS0UsT0FBTCxHQUFlLEtBQUtDLFVBQUwsRUFBZjtBQUVBLFNBQUtGLEdBQUwsQ0FBU0csSUFBVCxDQUFjLGNBQWQ7QUFFQSxTQUFLQyxHQUFMLEdBQVcsaUJBQVFULEtBQVIsRUFBZUMsTUFBZixFQUF1QkMsT0FBdkIsQ0FBWDtBQUNBLFNBQUtRLFFBQUwsR0FBZ0Isc0JBQWEsSUFBYixDQUFoQjtBQUNBLFNBQUtDLGVBQUwsR0FBdUIsNkJBQW9CLElBQXBCLENBQXZCO0FBQ0EsU0FBS0MsV0FBTCxHQUFtQix5QkFBZ0IsSUFBaEIsQ0FBbkI7QUFDQSxTQUFLQyxPQUFMLEdBQWUscUJBQVksSUFBWixDQUFmO0FBQ0EsU0FBS0MsU0FBTCxHQUFpQix1QkFBYyxJQUFkLENBQWpCO0FBQ0EsU0FBS0MsUUFBTCxHQUFnQixzQkFBYSxJQUFiLENBQWhCO0FBQ0EsU0FBS0MsS0FBTCxHQUFhO0FBQ1RqQyxZQURTO0FBRVRNLG1CQUZTO0FBR1RGO0FBSFMsS0FBYjtBQUtIO0FBRUQ7Ozs7Ozs7OztpQ0FLYTtBQUNULFVBQUksS0FBS21CLE9BQVQsRUFBa0I7QUFDZCxlQUFPLEtBQUtBLE9BQVo7QUFDSDs7QUFFRCxVQUFJQSxVQUFVLElBQWQ7O0FBQ0EsVUFBSTtBQUFBLDBCQUNlVyxLQUFLQyxLQUFMLENBQ1gsWUFBR0MsWUFBSCxDQUFnQixjQUFLQyxJQUFMLENBQVVDLFNBQVYsRUFBcUIsSUFBckIsRUFBMkIsY0FBM0IsQ0FBaEIsRUFBNEQsT0FBNUQsQ0FEVyxDQURmOztBQUNHZixlQURILGVBQ0dBLE9BREg7QUFJSCxPQUpELENBSUUsT0FBT3BCLENBQVAsRUFBVTtBQUNSLGFBQUttQixHQUFMLENBQVNpQixLQUFULENBQWdCLDhCQUE2QixjQUFLRixJQUFMLENBQVVDLFNBQVYsRUFBcUIsY0FBckIsQ0FBcUMsRUFBbEYsRUFBcUZuQyxDQUFyRjtBQUNBcUMsZ0JBQVFDLElBQVIsQ0FBYSxDQUFiO0FBQ0g7O0FBQ0QsYUFBT2xCLE9BQVA7QUFDSDtBQUVEOzs7Ozs7Ozt5Q0FLcUI7QUFDakIsVUFBSUEsVUFBVSxJQUFkOztBQUNBLFVBQUk7QUFDQUEsa0JBQVVXLEtBQUtDLEtBQUwsQ0FDTixZQUFHQyxZQUFILENBQWdCLGNBQUtDLElBQUwsQ0FBVUMsU0FBVixFQUFxQixJQUFyQixFQUEyQixjQUEzQixDQUFoQixFQUE0RCxPQUE1RCxDQURNLEVBRVJsQixZQUZRLENBRUtPLFFBRmY7QUFHSCxPQUpELENBSUUsT0FBT3hCLENBQVAsRUFBVTtBQUNSLGFBQUttQixHQUFMLENBQVNpQixLQUFULENBQWdCLDhCQUE2QixjQUFLRixJQUFMLENBQVVDLFNBQVYsRUFBcUIsY0FBckIsQ0FBcUMsRUFBbkUsR0FDWCxpQ0FESixFQUN1Q25DLENBRHZDO0FBRUFxQyxnQkFBUUMsSUFBUixDQUFhLENBQWI7QUFDSDs7QUFDRCxhQUFPbEIsT0FBUDtBQUNIOzs7MkJBR007QUFDSCxXQUFLTyxPQUFMLENBQWFZLFFBQWI7QUFDQSxXQUFLWCxTQUFMLENBQWVZLGVBQWY7QUFDSDs7Ozs7Ozs7Ozs7QUFHRyxxQkFBS2pCLEdBQUwsQ0FBU1AsT0FBVCxDQUFpQnlCLGNBQWpCLEdBQWtDLElBQWxDOzt1QkFDTSxLQUFLZixXQUFMLENBQWlCZ0IsS0FBakIsRTs7Ozs7dUJBRUksS0FBS2pCLGVBQUwsQ0FBcUJpQixLQUFyQixFOzs7Ozs7Ozs7QUFFTixxQkFBS3ZCLEdBQUwsQ0FBU2lCLEtBQVQsQ0FBZSx5Q0FBZjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozt1QkFLRSxLQUFLVixXQUFMLENBQWlCZ0IsS0FBakIsQ0FBdUIsSUFBdkIsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozt1QkFJQSxLQUFLaEIsV0FBTCxDQUFpQmdCLEtBQWpCLEU7Ozs7Ozs7Ozs7Ozs7Ozs7OEJBR0E7QUFDTixXQUFLbEIsUUFBTCxDQUFjbUIsR0FBZDtBQUNIOzs7Ozs7Ozs7Ozs7Ozt1QkFHUyxLQUFLakIsV0FBTCxDQUFpQmdCLEtBQWpCLEU7OztBQUVOLHFCQUFLYixRQUFMLENBQWNlLFVBQWQsR0FBMkJDLEtBQTNCLENBQWlDLFVBQUM3QyxDQUFELEVBQU87QUFDcEMsd0JBQUttQixHQUFMLENBQVNpQixLQUFULENBQWdCLHNEQUFxRHBDLENBQUUsRUFBdkU7QUFDSCxpQkFGRDs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQU1PLFNBQVM4QyxRQUFULENBQWlCaEMsS0FBakIsRUFBd0JDLE1BQXhCLEVBQWdDQyxPQUFoQyxFQUE2RTtBQUFBLGlGQUFqQjtBQUFFRztBQUFGLEdBQWlCO0FBQUEsc0JBQWxDQSxHQUFrQztBQUFBLE1BQWxDQSxHQUFrQzs7QUFDeEYsU0FBTyxJQUFJTixhQUFKLENBQWtCQyxLQUFsQixFQUF5QkMsTUFBekIsRUFBaUNDLE9BQWpDLEVBQTBDO0FBQUVHO0FBQUYsR0FBMUMsQ0FBUDtBQUNIIiwiZmlsZSI6ImluZGV4LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHJlZ2VuZXJhdG9yUnVudGltZSBmcm9tICdyZWdlbmVyYXRvci1ydW50aW1lL3J1bnRpbWUnO1xuaW1wb3J0IGZzIGZyb20gJ2ZzJztcbmltcG9ydCBwYXRoIGZyb20gJ3BhdGgnO1xuaW1wb3J0IHNoZWxsIGZyb20gJ3NoZWxsanMnO1xuaW1wb3J0IEVudiBmcm9tICcuL2Vudic7XG5pbXBvcnQgRWxlY3Ryb24gZnJvbSAnLi9lbGVjdHJvbic7XG5pbXBvcnQgTG9nZ2VyIGZyb20gJy4vbG9nJztcbmltcG9ydCBEZXNrdG9wIGZyb20gJy4vZGVza3RvcCc7XG5pbXBvcnQgRWxlY3Ryb25BcHAgZnJvbSAnLi9lbGVjdHJvbkFwcCc7XG5pbXBvcnQgTWV0ZW9yQXBwIGZyb20gJy4vbWV0ZW9yQXBwJztcbmltcG9ydCBFbGVjdHJvbkJ1aWxkZXIgZnJvbSAnLi9lbGVjdHJvbkJ1aWxkZXInO1xuaW1wb3J0IFBhY2thZ2VyIGZyb20gJy4vcGFja2FnZXInO1xuXG5zaGVsbC5jb25maWcuZmF0YWwgPSB0cnVlO1xuXG4vKipcbiAqIEV4aXN0c1xuICogQHBhcmFtIHtzdHJpbmd9IHBhdGhUb0NoZWNrXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAqL1xuZnVuY3Rpb24gZXhpc3RzKHBhdGhUb0NoZWNrKSB7XG4gICAgdHJ5IHtcbiAgICAgICAgZnMuYWNjZXNzU3luYyhwYXRoVG9DaGVjayk7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbn1cblxuXG4vKipcbiAqIEV4aXN0c1xuICogQHBhcmFtIHtzdHJpbmd9IHBhdGhUb0NoZWNrXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAqL1xuZnVuY3Rpb24gc3ltbGlua0V4aXN0cyhwYXRoVG9DaGVjaykge1xuICAgIHRyeSB7XG4gICAgICAgIGZzLnJlYWRsaW5rU3luYyhwYXRoVG9DaGVjayk7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbn1cblxuLyoqXG4gKiBTaW1wbGUgd3JhcHBlciBmb3Igc2hlbGxqcy5ybSB3aXRoIGFkZGl0aW9uYWwgcmV0cmllcyBpbiBjYXNlIG9mIGZhaWx1cmUuXG4gKiBJdCBpcyB1c2VmdWwgd2hlbiBzb21ldGhpbmcgaXMgY29uY3VycmVudGx5IHJlYWRpbmcgdGhlIGRpciB5b3Ugd2FudCB0byByZW1vdmUuXG4gKi9cbmZ1bmN0aW9uIHJtV2l0aFJldHJpZXMoLi4uYXJncykge1xuICAgIGxldCByZXRyaWVzID0gMDtcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICBmdW5jdGlvbiBybSguLi5ybUFyZ3MpIHtcbiAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgc2hlbGwuY29uZmlnLmZhdGFsID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICBzaGVsbC5ybSguLi5ybUFyZ3MpO1xuICAgICAgICAgICAgICAgIHNoZWxsLmNvbmZpZy5yZXNldCgpO1xuICAgICAgICAgICAgICAgIHJlc29sdmUoKTtcbiAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgICAgICByZXRyaWVzICs9IDE7XG4gICAgICAgICAgICAgICAgaWYgKHJldHJpZXMgPCA1KSB7XG4gICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgcm0oLi4ucm1BcmdzKTtcbiAgICAgICAgICAgICAgICAgICAgfSwgMTAwKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBzaGVsbC5jb25maWcucmVzZXQoKTtcbiAgICAgICAgICAgICAgICAgICAgcmVqZWN0KGUpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBybSguLi5hcmdzKTtcbiAgICB9KTtcbn1cblxuLyoqXG4gKiBNYWluIGVudGl0eS5cbiAqIEBjbGFzc1xuICogQHByb3BlcnR5IHtFbnZ9IGVudlxuICogQHByb3BlcnR5IHtFbGVjdHJvbn0gZWxlY3Ryb25cbiAqIEBwcm9wZXJ0eSB7SW5zdGFsbGVyQnVpbGRlcn0gaW5zdGFsbGVyQnVpbGRlclxuICogQHByb3BlcnR5IHtFbGVjdHJvbkFwcH0gZWxlY3Ryb25BcHBcbiAqIEBwcm9wZXJ0eSB7RGVza3RvcH0gZGVza3RvcFxuICogQHByb3BlcnR5IHtNZXRlb3JBcHB9IG1ldGVvckFwcFxuICovXG5jbGFzcyBNZXRlb3JEZXNrdG9wIHtcbiAgICAvKipcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gaW5wdXQgICAgICAgIC0gTWV0ZW9yIGFwcCBkaXJcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gb3V0cHV0ICAgICAgIC0gb3V0cHV0IGRpciBmb3IgYnVuZGxlL3BhY2thZ2UvaW5zdGFsbGVyXG4gICAgICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnMgICAgICAtIG9wdGlvbnMgZnJvbSBjbGkuanNcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gZGVwZW5kZW5jaWVzIC0gZGVwZW5kZW5jaWVzIG9iamVjdFxuICAgICAqIEBjb25zdHJ1Y3RvclxuICAgICAqL1xuICAgIGNvbnN0cnVjdG9yKGlucHV0LCBvdXRwdXQsIG9wdGlvbnMsIGRlcGVuZGVuY2llcykge1xuICAgICAgICBjb25zdCBMb2cgPSBkZXBlbmRlbmNpZXMubG9nO1xuICAgICAgICB0aGlzLmxvZyA9IG5ldyBMb2coJ2luZGV4Jyk7XG4gICAgICAgIHRoaXMudmVyc2lvbiA9IHRoaXMuZ2V0VmVyc2lvbigpO1xuXG4gICAgICAgIHRoaXMubG9nLmluZm8oJ2luaXRpYWxpemluZycpO1xuXG4gICAgICAgIHRoaXMuZW52ID0gbmV3IEVudihpbnB1dCwgb3V0cHV0LCBvcHRpb25zKTtcbiAgICAgICAgdGhpcy5lbGVjdHJvbiA9IG5ldyBFbGVjdHJvbih0aGlzKTtcbiAgICAgICAgdGhpcy5lbGVjdHJvbkJ1aWxkZXIgPSBuZXcgRWxlY3Ryb25CdWlsZGVyKHRoaXMpO1xuICAgICAgICB0aGlzLmVsZWN0cm9uQXBwID0gbmV3IEVsZWN0cm9uQXBwKHRoaXMpO1xuICAgICAgICB0aGlzLmRlc2t0b3AgPSBuZXcgRGVza3RvcCh0aGlzKTtcbiAgICAgICAgdGhpcy5tZXRlb3JBcHAgPSBuZXcgTWV0ZW9yQXBwKHRoaXMpO1xuICAgICAgICB0aGlzLnBhY2thZ2VyID0gbmV3IFBhY2thZ2VyKHRoaXMpO1xuICAgICAgICB0aGlzLnV0aWxzID0ge1xuICAgICAgICAgICAgZXhpc3RzLFxuICAgICAgICAgICAgcm1XaXRoUmV0cmllcyxcbiAgICAgICAgICAgIHN5bWxpbmtFeGlzdHNcbiAgICAgICAgfTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBUcmllcyB0byByZWFkIHRoZSB2ZXJzaW9uIGZyb20gb3VyIG93biBwYWNrYWdlLmpzb24uXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7c3RyaW5nfVxuICAgICAqL1xuICAgIGdldFZlcnNpb24oKSB7XG4gICAgICAgIGlmICh0aGlzLnZlcnNpb24pIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnZlcnNpb247XG4gICAgICAgIH1cblxuICAgICAgICBsZXQgdmVyc2lvbiA9IG51bGw7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICAoeyB2ZXJzaW9uIH0gPSBKU09OLnBhcnNlKFxuICAgICAgICAgICAgICAgIGZzLnJlYWRGaWxlU3luYyhwYXRoLmpvaW4oX19kaXJuYW1lLCAnLi4nLCAncGFja2FnZS5qc29uJyksICdVVEYtOCcpXG4gICAgICAgICAgICApKTtcbiAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgdGhpcy5sb2cuZXJyb3IoYGVycm9yIHdoaWxlIHRyeWluZyB0byByZWFkICR7cGF0aC5qb2luKF9fZGlybmFtZSwgJ3BhY2thZ2UuanNvbicpfWAsIGUpO1xuICAgICAgICAgICAgcHJvY2Vzcy5leGl0KDEpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB2ZXJzaW9uO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFRyaWVzIHRvIHJlYWQgdGhlIHZlcnNpb24gZnJvbSBvdXIgb3duIHBhY2thZ2UuanNvbi5cbiAgICAgKlxuICAgICAqIEByZXR1cm5zIHtzdHJpbmd9XG4gICAgICovXG4gICAgZ2V0RWxlY3Ryb25WZXJzaW9uKCkge1xuICAgICAgICBsZXQgdmVyc2lvbiA9IG51bGw7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICB2ZXJzaW9uID0gSlNPTi5wYXJzZShcbiAgICAgICAgICAgICAgICBmcy5yZWFkRmlsZVN5bmMocGF0aC5qb2luKF9fZGlybmFtZSwgJy4uJywgJ3BhY2thZ2UuanNvbicpLCAnVVRGLTgnKVxuICAgICAgICAgICAgKS5kZXBlbmRlbmNpZXMuZWxlY3Ryb247XG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgIHRoaXMubG9nLmVycm9yKGBlcnJvciB3aGlsZSB0cnlpbmcgdG8gcmVhZCAke3BhdGguam9pbihfX2Rpcm5hbWUsICdwYWNrYWdlLmpzb24nKX1gICtcbiAgICAgICAgICAgICAgICAnb3IgdGhlIGVsZWN0cm9uIHZlcnNpb24gZnJvbSBpdCcsIGUpO1xuICAgICAgICAgICAgcHJvY2Vzcy5leGl0KDEpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB2ZXJzaW9uO1xuICAgIH1cblxuXG4gICAgaW5pdCgpIHtcbiAgICAgICAgdGhpcy5kZXNrdG9wLnNjYWZmb2xkKCk7XG4gICAgICAgIHRoaXMubWV0ZW9yQXBwLnVwZGF0ZUdpdElnbm9yZSgpO1xuICAgIH1cblxuICAgIGFzeW5jIGJ1aWxkSW5zdGFsbGVyKCkge1xuICAgICAgICB0aGlzLmVudi5vcHRpb25zLmluc3RhbGxlckJ1aWxkID0gdHJ1ZTtcbiAgICAgICAgYXdhaXQgdGhpcy5lbGVjdHJvbkFwcC5idWlsZCgpO1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgYXdhaXQgdGhpcy5lbGVjdHJvbkJ1aWxkZXIuYnVpbGQoKTtcbiAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgdGhpcy5sb2cuZXJyb3IoJ2Vycm9yIG9jY3VycmVkIHdoaWxlIGJ1aWxkaW5nIGluc3RhbGxlcicsIGUpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgYXN5bmMgcnVuKCkge1xuICAgICAgICBhd2FpdCB0aGlzLmVsZWN0cm9uQXBwLmJ1aWxkKHRydWUpO1xuICAgIH1cblxuICAgIGFzeW5jIGJ1aWxkKCkge1xuICAgICAgICBhd2FpdCB0aGlzLmVsZWN0cm9uQXBwLmJ1aWxkKCk7XG4gICAgfVxuXG4gICAganVzdFJ1bigpIHtcbiAgICAgICAgdGhpcy5lbGVjdHJvbi5ydW4oKTtcbiAgICB9XG5cbiAgICBhc3luYyBydW5QYWNrYWdlcigpIHtcbiAgICAgICAgYXdhaXQgdGhpcy5lbGVjdHJvbkFwcC5idWlsZCgpO1xuXG4gICAgICAgIHRoaXMucGFja2FnZXIucGFja2FnZUFwcCgpLmNhdGNoKChlKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmxvZy5lcnJvcihgd2hpbGUgdHJ5aW5nIHRvIGJ1aWxkIGEgcGFja2FnZSBhbiBlcnJvciBvY2N1cnJlZDogJHtlfWApO1xuICAgICAgICB9KTtcbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIGV4cG9ydHMoaW5wdXQsIG91dHB1dCwgb3B0aW9ucywgeyBsb2cgPSBMb2dnZXIgfSA9IHsgbG9nOiBMb2dnZXIgfSkge1xuICAgIHJldHVybiBuZXcgTWV0ZW9yRGVza3RvcChpbnB1dCwgb3V0cHV0LCBvcHRpb25zLCB7IGxvZyB9KTtcbn1cbiJdfQ==