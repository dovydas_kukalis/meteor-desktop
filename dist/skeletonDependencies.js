"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = {
  connect: '3.5.0',
  'server-destroy': '1.0.1',
  'connect-modrewrite': '0.9.0',
  winston: '2.3.0',
  'find-port': '2.0.1',
  rimraf: '2.5.4',
  shelljs: '0.7.5',
  lodash: '4.17.4',
  request: '2.79.0',
  queue: '4.0.1',
  reify: '0.4.4',
  send: '0.14.1',
  'fs-plus': '2.9.3'
};
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL2xpYi9za2VsZXRvbkRlcGVuZGVuY2llcy5qcyJdLCJuYW1lcyI6WyJjb25uZWN0Iiwid2luc3RvbiIsInJpbXJhZiIsInNoZWxsanMiLCJsb2Rhc2giLCJyZXF1ZXN0IiwicXVldWUiLCJyZWlmeSIsInNlbmQiXSwibWFwcGluZ3MiOiI7Ozs7OztlQUFlO0FBQ1hBLFdBQVMsT0FERTtBQUVYLG9CQUFrQixPQUZQO0FBR1gsd0JBQXNCLE9BSFg7QUFJWEMsV0FBUyxPQUpFO0FBS1gsZUFBYSxPQUxGO0FBTVhDLFVBQVEsT0FORztBQU9YQyxXQUFTLE9BUEU7QUFRWEMsVUFBUSxRQVJHO0FBU1hDLFdBQVMsUUFURTtBQVVYQyxTQUFPLE9BVkk7QUFXWEMsU0FBTyxPQVhJO0FBWVhDLFFBQU0sUUFaSztBQWFYLGFBQVc7QUFiQSxDIiwiZmlsZSI6InNrZWxldG9uRGVwZW5kZW5jaWVzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGRlZmF1bHQge1xuICAgIGNvbm5lY3Q6ICczLjUuMCcsXG4gICAgJ3NlcnZlci1kZXN0cm95JzogJzEuMC4xJyxcbiAgICAnY29ubmVjdC1tb2RyZXdyaXRlJzogJzAuOS4wJyxcbiAgICB3aW5zdG9uOiAnMi4zLjAnLFxuICAgICdmaW5kLXBvcnQnOiAnMi4wLjEnLFxuICAgIHJpbXJhZjogJzIuNS40JyxcbiAgICBzaGVsbGpzOiAnMC43LjUnLFxuICAgIGxvZGFzaDogJzQuMTcuNCcsXG4gICAgcmVxdWVzdDogJzIuNzkuMCcsXG4gICAgcXVldWU6ICc0LjAuMScsXG4gICAgcmVpZnk6ICcwLjQuNCcsXG4gICAgc2VuZDogJzAuMTQuMScsXG4gICAgJ2ZzLXBsdXMnOiAnMi45LjMnXG59O1xuIl19