"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _shelljs = _interopRequireDefault(require("shelljs"));

var _fs = _interopRequireDefault(require("fs"));

var _path = _interopRequireDefault(require("path"));

var _hashFiles = _interopRequireDefault(require("hash-files"));

var _log = _interopRequireDefault(require("./log"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

_shelljs.default.config.fatal = true;
/**
 * Checks if the path is empty.
 * @param {string} searchPath
 * @returns {boolean}
 */

function isEmptySync(searchPath) {
  var stat;

  try {
    stat = _fs.default.statSync(searchPath);
  } catch (e) {
    return true;
  }

  if (stat.isDirectory()) {
    var items = _fs.default.readdirSync(searchPath);

    return !items || !items.length;
  }

  return false;
}
/**
 * Represents the .desktop directory.
 * @class
 * @property {desktopSettings} settings
 */


var Desktop =
/*#__PURE__*/
function () {
  /**
   * @param {MeteorDesktop} $ - context
   *
   * @constructor
   */
  function Desktop($) {
    _classCallCheck(this, Desktop);

    this.$ = $;
    this.log = new _log.default('desktop');
    this.settings = null;
    this.dependencies = null;
  }
  /**
   * Tries to read and returns settings.json contents from .desktop dir.
   *
   * @returns {desktopSettings|null}
   */


  _createClass(Desktop, [{
    key: "getSettings",
    value: function getSettings() {
      if (!this.settings) {
        try {
          this.settings = JSON.parse(_fs.default.readFileSync(this.$.env.paths.desktop.settings, 'UTF-8'));
        } catch (e) {
          this.log.error('error while trying to read \'.desktop/settings.json\': ', e);
          process.exit(1);
        }
      }

      return this.settings;
    }
    /**
     * Returns a version hash representing current .desktop contents.
     * @returns {string}
     */

  }, {
    key: "getHashVersion",
    value: function getHashVersion() {
      this.log.info('calculating hash version from .desktop contents');

      var version = _hashFiles.default.sync({
        files: [`${this.$.env.paths.desktop.root}${_path.default.sep}**`]
      });

      this.log.verbose(`calculated .desktop hash version is ${version}`);
      return version;
    }
    /**
     * Tries to read a module.json file from a module at provided path.
     *
     * @param {string} modulePath - path to the module dir
     * @returns {Object}
     */

  }, {
    key: "getModuleConfig",
    value: function getModuleConfig(modulePath) {
      var moduleConfig = {};

      try {
        moduleConfig = JSON.parse(_fs.default.readFileSync(_path.default.join(modulePath, 'module.json'), 'UTF-8'));
      } catch (e) {
        this.log.error(`error while trying to read 'module.json' from '${modulePath}' module: `, e);
        process.exit(1);
      }

      if (!('name' in moduleConfig)) {
        this.log.error(`no 'name' field defined in 'module.json' in '${modulePath}' module.`);
        process.exit(1);
      }

      return moduleConfig;
    }
    /**
     * Scans all modules for module.json and gathers this configuration altogether.
     *
     * @returns {[]}
     */

  }, {
    key: "gatherModuleConfigs",
    value: function gatherModuleConfigs() {
      var _this = this;

      var configs = [];

      if (!isEmptySync(this.$.env.paths.desktop.modules)) {
        _shelljs.default.ls('-d', _path.default.join(this.$.env.paths.desktop.modules, '*')).forEach(function (module) {
          if (_fs.default.lstatSync(module).isDirectory()) {
            var moduleConfig = _this.getModuleConfig(module);

            moduleConfig.dirName = _path.default.parse(module).name;
            configs.push(moduleConfig);
          }
        });
      }

      return configs;
    }
    /**
     * Summarizes all dependencies defined in .desktop.
     *
     * @params {Object} settings      - settings.json
     * @params {boolean} checkModules - whether to gather modules dependencies
     * @params {boolean} refresh      - recompute
     * @returns {{fromSettings: {}, plugins: {}, modules: {}}}
     */

  }, {
    key: "getDependencies",
    value: function getDependencies() {
      var _this2 = this;

      var settings = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      var checkModules = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
      var refresh = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

      if (!refresh && this.dependencies) {
        return this.dependencies;
      }

      var dependencies = {
        fromSettings: {},
        plugins: {},
        modules: {}
      };
      /** @type {desktopSettings} * */

      var settingsJson = settings || this.getSettings(); // Settings can have a 'dependencies' field.

      if ('dependencies' in settingsJson) {
        dependencies.fromSettings = settingsJson.dependencies;
      } // Plugins are also a npm packages.


      if ('plugins' in settingsJson) {
        dependencies.plugins = Object.keys(settingsJson.plugins).reduce(function (plugins, plugin) {
          /* eslint-disable no-param-reassign */
          if (typeof settingsJson.plugins[plugin] === 'object') {
            plugins[plugin] = settingsJson.plugins[plugin].version;
          } else {
            plugins[plugin] = settingsJson.plugins[plugin];
          }

          return plugins;
        }, {});
      } // Each module can have its own dependencies defined.


      var moduleDependencies = {};

      if (checkModules) {
        var configs = this.gatherModuleConfigs();
        configs.forEach(function (moduleConfig) {
          if (!('dependencies' in moduleConfig)) {
            moduleConfig.dependencies = {};
          }

          if (moduleConfig.name in moduleDependencies) {
            _this2.log.error(`duplicate name '${moduleConfig.name}' in 'module.json' in ` + `'${moduleConfig.dirName}' - another module already registered the same name.`);

            process.exit(1);
          }

          moduleDependencies[moduleConfig.name] = moduleConfig.dependencies;
        });
      }

      dependencies.modules = moduleDependencies;
      this.dependencies = dependencies;
      return dependencies;
    }
    /**
     * Copies the .desktop scaffold into the meteor app dir.
     * Adds entry to .meteor/.gitignore.
     */

  }, {
    key: "scaffold",
    value: function scaffold() {
      this.log.info('creating .desktop scaffold in your project');

      if (this.$.utils.exists(this.$.env.paths.desktop.root)) {
        this.log.warn('.desktop already exists - delete it if you want a new one to be ' + 'created');
        return;
      }

      _shelljs.default.cp('-r', this.$.env.paths.scaffold, this.$.env.paths.desktop.root);

      _shelljs.default.mkdir(this.$.env.paths.desktop.import);

      this.log.info('.desktop directory prepared');
    }
    /**
     * Verifies if all mandatory files are present in the .desktop.
     *
     * @returns {boolean}
     */

  }, {
    key: "check",
    value: function check() {
      this.log.verbose('checking .desktop existence');
      return !!(this.$.utils.exists(this.$.env.paths.desktop.root) && this.$.utils.exists(this.$.env.paths.desktop.settings) && this.$.utils.exists(this.$.env.paths.desktop.desktop));
    }
  }]);

  return Desktop;
}();
/**
 * @typedef {Object} desktopSettings
 * @property {string} name
 * @property {string} projectName
 * @property {boolean} devTools
 * @property {boolean} devtron
 * @property {boolean} desktopHCP
 * @property {string} autoUpdateFeedUrl
 * @property {Object} autoUpdateFeedHeaders
 * @property {Object} autoUpdateManualCheck
 * @property {Object} desktopHCPSettings
 * @property {boolean} desktopHCPSettings.ignoreCompatibilityVersion
 * @property {boolean} desktopHCPSettings.blockAppUpdateOnDesktopIncompatibility
 * @property {number} webAppStartupTimeout
 * @property {Object} window
 * @property {Object} windowDev
 * @property {Object} packageJsonFields
 * @property {Object} builderOptions
 * @property {Object} builderCliOptions
 * @property {Object} packagerOptions
 * @property {Object} plugins
 * @property {Object} dependencies
 * @property {boolean} uglify
 * @property {string} version
 * */


exports.default = Desktop;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL2xpYi9kZXNrdG9wLmpzIl0sIm5hbWVzIjpbImNvbmZpZyIsImZhdGFsIiwiaXNFbXB0eVN5bmMiLCJzZWFyY2hQYXRoIiwic3RhdCIsInN0YXRTeW5jIiwiZSIsImlzRGlyZWN0b3J5IiwiaXRlbXMiLCJyZWFkZGlyU3luYyIsImxlbmd0aCIsIkRlc2t0b3AiLCIkIiwibG9nIiwic2V0dGluZ3MiLCJkZXBlbmRlbmNpZXMiLCJKU09OIiwicGFyc2UiLCJyZWFkRmlsZVN5bmMiLCJlbnYiLCJwYXRocyIsImRlc2t0b3AiLCJlcnJvciIsInByb2Nlc3MiLCJleGl0IiwiaW5mbyIsInZlcnNpb24iLCJzeW5jIiwiZmlsZXMiLCJyb290Iiwic2VwIiwidmVyYm9zZSIsIm1vZHVsZVBhdGgiLCJtb2R1bGVDb25maWciLCJqb2luIiwiY29uZmlncyIsIm1vZHVsZXMiLCJscyIsImZvckVhY2giLCJtb2R1bGUiLCJsc3RhdFN5bmMiLCJnZXRNb2R1bGVDb25maWciLCJkaXJOYW1lIiwibmFtZSIsInB1c2giLCJjaGVja01vZHVsZXMiLCJyZWZyZXNoIiwiZnJvbVNldHRpbmdzIiwicGx1Z2lucyIsInNldHRpbmdzSnNvbiIsImdldFNldHRpbmdzIiwiT2JqZWN0Iiwia2V5cyIsInJlZHVjZSIsInBsdWdpbiIsIm1vZHVsZURlcGVuZGVuY2llcyIsImdhdGhlck1vZHVsZUNvbmZpZ3MiLCJ1dGlscyIsImV4aXN0cyIsIndhcm4iLCJjcCIsInNjYWZmb2xkIiwibWtkaXIiLCJpbXBvcnQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFFQTs7Ozs7Ozs7OztBQUVBLGlCQUFNQSxNQUFOLENBQWFDLEtBQWIsR0FBcUIsSUFBckI7QUFFQTs7Ozs7O0FBS0EsU0FBU0MsV0FBVCxDQUFxQkMsVUFBckIsRUFBaUM7QUFDN0IsTUFBSUMsSUFBSjs7QUFDQSxNQUFJO0FBQ0FBLFdBQU8sWUFBR0MsUUFBSCxDQUFZRixVQUFaLENBQVA7QUFDSCxHQUZELENBRUUsT0FBT0csQ0FBUCxFQUFVO0FBQ1IsV0FBTyxJQUFQO0FBQ0g7O0FBQ0QsTUFBSUYsS0FBS0csV0FBTCxFQUFKLEVBQXdCO0FBQ3BCLFFBQU1DLFFBQVEsWUFBR0MsV0FBSCxDQUFlTixVQUFmLENBQWQ7O0FBQ0EsV0FBTyxDQUFDSyxLQUFELElBQVUsQ0FBQ0EsTUFBTUUsTUFBeEI7QUFDSDs7QUFDRCxTQUFPLEtBQVA7QUFDSDtBQUVEOzs7Ozs7O0lBS3FCQyxPOzs7QUFDakI7Ozs7O0FBS0EsbUJBQVlDLENBQVosRUFBZTtBQUFBOztBQUNYLFNBQUtBLENBQUwsR0FBU0EsQ0FBVDtBQUNBLFNBQUtDLEdBQUwsR0FBVyxpQkFBUSxTQUFSLENBQVg7QUFDQSxTQUFLQyxRQUFMLEdBQWdCLElBQWhCO0FBQ0EsU0FBS0MsWUFBTCxHQUFvQixJQUFwQjtBQUNIO0FBRUQ7Ozs7Ozs7OztrQ0FLYztBQUNWLFVBQUksQ0FBQyxLQUFLRCxRQUFWLEVBQW9CO0FBQ2hCLFlBQUk7QUFDQSxlQUFLQSxRQUFMLEdBQWdCRSxLQUFLQyxLQUFMLENBQ1osWUFBR0MsWUFBSCxDQUFnQixLQUFLTixDQUFMLENBQU9PLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsT0FBakIsQ0FBeUJQLFFBQXpDLEVBQW1ELE9BQW5ELENBRFksQ0FBaEI7QUFHSCxTQUpELENBSUUsT0FBT1IsQ0FBUCxFQUFVO0FBQ1IsZUFBS08sR0FBTCxDQUFTUyxLQUFULENBQWUseURBQWYsRUFBMEVoQixDQUExRTtBQUNBaUIsa0JBQVFDLElBQVIsQ0FBYSxDQUFiO0FBQ0g7QUFDSjs7QUFDRCxhQUFPLEtBQUtWLFFBQVo7QUFDSDtBQUVEOzs7Ozs7O3FDQUlpQjtBQUNiLFdBQUtELEdBQUwsQ0FBU1ksSUFBVCxDQUFjLGlEQUFkOztBQUNBLFVBQU1DLFVBQVUsbUJBQUtDLElBQUwsQ0FBVTtBQUN0QkMsZUFBTyxDQUFFLEdBQUUsS0FBS2hCLENBQUwsQ0FBT08sR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxPQUFqQixDQUF5QlEsSUFBSyxHQUFFLGNBQUtDLEdBQUksSUFBN0M7QUFEZSxPQUFWLENBQWhCOztBQUdBLFdBQUtqQixHQUFMLENBQVNrQixPQUFULENBQWtCLHVDQUFzQ0wsT0FBUSxFQUFoRTtBQUNBLGFBQU9BLE9BQVA7QUFDSDtBQUVEOzs7Ozs7Ozs7b0NBTWdCTSxVLEVBQVk7QUFDeEIsVUFBSUMsZUFBZSxFQUFuQjs7QUFDQSxVQUFJO0FBQ0FBLHVCQUFlakIsS0FBS0MsS0FBTCxDQUNYLFlBQUdDLFlBQUgsQ0FBZ0IsY0FBS2dCLElBQUwsQ0FBVUYsVUFBVixFQUFzQixhQUF0QixDQUFoQixFQUFzRCxPQUF0RCxDQURXLENBQWY7QUFHSCxPQUpELENBSUUsT0FBTzFCLENBQVAsRUFBVTtBQUNSLGFBQUtPLEdBQUwsQ0FBU1MsS0FBVCxDQUNLLGtEQUFpRFUsVUFBVyxZQURqRSxFQUVJMUIsQ0FGSjtBQUlBaUIsZ0JBQVFDLElBQVIsQ0FBYSxDQUFiO0FBQ0g7O0FBQ0QsVUFBSSxFQUFFLFVBQVVTLFlBQVosQ0FBSixFQUErQjtBQUMzQixhQUFLcEIsR0FBTCxDQUFTUyxLQUFULENBQWdCLGdEQUErQ1UsVUFBVyxXQUExRTtBQUNBVCxnQkFBUUMsSUFBUixDQUFhLENBQWI7QUFDSDs7QUFDRCxhQUFPUyxZQUFQO0FBQ0g7QUFFRDs7Ozs7Ozs7MENBS3NCO0FBQUE7O0FBQ2xCLFVBQU1FLFVBQVUsRUFBaEI7O0FBRUEsVUFBSSxDQUFDakMsWUFBWSxLQUFLVSxDQUFMLENBQU9PLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsT0FBakIsQ0FBeUJlLE9BQXJDLENBQUwsRUFBb0Q7QUFDaEQseUJBQU1DLEVBQU4sQ0FBUyxJQUFULEVBQWUsY0FBS0gsSUFBTCxDQUFVLEtBQUt0QixDQUFMLENBQU9PLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsT0FBakIsQ0FBeUJlLE9BQW5DLEVBQTRDLEdBQTVDLENBQWYsRUFBaUVFLE9BQWpFLENBQ0ksVUFBQ0MsTUFBRCxFQUFZO0FBQ1IsY0FBSSxZQUFHQyxTQUFILENBQWFELE1BQWIsRUFBcUJoQyxXQUFyQixFQUFKLEVBQXdDO0FBQ3BDLGdCQUFNMEIsZUFBZSxNQUFLUSxlQUFMLENBQXFCRixNQUFyQixDQUFyQjs7QUFDQU4seUJBQWFTLE9BQWIsR0FBdUIsY0FBS3pCLEtBQUwsQ0FBV3NCLE1BQVgsRUFBbUJJLElBQTFDO0FBQ0FSLG9CQUFRUyxJQUFSLENBQWFYLFlBQWI7QUFDSDtBQUNKLFNBUEw7QUFTSDs7QUFDRCxhQUFPRSxPQUFQO0FBQ0g7QUFFRDs7Ozs7Ozs7Ozs7c0NBUXVFO0FBQUE7O0FBQUEsVUFBdkRyQixRQUF1RCx1RUFBNUMsSUFBNEM7QUFBQSxVQUF0QytCLFlBQXNDLHVFQUF2QixJQUF1QjtBQUFBLFVBQWpCQyxPQUFpQix1RUFBUCxLQUFPOztBQUNuRSxVQUFJLENBQUNBLE9BQUQsSUFBWSxLQUFLL0IsWUFBckIsRUFBbUM7QUFDL0IsZUFBTyxLQUFLQSxZQUFaO0FBQ0g7O0FBRUQsVUFBTUEsZUFBZTtBQUNqQmdDLHNCQUFjLEVBREc7QUFFakJDLGlCQUFTLEVBRlE7QUFHakJaLGlCQUFTO0FBSFEsT0FBckI7QUFLQTs7QUFDQSxVQUFNYSxlQUFlbkMsWUFBWSxLQUFLb0MsV0FBTCxFQUFqQyxDQVhtRSxDQWFuRTs7QUFDQSxVQUFJLGtCQUFrQkQsWUFBdEIsRUFBb0M7QUFDaENsQyxxQkFBYWdDLFlBQWIsR0FBNEJFLGFBQWFsQyxZQUF6QztBQUNILE9BaEJrRSxDQWtCbkU7OztBQUNBLFVBQUksYUFBYWtDLFlBQWpCLEVBQStCO0FBQzNCbEMscUJBQWFpQyxPQUFiLEdBQXVCRyxPQUFPQyxJQUFQLENBQVlILGFBQWFELE9BQXpCLEVBQWtDSyxNQUFsQyxDQUF5QyxVQUFDTCxPQUFELEVBQVVNLE1BQVYsRUFBcUI7QUFDakY7QUFDQSxjQUFJLE9BQU9MLGFBQWFELE9BQWIsQ0FBcUJNLE1BQXJCLENBQVAsS0FBd0MsUUFBNUMsRUFBc0Q7QUFDbEROLG9CQUFRTSxNQUFSLElBQWtCTCxhQUFhRCxPQUFiLENBQXFCTSxNQUFyQixFQUE2QjVCLE9BQS9DO0FBQ0gsV0FGRCxNQUVPO0FBQ0hzQixvQkFBUU0sTUFBUixJQUFrQkwsYUFBYUQsT0FBYixDQUFxQk0sTUFBckIsQ0FBbEI7QUFDSDs7QUFDRCxpQkFBT04sT0FBUDtBQUNILFNBUnNCLEVBUXBCLEVBUm9CLENBQXZCO0FBU0gsT0E3QmtFLENBK0JuRTs7O0FBQ0EsVUFBTU8scUJBQXFCLEVBQTNCOztBQUNBLFVBQUlWLFlBQUosRUFBa0I7QUFDZCxZQUFNVixVQUFVLEtBQUtxQixtQkFBTCxFQUFoQjtBQUVBckIsZ0JBQVFHLE9BQVIsQ0FDSSxVQUFDTCxZQUFELEVBQWtCO0FBQ2QsY0FBSSxFQUFFLGtCQUFrQkEsWUFBcEIsQ0FBSixFQUF1QztBQUNuQ0EseUJBQWFsQixZQUFiLEdBQTRCLEVBQTVCO0FBQ0g7O0FBQ0QsY0FBSWtCLGFBQWFVLElBQWIsSUFBcUJZLGtCQUF6QixFQUE2QztBQUN6QyxtQkFBSzFDLEdBQUwsQ0FBU1MsS0FBVCxDQUFnQixtQkFBa0JXLGFBQWFVLElBQUssd0JBQXJDLEdBQ1YsSUFBR1YsYUFBYVMsT0FBUSxzREFEN0I7O0FBRUFuQixvQkFBUUMsSUFBUixDQUFhLENBQWI7QUFDSDs7QUFDRCtCLDZCQUFtQnRCLGFBQWFVLElBQWhDLElBQXdDVixhQUFhbEIsWUFBckQ7QUFDSCxTQVhMO0FBYUg7O0FBRURBLG1CQUFhcUIsT0FBYixHQUF1Qm1CLGtCQUF2QjtBQUNBLFdBQUt4QyxZQUFMLEdBQW9CQSxZQUFwQjtBQUNBLGFBQU9BLFlBQVA7QUFDSDtBQUVEOzs7Ozs7OytCQUlXO0FBQ1AsV0FBS0YsR0FBTCxDQUFTWSxJQUFULENBQWMsNENBQWQ7O0FBRUEsVUFBSSxLQUFLYixDQUFMLENBQU82QyxLQUFQLENBQWFDLE1BQWIsQ0FBb0IsS0FBSzlDLENBQUwsQ0FBT08sR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxPQUFqQixDQUF5QlEsSUFBN0MsQ0FBSixFQUF3RDtBQUNwRCxhQUFLaEIsR0FBTCxDQUFTOEMsSUFBVCxDQUFjLHFFQUNWLFNBREo7QUFFQTtBQUNIOztBQUVELHVCQUFNQyxFQUFOLENBQVMsSUFBVCxFQUFlLEtBQUtoRCxDQUFMLENBQU9PLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQnlDLFFBQWhDLEVBQTBDLEtBQUtqRCxDQUFMLENBQU9PLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsT0FBakIsQ0FBeUJRLElBQW5FOztBQUNBLHVCQUFNaUMsS0FBTixDQUFZLEtBQUtsRCxDQUFMLENBQU9PLEdBQVAsQ0FBV0MsS0FBWCxDQUFpQkMsT0FBakIsQ0FBeUIwQyxNQUFyQzs7QUFDQSxXQUFLbEQsR0FBTCxDQUFTWSxJQUFULENBQWMsNkJBQWQ7QUFDSDtBQUVEOzs7Ozs7Ozs0QkFLUTtBQUNKLFdBQUtaLEdBQUwsQ0FBU2tCLE9BQVQsQ0FBaUIsNkJBQWpCO0FBQ0EsYUFBTyxDQUFDLEVBQUUsS0FBS25CLENBQUwsQ0FBTzZDLEtBQVAsQ0FBYUMsTUFBYixDQUFvQixLQUFLOUMsQ0FBTCxDQUFPTyxHQUFQLENBQVdDLEtBQVgsQ0FBaUJDLE9BQWpCLENBQXlCUSxJQUE3QyxLQUNOLEtBQUtqQixDQUFMLENBQU82QyxLQUFQLENBQWFDLE1BQWIsQ0FBb0IsS0FBSzlDLENBQUwsQ0FBT08sR0FBUCxDQUFXQyxLQUFYLENBQWlCQyxPQUFqQixDQUF5QlAsUUFBN0MsQ0FETSxJQUVOLEtBQUtGLENBQUwsQ0FBTzZDLEtBQVAsQ0FBYUMsTUFBYixDQUFvQixLQUFLOUMsQ0FBTCxDQUFPTyxHQUFQLENBQVdDLEtBQVgsQ0FBaUJDLE9BQWpCLENBQXlCQSxPQUE3QyxDQUZJLENBQVI7QUFHSDs7Ozs7QUFHTCIsImZpbGUiOiJkZXNrdG9wLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHNoZWxsIGZyb20gJ3NoZWxsanMnO1xuaW1wb3J0IGZzIGZyb20gJ2ZzJztcbmltcG9ydCBwYXRoIGZyb20gJ3BhdGgnO1xuaW1wb3J0IGhhc2ggZnJvbSAnaGFzaC1maWxlcyc7XG5cbmltcG9ydCBMb2cgZnJvbSAnLi9sb2cnO1xuXG5zaGVsbC5jb25maWcuZmF0YWwgPSB0cnVlO1xuXG4vKipcbiAqIENoZWNrcyBpZiB0aGUgcGF0aCBpcyBlbXB0eS5cbiAqIEBwYXJhbSB7c3RyaW5nfSBzZWFyY2hQYXRoXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAqL1xuZnVuY3Rpb24gaXNFbXB0eVN5bmMoc2VhcmNoUGF0aCkge1xuICAgIGxldCBzdGF0O1xuICAgIHRyeSB7XG4gICAgICAgIHN0YXQgPSBmcy5zdGF0U3luYyhzZWFyY2hQYXRoKTtcbiAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cbiAgICBpZiAoc3RhdC5pc0RpcmVjdG9yeSgpKSB7XG4gICAgICAgIGNvbnN0IGl0ZW1zID0gZnMucmVhZGRpclN5bmMoc2VhcmNoUGF0aCk7XG4gICAgICAgIHJldHVybiAhaXRlbXMgfHwgIWl0ZW1zLmxlbmd0aDtcbiAgICB9XG4gICAgcmV0dXJuIGZhbHNlO1xufVxuXG4vKipcbiAqIFJlcHJlc2VudHMgdGhlIC5kZXNrdG9wIGRpcmVjdG9yeS5cbiAqIEBjbGFzc1xuICogQHByb3BlcnR5IHtkZXNrdG9wU2V0dGluZ3N9IHNldHRpbmdzXG4gKi9cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIERlc2t0b3Age1xuICAgIC8qKlxuICAgICAqIEBwYXJhbSB7TWV0ZW9yRGVza3RvcH0gJCAtIGNvbnRleHRcbiAgICAgKlxuICAgICAqIEBjb25zdHJ1Y3RvclxuICAgICAqL1xuICAgIGNvbnN0cnVjdG9yKCQpIHtcbiAgICAgICAgdGhpcy4kID0gJDtcbiAgICAgICAgdGhpcy5sb2cgPSBuZXcgTG9nKCdkZXNrdG9wJyk7XG4gICAgICAgIHRoaXMuc2V0dGluZ3MgPSBudWxsO1xuICAgICAgICB0aGlzLmRlcGVuZGVuY2llcyA9IG51bGw7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogVHJpZXMgdG8gcmVhZCBhbmQgcmV0dXJucyBzZXR0aW5ncy5qc29uIGNvbnRlbnRzIGZyb20gLmRlc2t0b3AgZGlyLlxuICAgICAqXG4gICAgICogQHJldHVybnMge2Rlc2t0b3BTZXR0aW5nc3xudWxsfVxuICAgICAqL1xuICAgIGdldFNldHRpbmdzKCkge1xuICAgICAgICBpZiAoIXRoaXMuc2V0dGluZ3MpIHtcbiAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXR0aW5ncyA9IEpTT04ucGFyc2UoXG4gICAgICAgICAgICAgICAgICAgIGZzLnJlYWRGaWxlU3luYyh0aGlzLiQuZW52LnBhdGhzLmRlc2t0b3Auc2V0dGluZ3MsICdVVEYtOCcpXG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmxvZy5lcnJvcignZXJyb3Igd2hpbGUgdHJ5aW5nIHRvIHJlYWQgXFwnLmRlc2t0b3Avc2V0dGluZ3MuanNvblxcJzogJywgZSk7XG4gICAgICAgICAgICAgICAgcHJvY2Vzcy5leGl0KDEpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLnNldHRpbmdzO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFJldHVybnMgYSB2ZXJzaW9uIGhhc2ggcmVwcmVzZW50aW5nIGN1cnJlbnQgLmRlc2t0b3AgY29udGVudHMuXG4gICAgICogQHJldHVybnMge3N0cmluZ31cbiAgICAgKi9cbiAgICBnZXRIYXNoVmVyc2lvbigpIHtcbiAgICAgICAgdGhpcy5sb2cuaW5mbygnY2FsY3VsYXRpbmcgaGFzaCB2ZXJzaW9uIGZyb20gLmRlc2t0b3AgY29udGVudHMnKTtcbiAgICAgICAgY29uc3QgdmVyc2lvbiA9IGhhc2guc3luYyh7XG4gICAgICAgICAgICBmaWxlczogW2Ake3RoaXMuJC5lbnYucGF0aHMuZGVza3RvcC5yb290fSR7cGF0aC5zZXB9KipgXVxuICAgICAgICB9KTtcbiAgICAgICAgdGhpcy5sb2cudmVyYm9zZShgY2FsY3VsYXRlZCAuZGVza3RvcCBoYXNoIHZlcnNpb24gaXMgJHt2ZXJzaW9ufWApO1xuICAgICAgICByZXR1cm4gdmVyc2lvbjtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBUcmllcyB0byByZWFkIGEgbW9kdWxlLmpzb24gZmlsZSBmcm9tIGEgbW9kdWxlIGF0IHByb3ZpZGVkIHBhdGguXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gbW9kdWxlUGF0aCAtIHBhdGggdG8gdGhlIG1vZHVsZSBkaXJcbiAgICAgKiBAcmV0dXJucyB7T2JqZWN0fVxuICAgICAqL1xuICAgIGdldE1vZHVsZUNvbmZpZyhtb2R1bGVQYXRoKSB7XG4gICAgICAgIGxldCBtb2R1bGVDb25maWcgPSB7fTtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIG1vZHVsZUNvbmZpZyA9IEpTT04ucGFyc2UoXG4gICAgICAgICAgICAgICAgZnMucmVhZEZpbGVTeW5jKHBhdGguam9pbihtb2R1bGVQYXRoLCAnbW9kdWxlLmpzb24nKSwgJ1VURi04JylcbiAgICAgICAgICAgICk7XG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgIHRoaXMubG9nLmVycm9yKFxuICAgICAgICAgICAgICAgIGBlcnJvciB3aGlsZSB0cnlpbmcgdG8gcmVhZCAnbW9kdWxlLmpzb24nIGZyb20gJyR7bW9kdWxlUGF0aH0nIG1vZHVsZTogYCxcbiAgICAgICAgICAgICAgICBlXG4gICAgICAgICAgICApO1xuICAgICAgICAgICAgcHJvY2Vzcy5leGl0KDEpO1xuICAgICAgICB9XG4gICAgICAgIGlmICghKCduYW1lJyBpbiBtb2R1bGVDb25maWcpKSB7XG4gICAgICAgICAgICB0aGlzLmxvZy5lcnJvcihgbm8gJ25hbWUnIGZpZWxkIGRlZmluZWQgaW4gJ21vZHVsZS5qc29uJyBpbiAnJHttb2R1bGVQYXRofScgbW9kdWxlLmApO1xuICAgICAgICAgICAgcHJvY2Vzcy5leGl0KDEpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBtb2R1bGVDb25maWc7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2NhbnMgYWxsIG1vZHVsZXMgZm9yIG1vZHVsZS5qc29uIGFuZCBnYXRoZXJzIHRoaXMgY29uZmlndXJhdGlvbiBhbHRvZ2V0aGVyLlxuICAgICAqXG4gICAgICogQHJldHVybnMge1tdfVxuICAgICAqL1xuICAgIGdhdGhlck1vZHVsZUNvbmZpZ3MoKSB7XG4gICAgICAgIGNvbnN0IGNvbmZpZ3MgPSBbXTtcblxuICAgICAgICBpZiAoIWlzRW1wdHlTeW5jKHRoaXMuJC5lbnYucGF0aHMuZGVza3RvcC5tb2R1bGVzKSkge1xuICAgICAgICAgICAgc2hlbGwubHMoJy1kJywgcGF0aC5qb2luKHRoaXMuJC5lbnYucGF0aHMuZGVza3RvcC5tb2R1bGVzLCAnKicpKS5mb3JFYWNoKFxuICAgICAgICAgICAgICAgIChtb2R1bGUpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGZzLmxzdGF0U3luYyhtb2R1bGUpLmlzRGlyZWN0b3J5KCkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IG1vZHVsZUNvbmZpZyA9IHRoaXMuZ2V0TW9kdWxlQ29uZmlnKG1vZHVsZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBtb2R1bGVDb25maWcuZGlyTmFtZSA9IHBhdGgucGFyc2UobW9kdWxlKS5uYW1lO1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uZmlncy5wdXNoKG1vZHVsZUNvbmZpZyk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICApO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBjb25maWdzO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFN1bW1hcml6ZXMgYWxsIGRlcGVuZGVuY2llcyBkZWZpbmVkIGluIC5kZXNrdG9wLlxuICAgICAqXG4gICAgICogQHBhcmFtcyB7T2JqZWN0fSBzZXR0aW5ncyAgICAgIC0gc2V0dGluZ3MuanNvblxuICAgICAqIEBwYXJhbXMge2Jvb2xlYW59IGNoZWNrTW9kdWxlcyAtIHdoZXRoZXIgdG8gZ2F0aGVyIG1vZHVsZXMgZGVwZW5kZW5jaWVzXG4gICAgICogQHBhcmFtcyB7Ym9vbGVhbn0gcmVmcmVzaCAgICAgIC0gcmVjb21wdXRlXG4gICAgICogQHJldHVybnMge3tmcm9tU2V0dGluZ3M6IHt9LCBwbHVnaW5zOiB7fSwgbW9kdWxlczoge319fVxuICAgICAqL1xuICAgIGdldERlcGVuZGVuY2llcyhzZXR0aW5ncyA9IG51bGwsIGNoZWNrTW9kdWxlcyA9IHRydWUsIHJlZnJlc2ggPSBmYWxzZSkge1xuICAgICAgICBpZiAoIXJlZnJlc2ggJiYgdGhpcy5kZXBlbmRlbmNpZXMpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmRlcGVuZGVuY2llcztcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IGRlcGVuZGVuY2llcyA9IHtcbiAgICAgICAgICAgIGZyb21TZXR0aW5nczoge30sXG4gICAgICAgICAgICBwbHVnaW5zOiB7fSxcbiAgICAgICAgICAgIG1vZHVsZXM6IHt9XG4gICAgICAgIH07XG4gICAgICAgIC8qKiBAdHlwZSB7ZGVza3RvcFNldHRpbmdzfSAqICovXG4gICAgICAgIGNvbnN0IHNldHRpbmdzSnNvbiA9IHNldHRpbmdzIHx8IHRoaXMuZ2V0U2V0dGluZ3MoKTtcblxuICAgICAgICAvLyBTZXR0aW5ncyBjYW4gaGF2ZSBhICdkZXBlbmRlbmNpZXMnIGZpZWxkLlxuICAgICAgICBpZiAoJ2RlcGVuZGVuY2llcycgaW4gc2V0dGluZ3NKc29uKSB7XG4gICAgICAgICAgICBkZXBlbmRlbmNpZXMuZnJvbVNldHRpbmdzID0gc2V0dGluZ3NKc29uLmRlcGVuZGVuY2llcztcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIFBsdWdpbnMgYXJlIGFsc28gYSBucG0gcGFja2FnZXMuXG4gICAgICAgIGlmICgncGx1Z2lucycgaW4gc2V0dGluZ3NKc29uKSB7XG4gICAgICAgICAgICBkZXBlbmRlbmNpZXMucGx1Z2lucyA9IE9iamVjdC5rZXlzKHNldHRpbmdzSnNvbi5wbHVnaW5zKS5yZWR1Y2UoKHBsdWdpbnMsIHBsdWdpbikgPT4ge1xuICAgICAgICAgICAgICAgIC8qIGVzbGludC1kaXNhYmxlIG5vLXBhcmFtLXJlYXNzaWduICovXG4gICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBzZXR0aW5nc0pzb24ucGx1Z2luc1twbHVnaW5dID09PSAnb2JqZWN0Jykge1xuICAgICAgICAgICAgICAgICAgICBwbHVnaW5zW3BsdWdpbl0gPSBzZXR0aW5nc0pzb24ucGx1Z2luc1twbHVnaW5dLnZlcnNpb247XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgcGx1Z2luc1twbHVnaW5dID0gc2V0dGluZ3NKc29uLnBsdWdpbnNbcGx1Z2luXTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgcmV0dXJuIHBsdWdpbnM7XG4gICAgICAgICAgICB9LCB7fSk7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBFYWNoIG1vZHVsZSBjYW4gaGF2ZSBpdHMgb3duIGRlcGVuZGVuY2llcyBkZWZpbmVkLlxuICAgICAgICBjb25zdCBtb2R1bGVEZXBlbmRlbmNpZXMgPSB7fTtcbiAgICAgICAgaWYgKGNoZWNrTW9kdWxlcykge1xuICAgICAgICAgICAgY29uc3QgY29uZmlncyA9IHRoaXMuZ2F0aGVyTW9kdWxlQ29uZmlncygpO1xuXG4gICAgICAgICAgICBjb25maWdzLmZvckVhY2goXG4gICAgICAgICAgICAgICAgKG1vZHVsZUNvbmZpZykgPT4ge1xuICAgICAgICAgICAgICAgICAgICBpZiAoISgnZGVwZW5kZW5jaWVzJyBpbiBtb2R1bGVDb25maWcpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBtb2R1bGVDb25maWcuZGVwZW5kZW5jaWVzID0ge307XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKG1vZHVsZUNvbmZpZy5uYW1lIGluIG1vZHVsZURlcGVuZGVuY2llcykge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2cuZXJyb3IoYGR1cGxpY2F0ZSBuYW1lICcke21vZHVsZUNvbmZpZy5uYW1lfScgaW4gJ21vZHVsZS5qc29uJyBpbiBgICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBgJyR7bW9kdWxlQ29uZmlnLmRpck5hbWV9JyAtIGFub3RoZXIgbW9kdWxlIGFscmVhZHkgcmVnaXN0ZXJlZCB0aGUgc2FtZSBuYW1lLmApO1xuICAgICAgICAgICAgICAgICAgICAgICAgcHJvY2Vzcy5leGl0KDEpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIG1vZHVsZURlcGVuZGVuY2llc1ttb2R1bGVDb25maWcubmFtZV0gPSBtb2R1bGVDb25maWcuZGVwZW5kZW5jaWVzO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICk7XG4gICAgICAgIH1cblxuICAgICAgICBkZXBlbmRlbmNpZXMubW9kdWxlcyA9IG1vZHVsZURlcGVuZGVuY2llcztcbiAgICAgICAgdGhpcy5kZXBlbmRlbmNpZXMgPSBkZXBlbmRlbmNpZXM7XG4gICAgICAgIHJldHVybiBkZXBlbmRlbmNpZXM7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQ29waWVzIHRoZSAuZGVza3RvcCBzY2FmZm9sZCBpbnRvIHRoZSBtZXRlb3IgYXBwIGRpci5cbiAgICAgKiBBZGRzIGVudHJ5IHRvIC5tZXRlb3IvLmdpdGlnbm9yZS5cbiAgICAgKi9cbiAgICBzY2FmZm9sZCgpIHtcbiAgICAgICAgdGhpcy5sb2cuaW5mbygnY3JlYXRpbmcgLmRlc2t0b3Agc2NhZmZvbGQgaW4geW91ciBwcm9qZWN0Jyk7XG5cbiAgICAgICAgaWYgKHRoaXMuJC51dGlscy5leGlzdHModGhpcy4kLmVudi5wYXRocy5kZXNrdG9wLnJvb3QpKSB7XG4gICAgICAgICAgICB0aGlzLmxvZy53YXJuKCcuZGVza3RvcCBhbHJlYWR5IGV4aXN0cyAtIGRlbGV0ZSBpdCBpZiB5b3Ugd2FudCBhIG5ldyBvbmUgdG8gYmUgJyArXG4gICAgICAgICAgICAgICAgJ2NyZWF0ZWQnKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHNoZWxsLmNwKCctcicsIHRoaXMuJC5lbnYucGF0aHMuc2NhZmZvbGQsIHRoaXMuJC5lbnYucGF0aHMuZGVza3RvcC5yb290KTtcbiAgICAgICAgc2hlbGwubWtkaXIodGhpcy4kLmVudi5wYXRocy5kZXNrdG9wLmltcG9ydCk7XG4gICAgICAgIHRoaXMubG9nLmluZm8oJy5kZXNrdG9wIGRpcmVjdG9yeSBwcmVwYXJlZCcpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFZlcmlmaWVzIGlmIGFsbCBtYW5kYXRvcnkgZmlsZXMgYXJlIHByZXNlbnQgaW4gdGhlIC5kZXNrdG9wLlxuICAgICAqXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAgICovXG4gICAgY2hlY2soKSB7XG4gICAgICAgIHRoaXMubG9nLnZlcmJvc2UoJ2NoZWNraW5nIC5kZXNrdG9wIGV4aXN0ZW5jZScpO1xuICAgICAgICByZXR1cm4gISEodGhpcy4kLnV0aWxzLmV4aXN0cyh0aGlzLiQuZW52LnBhdGhzLmRlc2t0b3Aucm9vdCkgJiZcbiAgICAgICAgICAgIHRoaXMuJC51dGlscy5leGlzdHModGhpcy4kLmVudi5wYXRocy5kZXNrdG9wLnNldHRpbmdzKSAmJlxuICAgICAgICAgICAgdGhpcy4kLnV0aWxzLmV4aXN0cyh0aGlzLiQuZW52LnBhdGhzLmRlc2t0b3AuZGVza3RvcCkpO1xuICAgIH1cbn1cblxuLyoqXG4gKiBAdHlwZWRlZiB7T2JqZWN0fSBkZXNrdG9wU2V0dGluZ3NcbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSBuYW1lXG4gKiBAcHJvcGVydHkge3N0cmluZ30gcHJvamVjdE5hbWVcbiAqIEBwcm9wZXJ0eSB7Ym9vbGVhbn0gZGV2VG9vbHNcbiAqIEBwcm9wZXJ0eSB7Ym9vbGVhbn0gZGV2dHJvblxuICogQHByb3BlcnR5IHtib29sZWFufSBkZXNrdG9wSENQXG4gKiBAcHJvcGVydHkge3N0cmluZ30gYXV0b1VwZGF0ZUZlZWRVcmxcbiAqIEBwcm9wZXJ0eSB7T2JqZWN0fSBhdXRvVXBkYXRlRmVlZEhlYWRlcnNcbiAqIEBwcm9wZXJ0eSB7T2JqZWN0fSBhdXRvVXBkYXRlTWFudWFsQ2hlY2tcbiAqIEBwcm9wZXJ0eSB7T2JqZWN0fSBkZXNrdG9wSENQU2V0dGluZ3NcbiAqIEBwcm9wZXJ0eSB7Ym9vbGVhbn0gZGVza3RvcEhDUFNldHRpbmdzLmlnbm9yZUNvbXBhdGliaWxpdHlWZXJzaW9uXG4gKiBAcHJvcGVydHkge2Jvb2xlYW59IGRlc2t0b3BIQ1BTZXR0aW5ncy5ibG9ja0FwcFVwZGF0ZU9uRGVza3RvcEluY29tcGF0aWJpbGl0eVxuICogQHByb3BlcnR5IHtudW1iZXJ9IHdlYkFwcFN0YXJ0dXBUaW1lb3V0XG4gKiBAcHJvcGVydHkge09iamVjdH0gd2luZG93XG4gKiBAcHJvcGVydHkge09iamVjdH0gd2luZG93RGV2XG4gKiBAcHJvcGVydHkge09iamVjdH0gcGFja2FnZUpzb25GaWVsZHNcbiAqIEBwcm9wZXJ0eSB7T2JqZWN0fSBidWlsZGVyT3B0aW9uc1xuICogQHByb3BlcnR5IHtPYmplY3R9IGJ1aWxkZXJDbGlPcHRpb25zXG4gKiBAcHJvcGVydHkge09iamVjdH0gcGFja2FnZXJPcHRpb25zXG4gKiBAcHJvcGVydHkge09iamVjdH0gcGx1Z2luc1xuICogQHByb3BlcnR5IHtPYmplY3R9IGRlcGVuZGVuY2llc1xuICogQHByb3BlcnR5IHtib29sZWFufSB1Z2xpZnlcbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSB2ZXJzaW9uXG4gKiAqL1xuIl19